<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-13T08:02:26+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-13T08:02:26+07:00
# @Copyright: https://annaya.id




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

/*Route::get('/login', function () {
    return view('pengguna.login');
})->name('login');

Route::post('/login', 'LoginController@login')->name('login');
Route::get('/logout', 'LoginController@logout')->name('logout');
*/

Auth::routes([
  'register' => false,
  'reset' => false,
]);

Route::get('/dashboard', ['as' => 'Dashboard', 'uses' => 'DashboardController@Dashboard']);
Route::get('/', ['as' => 'Index', 'uses' => 'DashboardController@index']);

Route::get('/suratobservasi', 'surat\SuratobservasiController@suratobservasi');
Route::get('/register', ['as' => 'Register', 'uses' => 'RegisterController@register']);
Route::post('/register', ['as' => 'SimpanRegister', 'uses' => 'RegisterController@SimpanRegister']);
Route::get('/lupapassword', ['as' => 'LupaPassword', 'uses' => 'ResetPasswordMahasiswaController@ResetPassword']);
Route::post('/lupapassword', ['as' => 'ProsesLupaPassword', 'uses' => 'ResetPasswordMahasiswaController@ProsesResetPassword']);
Route::post('/lupapassword/simpan', ['as' => 'SimpanResetPassword', 'uses' => 'ResetPasswordMahasiswaController@SimpanResetPassword']);



Route::prefix('mhs')->name('mhs.')->group(function () {
  //Surat Riset Individu
  Route::get('/surat/risetindividu/tambah', ['as' => 'TambahSuratIjinRisetIndividu', 'uses' => 'Mhs\SuratIjinRisetIndividuController@TambahSuratIjinRisetIndividu']);
  Route::post('/surat/risetindividu/tambah', ['as' => 'SimpanTambahSuratIjinRisetIndividu', 'uses' => 'Mhs\SuratIjinRisetIndividuController@SimpanTambahSuratIjinRisetIndividu']);
  Route::get('/surat/risetindividu', ['as' => 'DaftarSuratIjinRisetIndividu', 'uses' => 'Mhs\SuratIjinRisetIndividuController@DaftarSuratIjinRisetIndividu']);
  Route::get('/surat/risetindividu/cetak/{id_surat}', ['as' => "CetakSuratIjinRisetIndividu", 'uses' => 'Mhs\SuratIjinRisetIndividuController@CetakSuratIjinRisetIndividu']);
  //Surat Observasi Pendahuluan
  Route::get('/surat/observasipendahuluan', ['as' => 'DaftarSuratIjinObservasiPendahuluan', 'uses' => 'Mhs\SuratIjinObservasiPendahuluanController@DaftarSuratIjinObservasiPendahuluan']);
  Route::get('/surat/observasipendahuluan/tambah', ['as' => 'TambahSuratIjinObservasiPendahuluan', 'uses' => 'Mhs\SuratIjinObservasiPendahuluanController@TambahSuratIjinObservasiPendahuluan']);
  Route::post('/surat/observasipendahuluan/tambah', ['as' => 'SimpanTambahSuratIjinObservasiPendahuluan', 'uses' => 'Mhs\SuratIjinObservasiPendahuluanController@SimpanTambahSuratIjinObservasiPendahuluan']);
  Route::get('/surat/observasipendahuluan/cetak/{id_surat}', ['as' => 'CetakSuratIjinObservasiPendahuluan', 'uses' => 'Mhs\SuratIjinObservasiPendahuluanController@CetakSuratIjinObservasiPendahuluan']);
  //Surat Observasi Kelas
  Route::get('/surat/observasikelas', ['as' => 'DaftarSuratIjinObservasiKelas', 'uses' => 'Mhs\SuratIjinObservasiKelasController@DaftarSuratIjinObservasiKelas']);
  Route::get('/surat/observasikelas/tambah', ['as' => 'TambahSuratIjinObservasiKelas', 'uses' => 'Mhs\SuratIjinObservasiKelasController@TambahSuratIjinObservasiKelas']);
  Route::post('/surat/observasikelas/tambah', ['as' => 'SimpanTambahSuratIjinObservasiKelas', 'uses' => 'Mhs\SuratIjinObservasiKelasController@SimpanTambahSuratIjinObservasiKelas']);
  Route::get('/surat/observasikelas/cetak/{id_surat}', ['as' => 'CetakSuratIjinObservasiKelas', 'uses' => 'Mhs\SuratIjinObservasiKelasController@CetakSuratIjinObservasiKelas']);
  //Surat Keterangan Lulus Skripsi
  Route::get('/surat/keteranganlulusskripsi', ['as' => 'DaftarSuratKeteranganLulusSkripsi', 'uses' => 'Mhs\SuratKeteranganLulusSkripsiController@DaftarSuratKeteranganLulusSkripsi']);
  Route::get('/surat/keteranganlulusskripsi/tambah', ['as' => 'TambahSuratKeteranganLulusSkripsi', 'uses' => 'Mhs\SuratKeteranganLulusSkripsiController@TambahSuratKeteranganLulusSkripsi']);
  Route::post('/surat/keteranganlulusskripsi/tambah', ['as' => 'SimpanTambahSuratKeteranganLulusSkripsi', 'uses' => 'Mhs\SuratKeteranganLulusSkripsiController@SimpanTambahSuratKeteranganLulusSkripsi']);
  Route::get('/surat/keteranganlulusskripsi/cetak/{id_surat}', ['as' => 'CetakSuratKeteranganLulusSkripsi', 'uses' => 'Mhs\SuratKeteranganLulusSkripsiController@CetakSuratKeteranganLulusSkripsi']);
  //Surat Tugas
  Route::get('/surat/tugas', ['as' => 'DaftarSuratTugas', 'uses' => 'Mhs\SuratTugasController@DaftarSuratTugas']);
  Route::get('/surat/tugas/tambah', ['as' => 'TambahSuratTugas', 'uses' => 'Mhs\SuratTugasController@TambahSuratTugas']);
  Route::post('/surat/tugas/tambah', ['as' => 'SimpanTambahSuratTugas', 'uses' => 'Mhs\SuratTugasController@SimpanTambahSuratTugas']);
  Route::get('/surat/tugas/cetak/{id_surat}', ['as' => 'CetakSuratTugas', 'uses' => 'Mhs\SuratTugasController@CetakSuratTugas']);
  //Surat Keterangan Masih Kuliah
  Route::get('/surat/keteranganmasihkuliah', ['as' => 'DaftarSuratKeteranganMasihKuliah', 'uses' => 'Mhs\SuratKeteranganMasihKuliahController@DaftarSuratKeteranganMasihKuliah']);
  Route::get('/surat/keteranganmasihkuliah/tambah', ['as' => 'TambahSuratKeteranganMasihKuliah', 'uses' => 'Mhs\SuratKeteranganMasihKuliahController@TambahSuratKeteranganMasihKuliah']);
  Route::post('/surat/keteranganmasihkuliah/tambah', ['as' => 'SimpanTambahSuratKeteranganMasihKuliah', 'uses' => 'Mhs\SuratKeteranganMasihKuliahController@SimpanTambahSuratKeteranganMasihKuliah']);
  Route::get('/surat/keteranganmasihkuliah/cetak/{id_surat}', ['as' => 'CetakSuratKeteranganMasihKuliah', 'uses' => 'Mhs\SuratKeteranganMasihKuliahController@CetakSuratKeteranganMasihKuliah']);
  //Surat Keterangan Pernah Kuliah
  Route::get('/surat/keteranganpernahkuliah', ['as' => 'DaftarSuratKeteranganPernahKuliah', 'uses' => 'Mhs\SuratKeteranganPernahKuliahController@DaftarSuratKeteranganPernahKuliah']);
  Route::get('/surat/keteranganpernahkuliah/tambah', ['as' => 'TambahSuratKeteranganPernahKuliah', 'uses' => 'Mhs\SuratKeteranganPernahKuliahController@TambahSuratKeteranganPernahKuliah']);
  Route::post('/surat/keteranganpernahkuliah/tambah', ['as' => 'SimpanTambahSuratKeteranganPernahKuliah', 'uses' => 'Mhs\SuratKeteranganPernahKuliahController@SimpanTambahSuratKeteranganPernahKuliah']);
  Route::get('/surat/keteranganpernahkuliah/cetak/{id_surat}', ['as' => 'CetakSuratKeteranganPernahKuliah', 'uses' => 'Mhs\SuratKeteranganPernahKuliahController@CetakSuratKeteranganPernahKuliah']);
  //Surat Keterangan Pengganti Ijazah
  Route::get('/surat/keteranganpenggantiijazah', ['as' => 'DaftarSuratKeteranganPenggantiIjazah', 'uses' => 'Mhs\SuratKeteranganPenggantiIjazahController@DaftarSuratKeteranganPenggantiIjazah']);
  Route::get('/surat/keteranganpenggantiijazah/tambah', ['as' => 'TambahSuratKeteranganPenggantiIjazah', 'uses' => 'Mhs\SuratKeteranganPenggantiIjazahController@TambahSuratKeteranganPenggantiIjazah']);
  Route::post('/surat/keteranganpenggantiijazah/tambah', ['as' => 'SimpanTambahSuratKeteranganPenggantiIjazah', 'uses' => 'Mhs\SuratKeteranganPenggantiIjazahController@SimpanTambahSuratKeteranganPenggantiIjazah']);
  Route::get('/surat/keteranganpenggantiijazah/cetak/{id_surat}', ['as' => 'CetakSuratKeteranganPenggantiIJazah', 'uses' => 'Mhs\SuratKeteranganPenggantiIjazahController@CetakSuratKeteranganPenggantiIJazah']);
  //Surat Permohonan Pernghargaan
  Route::get('/surat/permohonanpenghargaan', ['as' => 'DaftarSuratPermohonanPenghargaan', 'uses' => 'Mhs\SuratPermohonanPenghargaanController@DaftarSuratPermohonanPenghargaan']);
  Route::get('/surat/permohonanpenghargaan/tambah', ['as' => 'TambahSuratPermohonanPenghargaan', 'uses' => 'Mhs\SuratPermohonanPenghargaanController@TambahSuratPermohonanPenghargaan']);
  Route::post('/surat/permohonanpenghargaan/tambah', ['as' => 'SimpanTambahSuratPermohonanPenghargaan', 'uses' => 'Mhs\SuratPermohonanPenghargaanController@SimpanTambahSuratPermohonanPenghargaan']);
  Route::get('/surat/permohonanpenghargaan/cetak/{id_surat}', ['as' => 'CetakSuratPermohonanPenghargaan', 'uses' => 'Mhs\SuratPermohonanPenghargaanController@CetakSuratPermohonanPenghargaan']);
});

Route::prefix('adm')->name('adm.')->group(function () {
  //Riset Individu
  Route::get('/mhs/surat/risetindividu/permintaan', ['as' => 'PermintaanSuratIjinRisetIndividu', 'uses' => 'Adm\Mhs\SuratIjinRisetIndividuController@PermintaanSuratIjinRisetIndividu']);
  Route::get('/mhs/surat/risetindividu/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratIjinRisetIndividu', 'uses' => 'Adm\Mhs\SuratIjinRisetIndividuController@ProsesPermintaanSuratIjinRisetIndividu']);
  Route::post('/mhs/surat/risetindividu/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratIjinRisetIndividu', 'uses' => 'Adm\Mhs\SuratIjinRisetIndividuController@SimpanProsesPermintaanSuratIjinRisetIndividu']);
  Route::get('/mhs/surat/risetindividu', ['as' => 'SuratIjinRisetIndividuSudahProses', 'uses' => 'Adm\Mhs\SuratIjinRisetIndividuController@SuratIjinRisetIndividuSudahProses']);
  Route::get('/mhs/surat/risetindividu/cetak/{id_surat}', ['as' => "CetakSuratIjinRisetIndividu", 'uses' => 'Adm\Mhs\SuratIjinRisetIndividuController@CetakSuratIjinRisetIndividu']);
  Route::get('/mhs/surat/risetindividu/jmlbelum', ['as' => 'JumlahBelumProsesSuratIjinRisetIndividu', 'uses' => 'Adm\Mhs\SuratIjinRisetIndividuController@JumlahSuratRisetIndividuBelumdiProses']);

  //Observasi Pendahuluan
  Route::get('/mhs/surat/observasipendahuluan/permintaan', ['as' => 'PermintaanSuratIjinObservasiPendahuluan', 'uses' => 'Adm\Mhs\SuratIjinObservasiPendahuluanController@PermintaanSuratIjinObservasiPendahuluan']);
  Route::get('/mhs/surat/observasipendahuluan/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratIjinObservasiPendahuluan', 'uses' => 'Adm\Mhs\SuratIjinObservasiPendahuluanController@ProsesPermintaanSuratIjinObservasiPendahuluan']);
  Route::post('/mhs/surat/observasipendahuluan/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratIjinObservasiPendahuluan', 'uses' => 'Adm\Mhs\SuratIjinObservasiPendahuluanController@SimpanProsesPermintaanSuratIjinObservasiPendahuluan']);
  Route::get('/mhs/surat/observasipendahuluan', ['as' => 'SuratIjinObservasiPendahuluanSudahProses', 'uses' => 'Adm\Mhs\SuratIjinObservasiPendahuluanController@SuratIjinObservasiPendahuluanSudahProses']);
  Route::get('/mhs/surat/observasipendahuluan/cetak/{id_surat}', ['as' => 'CetakSuratIjinObservasiPendahuluan', 'uses' => 'Adm\Mhs\SuratIjinObservasiPendahuluanController@CetakSuratIjinObservasiPendahuluan']);
  Route::get('/mhs/surat/observasipendahuluan/jmlbelum', ['as' => 'JumlahBelumProsesSuratIjinObservasiPendahuluan', 'uses' => 'Adm\Mhs\SuratIjinObservasiPendahuluanController@JumlahSuratObservasiPendahuluanBelumdiProses']);
  //Observasi Kelas
  Route::get('/mhs/surat/observasikelas/permintaan', ['as' => 'PermintaanSuratIjinObservasiKelas', 'uses' => 'Adm\Mhs\SuratIjinObservasiKelasController@PermintaanSuratIjinObservasiKelas']);
  Route::get('/mhs/surat/observasikelas/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratIjinObservasiKelas', 'uses' => 'Adm\Mhs\SuratIjinObservasiKelasController@ProsesPermintaanSuratIjinObservasiKelas']);
  Route::post('/mhs/surat/observasikelas/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratIjinObservasiKelas', 'uses' => 'Adm\Mhs\SuratIjinObservasiKelasController@SimpanProsesPermintaanSuratIjinObservasiKelas']);
  Route::get('/mhs/surat/observasikelas', ['as' => 'SuratIjinObservasiKelasSudahProses', 'uses' => 'Adm\Mhs\SuratIjinObservasiKelasController@SuratIjinObservasiKelasSudahProses']);
  Route::get('/mhs/surat/observasikelas/cetak/{id_surat}', ['as' => 'CetakSuratIjinObservasiKelas', 'uses' => 'Adm\Mhs\SuratIjinObservasiKelasController@CetakSuratIjinObservasiKelas']);
  Route::get('/mhs/surat/observasikelas/jmlbelum', ['as' => 'JumlahBelumProsesSuratIjinObservasiKelas', 'uses' => 'Adm\Mhs\SuratIjinObservasiKelasController@JumlahSuratObservasiKelasBelumdiProses']);
  //Keterangan Lulus
  Route::get('/mhs/surat/keteranganlulusskripsi/permintaan', ['as' => 'PermintaanSuratKeteranganLulusSkripsi', 'uses' => 'Adm\Mhs\SuratKeteranganLulusSkripsiController@PermintaanSuratKeteranganLulusSkripsi']);
  Route::get('/mhs/surat/keteranganlulusskripsi/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratKeteranganLulusSkripsi', 'uses' => 'Adm\Mhs\SuratKeteranganLulusSkripsiController@ProsesPermintaanSuratKeteranganLulusSkripsi']);
  Route::post('/mhs/surat/keteranganlulusskripsi/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratKeteranganLulusSkripsi', 'uses' => 'Adm\Mhs\SuratKeteranganLulusSkripsiController@SimpanProsesPermintaanSuratKeteranganLulusSkripsi']);
  Route::get('/mhs/surat/keteranganlulusskripsi', ['as' => 'SuratKeteranganLulusSkripsiSudahProses', 'uses' => 'Adm\Mhs\SuratKeteranganLulusSkripsiController@SuratKeteranganLulusSkripsiSudahProses']);
  Route::get('/mhs/surat/keteranganlulusskripsi/cetak/{id_surat}', ['as' => 'CetakSuratKeteranganLulusSkripsi', 'uses' => 'Adm\Mhs\SuratKeteranganLulusSkripsiController@CetakSuratKeteranganLulusSkripsi']);
  Route::get('/mhs/surat/keteranganlulusskripsi/jmlbelum', ['as' => 'JumlahBelumProsesSuratKeteranganLulusSkripsi', 'uses' => 'Adm\Mhs\SuratKeteranganLulusSkripsiController@JumlahSuratLulusSkripsiBelumdiProses']);
  //Surat Tugas
  Route::get('/mhs/surat/tugas/permintaan', ['as' => 'PermintaanSuratTugas', 'uses' => 'Adm\Mhs\SuratTugasController@PermintaanSuratTugas']);
  Route::get('/mhs/surat/tugas/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratTugas', 'uses' => 'Adm\Mhs\SuratTugasController@ProsesPermintaanSuratTugas']);
  Route::post('/mhs/surat/tugas/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratTugas', 'uses' => 'Adm\Mhs\SuratTugasController@SimpanProsesPermintaanSuratTugas']);
  Route::get('/mhs/surat/tugas', ['as' => 'SuratTugasSudahProses', 'uses' => 'Adm\Mhs\SuratTugasController@SuratTugasSudahProses']);
  Route::get('/mhs/surat/tugas/cetak/{id_surat}', ['as' => 'CetakSuratTugas', 'uses' => 'Adm\Mhs\SuratTugasController@CetakSuratTugas']);
  Route::get('/mhs/surat/tugas/jmlbelum', ['as' => 'JumlahBelumProsesSuratTugas', 'uses' => 'Adm\Mhs\SuratTugasController@JumlahSuratTugasBelumdiProses']);
  //Surat Keterangan Masih Kuliah
  Route::get('/mhs/surat/keteranganmasihkuliah/permintaan', ['as' => 'PermintaanSuratKeteranganMasihKuliah', 'uses' => 'Adm\Mhs\SuratKeteranganMasihKuliahController@PermintaanSuratKeteranganMasihKuliah']);
  Route::get('/mhs/surat/keteranganmasihkuliah/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratKeteranganMasihKuliah', 'uses' => 'Adm\Mhs\SuratKeteranganMasihKuliahController@ProsesPermintaanSuratKeteranganMasihKuliah']);
  Route::post('/mhs/surat/keteranganmasihkuliah/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratKeteranganMasihKuliah', 'uses' => 'Adm\Mhs\SuratKeteranganMasihKuliahController@SimpanProsesPermintaanSuratKeteranganMasihKuliah']);
  Route::get('/mhs/surat/keteranganmasihkuliah', ['as' => 'SuratKeteranganMasihKuliahSudahProses', 'uses' => 'Adm\Mhs\SuratKeteranganMasihKuliahController@SuratKeteranganMasihKuliahSudahProses']);
  Route::get('/mhs/surat/keteranganmasihkuliah/cetak/{id_surat}', ['as' => 'CetakSuratKeteranganMasihKuliah', 'uses' => 'Adm\Mhs\SuratKeteranganMasihKuliahController@CetakSuratKeteranganMasihKuliah']);
  Route::get('/mhs/surat/keteranganmasihkuliah/jmlbelum', ['as' => 'JumlahBelumProsesSuratKeteranganMasihKuliah', 'uses' => 'Adm\Mhs\SuratKeteranganMasihKuliahController@JumlahSuratKeteranganMasihKuliahBelumdiProses']);
  //Surat Keterangan Pernah Kuliah
  Route::get('/mhs/surat/keteranganpernahkuliah/permintaan', ['as' => 'PermintaanSuratKeteranganPernahKuliah', 'uses' => 'Adm\Mhs\SuratKeteranganPernahKuliahController@PermintaanSuratKeteranganPernahKuliah']);
  Route::get('/mhs/surat/keteranganpernahkuliah/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratKeteranganPernahKuliah', 'uses' => 'Adm\Mhs\SuratKeteranganPernahKuliahController@ProsesPermintaanSuratKeteranganPernahKuliah']);
  Route::post('/mhs/surat/keteranganpernahkuliah/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratKeteranganPernahKuliah', 'uses' => 'Adm\Mhs\SuratKeteranganPernahKuliahController@SimpanProsesPermintaanSuratKeteranganPernahKuliah']);
  Route::get('/mhs/surat/keteranganpernahkuliah', ['as' => 'SuratKeteranganPernahKuliahSudahProses', 'uses' => 'Adm\Mhs\SuratKeteranganPernahKuliahController@SuratKeteranganPernahKuliahSudahProses']);
  Route::get('/mhs/surat/keteranganpernahkuliah/cetak/{id_surat}', ['as' => 'CetakSuratKeteranganPernahKuliah', 'uses' => 'Adm\Mhs\SuratKeteranganPernahKuliahController@CetakSuratKeteranganPernahKuliah']);
  Route::get('/mhs/surat/keteranganpernahkuliah/jmlbelum', ['as' => 'JumlahBelumProsesSuratKeteranganPernahKuliah', 'uses' => 'Adm\Mhs\SuratKeteranganPernahKuliahController@JumlahSuratKeteranganPernahKuliahBelumdiProses']);
  //Surat Keterangan Pengganti Ijazah
  Route::get('/mhs/surat/keteranganpenggantiijazah/permintaan', ['as' => 'PermintaanSuratKeteranganPenggantiIjazah', 'uses' => 'Adm\Mhs\SuratKeteranganPenggantiIjazahController@PermintaanSuratKeteranganPenggantiIjazah']);
  Route::get('/mhs/surat/keteranganpenggantiijazah/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratKeteranganPenggantiIjazah', 'uses' => 'Adm\Mhs\SuratKeteranganPenggantiIjazahController@ProsesPermintaanSuratKeteranganPenggantiIjazah']);
  Route::post('/mhs/surat/keteranganpenggantiijazah/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratKeteranganPenggantiIjazah', 'uses' => 'Adm\Mhs\SuratKeteranganPenggantiIjazahController@SimpanProsesPermintaanSuratKeteranganPenggantiIjazah']);
  Route::get('/mhs/surat/keteranganpenggantiijazah', ['as' => 'SuratKeteranganPenggangtiIjazahSudahProses', 'uses' => 'Adm\Mhs\SuratKeteranganPenggantiIjazahController@SuratKeteranganPenggantiIjazahSudahProses']);
  Route::get('/mhs/surat/keteranganpenggantiijazah/cetak/{id_surat}', ['as' => 'CetakSuratKeteranganPenggantiIJazah', 'uses' => 'Adm\Mhs\SuratKeteranganPenggantiIjazahController@CetakSuratKeteranganPenggantiIJazah']);
  Route::get('/mhs/surat/keteranganpenggantiijazah/jmlbelum', ['as' => 'JumlahBelumProsesSuratKeteranganPenggantiIjazah', 'uses' => 'Adm\Mhs\SuratKeteranganPenggantiIjazahController@JumlahSuratKeteranganPenggantiIjazahBelumdiProses']);
  //Surat Permohonan Penghargaan
  Route::get('/mhs/surat/permohonanpenghargaan/permintaan', ['as' => 'PermintaanSuratPermohonanPenghargaan', 'uses' => 'Adm\Mhs\SuratPermohonanPenghargaanController@PermintaanSuratPermohonanPenghargaan']);
  Route::get('/mhs/surat/permohonanpenghargaan/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratPermohonanPenghargaan', 'uses' => 'Adm\Mhs\SuratPermohonanPenghargaanController@ProsesPermintaanSuratPermohonanPenghargaan']);
  Route::post('/mhs/surat/permohonanpenghargaan/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratPermohonanPenghargaan', 'uses' => 'Adm\Mhs\SuratPermohonanPenghargaanController@SimpanProsesPermintaanSuratPermohonanPenghargaan']);
  Route::get('/mhs/surat/permohonanpenghargaan', ['as' => 'SuratPermohonanPenghargaanSudahProses', 'uses' => 'Adm\Mhs\SuratPermohonanPenghargaanController@SuratPermohonanPenghargaanSudahProses']);
  Route::get('/mhs/surat/permohonanpenghargaan/cetak/{id_surat}', ['as' => 'CetakSuratPermohonanPenghargaan', 'uses' => 'Adm\Mhs\SuratPermohonanPenghargaanController@CetakSuratPermohonanPenghargaan']);
  Route::get('/mhs/surat/permohonanpenghargaan/jmlbelum', ['as' => 'JumlahBelumProsesSuraPermohonanPenghargaan', 'uses' => 'Adm\Mhs\SuratPermohonanPenghargaanController@JumlahSuratPermohonanPenghargaanBelumdiProses']);
});


Route::prefix('wadek1')->name('wadek1.')->group(function () {
  //Surat Keterangan Masih Kuliah
  Route::get('/mhs/surat/keteranganmasihkuliah/permintaan', ['as' => 'PermintaanSuratKeteranganMasihKuliah', 'uses' => 'Wadek1\Mhs\SuratKeteranganMasihKuliahController@PermintaanSuratKeteranganMasihKuliah']);
  Route::get('/mhs/surat/keteranganmasihkuliah/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratKeteranganMasihKuliah', 'uses' => 'Wadek1\Mhs\SuratKeteranganMasihKuliahController@ProsesPermintaanSuratKeteranganMasihKuliah']);
  Route::post('/mhs/surat/keteranganmasihkuliah/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratKeteranganMasihKuliah', 'uses' => 'Wadek1\Mhs\SuratKeteranganMasihKuliahController@SimpanProsesPermintaanSuratKeteranganMasihKuliah']);
  Route::get('/mhs/surat/keteranganmasihkuliah', ['as' => 'SuratKeteranganMasihKuliahSiap', 'uses' => 'Wadek1\Mhs\SuratKeteranganMasihKuliahController@SuratKeteranganMasihKuliahSiap']);
  Route::get('/mhs/surat/keteranganmasihkuliah/cetak/{id_surat}', ['as' => 'CetakSuratKeteranganMasihKuliah', 'uses' => 'Wadek1\Mhs\SuratKeteranganMasihKuliahController@CetakSuratKeteranganMasihKuliah']);
  Route::get('/mhs/surat/keteranganmasihkuliah/jmlbelum', ['as' => 'JumlahBelumProsesKeteranganMasihKuliah', 'uses' => 'Wadek1\Mhs\SuratKeteranganMasihKuliahController@JumlahSuratKeteranganMasihKuliahBelumdiProses']);
  //Surat Keterangan Pernah Kuliah
  Route::get('/mhs/surat/keteranganpernahkuliah/permintaan', ['as' => 'PermintaanSuratKeteranganPernahKuliah', 'uses' => 'Wadek1\Mhs\SuratKeteranganPernahKuliahController@PermintaanSuratKeteranganPernahKuliah']);
  Route::get('/mhs/surat/keteranganpernahkuliah/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratKeteranganPernahKuliah', 'uses' => 'Wadek1\Mhs\SuratKeteranganPernahKuliahController@ProsesPermintaanSuratKeteranganPernahKuliah']);
  Route::post('/mhs/surat/keteranganpernahkuliah/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratKeteranganPernahKuliah', 'uses' => 'Wadek1\Mhs\SuratKeteranganPernahKuliahController@SimpanProsesPermintaanSuratKeteranganPernahKuliah']);
  Route::get('/mhs/surat/keteranganpernahkuliah', ['as' => 'SuratKeteranganPernahKuliahSiap', 'uses' => 'Wadek1\Mhs\SuratKeteranganPernahKuliahController@SuratKeteranganPernahKuliahSiap']);
  Route::get('/mhs/surat/keteranganpernahkuliah/cetak/{id_surat}', ['as' => 'CetakSuratKeteranganPernahKuliah', 'uses' => 'Wadek1\Mhs\SuratKeteranganPernahKuliahController@CetakSuratKeteranganPernahKuliah']);
  Route::get('/mhs/surat/keteranganpernahkuliah/jmlbelum', ['as' => 'JumlahBelumProsesKeteranganPernahKuliah', 'uses' => 'Wadek1\Mhs\SuratKeteranganPernahKuliahController@JumlahSuratPernahKuliahBelumdiProses']);
});


Route::prefix('dekan')->name('dekan.')->group(function () {
  //Surat Keterangan Pengganti Ijazah
  Route::get('/mhs/surat/keteranganpenggantiijazah/permintaan', ['as' => 'PermintaanSuratKeteranganPenggantiIjazah', 'uses' => 'Dekan\Mhs\SuratKeteranganPenggantiIjazahController@PermintaanSuratKeteranganPenggantiIjazah']);
  Route::get('/mhs/surat/keteranganpenggantiijazah/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratKeteranganPenggantiIjazah', 'uses' => 'Dekan\Mhs\SuratKeteranganPenggantiIjazahController@ProsesPermintaanSuratKeteranganPenggantiIjazah']);
  Route::post('/mhs/surat/keteranganpenggantiijazah/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratKeteranganPenggantiIjazah', 'uses' => 'Dekan\Mhs\SuratKeteranganPenggantiIjazahController@SimpanProsesPermintaanSuratKeteranganPenggantiIjazah']);
  Route::get('/mhs/surat/keteranganpenggantiijazah', ['as' => 'SuratKeteranganPenggantiIjazahSiap', 'uses' => 'Dekan\Mhs\SuratKeteranganPenggantiIjazahController@SuratKeteranganPenggantiIjazahSiap']);
  Route::get('/mhs/surat/keteranganpenggantiijazah/cetak/{id_surat}', ['as' => 'CetakSuratKeteranganPenggantiIJazah', 'uses' => 'Dekan\Mhs\SuratKeteranganPenggantiIjazahController@CetakSuratKeteranganPenggantiIJazah']);
  Route::get('/mhs/surat/keteranganpenggantiijazah/jmlbelum', ['as' => 'JumlahBelumProsesKeteranganPenggantiIJazah', 'uses' => 'Dekan\Mhs\SuratKeteranganPenggantiIjazahController@JumlahSuratPenggantiIjazahBelumdiProses']);
});

Route::prefix('wadek3')->name('wadek3.')->group(function () {
  //Surat Tugas
  Route::get('/mhs/surat/tugas/permintaan', ['as' => 'PermintaanSuratTugas', 'uses' => 'Wadek3\Mhs\SuratTugasController@PermintaanSuratTugas']);
  Route::get('/mhs/surat/tugas/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratTugas', 'uses' => 'Wadek3\Mhs\SuratTugasController@ProsesPermintaanSuratTugas']);
  Route::post('/mhs/surat/tugas/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratTugas', 'uses' => 'Wadek3\Mhs\SuratTugasController@SimpanProsesPermintaanSuratTugas']);
  Route::get('/mhs/surat/tugas', ['as' => 'SuratTugasSiap', 'uses' => 'Wadek3\Mhs\SuratTugasController@SuratTugasSiap']);
  Route::get('/mhs/surat/tugas/cetak/{id_surat}', ['as' => 'CetakSuratTugas', 'uses' => 'Wadek3\Mhs\SuratTugasController@CetakSuratTugas']);
  Route::get('/mhs/surat/tugas/jmlbelum', ['as' => 'JumlahBelumProsesSuratTugas', 'uses' => 'Wadek3\Mhs\SuratTugasController@JumlahSuratTugasBelumdiProses']);
  //Surat Permohonan Penghargaan
  Route::get('/mhs/surat/permohonanpenghargaan/perminataan', ['as' => 'PermintaanSuratPermohonanPenghargaan', 'uses' => 'Wadek3\Mhs\SuratPermohonanPenghargaanController@PermintaanSuratPermohonanPenghargaan']);
  Route::get('/mhs/surat/permohonanpenghargaan/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratPermohonanPenghargaan', 'uses' => 'Wadek3\Mhs\SuratPermohonanPenghargaanController@ProsesPermintaanSuratPermohonanPenghargaan']);
  Route::post('/mhs/surat/permohonanpenghargaan/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratPermohonanPenghargaan', 'uses' => 'Wadek3\Mhs\SuratPermohonanPenghargaanController@SimpanProsesPermintaanSuratPermohonanPenghargaan']);
  Route::get('/mhs/surat/permohonanpenghargaan', ['as' => 'SuratPermohonanPenghargaanSiap', 'uses' => 'Wadek3\Mhs\SuratPermohonanPenghargaanController@SuratPermohonanPenghargaanSiap']);
  Route::get('/mhs/surat/permohonanpenghargaan/cetak/{id_surat}', ['as' => 'CetakSuratPermohonanPenghargaan', 'uses' => 'Wadek3\Mhs\SuratPermohonanPenghargaanController@CetakSuratPermohonanPenghargaan']);
  Route::get('/mhs/surat/permohonanpenghargaan/jmlbelum', ['as' => 'JumlahBelumProsesPermohonanPenghargaan', 'uses' => 'Wadek3\Mhs\SuratPermohonanPenghargaanController@JumlahSuratPermohonanPenghargaanBelumdiProses']);
});

Route::prefix('kajur')->name('kajur.')->group(function () {
  //Surat Observasi Kelas
  Route::get('/mhs/surat/observasikelas/permintaan', ['as' => 'PermintaanSuratIjinObservasiKelas', 'uses' => 'Kajur\Mhs\SuratIjinObservasiKelasController@PermintaanSuratIjinObservasiKelas']);
  Route::get('/mhs/surat/observasikelas/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratIjinObservasiKelas', 'uses' => 'Kajur\Mhs\SuratIjinObservasiKelasController@ProsesPermintaanSuratIjinObservasiKelas']);
  Route::post('/mhs/surat/observasikelas/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratIjinObservasiKelas', 'uses' => 'Kajur\Mhs\SuratIjinObservasiKelasController@SimpanProsesPermintaanSuratIjinObservasiKelas']);
  Route::get('/mhs/surat/observasikelas/prosessemua', ['as' => 'ProsesSemuaPermintaanSuratIjinObservasiKelas', 'uses' => 'Kajur\Mhs\SuratIjinObservasiKelasController@ProsesSemuaPermintaanSuratIjinObservasiKelas']);
  Route::post('/mhs/surat/observasikelas/prosessemua', ['as' => 'SimpanProsesSemuaPermintaanSuratIjinObservasiKelas', 'uses' => 'Kajur\Mhs\SuratIjinObservasiKelasController@SimpanProsesSemuaPermintaanSuratIjinObservasiKelas']);
  Route::get('/mhs/surat/observasikelas', ['as' => 'SuratIjinObservasiKelasSiap', 'uses' => 'Kajur\Mhs\SuratIjinObservasiKelasController@SuratIjinObservasiKelasSiap']);
  Route::get('/mhs/surat/observasikelas/cetak/{id_surat}', ['as' => 'CetakSuratIjinObservasiKelas', 'uses' => 'Kajur\Mhs\SuratIjinObservasiKelasController@CetakSuratIjinObservasiKelas']);
  Route::get('/mhs/surat/observasikelas/jmlbelum', ['as' => 'JumlahBelumProsesIjinObservasiKelas', 'uses' => 'Kajur\Mhs\SuratIjinObservasiKelasController@JumlahSuratObservasiKelasBelumdiProses']);
  //SUrat Observasi Pendahuluan
  Route::get('/mhs/surat/observasipendahuluan/permintaan', ['as' => 'PermintaanSuratIjinObservasiPendahuluan', 'uses' => 'Kajur\Mhs\SuratIjinObservasiPendahuluanController@PermintaanSuratIjinObservasiPendahuluan']);
  Route::get('/mhs/surat/observasipendahuluan/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratIjinObservasiPendahuluan', 'uses' => 'Kajur\Mhs\SuratIjinObservasiPendahuluanController@ProsesPermintaanSuratIjinObservasiPendahuluan']);
  Route::post('/mhs/surat/observasipendahuluan/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratIjinObservasiPendahuluan', 'uses' => 'Kajur\Mhs\SuratIjinObservasiPendahuluanController@SimpanProsesPermintaanSuratIjinObservasiPendahuluan']);
  Route::get('/mhs/surat/observasipendahuluan', ['as' => 'SuratIjinObservasiPendahuluanSiap', 'uses' => 'Kajur\Mhs\SuratIjinObservasiPendahuluanController@SuratIjinObservasiPendahuluanSiap']);
  Route::get('/mhs/surat/observasipendahuluan/cetak/{id_surat}', ['as' => 'CetakSuratIjinObservasiPendahuluan', 'uses' => 'Kajur\Mhs\SuratIjinObservasiPendahuluanController@CetakSuratIjinObservasiPendahuluan']);
  Route::get('/mhs/surat/observasipendahuluan/jmlbelum', ['as' => 'JumlahSuratBelumProsesIjinObservasiPendahuluan', 'uses' => 'Kajur\Mhs\SuratIjinObservasiPendahuluanController@JumlahSuratIjinObservasiPendahuluanBelumdiProses']);
  //Surat Ijin Riset Individu
  Route::get('/mhs/surat/risetindividu/permintaan', ['as' => 'PermintaanSuratIjinRisetIndividu', 'uses' => 'Kajur\Mhs\SuratIjinRisetIndividuController@PermintaanSuratIjinRisetIndividu']);
  Route::get('/mhs/surat/risetindividu/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratIjinRisetIndividu', 'uses' => 'Kajur\Mhs\SuratIjinRisetIndividuController@ProsesPermintaanSuratIjinRisetIndividu']);
  Route::post('/mhs/surat/risetindividu/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratIjinRisetIndividu', 'uses' => 'Kajur\Mhs\SuratIjinRisetIndividuController@SimpanProsesPermintaanSuratIjinRisetIndividu']);
  Route::get('/mhs/surat/risetindividu', ['as' => 'SuratIjinRisetIndividuSiap', 'uses' => 'Kajur\Mhs\SuratIjinRisetIndividuController@SuratIjinRisetIndividuSiap']);
  Route::get('/mhs/surat/risetindividu/cetak/{id_surat}', ['as' => 'CetakSuratIjinRisetIndividu', 'uses' => 'Kajur\Mhs\SuratIjinRisetIndividuController@CetakSuratIjinRisetIndividu']);
  Route::get('/mhs/surat/risetindividu/jmlbelum', ['as' => 'JumlahBelumProsesSuratIjinRisetIndividu', 'uses' => 'Kajur\Mhs\SuratIjinRisetIndividuController@JumlahSuratIjinRisetIndividuBelumdiProses']);
  //Surat Keterangan Lulus Skripsi
  Route::get('/mhs/surat/keteranganlulusskripsi/permintaan', ['as' => 'PermintaanSuratKeteranganLulusSkripsi', 'uses' => 'Kajur\Mhs\SuratKeteranganLulusSkripsiController@PermintaanSuratKeteranganLulusSkripsi']);
  Route::get('/mhs/surat/keteranganlulusskripsi/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratKeteranganLulusSkripsi', 'uses' => 'Kajur\Mhs\SuratKeteranganLulusSkripsiController@ProsesPermintaanSuratKeteranganLulusSkripsi']);
  Route::post('/mhs/surat/keteranganlulusskripsi/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratKeteranganLulusSkripsi', 'uses' => 'Kajur\Mhs\SuratKeteranganLulusSkripsiController@SimpanProsesPermintaanSuratKeteranganLulusSkripsi']);
  Route::get('/mhs/surat/keteranganlulusskripsi', ['as' => 'SuratKeteranganLulusSkripsiSiap', 'uses' => 'Kajur\Mhs\SuratKeteranganLulusSkripsiController@SuratKeteranganLulusSkripsiSiap']);
  Route::get('/mhs/surat/keteranganlulusskripsi/cetak/{id_surat}', ['as' => 'CetakSuratKeteranganLulusSkripsi', 'uses' => 'Kajur\Mhs\SuratKeteranganLulusSkripsiController@CetakSuratKeteranganLulusSkripsi']);
  Route::get('/mhs/surat/keteranganlulusskripsi/jmlbelum', ['as' => 'JumlahBelumProsesKeteranganLulusSkripsi', 'uses' => 'Kajur\Mhs\SuratKeteranganLulusSkripsiController@JumlahSuratKeteranganLulusBelumdiProses']);
  //Surat Keterangan Masih Kuliah
  Route::get('/mhs/surat/keteranganmasihkuliah/permintaan', ['as' => 'PermintaanSuratKeteranganMasihKuliah', 'uses' => 'Kajur\Mhs\SuratKeteranganMasihKuliahController@PermintaanSuratKeteranganMasihKuliah']);
  Route::get('/mhs/surat/keteranganmasihkuliah/proses/{id_surat}', ['as' => 'ProsesPermintaanSuratKeteranganMasihKuliah', 'uses' => 'Kajur\Mhs\SuratKeteranganMasihKuliahController@ProsesPermintaanSuratKeteranganMasihKuliah']);
  Route::post('/mhs/surat/keteranganmasihkuliah/proses/{id_surat}', ['as' => 'SimpanProsesPermintaanSuratKeteranganMasihKuliah', 'uses' => 'Kajur\Mhs\SuratKeteranganMasihKuliahController@SimpanProsesPermintaanSuratKeteranganMasihKuliah']);
  Route::get('/mhs/surat/keteranganmasihkuliah', ['as' => 'SuratKeteranganMasihKuliahSiap', 'uses' => 'Kajur\Mhs\SuratKeteranganMasihKuliahController@SuratKeteranganMasihKuliahSiap']);
  Route::get('/mhs/surat/keteranganmasihkuliah/cetak/{id_surat}', ['as' => 'CetakSuratKeteranganMasihKuliah', 'uses' => 'Kajur\Mhs\SuratKeteranganMasihKuliahController@CetakSuratKeteranganMasihKuliah']);
  Route::get('/mhs/surat/keteranganmasihkuliah/jmlbelum', ['as' => 'JumlahBelumProsesKeteranganMasihKuliah', 'uses' => 'Kajur\Mhs\SuratKeteranganMasihKuliahController@JumlahSuratKeteranganMasihKuliahBelumdiProses']);
});

Route::prefix('validasi')->name('validasi.')->group(function () {
  Route::get('/suratijinrisetindividu/{id_surat}-{unique_key}', ['as' => 'SuratIjinRisetIndividu', 'uses' => 'Validasi\ValidasiSuratMahasiswaController@ValidasiSuratIjinRisetIndividu']);
  Route::get('/suratijinobservasipendahuluan/{id_surat}-{unique_key}', ['as' => 'SuratIjinObservasiPendahuluan', 'uses' => 'Validasi\ValidasiSuratMahasiswaController@ValidasiSuratIjinObservasiPendahuluan']);
  Route::get('/suratijinobservasikelas/{id_surat}-{unique_key}', ['as' => 'SuratIjinObservasiKelas', 'uses' => 'Validasi\ValidasiSuratMahasiswaController@ValidasiSuratIjinObservasiKelas']);
  Route::get('/suratketeranganlulusskripsi/{id_surat}-{unique_key}', ['as' => 'SuratKeteranganLulusSkripsi', 'uses' => 'Validasi\ValidasiSuratMahasiswaController@ValidasiSuratKeteranganLulusSkripsi']);
  Route::get('/suratugas/{id_surat}-{unique_key}', ['as' => 'SuratTugas', 'uses' => 'Validasi\ValidasiSuratMahasiswaController@ValidasiSuratTugas']);
  Route::get('/suratketeranganmasihkuliah/{id_surat}-{unique_key}', ['as' => 'SuratKeteranganMasihKuliah', 'uses' => 'Validasi\ValidasiSuratMahasiswaController@ValidasiSuratKeteranganMasihKuliah']);
  Route::get('/suratketeranganpernahkuliah/{id_surat}-{unique_key}', ['as' => 'SuratKeteranganPernahKuliah', 'uses' => 'Validasi\ValidasiSuratMahasiswaController@ValidasiSuratKeteranganPernahKuliah']);
  Route::get('/suratketeranganpenggantiijazah/{id_surat}-{unique_key}', ['as' => 'SuratKeteranganPenggantiIjazah', 'uses' => 'Validasi\ValidasiSuratMahasiswaController@ValidasiSuratKeteranganPenggantiIjazah']);
  Route::get('/suratpermohonanapenghargaan/{id_surat}-{unique_key}', ['as' => 'SuratPermohonanPenghargaan', 'uses' => 'Validasi\ValidasiSuratMahasiswaController@ValidasiSuratPermohonanPenghargaan']);
});

Route::prefix('admin')->name('admin.')->group(function () {
  Route::get('/detailadmin', ['as' => 'DetailAdmin', 'uses' => 'Admin\AdminController@DetailAdmin']);
  Route::get('/tambahadmin', ['as' => 'TambahAdmin', 'uses' => 'Admin\AdminController@TambahAdmin']);
  Route::post('/simpanadmin', ['as' => 'SimpanAdmin', 'uses' => 'Admin\AdminController@SimpanAdmin']);
  Route::get('/editadmin/{id_user}', ['as' => 'EditAdmin', 'uses' => 'Admin\AdminController@EditAdmin']);
  Route::post('/updateadmin/{id_user}', ['as' => 'UpdateAdmin', 'uses' => 'Admin\AdminController@UpdateAdmin']);
  Route::get('/ubahpassword', ['as' => 'UbahPassword', 'uses' => 'Admin\AdminController@UbahPassword']);
  Route::post('/updatepassword', ['as' => 'UpdatePassword', 'uses' => 'Admin\AdminController@UpdatePassword']);
  Route::get('/tampiladmin', ['as' => 'TampilAdmin', 'uses' => 'Admin\AdminController@TampilAdmin']);
  Route::get('/detailadmin/admincari', ['as' => 'AdminCari', 'uses' => 'Admin\AdminController@AdminCari']);
  Route::get('/adm/detailadm', ['as' => 'DetailAdm', 'uses' => 'Admin\AdmController@DetailAdm']);
  Route::get('/adm/tambahadm', ['as' => 'TambahAdm', 'uses' => 'Admin\AdmController@TambahAdm']);
  Route::post('/adm/simpanadm', ['as' => 'SimpanAdm', 'uses' => 'Admin\AdmController@SimpanAdm']);
  Route::get('/adm/editadm/{id_user}', ['as' => 'EditAdm', 'uses' => 'Admin\AdmController@EditAdm']);
  Route::post('/adm/updateadm/{id_user}', ['as' => 'UpdateAdm', 'uses' => 'Admin\AdmController@UpdateAdm']);
  Route::get('/adm/tampiladm/{id_user}', ['as' => 'TampilAdm', 'uses' => 'Admin\AdmController@TampilAdm']);
  Route::get('/adm/ubahpassword/{id_user}', ['as' => 'UbahPasswordAdm', 'uses' => 'Admin\AdmController@UbahPassword']);
  Route::post('/adm/updatepasswordadm/{id_user}', ['as' => 'UpdatePasswordAdm', 'uses' => 'Admin\AdmController@UpdatePassword']);
  Route::get('/admcari', ['as' => 'AdmCari', 'uses' => 'Admin\AdmController@AdmCari']);



  Route::get('/tampilsuratmhs', ['as' => 'TampilSuratMhs', 'uses' => 'Admin\AdminController@TampilSuratMhs']);
  Route::get('/editsuratmhs/{id_surat_mahasiswa}', ['as' => 'EditSuratMhs', 'uses' => 'Admin\AdminController@EditSuratMhs']);
  Route::post('/updatesuratmhs/{id_surat_mahasiswa}', ['as' => 'UpdateSuratMhs', 'uses' => 'Admin\AdminController@UpdateSuratMhs']);
  Route::get('/detailsuratmh/{id_surat_mahasiswa}', ['as' => 'DetailSuratMhs', 'uses' => 'Admin\AdminController@DetailSuratMhs']);
  Route::get('/tampilsuratmhs/suratmhscari', ['as' => 'SuratMhsCari', 'uses' => 'Admin\AdminController@SuratMhsCari']);

  Route::get('/mhs/detailmhs', ['as' => 'DetailMhs', 'uses' => 'Admin\MhsController@DetailMhs']);
  Route::get('/mhs/editmhs/{id_user}', ['as' => 'EditMhs', 'uses' => 'Admin\MhsController@EditMhs']);
  Route::post('/mhs/updatemhs/{id_user}', ['as' => 'UpdateMhs', 'uses' => 'Admin\MhsController@UpdateMhs']);
  Route::get('/mhs/tampilmhs/{id_user}', ['as' => 'TampilMhs', 'uses' => 'Admin\MhsController@TampilMhs']);
  Route::get('/mhs/ubahpasswordmhs/{id_user}', ['as' => 'UbahPasswordMhs', 'uses' => 'Admin\MhsController@UbahPassword']);
  Route::post('/mhs/updatepasswordmhs/{id_user}', ['as' => 'UpdatePasswordMhs', 'uses' => 'Admin\MhsController@UpdatePassword']);
  Route::get('/mhscari', ['as' => 'MhsCari', 'uses' => 'Admin\MhsController@MhsCari']);

  Route::get('/dosen/detaildosen', ['as' => 'DetailDosen', 'uses' => 'Admin\DosenController@DetailDosen']);
  Route::get('/dosen/tambahdosen', ['as' => 'TambahDosen', 'uses' => 'Admin\DosenController@TambahDosen']);
  Route::post('/dosen/simpandosen', ['as' => 'SimpanDosen', 'uses' => 'Admin\DosenController@SimpanDosen']);
  Route::get('/dosen/editdosen/{id_user}', ['as' => 'EditDosen', 'uses' => 'Admin\DosenController@EditDosen']);
  Route::post('/dosen/updatedosen/{id_user}', ['as' => 'UpdateDosen', 'uses' => 'Admin\DosenController@UpdateDosen']);
  Route::get('/dosen/tampildosen/{id_user}', ['as' => 'TampilDosen', 'uses' => 'Admin\DosenController@TampilDosen']);
  Route::get('/dosen/ubahpassworddosen/{id_user}', ['as' => 'UbahPasswordDosen', 'uses' => 'Admin\DosenController@UbahPassword']);
  Route::post('/dosen/updatepassworddosen/{id_user}', ['as' => 'UpdatePasswordDosen', 'uses' => 'Admin\DosenController@UpdatePassword']);
  Route::get('/dosencari', ['as' => 'DosenCari', 'uses' => 'Admin\DosenController@DosenCari']);

  Route::get('/jurusan',['as'=>'Jurusan','uses'=>'Admin\ProdiJurusanJabatanController@DaftarJurusan']);
  Route::get('/jurusan/ubah/{id_jurusan}',['as'=>'UbahJurusan','uses'=>'Admin\ProdiJurusanJabatanController@UbahJurusan']);
  Route::post('/jurusan/ubah/{id_jurusan}',['as'=>'SimpanUbahJurusan','uses'=>'Admin\ProdiJurusanJabatanController@SimpanUbahJurusan']);
  Route::get('/prodi',['as'=>'Prodi','uses'=>'Admin\ProdiJurusanJabatanController@DaftarProdi']);
  Route::get('/prodi/ubah/{id_prodi}',['as'=>'UbahProdi','uses'=>'Admin\ProdiJurusanJabatanController@UbahProdi']);
  Route::post('/prodi/ubah/{id_prodi}',['as'=>'SimpanUbahProdi','uses'=>'Admin\ProdiJurusanJabatanController@SimpanUbahProdi']);
  Route::get('/dekanwadek',['as'=>'DekanWadek','uses'=>'Admin\ProdiJurusanJabatanController@DaftarDekanWadek']);
  Route::get('/dekanwadek/ubah/{jabatan}',['as'=>'UbahDekanWadek','uses'=>'Admin\ProdiJurusanJabatanController@UbahDekanWadek']);
  Route::post('/dekanwadek/ubah/{jabatan}',['as'=>'SimpanUbahDekanWadek','uses'=>'Admin\ProdiJurusanJabatanController@SimpanUbahDekanWadek']);
  Route::get('/adminprodi',['as'=>'AdminProdi','uses'=>'Admin\ProdiJurusanJabatanController@AdminProdi']);
  Route::get('/adminprodi/tambah',['as'=>'TambahAdminProdi','uses'=>'Admin\ProdiJurusanJabatanController@TambahAdminProdi']);
  Route::post('/adminprodi/tambah',['as'=>'SimpanTambahAdminProdi','uses'=>'Admin\ProdiJurusanJabatanController@SimpanTambahAdminProdi']);
});



Route::get('/tampildekan', ['as' => 'TampilPenggunaDekan', 'uses' => 'PengaturanUserController@TampilPenggunaDekan']);
Route::get('/detaildekan', ['as' => 'DetailPenggunaDekan', 'uses' => 'PengaturanUserController@DetailPenggunaDekan']);
Route::get('/ubahdekan', ['as' => 'EditPenggunaDekan', 'uses' => 'PengaturanUserController@EditPenggunaDekan']);
Route::post('/updatedekan', ['as' => 'UpdatePenggunaDekan', 'uses' => 'PengaturanUserController@UpdatePenggunaDekan']);
Route::get('/ubahpassworddekan', ['as' => 'UbahPasswordPenggunaDekan', 'uses' => 'PengaturanUserController@UbahPasswordPenggunaDekan']);
Route::post('/updatepassworddekan', ['as' => 'UpdatePasswordPenggunaDekan', 'uses' => 'PengaturanUserController@UpdatePasswordPenggunaDekan']);

Route::get('/tampilwadek1', ['as' => 'TampilPenggunaWadek1', 'uses' => 'PengaturanUserController@TampilPenggunaWadek1']);
Route::get('/detailwadek1', ['as' => 'DetailPenggunaWadek1', 'uses' => 'PengaturanUserController@DetailPenggunaWadek1']);
Route::get('/ubahwadek1', ['as' => 'EditPenggunaWadek1', 'uses' => 'PengaturanUserController@EditPenggunaWadek1']);
Route::post('/updatewadek1', ['as' => 'UpdatePenggunaWadek1', 'uses' => 'PengaturanUserController@UpdatePenggunaWadek1']);
Route::get('/ubahpasswordwadek1', ['as' => 'UbahPasswordPenggunaWadek1', 'uses' => 'PengaturanUserController@UbahPasswordPenggunaWadek1']);
Route::post('/updatepasswordwadek1', ['as' => 'UpdatePasswordPenggunaWadek1', 'uses' => 'PengaturanUserController@UpdatePasswordPenggunaWadek1']);

Route::get('/detailwadek2', ['as' => 'DetailPenggunaWadek2', 'uses' => 'PengaturanUserController@DetailPenggunaWadek2']);
Route::get('/ubahwadek2', ['as' => 'EditPenggunaWadek2', 'uses' => 'PengaturanUserController@EditPenggunaWadek2']);
Route::post('/updatewadek2', ['as' => 'UpdatePenggunaWadek2', 'uses' => 'PengaturanUserController@UpdatePenggunaWadek2']);
Route::get('/ubahpasswordwadek2', ['as' => 'UbahPasswordPenggunaWadek2', 'uses' => 'PengaturanUserController@UbahPasswordPenggunaWadek2']);
Route::post('/updatepasswordwadek2', ['as' => 'UpdatePasswordPenggunaWadek2', 'uses' => 'PengaturanUserController@UpdatePasswordPenggunaWadek2']);

Route::get('/detailwadek3', ['as' => 'DetailPenggunaWadek3', 'uses' => 'PengaturanUserController@DetailPenggunaWadek3']);
Route::get('/ubahwadek3', ['as' => 'EditPenggunaWadek3', 'uses' => 'PengaturanUserController@EditPenggunaWadek3']);
Route::post('/updatewadek3', ['as' => 'UpdatePenggunaWadek3', 'uses' => 'PengaturanUserController@UpdatePenggunaWadek3']);
Route::get('/ubahpasswordwadek3', ['as' => 'UbahPasswordPenggunaWadek3', 'uses' => 'PengaturanUserController@UbahPasswordPenggunaWadek3']);
Route::post('/updatepasswordwadek3', ['as' => 'UpdatePasswordPenggunaWadek3', 'uses' => 'PengaturanUserController@UpdatePasswordPenggunaWadek3']);

Route::get('/tampildosen', ['as' => 'TampilPenggunaDosen', 'uses' => 'PengaturanUserController@TampilPenggunaDosen']);
Route::get('/detaildosen', ['as' => 'DetailPenggunaDosen', 'uses' => 'PengaturanUserController@DetailPenggunaDosen']);
Route::get('/ubahdosen', ['as' => 'EditPenggunaDosen', 'uses' => 'PengaturanUserController@EditPenggunaDosen']);
Route::post('/updatedosen', ['as' => 'UpdatePenggunaDosen', 'uses' => 'PengaturanUserController@UpdatePenggunaDosen']);
Route::get('/ubahpassworddosen', ['as' => 'UbahPasswordPenggunaDosen', 'uses' => 'PengaturanUserController@UbahPasswordPenggunaDosen']);
Route::post('/updatepassworddosen', ['as' => 'UpdatePasswordPenggunaDosen', 'uses' => 'PengaturanUserController@UpdatePasswordPenggunaDosen']);

Route::get('/tampilkaprodi', ['as' => 'TampilPenggunaKaprodi', 'uses' => 'PengaturanUserController@TampilPenggunaKaprodi']);
Route::get('/detailkaprodi', ['as' => 'DetailPenggunaKaprodi', 'uses' => 'PengaturanUserController@DetailPenggunaKaprodi']);
Route::get('/ubahkaprodi', ['as' => 'EditPenggunaKaprodi', 'uses' => 'PengaturanUserController@EditPenggunaKaprodi']);
Route::post('/updatekaprodi', ['as' => 'UpdatePenggunaKaprodi', 'uses' => 'PengaturanUserController@UpdatePenggunaKaprodi']);
Route::get('/ubahpasswordkaprodi', ['as' => 'UbahPasswordPenggunaKaprodi', 'uses' => 'PengaturanUserController@UbahPasswordPenggunaKaprodi']);
Route::post('/updatepasswordkaprodi', ['as' => 'UpdatePasswordPenggunaKaprodi', 'uses' => 'PengaturanUserController@UpdatePasswordPenggunaKaprodi']);

Route::get('/tampiladm', ['as' => 'TampilPenggunaAdm', 'uses' => 'PengaturanUserController@TampilPenggunaAdm']);
Route::get('/detailadm', ['as' => 'DetailPenggunaAdm', 'uses' => 'PengaturanUserController@DetailPenggunaAdm']);
Route::get('/ubahadm', ['as' => 'EditPenggunaAdm', 'uses' => 'PengaturanUserController@EditPenggunaAdm']);
Route::post('/updateadm', ['as' => 'UpdatePenggunaAdm', 'uses' => 'PengaturanUserController@UpdatePenggunaAdm']);
Route::get('/ubahpasswordadm', ['as' => 'UbahPassworPenggunadAdm', 'uses' => 'PengaturanUserController@UbahPassworPenggunadAdm']);
Route::post('/updatepasswordadm', ['as' => 'UpdatePasswordPenggunaAdm', 'uses' => 'PengaturanUserController@UpdatePasswordPenggunaAdm']);

Route::get('/tampilmhs', ['as' => 'TampilPenggunaMhs', 'uses' => 'PengaturanUserController@TampilPenggunaMhs']);
Route::get('/detailmhs', ['as' => 'DetailPenggunaMhs', 'uses' => 'PengaturanUserController@DetailPenggunaMhs']);
Route::get('/ubahmhs', ['as' => 'EditPenggunaMhs', 'uses' => 'PengaturanUserController@EditPenggunaMhs']);
Route::post('/updatemhs', ['as' => 'UpdatePenggunaMhs', 'uses' => 'PengaturanUserController@UpdatePenggunaMhs']);
Route::get('/ubahpasswordmhs', ['as' => 'UbahPasswordPenggunaMhs', 'uses' => 'PengaturanUserController@UbahPasswordPenggunaMhs']);
Route::post('/updatepasswordmhs', ['as' => 'UpdatePasswordPenggunaMhs', 'uses' => 'PengaturanUserController@UpdatePasswordPenggunaMhs']);

//controller surat belum di proses
Route::get('/jumlahsuratrobservasikelas', ['as' => 'JumlahSuratObservasiKelas', 'uses' => 'Adm\Mhs\SuratIjinObservasiKelasController@JumlahSuratObservasiKelas']);
Route::get('/jumlahsuratpendahuluan', ['as' => 'JumlahSuratObservasiPendahuluan', 'uses' => 'Adm\Mhs\SuratIjinObservasiPendahuluanController@JumlahSuratObservasiPendahuluan']);
Route::get('/jumlahsuratrisetindividu', ['as' => 'JumlahSuratRisetIndividu', 'uses' => 'Adm\Mhs\SuratIjinRisetIndividuController@JumlahSuratRisetIndividuBelumdiProses']);
Route::get('/jumlahsuratlulusskripsi', ['as' => 'JumlahSuratKeteranganLulusSkripsi', 'uses' => 'Adm\Mhs\SuratKeteranganLulusSkripsiController@JumlahSuratKeteranganLulusSkripsi']);
Route::get('/jumlahsuratmasihkuliah', ['as' => 'JumlahSuratKeteranganMasihKuliah', 'uses' => 'Adm\Mhs\SuratKeteranganMasihKuliahController@JumlahSuratKeteranganMasihKuliah']);
Route::get('/jumlahsuratpenggantiijazah', ['as' => 'JumlahSuratKeteranganPenggantiIjazah', 'uses' => 'Adm\Mhs\SuratKeteranganPenggantiIjazahController@JumlahSuratKeteranganPenggantiIjazah']);
Route::get('/jumlahsuratpernahkuliah', ['as' => 'JumlahSuratPernahKuliah', 'uses' => 'Adm\Mhs\SuratKeteranganPernahKuliahController@JumlahSuratPernahKuliah']);
Route::get('/jumlahsurattugas', ['as' => 'JumlahSuratTugas', 'uses' => 'Adm\Mhs\SuratTugasController@JumlahSuratTugas']);
