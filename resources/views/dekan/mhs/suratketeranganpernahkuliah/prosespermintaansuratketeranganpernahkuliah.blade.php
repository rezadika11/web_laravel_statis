@extends('layout.main')
@section('title','Surat Keterangan Pernah Kuliah')
@section('page','Surat Keterangan Pernah Kuliah')

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Keterangan Pernah Kuliah</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Surat Keterangan Pernah Kuliah
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            @if(Session::has('simpan_gagal'))
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('simpan_gagal') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
            <form method="POST" action="{{route('dekan.SimpanProsesPermintaanSuratKeteranganPernahKuliah',['id_surat'=>$Surat->id_surat])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <label for="nim">Nama</label>
                    <p>
                      {{$Surat->nama_mahasiswa}}
                    </p>
                    <label for="nim">NIM</label>
                    <p>
                      {{$Surat->nim}}
                    </p>
                    <label for="nim">Program Studi</label>
                    <p>
                      {{$Surat->nama_prodi}}
                    </p>
                    <label for="alamat">Alamat</label>
                    <p>
                      {{$Surat->alamat}}
                    </p>
                    <label for="tahun_angkatan">Tahun Angkatan</label>
                    <p>
                      {{$Surat->angkatan}}
                    </p>
                    <label for="tahun_akademik_masuk">Tahun Akademik Masuk</label>
                    <p>
                      {{$Surat->tahun_akademik_masuk}}
                    </p>
                    <label for="no_surat">No. Surat</label>
                    <p>
                      {{$Surat->no_surat}}
                    </p>
                    <label for="no_surat">Tanggal Surat</label>
                    <p>
                      {{date('d-m-Y',strtotime($Surat->tanggal_surat))}}
                    </p>
                  </div>
                  <div class="col-md-6">
                    <label for="semester_keluar">Semester Keluar</label>
                    <p>
                      {{$SemesterKeluar}}
                    </p>
                    <label for="tahun_akademik_keluar">Tahun  Akademik Keluar</label>
                    <p>
                      {{$Surat->tahun_akademik_keluar}}
                    </p>
                    <label for="selesai_beban_studi">Selesai Beban Studi <sup>* dalam persen (%)</sup></label>
                    <p>
                      {{$Surat->selesai_beban_studi}} %
                    </p>
                    <label for="lama_semester_kuliah">Lama Semester Kuliah</label>
                    <p>
                      {{$LamaSemesterKuliah}}
                    </p>
                    <label for="prodi_tujuan">Program Studi Tujuan</label>
                    <p>
                      {{$Surat->prodi_tujuan}}
                    </p>
                    <label for="pt_tujuan">Nama Perguruan Tinggi Tujuan</label>
                    <p>
                      {{$Surat->pt_tujuan}}
                    </p>
                    <label for="alamat_pt_tujuan">Alamat Perguruan Tinggi Tujuan</label>
                    <p>
                      {{$Surat->alamat_pt_tujuan}}
                    </p>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Tanda Tangani</button>
                <a href="{{route('dekan.PermintaanSuratKeteranganPernahKuliah')}}" class="btn btn-secondary">Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('javascript')
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script>
  $(function () {
    $('.select2').select2()
  })
</script>
@endsection
