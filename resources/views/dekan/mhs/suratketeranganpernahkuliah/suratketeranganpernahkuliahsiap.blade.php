@extends('layout.main')
@section('title','Surat Keterangan Pernah Kuliah Siap')
@section('page','Surat Keterangan Pernah Kuliah Siap')

@section('css')

  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Surat Keterangan Pernah Kuliah Siap</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">

          <!-- jquery validation -->
          <div class="card card-primary">
            <div class="card-header">
              Surat Keterangan Pernah Kuliah Siap
            </div>
            <div class="card-body">
              
              @if(Session::has('simpan_gagal'))
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('simpan_gagal') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
              @if(Session::has('simpan_sukses'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('simpan_sukses') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif 
              <table id="permintaansurat" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>
                      No. Surat
                    </th>
                    <th>
                      Tanggal Surat
                    </th>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Angkatan</th>
                    <th>
                      Tahun Akademik Masuk
                    </th>
                    <th>
                      Semester Keluar
                    </th>
                    <th>
                      Tahun Akademik Keluar
                    </th>
                    <th>
                      Lama Semester Kuliah
                    </th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($SuratSiapCetak as $Surat)
                  <tr>
                    <td>
                      {{$no++}}
                    </td>
                    <td>
                      {{$Surat->no_surat}}
                    </td>
                    <td>
                      {{date('d-m-Y',strtotime($Surat->tanggal_surat))}}
                    </td>
                    <td>
                      {{$Surat->nim}}
                    </td>
                    <td>
                      {{$Surat->nama_mahasiswa}}
                    </td>
                    <td>
                      {{$Surat->angkatan}}
                    </td>
                    <td>
                      {{$Surat->tahun_akademik_masuk}}
                    </td>
                    <td>
                      @switch($Surat->semester_keluar)
                      @case('1')
                      Ganjil
                      @break
                      @case('2')
                      Genap
                      @break
                      @endswitch
                    </td>
                    <td>
                      {{$Surat->tahun_akademik_keluar}}
                    </td>
                    <td>
                      {{App\SuratMahasiswa::Semester($Surat->lama_semester_kuliah)}}
                    </td>
                    <td>
                      <a href="{{route('dekan.CetakSuratKeteranganPernahKuliah',['id_surat'=>$Surat->id_surat])}}" title="Cetak Surat" class="btn btn-primary btn-sm"><i class="fas fa-print"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="mt-3">
                {{ $SuratSiapCetak->links() }}
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
<!-- DataTables  & Plugins -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script>
  $(function () {
    $('#permintaansurat').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": true,
      "responsive": true,
      "scrollX": true,
    });
  });
</script>
@endsection
