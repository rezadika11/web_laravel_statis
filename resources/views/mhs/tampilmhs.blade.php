@extends('layout.main')
@section('title','Halaman Mahasiswa')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Data Mahasiswa</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Data Mahasiswa
          </div>
          <div class="card-body">
            @if (session('data_diedit'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              {{ session('data_diedit') }}
            </div>
            @endif
          
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                <th>No.</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Aksi</th>
                </tr>
              </thead>
              @forelse ($data as $adm)
              <tr>
                <td>{{ $loop->iteration }}.</td>
                <td>{{ $adm->nim }}</td>
                <td>{{ $adm->nama_mahasiswa }}</td>
                <td>{{ $adm->email }}</td>
                <td>
                  <a href="{{ route('EditPenggunaMhs') }}" title="Edit" class="btn btn-info"><i class="fas fa-edit"></i></a>
                  <a href="{{ route('DetailPenggunaMhs') }}" title="Lihat Detail" class="btn btn-danger"><i class="fas fa-eye"></i></a>
               </td>
              </tr>
              @empty
              <tr>
                <td colspan="5" class="text-center">Data Tidak Ada</td>
              </tr>
              @endforelse


            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
