@extends('layout.main')
@section('title','Surat Permohonan Penghargaan')
@section('page','Surat Permohonan Penghargaan')

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Form Surat Permohonan Penghargaan</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
         
          <div class="card card-primary">
            <div class="card-header">
              Form Surat Permohonan Penghargaan
            </div>
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif 
            <form method="POST" action="{{route('mhs.SimpanTambahSuratPermohonanPenghargaan')}}">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nama_perlombaan">Nama Perlombaan</label>
                      <input type="text" name="nama_perlombaan"  class="form-control {{$errors->has('nama_perlombaan') ? 'is-invalid' : '' }}" id="nama_perlombaan" placeholder="Contoh: Lomba Karya Tulis Tingkat Nasional" value="{{old('nama_perlombaan')}}">
                      @error('nama_perlombaan')
                      <span class="error invalid-feedback">
                          {{$errors->first('nama_perlombaan')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tanggal_mulai_pelaksanaan">Tanggal Mulai Pelaksanaan</label>
                      <input type="date" name="tanggal_mulai_pelaksanaan"  class="form-control {{$errors->has('tanggal_mulai_pelaksanaan') ? 'is-invalid' : '' }}" id="tanggal_mulai_pelaksanaan" value="{{old('tanggal_mulai_pelaksanaan')}}">
                      @error('tanggal_mulai_pelaksanaan')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_mulai_pelaksanaan')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tanggal_selsai_pelaksanaan">Tanggal Selesai Pelaksanaan</label>
                      <input type="date" name="tanggal_selesai_pelaksanaan"  class="form-control {{$errors->has('tanggal_selesai_pelaksanaan') ? 'is-invalid' : '' }}" id="tanggal_selesai_pelaksanaan" value="{{old('tanggal_selesai_pelaksanaan')}}">
                      @error('tanggal_selesai_pelaksanaan')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_selesai_pelaksanaan')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="tempat">Tempat</label>
                      <input type="text" name="tempat"  class="form-control {{$errors->has('tempat') ? 'is-invalid' : '' }}" id="tempat" placeholder="Contoh: Universitas Indonesia" value="{{old('tempat')}}">
                      @error('tempat')
                      <span class="error invalid-feedback">
                          {{$errors->first('tempat')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="juara">Juara</label>
                      <select class="form-control select2 {{$errors->has('juara') ? 'is-invalid' : '' }}" name="juara">
                        <option disabled selected>
                          Juara
                        </option>
                        @foreach($PilihJuara as $val=>$label)
                          <option value="{{$val}}" {{old('juara')==$val ? 'selected' : ''}}>
                            {{$val}} ({{$label}})
                          </option> 
                        @endforeach 
                      </select>
                      @error('juara')
                      <span class="error invalid-feedback">
                          {{$errors->first('juara')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="kelompok">Kelompok</label>
                      <input type="text" name="kelompok"  class="form-control {{$errors->has('kelompok') ? 'is-invalid' : '' }}" id="kelompok" placeholder="Contoh: Inidividu, Kelompok" value="{{old('kelompok')}}">
                      @error('kelompok')
                      <span class="error invalid-feedback">
                          {{$errors->first('kelompok')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{route('mhs.DaftarSuratPermohonanPenghargaan')}}" class="btn btn-secondary">Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('javascript')
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script>
  $(function () {
    $('.select2').select2()
  })
</script>

@endsection
