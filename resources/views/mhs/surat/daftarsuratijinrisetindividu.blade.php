@extends('layout.main')
@section('title','Daftar Surat Ijin Riset Individu')
@section('page','Daftar Surat Ijin Riset Individu')

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

<!-- DataTables -->
  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper" style="min-height: 2080.12px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Daftar Surat Ijin Riset Individu</h1>
          </div> 
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">

                <p class="card-title"> Daftar Surat Ijin Riset Individu</p>
              </div>
              <div class="card-body">
                @if(Session::has('simpan_sukses'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  {{ session('simpan_sukses') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <a href="{{ route('mhs.TambahSuratIjinRisetIndividu') }}" class="btn btn-success mb-3"><i class="fas fa-plus-circle"></i> Tambah Surat</a>
                <table class="table table-striped table-bordered mb-4" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Surat Ditujukan</th>
                            <th>Kecamatan</th>
                            <th>Judul Riset</th>
                            <th>Objek</th>
                            <th>Tempat</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>Metode Penelitian</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($DaftarSurat as $Surat)

                      <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $Surat->kepada }}</td>
                        <td>{{ $Surat->kec }}</td>
                        <td>{{ $Surat->judul }}</td>
                        <td>{{ $Surat->obyek }}</td>
                        <td>{{ $Surat->tempat }}</td>
                        <td>{{ $Surat->tanggal_mulai }}</td>
                        <td>{{ $Surat->tanggal_selesai}}</td>
                        <td>{{ $Surat->metode_penelitian }}</td>
                        <td>
                            @if($Surat->status_persetujuan_adm == 1 && $Surat->status_persetujuan_kajur == 1)
                              <a href="{{route('mhs.CetakSuratIjinRisetIndividu',['id_surat'=>$Surat->id_surat])}}" title="Cetak Surat" class="btn btn-primary btn-sm"><i class="fas fa-print"></i></a>
                            @else
                              <button class="btn btn-sm btn-warning" title="Menunggu Persetujuan"><i class="fas fa-sync-alt"></i></button>
                            @endif
                        </td>
                    </tr>

                      @endforeach

                    </tbody>

                </table>

                {{ $DaftarSurat->links() }}

              </div>


            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    <!-- /.content -->
  </div>
  </section>
</div>

@endsection
@section('javascript')
<!-- DataTables  & Plugins -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>

@endsection
