@extends('layout.main')
@section('title','Surat Keterangan Lulus Skripsi')
@section('page','Surat Keterangan Lulus Skripsi')

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Surat Keterangan Lulus Skripsi</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div> 
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
         
          <div class="card card-primary">
            <div class="card-header">
              <h4 class="card-title">Form Surat Keterangan Lulus Skripsi</h4>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            @if(Session::has('simpan_gagal'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  {{ session('simpan_gagal') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
            <form method="POST" action="{{route('mhs.SimpanTambahSuratKeteranganLulusSkripsi')}}" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="tahun_akademik">Tahun  Akademik</label>
                      <select class="form-control select2 {{$errors->has('tahun_akademik') ? 'is-invalid' : '' }}" name="tahun_akademik">
                        <option disabled selected>
                          Tahun Akademik
                        </option>
                        @foreach($TahunAkademik as $val)
                          <option value="{{$val}}" {{old('tahun_akademik')==$val ? 'selected' : ''}}>
                            {{$val}}
                          </option>
                        @endforeach
                      </select>
                      @error('tahun_akademik')
                      <span class="error invalid-feedback">
                          {{$errors->first('tahun_akademik')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tanggal_ujian">Tanggal Ujian</label>
                      <input type="date" name="tanggal_ujian" class="form-control {{$errors->has('tanggal_ujian') ? 'is-invalid' : '' }}" id="tanggal_ujian" value="{{old('tanggal_ujian')}}">
                      @error('tanggal_ujian')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_ujian')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="lampiran_munaqosyah">Lampiran Berita Acara Munaqosyah</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input {{$errors->has('lampiran_munaqosyah') ? 'is-invalid' : '' }}" id="lampiran_munaqosyah" name="lampiran_munaqosyah" value="{{ old('lampiran_munaqosyah') }}"> 
                          <label class="custom-file-label" for="exampleInputFile">Pilih File</label>
                          @error('lampiran_munaqosyah')
                          <span class="error invalid-feedback">
                           {{$errors->first('lampiran_munaqosyah')}}
                          </span>
                           @enderror
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{route('mhs.DaftarSuratIjinObservasiKelas')}}" class="btn btn-secondary">Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('javascript')
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script>
  $(function () {
    $('.select2').select2()
  })
</script>
@endsection
