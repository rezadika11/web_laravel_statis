@extends('layout.main')
@section('title','Surat Keterangan Masih Kuliah')
@section('page','Surat Keterangan Masih Kuliah')

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Form Surat Keterangan Masih Kuliah</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">

          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Form Surat Keterangan Masih Kuliah</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('mhs.SimpanTambahSuratKeteranganMasihKuliah')}}">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="tahun_akademik">Tahun  Akademik</label>
                      <select class="form-control select2 {{$errors->has('tahun_akademik') ? 'is-invalid' : '' }}" name="tahun_akademik">
                        <option disabled selected>
                          Tahun Akademik
                        </option>
                        @foreach($TahunAkademik as $val)
                          <option value="{{$val}}" {{old('tahun_akademik')==$val ? 'selected' : ''}}>
                            {{$val}}
                          </option>
                        @endforeach
                      </select>
                      @error('tahun_akademik')
                      <span class="error invalid-feedback">
                          {{$errors->first('tahun_akademik')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" name="wali">
                        <label class="custom-control-label" for="customSwitch1">Tambahkan Identitas Wali (Untuk Kepentingan Tunjangan PNS dan Sebagainya.)</label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama Orang Tua / Wali</label>
                      <input type="text" name="nama" class="form-control {{$errors->has('nama') ? 'is-invalid' : '' }}" id="nama" placeholder="Nama Orang Tua / Wali" value="{{old('nama')}}">
                      @error('nama')
                      <span class="error invalid-feedback">
                          {{$errors->first('nama')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                        <label for="nip">NIP / NRP</label>
                        <input type="text" name="nip" class="form-control {{$errors->has('nip') ? 'is-invalid' : '' }}" id="nip" placeholder="NIP / NRP" value="{{old('nip')}}">
                        @error('nip')
                        <span class="error invalid-feedback">
                            {{$errors->first('nip')}}
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="pangkat_golongan">Pangkat / Golongan / Ruang</label>
                        <input type="text" name="pangkat_golongan" class="form-control {{$errors->has('pangkat_golongan') ? 'is-invalid' : '' }}" id="pangkat_golongan" placeholder="Pangkat / Golongan / Ruang" value="{{old('pangkat_golongan')}}">
                        @error('pangkat_golongan')
                        <span class="error invalid-feedback">
                            {{$errors->first('pangkat_golongan')}}
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="instansi">Pada Instansi</label>
                        <input type="text" name="instansi" class="form-control {{$errors->has('instansi') ? 'is-invalid' : '' }}" id="instansi" placeholder="Pada Instansi" value="{{old('instansi')}}">
                        @error('instansi')
                        <span class="error invalid-feedback">
                            {{$errors->first('instansi')}}
                        </span>
                        @enderror
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{route('mhs.DaftarSuratKeteranganMasihKuliah')}}" class="btn btn-secondary">Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('javascript')
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script>
  function KeteranganWali(){
    if($("input[name=wali]").prop("checked") == true){
      $("#nama").prop('readonly',false)
      $("#nip").prop('readonly',false)
      $("#pangkat_golongan").prop('readonly',false)
      $("#instansi").prop('readonly',false)
    }else {
      $("#nama").prop('readonly',true)
      $("#nama").val("")
      $("#nip").prop('readonly',true)
      $("#nip").val("")
      $("#pangkat_golongan").prop('readonly',true)
      $("#pangkat_golongan").val("")
      $("#instansi").prop('readonly',true)
      $("#instansi").val("")
    }
  }

  $(function () {
    $('.select2').select2()
    @if(old('wali')=='on')
    $("input[name=wali]").prop('checked',true)
    @endif
    KeteranganWali()
  })
  $("input[name=wali]").change(function(){
    KeteranganWali()
  })
</script>
@endsection
