@extends('layout.main')
@section('title','Surat Ijin Riset Individu')
@section('page','Surat Ijin Riset Individu')

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Form Surat Ijin Riset Individu</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Form Surat Ijin Riset Individu</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            @if(Session::has('simpan_gagal'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  {{ session('simpan_gagal') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
            <form method="POST" action="{{route('mhs.SimpanTambahSuratIjinRisetIndividu')}}">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">Kepada</label>
                      <input type="text" name="kepada"  class="form-control {{$errors->has('kepada') ? 'is-invalid' : '' }}" id="kepada" placeholder="Surat Ditujukan Kepada" value="{{old('kepada')}}">
                      @error('kepada')
                      <span class="error invalid-feedback">
                          {{$errors->first('kepada')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="kec">Kecamatan</label>
                      <input type="text" name="kec"  class="form-control {{$errors->has('kec') ? 'is-invalid' : '' }}" id="kec" placeholder="Kecamatan" value="{{old('kec')}}">
                      @error('kec')
                      <span class="error invalid-feedback">
                          {{$errors->first('kec')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="semester">Semester</label>
                      <select class="form-control select2bs4 {{$errors->has('semester') ? 'is-invalid' : '' }}" name="semester" style="width: 100%;">
                        <option disabled selected>
                          Semester
                        </option>
                        @foreach($semester as $val => $label)
                          <option value="{{$val}}" {{old('semester')==$val ? 'selected' : ''}}>
                            {{$label}}
                          </option>
                        @endforeach
                      </select>
                      @error('semester')
                      <span class="error invalid-feedback">
                          {{$errors->first('semester')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="kec">Tembusan</label>
                      <input type="text" name="tembusan"  class="form-control {{$errors->has('tembusan') ? 'is-invalid' : '' }}" id="tembusan" placeholder="Pisahkan dengan ; (Titik Koma) apabila lebih dari satu tembusan." value="{{old('tembusan')}}">
                      @error('tembusan')
                      <span class="error invalid-feedback">
                          {{$errors->first('tembusan')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="lampiran">Lampiran Surat Telah Semprop</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="exampleInputFile">
                          <label class="custom-file-label" for="exampleInputFile">Pilih File</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="judul">Judul Riset</label>
                      <input type="text" name="judul" class="form-control {{$errors->has('judul') ? 'is-invalid' : '' }}" id="judul" placeholder="Judul Riset" value="{{old('judul')}}">
                      @error('judul')
                      <span class="error invalid-feedback">
                          {{$errors->first('judul')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                        <label for="Obyek">Obyek</label>
                        <input type="text" name="obyek" class="form-control {{$errors->has('obyek') ? 'is-invalid' : '' }}" id="obyek" placeholder="Obyek" value="{{old('obyek')}}">
                        @error('obyek')
                        <span class="error invalid-feedback">
                            {{$errors->first('obyek')}}
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                      <label for="tempat">Tempat</label>
                      <input type="text" name="tempat" class="form-control {{$errors->has('tempat') ? 'is-invalid' : '' }}" id="tempat" placeholder="Tempat" value="{{old('tempat')}}">
                      @error('tempat')
                      <span class="error invalid-feedback">
                          {{$errors->first('tempat')}}
                      </span>
                      @enderror
                    </div>
                    {{-- <div class="form-group">
                      <label for="tgl">Tanggal Mulai Riset</label>
                      <input type="date" name="tanggal_mulai" class="form-control {{$errors->has('tanggal_mulai') ? 'is-invalid' : '' }}" id="tgl" placeholder="Tanggal Riset" value="{{old('tanggal_mulai')}}">
                      @error('tanggal_mulai')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_mulai')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tgl">Tanggal Selesai Riset</label>
                      <input type="date" name="tanggal_selesai" class="form-control {{$errors->has('tanggal_selesai') ? 'is-invalid' : '' }}" id="tgl" placeholder="Tanggal Riset" value="{{old('tanggal_selesai')}}">
                      @error('tanggal_selesai')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_selesai')}}
                      </span>
                      @enderror
                    </div> --}}
                    <div class="form-group">
                      <label for="metode">Metode Penelitian</label>
                      <input type="text" name="metode_penelitian" class="form-control {{$errors->has('metode_penelitian') ? 'is-invalid' : '' }}" id="metode" placeholder="Metode Penelitian" value="{{old('metode_penelitian')}}">
                      @error('metode_penelitian')
                      <span class="error invalid-feedback">
                          {{$errors->first('metode_penelitian')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer mb-2">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{ route('mhs.DaftarSuratIjinRisetIndividu') }}" class="btn btn-secondary">Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('javascript')
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script>
  $(function () {
    $('.select2').select2()

//Initialize Select2 Elements
$('.select2bs4').select2({
  theme: 'bootstrap4'
})
  })
</script>

@endsection
