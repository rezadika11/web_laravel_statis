@extends('layout.main')
@section('title','Daftar Surat Ijin Observasi Pendahuluan')
@section('page','Daftar Surat Ijin Observasi Pendahuluan')

@section('css')
<!-- DataTables -->
  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper" style="min-height: 2080.12px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Daftar Surat Ijin Observasi Pendahuluan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
               Daftar Surat Ijin Observasi Pendahuluan
              </div>
              <div class="card-body">
                @if(Session::has('simpan_sukses'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  {{ session('simpan_sukses') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif

                <a href="{{ route('mhs.TambahSuratIjinObservasiPendahuluan') }}" class="btn btn-success mb-3"><i class="fas fa-plus-circle"></i> Tambah Surat</a>
                <table class="table table-striped table-bordered mb-4" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kepada</th>
                            <th>Objek</th>
                            <th>
                              Lokasi
                            </th>
                            <th>
                              Tanggal Observasi
                            </th>
                            <th>
                              Tahun Akademik
                            </th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($DataSurat as $Surat)

                      <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $Surat->kepada }}</td>
                        <td>{{ $Surat->obyek }}</td>
                        <td>{{ $Surat->lokasi }}</td>
                        <td>{{ date('d-m-Y',strtotime($Surat->tanggal_observasi)) }}</td>
                        <td>{{ $Surat->tahun_akademik }}</td>
                        <td>
                            @if($Surat->status_persetujuan_adm == 1 && $Surat->status_persetujuan_kajur == 1)
                              <a href="{{route('mhs.CetakSuratIjinObservasiPendahuluan',['id_surat'=>$Surat->id_surat])}}" title="Cetak Surat" class="btn btn-primary btn-sm"><i class="fas fa-print"></i></a>
                            @else
                              <button class="btn btn-sm btn-warning" title="Menunggu Persetujuan"><i class="fas fa-sync-alt"></i></button>
                            @endif
                        </td>
                    </tr>

                      @endforeach

                    </tbody>

                </table>

                {{ $DataSurat->links()  }}

              </div>


            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  </div>
@endsection
@section('javascript')
<!-- DataTables  & Plugins -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<script>
  $(function () {
    $('.select2').select2()
  });
  $(document).ready(function() {
    $('#example').DataTable();
} );

</script>

@endsection
