@extends('layout.main')
@section('title','Surat Ijin Observasi Kelas')
@section('page','Surat Ijin Observasi Kelas')

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Form Surat Ijin Observasi Kelas</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">

          <div class="card card-primary">
            <div class="card-header">
              <h4 class="card-title">Form Surat Ijin Observasi Kelas</h4>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('mhs.SimpanTambahSuratIjinObservasiKelas')}}">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="kepada">Kepada</label>
                      <input type="text" name="kepada"  class="form-control {{$errors->has('kepada') ? 'is-invalid' : '' }}" id="kepada" placeholder="Contoh: Kepala SMK N 2 Purwokerto" value="{{old('kepada')}}">
                      @error('kepada')
                      <span class="error invalid-feedback">
                          {{$errors->first('kepada')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="mata_kuliah">Mata Kuliah</label>
                      <input type="text" name="mata_kuliah"  class="form-control {{$errors->has('mata_kuliah') ? 'is-invalid' : '' }}" id="mata_kuliah" placeholder="Contoh: Manajemen Sekolah" value="{{old('mata_kuliah')}}">
                      @error('mata_kuliah')
                      <span class="error invalid-feedback">
                          {{$errors->first('mata_kuliah')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="dosen_pengampu">Dosen Pengampu</label>
                      <input type="text" name="dosen_pengampu"  class="form-control {{$errors->has('dosen_pengampu') ? 'is-invalid' : '' }}" id="dosen_pengampu" placeholder="Dosen Pengampu" value="{{old('dosen_pengampu')}}">
                      @error('dosen_pengampu')
                      <span class="error invalid-feedback">
                          {{$errors->first('dosen_pengampu')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="semester">Semester</label>
                      <select class="form-control select2 {{$errors->has('semester') ? 'is-invalid' : '' }}" name="semester">
                        <option disabled selected>
                          Semester
                        </option>
                        @foreach($semester as $val => $label)
                          <option value="{{$val}}" {{old('semester')==$val ? 'selected' : ''}}>
                            {{$label}}
                          </option>
                        @endforeach
                      </select>
                      @error('semester')
                      <span class="error invalid-feedback">
                          {{$errors->first('semester')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="obyek">Tema</label>
                      <input type="text" name="tema" class="form-control {{$errors->has('tema') ? 'is-invalid' : '' }}" id="tema" placeholder="Tema" value="{{old('tema')}}">
                      @error('tema')
                      <span class="error invalid-feedback">
                          {{$errors->first('tema')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tanggal_mulai">Tanggal Mulai</label>
                      <input type="date" name="tanggal_mulai" class="form-control {{$errors->has('tanggal_mulai') ? 'is-invalid' : '' }}" id="tanggal_mulai" value="{{old('tanggal_mulai')}}">
                      @error('tanggal_mulai')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_mulai')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tanggal_selesai">Tanggal Selesai</label>
                      <input type="date" name="tanggal_selesai" class="form-control {{$errors->has('tanggal_selesai') ? 'is-invalid' : '' }}" id="tanggal_selesai" value="{{old('tanggal_selesai')}}">
                      @error('tanggal_selesai')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_selesai')}}
                      </span>
                      @enderror
                    </div>
                    <label>Anggota Kelompok (Nama Anda Tidak Perlu Dimasukan)</label>
                    @error('nim.0')
                    <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$errors->first('nim.0')}}
                    </div>
                    @enderror
                    @error('peserta')
                    <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$errors->first('peserta')}}
                    </div>
                    @enderror
                      <div class="form-group" id="MahasiswaPeserta">
                        <div class="row">
                          <input type="text" class="form-control col-md-4" name="peserta[0][nim]" placeholder="NIM"/>
                          <input type="text" class="form-control col-md-8" name="peserta[0][nama]" placeholder="Nama"/>
                        </div>
                    </div>
                    <button type="button" onclick="TambahMahasiswa()" class="btn btn-success btn-sm">Tambah Mahasiswa</button>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{route('mhs.DaftarSuratIjinObservasiKelas')}}" class="btn btn-secondary">Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('javascript')
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script>
  $(function () {
    $('.select2').select2()
  })
  var angka = 1;
  function TambahMahasiswa(){
    var items=[];
    items.push('<div class="row"><input type="text" class="form-control col-md-4" name="peserta['+angka+'][nim]" placeholder="NIM"/><input type="text" class="form-control col-md-8" name="peserta['+angka+'][nama]" placeholder="Nama"/></div>');
    $("#MahasiswaPeserta").append(items);
    angka = angka+1;
  }
</script>

@endsection
