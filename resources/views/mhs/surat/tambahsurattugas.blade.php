@extends('layout.main')
@section('title','Surat Tugas')
@section('page','Surat Tugas')

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Form Surat Tugas</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
         
          <div class="card card-primary">
            <div class="card-header">
              Form Surat Tugas
            </div>
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif 
            <form method="POST" action="{{route('mhs.SimpanTambahSuratTugas')}}">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="kegiatan">Kegiatan</label>
                      <input type="text" name="kegiatan"  class="form-control {{$errors->has('kegiatan') ? 'is-invalid' : '' }}" id="kegiatan" placeholder="Contoh: Lomba Karya Tulis Tingkat Nasional" value="{{old('kegiatan')}}">
                      @error('kegiatan')
                      <span class="error invalid-feedback">
                          {{$errors->first('kegiatan')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="Penyelenggara">Penyelenggara</label>
                      <input type="text" name="penyelenggara"  class="form-control {{$errors->has('penyelenggara') ? 'is-invalid' : '' }}" id="penyelenggara" placeholder="Contoh: Universitas Indonesia" value="{{old('penyelenggara')}}">
                      @error('penyelenggara')
                      <span class="error invalid-feedback">
                          {{$errors->first('penyelenggara')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="tanggal_mulai">Tanggal Mulai</label>
                      <input type="date" name="tanggal_mulai"  class="form-control {{$errors->has('tanggal_mulai') ? 'is-invalid' : '' }}" id="tanggal_mulai" value="{{old('tanggal_mulai')}}">
                      @error('tanggal_mulai')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_mulai')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tanggal_selesai">Tanggal Selesai</label>
                      <input type="date" name="tanggal_selesai"  class="form-control {{$errors->has('tanggal_selesai') ? 'is-invalid' : '' }}" id="tanggal_selesai" value="{{old('tanggal_selesai')}}">
                      @error('tanggal_selesai')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_selesai')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <label>Peserta <sup>* Tidak Perlu Tambahkan Nama Anda</sup></label> <button type="button" onclick="TambahMahasiswa()" class="btn btn-success btn-sm">Tambah Mahasiswa</button>
                    @error('peserta.0.nim')
                    <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$errors->first('peserta.0.nim')}}
                    </div>
                    @enderror
                    @error('peserta.0.nama')
                    <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$errors->first('peserta.0.nama')}}
                    </div>
                    @enderror
                    @error('peserta.0.id_prodi')
                    <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$errors->first('peserta.0.id_prodi')}}
                    </div>
                    @enderror
                    @error('peserta')
                    <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$errors->first('peserta')}}
                    </div>
                    @enderror
                      <div class="form-group" id="MahasiswaPeserta">
                        <div class="row">
                          <input type="text" class="form-control col-md-2" name="peserta[0][nim]" placeholder="NIM"/>
                          <input type="text" class="form-control col-md-4" name="peserta[0][nama]" placeholder="Nama"/>
                            <select class="form-control col-md-6 select2" name="peserta[0][id_prodi]">
                              <option disabled selected>- Jurusan -</option>
                              @foreach($Jurusan as $jrs)
                                <option value="{{$jrs->id_prodi}}">
                                  {{$jrs->nama_jurusan}}
                                </option>
                              @endforeach
                            </select>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{route('mhs.DaftarSuratTugas')}}" class="btn btn-secondary">Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('javascript')
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script>
  $(function () {
    $('.select2').select2()
  })
  var angka = 1;
  function TambahMahasiswa(){
    var items=[];
    var select = `
      <select class="form-control col-md-6 select2" name="peserta[`+angka+`][id_prodi]">
        <option disabled selected>- Jurusan -</option>
        @foreach($Jurusan as $jrs)
          <option value="{{$jrs->id_prodi}}">
            {{$jrs->nama_jurusan}}
          </option>
        @endforeach
      </select>`;
    items.push('<div class="row"><input type="text" class="form-control col-md-2" name="peserta['+angka+'][nim]" placeholder="NIM"/><input type="text" class="form-control col-md-4" name="peserta['+angka+'][nama]" placeholder="Nama"/>'+select+'</div>');
    $("#MahasiswaPeserta").append(items);
    angka = angka+1;
    $('.select2').select2()
  }
</script>

@endsection
