@extends('layout.main')
@section('title','Surat Ijin Observasi Pendahuluan')
@section('page','Surat Ijin Observasi Pendahuluan')

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0"> Surat Ijin Observasi Pendahuluan</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">

          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Form Surat Ijin Observasi Pendahuluan</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('mhs.SimpanTambahSuratIjinObservasiPendahuluan')}}">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">Kepada</label>
                      <input type="text" name="kepada"  class="form-control {{$errors->has('kepada') ? 'is-invalid' : '' }}" id="kepada" placeholder="Contoh: Kepala SMK N 2 Purwokerto" value="{{old('kepada')}}">
                      @error('kepada')
                      <span class="error invalid-feedback">
                          {{$errors->first('kepada')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="semester">Semester</label>
                      <select class="form-control select2 {{$errors->has('semester') ? 'is-invalid' : '' }}" name="semester">
                        <option disabled selected>
                          Semester
                        </option>
                        @foreach($semester as $val => $label)
                          <option value="{{$val}}" {{old('semester')==$val ? 'selected' : ''}}>
                            {{$label}}
                          </option>
                        @endforeach
                      </select>
                      @error('semester')
                      <span class="error invalid-feedback">
                          {{$errors->first('semester')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tahun_akademik">Tahun  Akademik</label>
                      <select class="form-control select2 {{$errors->has('tahun_akademik') ? 'is-invalid' : '' }}" name="tahun_akademik">
                        <option disabled selected>
                          Tahun Akademik
                        </option>
                        @foreach($TahunAkademik as $val)
                          <option value="{{$val}}" {{old('tahun_akademik')==$val ? 'selected' : ''}}>
                            {{$val}}
                          </option>
                        @endforeach
                      </select>
                      @error('tahun_akademik')
                      <span class="error invalid-feedback">
                          {{$errors->first('tahun_akademik')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="obyek">Obyek</label>
                      <input type="text" name="obyek" class="form-control {{$errors->has('obyek') ? 'is-invalid' : '' }}" id="obyek" placeholder="Obyek" value="{{old('obyek')}}">
                      @error('obyek')
                      <span class="error invalid-feedback">
                          {{$errors->first('obyek')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                        <label for="Obyek">Lokasi</label>
                        <input type="text" name="lokasi" class="form-control {{$errors->has('lokasi') ? 'is-invalid' : '' }}" id="lokasi" placeholder="lokasi" value="{{old('lokasi')}}">
                        @error('lokasi')
                        <span class="error invalid-feedback">
                            {{$errors->first('lokasi')}}
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                      <label for="tempat">Tanggal Observasi</label>
                      <input type="date" name="tanggal_observasi" class="form-control {{$errors->has('tanggal_observasi') ? 'is-invalid' : '' }}" id="tanggal_observasi" value="{{old('tanggal_observasi')}}">
                      @error('tanggal_observasi')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_observasi')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{route('mhs.DaftarSuratIjinObservasiPendahuluan')}}" class="btn btn-secondary">Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('javascript')
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script>
  $(function () {
    $('.select2').select2()
  })
</script>

@endsection
