@extends('layout.main')
@section('title','Halaman Edit Mahasiswa')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Ubah Mahasiswa</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
      @if(Session::has('simpan_gagal'))
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('simpan_gagal') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        </button>
      </div>
      @endif
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Form Ubah Mahasiswa
          </div>
          <div class="card-body">
              <form action="{{ route('UpdatePenggunaMhs') }}" method="POST">
                {{ csrf_field() }}
                  <div class="mb-3 ">
                    <label for="nim">NIM</label>
                    <input type="text" class="form-control @error('nim')
                    is-invalid @enderror" placeholder="NIM" value="{{ old('nim', $data->nim )  }}" readonly>
                    @error('nim')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror

                  </div>
                  <div class="mb-3">
                    <label for="nama">Nama Lengkap</label>
                    <input type="text" name="nama_mahasiswa" placeholder="Nama Lengkap" class="form-control
                    @error('nama_mahasiswa')   is-invalid @enderror" value="{{ old('nama_mahasiswa', $data->nama_mahasiswa ) }}">
                    @error('nama_mahasiswa')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="email">Email</label>
                    <input type="text" name="email" placeholder="email" class="form-control
                    @error('email')   is-invalid @enderror" value="{{ old('email', $data->email ) }}" readonly>
                    @error('email')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="prodi">Prodi</label>
                    <input type="text" name="nama_prodi" placeholder="Prodi" class="form-control
                    @error('nama_prodi')   is-invalid @enderror" value="{{ old('id_prodi', $data->nama_prodi ) }}" readonly>
                    @error('nama_prodi')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="telp">Tempat Lahir</label>
                    <input type="text" name="tempat_lahir" placeholder="Tempat Lahir" class="form-control
                    @error('tempat_lahir')   is-invalid @enderror" value="{{ old('tempat_lahir', $data->tempat_lahir ) }}" >
                    @error('tempat_lahir')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="telp">Tanggal Lahir</label>
                    <input type="date" name="tanggal_lahir" placeholder="Tanggal Lahir" class="form-control
                    @error('tanggal_lahir')   is-invalid @enderror" value="{{ old('tanggal_lahir', $data->tanggal_lahir ) }}" >
                    @error('tanggal_lahir')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>

                  <div class="mb-3">
                    <textarea name="alamat" id="alamat" cols="10" rows="5" class=" form-control @error('alamat') is-invalid @enderror">{{ old('alamat',$data->alamat) }}</textarea>
                    @error('alamat')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="telp">Nomor Telepon</label>
                    <input type="number" name="no_hp" placeholder="Nomor Telepon" class="form-control
                    @error('no_hp')   is-invalid @enderror" value="{{ old('no_hp', $data->no_hp ) }}" >
                    @error('no_hp')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>

                  <button type="submit" class="btn btn-primary">Simpan</button>
                 <a href="{{ route('DetailPenggunaMhs') }}" class="btn btn-secondary">Kembali</a>
              </form>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
