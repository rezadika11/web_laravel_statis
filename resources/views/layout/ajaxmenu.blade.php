@switch(App\DetilUser::AmbilDetil()->level)
@case('adm')
<script>
function LoadStatusSurat(){
  $(function() {
          $.getJSON(
          "{{route('adm.JumlahBelumProsesSuratIjinObservasiPendahuluan')}}",
          function(json){
            if(json>0){
              $("#admIjinObservasiPendahuluan").addClass('badge-danger')
              $("#admIjinObservasiPendahuluan").text(json)
            }
          });
          $.getJSON(
          "{{route('adm.JumlahBelumProsesSuratIjinRisetIndividu')}}",
          function(json){
            if(json>0){
              $("#admIjinRisetIndividu").addClass('badge-danger')
              $("#admIjinRisetIndividu").text(json)
            }
          });
          $.getJSON(
          "{{route('adm.JumlahBelumProsesSuratIjinObservasiKelas')}}",
          function(json){
            if(json>0){
              $("#admIjinObservasiKelas").addClass('badge-danger')
              $("#admIjinObservasiKelas").text(json)
            }
          });
          $.getJSON(
          "{{route('adm.JumlahBelumProsesSuratKeteranganLulusSkripsi')}}",
          function(json){
            if(json>0){
              $("#admKeteranganLulus").addClass('badge-danger')
              $("#admKeteranganLulus").text(json)
            }
          });
          $.getJSON(
          "{{route('adm.JumlahBelumProsesSuratTugas')}}",
          function(json){
            if(json>0){
              $("#admSuratTugas").addClass('badge-danger')
              $("#admSuratTugas").text(json)
            }
          });
          $.getJSON(
          "{{route('adm.JumlahBelumProsesSuratKeteranganMasihKuliah')}}",
          function(json){
            if(json>0){
              $("#admKeteranganMasihKuliah").addClass('badge-danger')
              $("#admKeteranganMasihKuliah").text(json)
            }
          });
          $.getJSON(
          "{{route('adm.JumlahBelumProsesSuratKeteranganPernahKuliah')}}",
          function(json){
            if(json>0){
              $("#admKeteranganPernahKuliah").addClass('badge-danger')
              $("#admKeteranganPernahKuliah").text(json)
            }
          });
          $.getJSON(
          "{{route('adm.JumlahBelumProsesSuratKeteranganPenggantiIjazah')}}",
          function(json){
            if(json>0){
              $("#admKeteranganPenggatiIjazah").addClass('badge-danger')
              $("#admKeteranganPenggatiIjazah").text(json)
            }
          });
          $.getJSON(
          "{{route('adm.JumlahBelumProsesSuraPermohonanPenghargaan')}}",
          function(json){
            if(json>0){
              $("#admPermohonanPenghargaan").addClass('badge-danger')
              $("#admPermohonanPenghargaan").text(json)
            }
          });
      });

}
LoadStatusSurat()
setInterval( LoadStatusSurat, 30000 );

</script>
@break
@case('dosen')
@switch(App\DetilUser::LevelDosen())
@case('wadek1')
<script>
function LoadStatusSurat(){
  $(function() {
          $.getJSON(
          "{{route('wadek1.JumlahBelumProsesKeteranganMasihKuliah')}}",
          function(json){
            if(json>0){
              $("#wadek1KeteranganMasihKuliah").addClass('badge-danger')
              $("#wadek1KeteranganMasihKuliah").text(json)
            }
          });
          $.getJSON(
          "{{route('wadek1.JumlahBelumProsesKeteranganPernahKuliah')}}",
          function(json){
            if(json>0){
              $("#wadek1KeteranganPernahKuliah").addClass('badge-danger')
              $("#wadek1KeteranganPernahKuliah").text(json)
            }
          });
      });

}
LoadStatusSurat()
setInterval( LoadStatusSurat, 30000 );

</script>
@break
@case('dekan')
<script>
function LoadStatusSurat(){
  $(function() {
          $.getJSON(
          "{{route('dekan.JumlahBelumProsesKeteranganPenggantiIJazah')}}",
          function(json){
            if(json>0){
              $("#dekanKeteranganPenggantiIjazah").addClass('badge-danger')
              $("#dekanKeteranganPenggantiIjazah").text(json)
            }
          });
      });

}
LoadStatusSurat()
setInterval( LoadStatusSurat, 30000 );

</script>
@case('wadek3')
<script>
function LoadStatusSurat(){
  $(function() {
          $.getJSON(
          "{{route('wadek3.JumlahBelumProsesSuratTugas')}}",
          function(json){
            if(json>0){
              $("#wadek3SuratTugas").addClass('badge-danger')
              $("#wadek3SuratTugas").text(json)
            }
          });
          $.getJSON(
          "{{route('wadek3.JumlahBelumProsesPermohonanPenghargaan')}}",
          function(json){
            if(json>0){
              $("#wadek3SuratPermohonanPenghargaan").addClass('badge-danger')
              $("#wadek3SuratPermohonanPenghargaan").text(json)
            }
          });
      });

}
LoadStatusSurat()
setInterval( LoadStatusSurat, 30000 );

</script>
@break
@case('kajur')
<script>
function LoadStatusSurat(){
  $(function() {
        $.getJSON(
        "{{route('kajur.JumlahSuratBelumProsesIjinObservasiPendahuluan')}}",
        function(json){
          if(json>0){
            $("#kajurIjinObservasiPendahuluan").addClass('badge-danger')
            $("#kajurIjinObservasiPendahuluan").text(json)
          }
        });
        $.getJSON(
        "{{route('kajur.JumlahBelumProsesIjinObservasiKelas')}}",
        function(json){
          if(json>0){
            $("#kajurIjinObservasiKelas").addClass('badge-danger')
            $("#kajurIjinObservasiKelas").text(json)
          }
        });
        $.getJSON(
        "{{route('kajur.JumlahBelumProsesSuratIjinRisetIndividu')}}",
        function(json){
          if(json>0){
            $("#kajurIjinRisetIndividu").addClass('badge-danger')
            $("#kajurIjinRisetIndividu").text(json)
          }
        });
        $.getJSON(
        "{{route('kajur.JumlahBelumProsesKeteranganLulusSkripsi')}}",
        function(json){
          if(json>0){
            $("#kajurKeteranganLulus").addClass('badge-danger')
            $("#kajurKeteranganLulus").text(json)
          }
        });
        $.getJSON(
        "{{route('kajur.JumlahBelumProsesKeteranganMasihKuliah')}}",
        function(json){
          if(json>0){
            $("#kajurKeteranganMasihKuliah").addClass('badge-danger')
            $("#kajurKeteranganMasihKuliah").text(json)
          }
        });
      });

}
LoadStatusSurat()
setInterval( LoadStatusSurat, 30000 );

</script>
@break
@endswitch
@break
@endswitch
