 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{ asset('img/uin.png') }}" alt="UIN" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">FTIK UIN Saizu</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        {{--<div class="image">
          <img src="{{ asset('Template/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>--}}
        <div class="info">
          <a href="#" class="d-block">{{\App\DetilUser::AmbilNama()['level']}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{route('Dashboard')}}" class="nav-link {{ Request::is('dashboard') ? 'active' : '' }} ">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                    Dashboard
                    <span class="right badge badge-danger"></span>
                  </p>
            </a>
          </li>
          @switch(Auth::user()->level)
          @case('dosen')
            @switch(App\DetilUser::LevelDosen())
            @case('dekan')
            <li class="nav-header">Dekan</li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th "></i>
                <p>
                  Keterangan Pengganti Ijazah
                  <span class="right badge" id="dekanKeteranganPenggantiIjazah"></span>
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('dekan.PermintaanSuratKeteranganPenggantiIjazah')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Permintaan Surat</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{route('dekan.SuratKeteranganPenggantiIjazahSiap')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Sudah Diproses</p>
                  </a>
                </li>
              </ul>
            </li>
            @break
            @case('wadek1')
            <li class="nav-header">- Wakil Dekan 1 -</li>
            <li class="nav-header"><p>Surat Mahasiswa</p></li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th "></i>
                <p>
                  Keterangan Masih Kuliah
                  <span class="right badge" id="wadek1KeteranganMasihKuliah"></span>
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('wadek1.PermintaanSuratKeteranganMasihKuliah')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Permintaan Surat</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{route('wadek1.SuratKeteranganMasihKuliahSiap')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Sudah Ditanda Tangani</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th "></i>
                <p>
                  Keterangan Pernah Kuliah
                  <span class="right badge" id="wadek1KeteranganPernahKuliah"></span>
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('wadek1.PermintaanSuratKeteranganPernahKuliah')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Permintaan Surat</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{route('wadek1.SuratKeteranganPernahKuliahSiap')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Sudah Ditanda Tangani</p>
                  </a>
                </li>
              </ul>
            </li>
            @break
            @case('wadek3')
            <li class="nav-header">Wakil Dekan 3</li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th "></i>
                <p>
                  Surat Tugas
                  <span class="right badge" id="wadek3SuratTugas"></span>
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('wadek3.PermintaanSuratTugas')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Permintaan Surat</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{route('wadek3.SuratTugasSiap')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Sudah Diproses</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th "></i>
                <p>
                  Surat Permohonan Penghargaan
                  <span class="right badge" id="wadek3SuratPermohonanPenghargaan"></span>
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('wadek3.PermintaanSuratPermohonanPenghargaan')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Permintaan Surat</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{route('wadek3.SuratPermohonanPenghargaanSiap')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Sudah Diproses</p>
                  </a>
                </li>
              </ul>
            </li>
            @break
            @case('kajur')
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th "></i>
                <p>
                  Ijin Observasi Kelas
                  <span class="right badge" id="kajurIjinObservasiKelas"></span>
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('kajur.PermintaanSuratIjinObservasiKelas')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Permintaan Surat</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{route('kajur.SuratIjinObservasiKelasSiap')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Sudah Ditanda Tangani</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th "></i>
                <p>
                  Ijin Observasi Pendahuluan
                  <span class="right badge" id="kajurIjinObservasiPendahuluan"></span>
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('kajur.PermintaanSuratIjinObservasiPendahuluan')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Permintaan Surat</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{route('kajur.SuratIjinObservasiPendahuluanSiap')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Sudah Ditanda Tangani</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th "></i>
                <p>
                  Ijin Riset Individu
                  <span class="right badge" id="kajurIjinRisetIndividu"></span>
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('kajur.PermintaanSuratIjinRisetIndividu')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Permintaan Surat</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{route('kajur.SuratIjinRisetIndividuSiap')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Sudah Ditanda Tangani</p>
                  </a>
                </li>
              </ul>
            </li><li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th "></i>
                <p>
                  Keterangan Lulus
                  <span class="right badge" id="kajurKeteranganLulus"></span>
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('kajur.PermintaanSuratKeteranganLulusSkripsi')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Permintaan Surat</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{route('kajur.SuratKeteranganLulusSkripsiSiap')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Sudah Diproses</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th "></i>
                <p>
                  Keterangan Masih Kuliah
                  <span class="right badge" id="kajurKeteranganMasihKuliah"></span>
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('kajur.PermintaanSuratKeteranganMasihKuliah')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Permintaan Surat</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{route('kajur.SuratKeteranganMasihKuliahSiap')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Sudah Ditanda Tangani</p>
                  </a>
                </li>
              </ul>
            </li>
            @break
            @endswitch
            <li class="nav-header">Dosen</li>
          @break
          @case('mhs')
            <li class="nav-header"> Surat Mahasiswa</li>
            <li class="nav-item">
              <a href="{{route('mhs.DaftarSuratIjinRisetIndividu')}}" class="nav-link {{ Request::is('mhs/surat/risetindividu/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Riset Individu</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('mhs.DaftarSuratIjinObservasiPendahuluan')}}" class="nav-link {{ Request::is('mhs/surat/observasipendahuluan/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Observasi Pendahuluan</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('mhs.DaftarSuratIjinObservasiKelas')}}" class="nav-link {{ Request::is('mhs/surat/observasikelas/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Observasi Kelas</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('mhs.DaftarSuratKeteranganLulusSkripsi')}}" class="nav-link {{ Request::is('mhs/surat/keteranganlulusskripsi') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Keterangan Lulus Skripsi</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('mhs.DaftarSuratTugas')}}" class="nav-link {{ Request::is('mhs/surat/tugas/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Surat Tugas</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('mhs.DaftarSuratKeteranganMasihKuliah')}}" class="nav-link {{ Request::is('mhs/surat/keteranganmasihkuliah/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Keterangan Masih Kuliah</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('mhs.DaftarSuratKeteranganPernahKuliah')}}" class="nav-link {{ Request::is('mhs/surat/keteranganpernahkuliah/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Keterangan Pernah Kuliah</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('mhs.DaftarSuratKeteranganPenggantiIjazah')}}" class="nav-link {{ Request::is('mhs/surat/keteranganpenggantiijazah/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Keterangan Pengganti Ijazah</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('mhs.DaftarSuratPermohonanPenghargaan')}}" class="nav-link {{ Request::is('mhs/surat/permohonanpenghargaan/*') ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Permohonan Penghargaan</p>
              </a>
            </li>
          @break
          @case('adm')
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th "></i>
              <p>
                Ijin Riset Individu
                <span class="right badge" id="admIjinRisetIndividu"></span>
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('adm.PermintaanSuratIjinRisetIndividu')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Permintaan Surat</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('adm.SuratIjinRisetIndividuSudahProses')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sudah Diproses</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th "></i>
              <p>
                Ijin Observasi Pendahuluan
                <span class="right badge" id="admIjinObservasiPendahuluan"></span>
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('adm.PermintaanSuratIjinObservasiPendahuluan')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Permintaan Surat</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('adm.SuratIjinObservasiPendahuluanSudahProses')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sudah Diproses</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th "></i>
              <p>
                Ijin Observasi Kelas
                <span class="right badge" id="admIjinObservasiKelas"></span>
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('adm.PermintaanSuratIjinObservasiKelas')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Permintaan Surat</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('adm.SuratIjinObservasiKelasSudahProses')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sudah Diproses</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th "></i>
              <p>
                Keterangan Lulus
                <span class="right badge" id="admKeteranganLulus"></span>
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('adm.PermintaanSuratKeteranganLulusSkripsi')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Permintaan Surat</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('adm.SuratKeteranganLulusSkripsiSudahProses')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sudah Diproses</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th "></i>
              <p>
                Surat Tugas
                <span class="right badge" id="admSuratTugas"></span>
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('adm.PermintaanSuratTugas')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Permintaan Surat</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('adm.SuratTugasSudahProses')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sudah Diproses</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th "></i>
              <p>
                Keterangan Masih Kuliah
                <span class="right badge" id="admKeteranganMasihKuliah"></span>
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('adm.PermintaanSuratKeteranganMasihKuliah')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Permintaan Surat</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('adm.SuratKeteranganMasihKuliahSudahProses')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sudah Diproses</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th "></i>
              <p>
                Keterangan Pernah Kuliah
                <span class="right badge" id="admKeteranganPernahKuliah"></span>
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('adm.PermintaanSuratKeteranganPernahKuliah')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Permintaan Surat</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('adm.SuratKeteranganPernahKuliahSudahProses')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sudah Diproses</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th "></i>
              <p>
                Keterangan Pengganti Ijazah
                <span class="right badge" id="admKeteranganPenggatiIjazah"></span>
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('adm.PermintaanSuratKeteranganPenggantiIjazah')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Permintaan Surat</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('adm.SuratKeteranganPenggangtiIjazahSudahProses')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sudah Diproses</p>
                </a>
              </li>
            </ul>
          </li><li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th "></i>
              <p>
                Permohonan Penghargaan
                <span class="right badge" id="admPermohonanPenghargaan"></span>
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('adm.PermintaanSuratPermohonanPenghargaan')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Permintaan Surat</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('adm.SuratPermohonanPenghargaanSudahProses')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sudah Diproses</p>
                </a>
              </li>
            </ul>
          </li>
          @break
          @case('admin')
          <li class="nav-item">
            <a href="{{ route('admin.TampilSuratMhs') }}" class="nav-link">
              <i class="nav-icon fas fa-envelope"></i>
              <p>
                Surat Mahasiswa
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.DekanWadek') }}" class="nav-link">
              <i class="nav-icon fas fa-database"></i>
              <p>
                Dekan & Wakil Dekan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.Jurusan') }}" class="nav-link">
              <i class="nav-icon fas fa-database"></i>
              <p>
                Jurusan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.Prodi') }}" class="nav-link">
              <i class="nav-icon fas fa-database"></i>
              <p>
                Program Studi
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.AdminProdi') }}" class="nav-link">
              <i class="nav-icon fas fa-database"></i>
              <p>
                Admin Program Studi
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-cog"></i>
              <p>
                Pengguna
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display: none;">
              <li class="nav-item">
                <a href="{{ route('admin.DetailAdm') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Administrasi</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.DetailDosen') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dosen</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.DetailMhs') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Mahasiswa</p>
                </a>
              </li>
            </ul>
          </li>

          @break
          @endswitch
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
