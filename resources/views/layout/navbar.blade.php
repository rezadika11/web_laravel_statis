  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
          {{\App\DetilUser::AmbilNama()['nama']}}
        </a>
        <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
         @switch(Auth::user()->level)
             @case('dosen')
              @switch(App\DetilUser::LevelDosen())
                @case('dekan')
                <li><a class="dropdown-item" href="{{ route('DetailPenggunaDekan') }}"> Pengaturan</a></li>
                <li><a class="dropdown-item" href="{{ route('UbahPasswordPenggunaDekan') }}"> Ubah Password</a></li>
                @break
                @case('wadek1')
                <li><a class="dropdown-item" href="{{ route('DetailPenggunaWadek1') }}" >Pengaturan</a></li>
                <li><a class="dropdown-item" href="{{ route('UbahPasswordPenggunaWadek1') }}"> Ubah Password</a></li>
                @break
                @case('wadek2')
                <li><a class="dropdown-item" href="{{ route('DetailPenggunaWadek2') }}" >Pengaturan</a></li>
                <li><a class="dropdown-item" href="{{ route('UbahPasswordPenggunaWadek2') }}"> Ubah Password</a></li>
                @break
                @case('wadek3')
                <li><a class="dropdown-item" href="{{ route('DetailPenggunaWadek3') }}" >Pengaturan</a></li>
                <li><a class="dropdown-item" href="{{ route('UbahPasswordPenggunaWadek3') }}"> Ubah Password</a></li>
                @break
                @case('dosen')
                <li><a class="dropdown-item" href="{{ route('DetailPenggunaDosen') }}"> Pengaturan </a></li>
                <li><a class="dropdown-item" href="{{ route('UbahPasswordPenggunaDosen') }}"> Ubah Password</a></li>
                @break
                @case('kaprodi')
                <li><a class="dropdown-item" href="{{ route('DetailPenggunaKaprodi') }}" > Pengaturan </a></li>
                <li><a class="dropdown-item" href="{{ route('UbahPasswordPenggunaKaprodi') }}" >Ubah Password</a></li>
                @break
                @default
                <li><a class="dropdown-item" href="{{ route('DetailPenggunaDosen') }}" > Pengaturan </a></li>
                <li><a class="dropdown-item" href="{{ route('UbahPasswordPenggunaDosen') }}" >Ubah Password</a></li>
                @break
              @endswitch
             @break
             @case('mhs')
              <li><a class="dropdown-item" href="{{ route('DetailPenggunaMhs') }}" >Pengaturan </a></li>
              <li><a class="dropdown-item" href="{{ route('UbahPasswordPenggunaMhs') }}" >Ubah Password</a></li>
             @break
             @case('adm')
             <li><a class="dropdown-item" href="{{ route('DetailPenggunaAdm') }}" >Pengaturan </a></li>
             <li><a class="dropdown-item" href="{{ route('UbahPassworPenggunadAdm') }}" >Ubah Password</a></li>
             @break
             @case('admin')
             <li><a class="dropdown-item" href="{{ route('admin.TampilAdmin') }}" >Pengaturan</a></li>
             <li><a class="dropdown-item" href="{{ route('admin.UbahPassword') }}" >Ubah Password</a></li>
             @break
         @endswitch


          <li><hr class="dropdown-divider"></li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                Logout

            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>
        </ul>
      </li>
    </ul>
  </nav>

  <!-- /.navbar -->
