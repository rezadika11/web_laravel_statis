@extends('layout.main')
@section('title','Halaman Detail Wadek1')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Detail Wadek1</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Detail Wadek1
          </div>
           <div class="card-body">
               @if (session('data_diedit'))
               <div class="alert alert-success alert-dismissible fade show" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
                 </button>
                 {{ session('data_diedit') }}
               </div>
                   
               @endif
               <table class="table table-striped">
                   <tr>
                       <td width="142">NIDN</td>
                       <td width="4">:</td>
                       <td width="300">{{ $data->nidn }}</td>
                   </tr>
                   <tr>
                        <td width="100">NIP</td>
                        <td width="6">:</td>
                        <td width="">{{ $data->nip }}</td>
                    </tr>
                    <tr>
                        <td width="142">Nama</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->nama_dosen }}</td>
                    </tr>
                    <tr>
                        <td width="142">Email</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->email }}</td>
                    </tr>
                    <tr>
                        <td width="142">Prodi</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->nama_prodi }}</td>
                    </tr>
                    <tr>
                        <td width="142">No Hp</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->no_hp }}</td>
                    </tr>
                   
               </table>

               <div class="mt-3">
                <a href="{{ route('EditPenggunaWadek1') }}" class="btn btn-primary">Edit</a>
               </div>
              
           </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
