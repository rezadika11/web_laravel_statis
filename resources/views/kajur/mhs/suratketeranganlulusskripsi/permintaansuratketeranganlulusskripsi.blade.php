@extends('layout.main')
@section('title','Permintaan Surat Keterangan Lulus Skripsi')
@section('page','Permintaan Surat Keterangan Lulus Skripsi')

@section('css')

  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
          <h1 class="m-0">Permintaan Surat Keterangan Lulus Skripsi</h1>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">

          <!-- jquery validation -->
          <div class="card card-primary">
            <div class="card-header">
              Daftar Permintaan Surat Keterangan Lulus Skripsi
            </div>
            <div class="card-body">

              @if(Session::has('simpan_gagal'))
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('simpan_gagal') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
              @if(Session::has('simpan_sukses'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('simpan_sukses') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
              <table id="permintaansurat" class="table table-bordered table-striped" style="width:100%;">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>
                      No. Surat
                    </th>
                    <th>
                      Tanggal Surat
                    </th>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Program Studi</th>
                    <th>Tahun Akademik</th>
                    <th>
                      Tanggal Ujian
                    </th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($PermohonanSurat as $Surat)
                  <tr>
                    <td>
                      {{$no++}}
                    </td>
                    <td>
                      {{$Surat->no_surat!=NULL ? $Surat->no_surat : '-'}}
                    </td>
                    <td>
                      {{$Surat->tanggal_surat!=NULL ? date('d-m-Y',strtotime($Surat->tanggal_surat)) : '-'}}
                    </td>
                    <td>
                      {{$Surat->nim}}
                    </td>
                    <td>
                      {{$Surat->nama_mahasiswa}}
                    </td>
                    <td>
                      {{$Surat->nama_prodi}}
                    </td>
                    <td>
                      {{$Surat->tahun_akademik}}
                    </td>
                    <td>
                      {{date('d-m-Y',strtotime($Surat->tanggal_ujian))}}
                    </td>
                    <td>
                      @if($Surat->status_persetujuan_adm == '0' or $Surat->no_surat==NULL or $Surat->tanggal_surat == NULL)
                        <button title="Menunggu Validasi dan Nomor Surat" class="btn btn-warning btn-sm"><i class="fas fa-sync"></i></button>
                      @elseif($Surat->status_persetujuan_adm == '1' and $Surat->no_surat=!NULL and $Surat->tanggal_surat =! NULL)
                        <a href="{{route('kajur.ProsesPermintaanSuratKeteranganLulusSkripsi',['id_surat'=>$Surat->id_surat])}}" class="btn btn-xs btn-primary">Proses</a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="mt-3">
                {{ $PermohonanSurat->links() }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
<!-- DataTables  & Plugins -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script>
  $(function () {
    $('#permintaansurat').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": true,
      "responsive": true,
      "scrollX": true,
    });
  });
</script>
@endsection
