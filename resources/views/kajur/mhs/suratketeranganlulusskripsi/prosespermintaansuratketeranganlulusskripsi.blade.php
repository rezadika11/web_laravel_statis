@extends('layout.main')
@section('title','Proses Surat Keterangan Lulus Skripsi')
@section('page','Proses Surat Keterangan Lulus Skripsi')

@section('css')
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Keterangan Lulus Skripsi</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        @if(Session::has('simpan_gagal'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          {{ session('simpan_gagal') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          </button>
        </div>
        @endif
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Surat Keterangan Lulus Skripsi
            </div>
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('kajur.SimpanProsesPermintaanSuratKeteranganLulusSkripsi',['id_surat'=>$Surat->id_surat])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                      <label for="nim">Nama</label>
                      <p>
                        {{$Surat->nama_mahasiswa}}
                      </p>
                      <label for="nim">NIM</label>
                      <p>
                        {{$Surat->nim}}
                      </p>
                      <label for="nim">Program Studi</label>
                      <p>
                        {{$Surat->nama_prodi}}
                      </p>
                      <label for="no_surat">No. Surat</label>
                      <p>
                        {{$Surat->no_surat}}
                      </p>
                      <label for="no_surat">Tanggal Surat</label>
                      <p>
                        {{date('d-m-Y',strtotime($Surat->tanggal_surat))}}
                      </p>
                  </div>
                  <div class="col-md-6">
                      <label for="nim">Tahun Akademik</label>
                      <p>
                        {{$Surat->tahun_akademik}}
                      </p>
                      <label for="tgl">Tanggal Ujian</label>
                      <p>
                        {{date('d-m-Y',strtotime($Surat->tanggal_ujian))}}
                      </p>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary pull-right">Tanda Tangani</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
@endsection
