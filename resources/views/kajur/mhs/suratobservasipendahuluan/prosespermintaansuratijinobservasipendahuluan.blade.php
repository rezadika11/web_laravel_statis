@extends('layout.main')
@section('title','Proses Surat Ijin Observasi Pendahuluan')
@section('page','Proses Surat Ijin Observasi Pendahuluan')

@section('css')
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Ijin Observasi Pendahuluan</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Surat Ijin Observasi Pendahuluan
            </div>
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('kajur.SimpanProsesPermintaanSuratIjinObservasiPendahuluan',['id_surat'=>$Surat->id_surat])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                      <label for="nim">Nama</label>
                      <p>
                        {{$Surat->nama_mahasiswa}}
                      </p>
                      <label for="nim">NIM</label>
                      <p>
                        {{$Surat->nim}}
                      </p>
                      <label for="nim">Program Studi</label>
                      <p>
                        {{$Surat->nama_prodi}}
                      </p>
                      <label for="semester">Semester</label>
                      <p>
                        {{$semester}}
                      </p>
                      <label for="semester">Tahun Akademik</label>
                      <p>
                        {{$Surat->tahun_akademik}}
                      </p>
                      <label for="no_surat">No. Surat</label>
                      <p>
                        {{$Surat->no_surat}}
                      </p>
                      <label for="no_surat">Tanggal Surat</label>
                      <p>
                        {{date('d-m-Y',strtotime($Surat->tanggal_surat))}}
                      </p>
                  </div>
                  <div class="col-md-6">
                      <label for="nim">Kepada</label>
                      <p>
                        {{$Surat->kepada}}
                      </p>
                      <label for="Obyek">Obyek</label>
                      <p>
                        {{$Surat->obyek}}
                      </p>
                      <label for="tempat">Lokasi</label>
                      <p>
                        {{$Surat->lokasi}}
                      </p>
                      <label for="tgl">Tanggal Observasi</label>
                      <p>
                        {{date('d-m-Y',strtotime($Surat->tanggal_observasi))}}
                      </p>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary pull-right">Tanda Tangani</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
@endsection
