@extends('layout.main')
@section('title','Proses Surat Ijin Observasi Kelas')
@section('page','Proses Surat Ijin Observasi Kelas')

@section('css')
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Semua Surat Ijin Observasi Kelas</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Semua Surat Ijin Observasi Kelas
            </div>
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('kajur.SimpanProsesSemuaPermintaanSuratIjinObservasiKelas')}}">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>
                              No.
                            </th>
                            <th>
                              NIM
                            </th>
                            <th>
                              Nama
                            </th>
                            <th>
                              Kepada
                            </th>
                            <th>
                              No. Surat
                            </th>
                            <th>
                              Tanggal Surat
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($Surat as $srt)
                          <input type="hidden" name="id_surat[]" value="{{$srt->id_surat}}" />
                          <tr>
                            <td>
                              {{$no++}}.
                            </td>
                            <td>
                              {{$srt->nim}}
                            </td>
                            <td>
                              {{$srt->nama_mahasiswa}}
                            </td>
                            <td>
                              {{$srt->kepada}}
                            </td>
                            <td>
                              {{$srt->no_surat}}
                            </td>
                            <td>
                              {{date('d-m-Y',strtotime($srt->tanggal_surat))}}
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary pull-right">Tanda Tangani</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
@endsection
