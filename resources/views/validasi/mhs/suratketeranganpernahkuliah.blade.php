
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Validasi Surat</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
</head>
<body class="bg-light">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10">
        <div class="card mt-5">
          <div class="card-header text-center">
            <h3>Validasi Surat Keterangan Pernah Kuliah</h3>
          </div>
          <div class="card-body register-card-body">
            @if($Validasi==NULL)
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              Surat Tidak Ditemukan. Silahkan Konfirmasi kepada <br />
              Fakultas Tarbiyah dan Ilmu Keguruan<br />
              UNIVERSITAS ISLAM NEGERI PROFESOR KIAI HAJI SAIFUDDIN ZUHRI PURWOKERTO<br />
              Terima Kasih.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              </button>
            </div>
            @else
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              Surat Tervalidasi.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              </button>
            </div>
              <table class="table">
                <tr>
                  <td>
                    No Surat
                  </td>
                  <td>
                    {{$Validasi->no_surat}}
                  </td>
                </tr>
                <tr>
                  <td>
                    Tanggal Surat
                  </td>
                  <td>
                    {{date('d-m-Y',strtotime($Validasi->tanggal_surat))}}
                  </td>
                </tr>
                <tr>
                <tr>
                  <td>
                    Nama Mahasiswa
                  </td>
                  <td>
                    {{$Validasi->nama_mahasiswa}}
                  </td>
                </tr>
                <tr>
                  <td>
                    NIM
                  </td>
                  <td>
                    {{$Validasi->nim}}
                  </td>
                </tr>
                <tr>
                  <td>
                    Program Studi
                  </td>
                  <td>
                    {{$Validasi->nama_prodi}}
                  </td>
                </tr>
                <tr>
                  <td>
                    Program Studi Tujuan
                  </td>
                  <td>
                    {{$Validasi->prodi_tujuan}}
                  </td>
                </tr>
                <tr>
                  <td>
                    Perguruan Tinggi Tujuan
                  </td>
                  <td>
                    {{$Validasi->pt_tujuan}}
                  </td>
                </tr>
                <tr>
                  <td>
                    Alamat Perguruan Tinggi Tujuan
                  </td>
                  <td>
                    {{$Validasi->alamat_pt_tujuan}}
                  </td>
                </tr>
                <tr>
                  <td>
                    Penanda Tangan
                  </td>
                  <td>
                    Atas Nama Dekan <br />
                    Wakil Dekan I
                  </td>
                </tr>
              </table>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</body>


<!-- /.register-box -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
</body>
</html>
