
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Validasi Surat</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
</head>
<body class="bg-light">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10">
        <div class="card mt-5">
          <div class="card-header text-center">
            <h3>Validasi Surat Tugas</h3>
          </div>
          <div class="card-body register-card-body">
            @if($Validasi==NULL)
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              Surat Tidak Ditemukan. Silahkan Konfirmasi kepada <br />
              Fakultas Tarbiyah dan Ilmu Keguruan<br />
              UNIVERSITAS ISLAM NEGERI PROFESOR KIAI HAJI SAIFUDDIN ZUHRI PURWOKERTO<br />
              Terima Kasih.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              </button>
            </div>
            @else
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              Surat Tervalidasi.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              </button>
            </div>
              <table class="table">
                <tr>
                  <td>
                    No Surat
                  </td>
                  <td>
                    {{$Validasi->no_surat}}
                  </td>
                </tr>
                <tr>
                  <td>
                    Kegiatan
                  </td>
                  <td>
                    {{$Validasi->kegiatan}}
                  </td>
                </tr>
                <tr>
                  <td>
                    Penyelenggara
                  </td>
                  <td>
                    {{$Validasi->penyelenggara}}
                  </td>
                </tr>
                <tr>
                  <td>
                    Tanggal Kegiatan
                  </td>
                  <td>
                    {{date('d-m-Y',strtotime($Validasi->tanggal_mulai))}} - {{date('d-m-Y',strtotime($Validasi->tanggal_selesai))}}
                  </td>
                </tr>
                <tr>
                  <td>
                    Penanda Tangan
                  </td>
                  <td>
                    Atas Nama Dekan <br />
                    Wakil Dekan III
                  </td>
                </tr>
              </table>
              <br />
              Peserta
              <br />
              <table class="table table-bordered">
                <thead>
                  <th>
                    No.
                  </th>
                  <th>
                    Nama
                  </th>
                  <th>
                    NIM
                  </th>
                </thead>
                <tbody>
                  @foreach($Peserta as $pst)
                    <tr>
                      <td>
                        {{$no++}}
                      </td>
                      <td>
                        {{$pst->nama}}
                      </td>
                      <td>
                        {{$pst->nim}}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</body>


<!-- /.register-box -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
</body>
</html>
