
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Halaman Lupa Password</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
</head>
<body class="bg-light">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="card mt-5">
          <div class="card-header text-center">
            <h1><b>Reset Password</b></h1>
            <p>
              Silahkan masukan password baru yang anda harapkan. Terima kasih.
            </p>
          </div>
          <div class="card-body register-card-body">
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              </button>
            </div>
            @endif
            <form action="{{route('SimpanResetPassword')}}" method="POST">
              @csrf
              <input type="hidden" name="reset_token" value="{{$token}}"/>
              <input type="hidden" name="id_user" value="{{$id_user}}" />
              <div class="input-group mb-3">
                <input type="password" name="password" class="form-control {{$errors->has('password') ? 'is-invalid' : '' }}" placeholder="Password">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                  </div>
                </div>
                @error('password')
                <span class="error invalid-feedback">
                    {{$errors->first('password')}}
                </span>
                @enderror
              </div>
              <div class="input-group mb-3">
                <input type="password" name="ulangi_password" class="form-control {{$errors->has('ulangi_password') ? 'is-invalid' : '' }}" placeholder="Ulangi Password">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                  </div>
                </div>
                @error('ulangi_password')
                <span class="error invalid-feedback">
                    {{$errors->first('ulangi_password')}}
                </span>
                @enderror
              </div>
              <div class="row mb-3">
                <div class="col-12">
                  <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                </div>
                <!-- /.col -->
              </div>
            </form>
          </div>
          <!-- /.form-box -->
        </div><!-- /.card -->

      </div>


    </div>

  </div>



</body>


<!-- /.register-box -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
</body>
</html>
