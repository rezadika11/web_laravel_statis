
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Halaman Register</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
</head>
<body class="bg-light">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="card mt-5">
          <div class="card-header text-center">
            <h1><b>Register</b></h1>
          </div>
          <div class="card-body register-card-body">
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              </button>
            </div>
            @endif
            <form action="{{route('SimpanRegister')}}" method="POST">
              @csrf
              <div class="row">
                <div class="col-sm-4">
                  <!-- text input -->
                  <div class="form-group">
                    <input type="text" class="form-control {{$errors->has('nim') ? 'is-invalid' : '' }}" placeholder="NIM" name="nim" value="{{old('nim')}}"autofocus >
                    @error('nim')
                    <span class="error invalid-feedback">
                        {{$errors->first('nim')}}
                    </span>
                    @enderror
                  </div>
                </div>
                <div class="col-sm-8">
                  <div class="form-group">
                    <input type="text" class="form-control {{$errors->has('nama_mahasiswa') ? 'is-invalid' : '' }}" name="nama_mahasiswa" placeholder="Nama Lengkap" value="{{old('nama_mahasiswa')}}">
                    @error('nama_mahasiswa')
                    <span class="error invalid-feedback">
                        {{$errors->first('nama_mahasiswa')}}
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4">
                  <label>Tanggal Lahir</label>
                </div>
                <div class="col-sm-8">
                  <div class="form-group {{$errors->has('tanggal_lahir') ? 'is-invalid' : '' }}">
                    <input type="date" class="form-control" name="tanggal_lahir" rows="3" placeholder="Tanggal Lahir" value="{{old('tanggal_lahir')}}"/>
                    @error('tanggal_lahir')
                    <span class="error invalid-feedback">
                        {{$errors->first('tanggal_lahir')}}
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="form-group {{$errors->has('tempat_lahir') ? 'is-invalid' : '' }}">
                <input type="text" class="form-control" name="tempat_lahir" rows="3" placeholder="Tempat Lahir" value="{{old('tempat_lahir')}}" />
                @error('tempat_lahir')
                <span class="error invalid-feedback">
                    {{$errors->first('tempat_lahir')}}
                </span>
                @enderror
              </div>
              <div class="form-group">
                <select class="form-control {{$errors->has('jenis_kelamin') ? 'is-invalid' : '' }}" name="jenis_kelamin">
                  <option disabled selected>- Jenis Kelamin -</option>
                  @foreach($JenisKelamin as $jk => $jenis)
                    <option value="{{$jk}}" {{old('jenis_kelamin') == $jk ? 'selected' : ''}}>
                      {{$jenis}}
                    </option>
                  @endforeach
                </select>
                @error('jenis_kelamin')
                <span class="error invalid-feedback">
                    {{$errors->first('jenis_kelamin')}}
                </span>
                @enderror
              </div>
              <div class="form-group">
                <select class="form-control {{$errors->has('id_prodi') ? 'is-invalid' : '' }}" name="id_prodi">
                  <option disabled selected>- Jurusan -</option> 
                  @foreach($Jurusan as $jrs)
                    <option value="{{$jrs->id_prodi}}" {{old('id_prodi') == $jrs->id_prodi ? 'selected' : ''}}>
                      {{$jrs->nama_jurusan}}
                    </option>
                  @endforeach
                </select>
                @error('kode_prodi')
                <span class="error invalid-feedback">
                    {{$errors->first('kode_prodi')}}
                </span>
                @enderror
              </div>
              <div class="form-group {{$errors->has('alamat') ? 'is-invalid' : '' }}">
                <textarea class="form-control" name="alamat" rows="3" placeholder="Alamat">{{old('alamat')}}</textarea>
                @error('alamat')
                <span class="error invalid-feedback">
                    {{$errors->first('alamat')}}
                </span>
                @enderror
              </div>
              <div class="input-group mb-3">
                <input type="text" class="form-control {{$errors->has('no_hp') ? 'is-invalid' : '' }}" name="no_hp" placeholder="Nomor HP Aktif"  value="{{old('no_hp')}}" required>
                @error('no_hp')
                <span class="error invalid-feedback">
                    {{$errors->first('no_hp')}}
                </span>
                @enderror
              </div>
              <div class="input-group mb-3">
                <input type="email" class="form-control {{$errors->has('email') ? 'is-invalid' : '' }}" name="email" placeholder="Email" value="{{old('email')}}" required>
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                  </div>
                </div>
                @error('email')
                <span class="error invalid-feedback">
                    {{$errors->first('email')}}
                </span>
                @enderror
              </div>

              <div class="input-group mb-3">
                <input type="password" name="password" class="form-control {{$errors->has('password') ? 'is-invalid' : '' }}" placeholder="Password">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                  </div>
                </div>
                @error('password')
                <span class="error invalid-feedback">
                    {{$errors->first('password')}}
                </span>
                @enderror
              </div>
              <div class="input-group mb-3">
                <input type="password" name="ulangi_password" class="form-control {{$errors->has('ulangi_password') ? 'is-invalid' : '' }}" placeholder="Ulangi Password">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                  </div>
                </div>
                @error('ulangi_password')
                <span class="error invalid-feedback">
                    {{$errors->first('ulangi_password')}}
                </span>
                @enderror
              </div>
              <div class="row mb-3">
                <div class="col-12">
                  <button type="submit" class="btn btn-primary btn-block">Register</button>
                </div>
                <!-- /.col -->
              </div>
            </form>
            Sudah Memiliki Akun?
            <a href="{{route('login')}}" class="text-center">Login</a>
          </div>
          <!-- /.form-box -->
        </div><!-- /.card -->

      </div>


    </div>

  </div>



</body>


<!-- /.register-box -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
</body>
</html>
