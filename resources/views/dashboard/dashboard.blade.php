@extends('layout.main')
@section('title','Halaman Dashboard')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Selamat Datang
          </div>
          <div class="card-body">
            <img src="{{ asset('img/uin.png') }}" alt="UIN" class="brand-image img-circle elevation-3" style="opacity: .8; width:100px; margin-left:auto; margin-right:auto; display: block;">
            <center>
              Sistem informasi Manajemen Administrasi  <br />
              Fakultas Tarbiyah dan Ilmu Keguruan <br />
              UNIVERSITAS ISLAM NEGERI PROFESOR KIAI HAJI SAIFUDDIN ZUHRI PURWOKERTO
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
