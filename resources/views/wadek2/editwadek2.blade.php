@extends('layout.main')
@section('title','Halaman Edit Wadek2')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Ubah Wadek2</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Form Ubah Wadek2
          </div>
          <div class="card-body">
              <form action="{{ route('UpdatePenggunaWadek2') }}" method="POST">
                {{ csrf_field() }}
                <div class="mb-3 ">
                    <label for="nip">NIDN</label>
                    <input type="text" class="form-control @error('nidn')  
                    is-invalid @enderror" name="nidn"  placeholder="NIDN" value="{{old('nidn', $data->nidn)}}" readonly>
                    @error('nidn')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                    
                  </div>
                  <div class="mb-3 ">
                    <label for="nip">NIP</label>
                    <input type="text" class="form-control @error('nip')  
                    is-invalid @enderror" name="nip"  placeholder="NIP" value="{{old('nip',$data->nip)}}" readonly>
                    @error('nip')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                    
                  </div>
                  <div class="mb-3">
                    <label for="nama">Nama Lengkap</label>
                    <input type="text" name="nama_dosen" placeholder="Nama Lengkap" class="form-control
                    @error('nama_dosen')   is-invalid @enderror" value="{{old('nama_dosen',$data->nama_dosen)}}" >
                    @error('nama_dosen')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="email">Email</label>
                    <input type="text" name="email" placeholder="email" class="form-control 
                    @error('email')   is-invalid @enderror" value="{{old('email',$data->email)}}" readonly >
                    @error('email')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="nama prodi">Prodi</label>
                    <input type="text" name="nama_prodi" placeholder="Prodi" class="form-control 
                    @error('nama_prodi')   is-invalid @enderror" value="{{old('nama_prodi',$data->nama_prodi)}}" readonly >
                    @error('nama_prodi')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                 
                  <div class="mb-3">
                    <label for="telp">Nomor Telepon</label>
                    <input type="number" name="no_hp" placeholder="Nomor Telepon" class="form-control 
                    @error('no_hp')   is-invalid @enderror" value="{{old('no_hp',$data->no_hp)}}" >
                    @error('no_hp')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                  <a href="{{ route('DetailPenggunaWadek2') }}" class="btn btn-secondary">Kembali</a>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
