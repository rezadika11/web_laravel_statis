@extends('layout.main')
@section('title','Proses Surat Tugas')
@section('page','Proses Surat Tugas')

@section('css')
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Tugas</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        @if(Session::has('simpan_gagal'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          {{ session('simpan_gagal') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          </button>
        </div>
        @endif
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Surat Tugas
            </div> 
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('wadek3.SimpanProsesPermintaanSuratTugas',['id_surat'=>$Surat->id_surat])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="no_surat">No. Surat</label>
                      <p>
                        {{$Surat->no_surat}}
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="no_surat">Tanggal Surat</label>
                      <p>
                        {{date('d-m-Y',strtotime($Surat->tanggal_surat))}}
                      </p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">Kegiatan</label>
                      <p>
                        {{$Surat->kegiatan}}
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="nim">Penyelenggara</label>
                      <p>
                        {{$Surat->penyelenggara}}
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="tanggal_mulai">Tanggal Kegiatan</label>
                      <p>
                        {{date('d-m-Y',strtotime($Surat->tanggal_mulai))}} - {{date('d-m-Y',strtotime($Surat->tanggal_selesai))}}
                      </p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <label>Peserta :</label>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 2em;">
                            No.
                          </th>
                          <th>
                            NIM
                          </th>
                          <th>
                            Nama
                          </th>
                          <th>
                            Program Studi
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($Peserta as $pst)
                        <tr>
                          <td>
                            {{$no++}}.
                          </td>
                          <td>
                            {{$pst->nim}}
                          </td>
                          <td>
                            {{$pst->nama}}
                          </td>
                          <td>
                            {{$pst->nama_prodi}}
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Tanda Tangani</button>
                <a href="{{route('wadek3.PermintaanSuratTugas')}}" class="btn btn-secondary">Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
@endsection
