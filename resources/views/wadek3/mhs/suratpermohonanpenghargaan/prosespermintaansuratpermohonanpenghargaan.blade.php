@extends('layout.main')
@section('title','Proses Surat Permohonan Penghargaan')
@section('page','Proses Surat Permohonan Penghargaan')

@section('css')
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Permohonan Penghargaan</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        @if(Session::has('simpan_gagal'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          {{ session('simpan_gagal') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          </button>
        </div>
        @endif
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Surat Permohonan Penghargaan
            </div>
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('wadek3.SimpanProsesPermintaanSuratPermohonanPenghargaan',['id_surat'=>$Surat->id_surat])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">Nama</label>
                      <p>
                        {{$Surat->nama_mahasiswa}}
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="nim">NIM</label>
                      <p>
                        {{$Surat->nim}}
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="nim">Program Studi</label>
                      <p>
                        {{$Surat->nama_prodi}}
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="alamat">Alamat</label>
                      <p>
                        {{$Surat->alamat}}
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="nim">No.HP</label>
                      <p>
                        {{$Surat->no_hp}}
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="no_surat">No. Surat</label>
                      <p>
                        {{$Surat->no_surat}}
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="no_surat">Tanggal Surat</label>
                      <p>
                        {{date('d-m-Y',strtotime($Surat->tanggal_surat))}}
                      </p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nama_perlombaan">Nama Perlombaan</label>
                      <p>
                        {{$Surat->nama_perlombaan}}
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="tanggal_mulai_pelaksanaan">Tanggal Mulai Pelaksanaan</label>
                      <p>
                        {{date('d-m-Y',strtotime($Surat->tanggal_mulai_pelaksanaan))}}
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="tanggal_selesai_pelaksanaan">Tanggal Selesai Pelaksanaan</label>
                      <p>
                        {{date('d-m-Y',strtotime($Surat->tanggal_selesai_pelaksanaan))}}
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="tempat">Tempat</label>
                      <p>
                        {{$Surat->tempat}}
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="juara">Juara</label>
                      <p>
                        {{$Surat->juara}} ({{$PilihJuara[$Surat->juara]}})
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="kelompok">Kelompok</label>
                      <p>
                        {{$Surat->kelompok}}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Tanda Tangani</button>
                <a href="{{route('wadek3.PermintaanSuratPermohonanPenghargaan')}}" class="btn btn-secondary">Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
@endsection
