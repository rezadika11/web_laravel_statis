@extends('layout.main')
@section('title','Jurusan')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Ubah Jabatan {{$DataJabatan[$jabatan]}}</h1>
          @if(Session::has('simpan_gagal'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('simpan_gagal') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-10">
        <form action="{{ route('admin.SimpanUbahDekanWadek',['jabatan'=>$jabatan]) }}" method="POST">
            @csrf
            <input type="hidden" name="jabatan" value="{{$jabatan}}" />
          <div class="card card-primary">
            <div class="card-body">
              <div class="form-group">
                <label for="id_dosen">Dosen</label>
                <select class="form-control select2 {{$errors->has('id_dosen') ? 'is-invalid' : '' }}" name="id_dosen">
                  <option disabled selected>
                    Pilih {{$DataJabatan[$jabatan]}}
                  </option>
                  @foreach($DataDosen as $Dosen)
                    <option value="{{$Dosen->id_dosen}}" {{old('id_dosen',isset($DataDekanWadek->id_dosen)?$DataDekanWadek->id_dosen:'')==$Dosen->id_dosen ? 'selected' : ''}}>
                      {{$Dosen->nama_dosen}}
                    </option>
                  @endforeach
                </select>
                @error('id_dosen')
                <span class="error invalid-feedback">
                    {{$errors->first('id_dosen')}}
                </span>
                @enderror
              </div>
              <div class="form-group">
                <label for="mulai_menjabat">Mulai Menjabat</label>
                <input type="date" name="mulai_menjabat" class="form-control {{$errors->has('mulai_menjabat') ? 'is-invalid' : '' }}" id="mulai_menjabat" value="{{old('mulai_menjabat',isset($DataDekanWadek->mulai_menjabat)?$DataDekanWadek->mulai_menjabat:'')}}">
                @error('mulai_menjabat')
                <span class="error invalid-feedback">
                    {{$errors->first('mulai_menjabat')}}
                </span>
                @enderror
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              <a href="{{ route('admin.DekanWadek')  }}" class="btn btn-secondary">Kembali</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
