@extends('layout.main')
@section('title','Jurusan')
@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-10">
          <h1 class="m-0">Tambah Admin Program Studi</h1>
          @if(Session::has('simpan_gagal'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('simpan_gagal') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-10">
        <form action="{{ route('admin.SimpanTambahAdminProdi') }}" method="POST">
            @csrf
          <div class="card card-primary">
            <div class="card-body">
              <div class="form-group">
                <label for="id_prodi">Program Studi</label>
                <select class="form-control select2 {{$errors->has('id_prodi') ? 'is-invalid' : '' }} select2" name="id_prodi">
                  <option disabled selected>
                    Pilih Program Studi
                  </option>
                  @foreach($DataProdi as $Prodi)
                    <option value="{{$Prodi->id_prodi}}" {{old('id_prodi')==$Prodi->id_prodi ? 'selected' : ''}}>
                      {{$Prodi->nama_prodi}}
                    </option>
                  @endforeach
                </select>
                @error('id_prodi')
                <span class="error invalid-feedback">
                    {{$errors->first('id_prodi')}}
                </span>
                @enderror
              </div>
              <div class="form-group">
                <label for="id_pegawai">Pegawai</label>
                <select class="form-control select2 {{$errors->has('id_pegawai') ? 'is-invalid' : '' }} select2" name="id_pegawai">
                  <option disabled selected>
                    Pilih Pegawai
                  </option>
                  @foreach($DataPegawai as $Pegawai)
                    <option value="{{$Pegawai->id_pegawai}}" {{old('id_pegawai')==$Pegawai->id_pegawai ? 'selected' : ''}}>
                      {{$Pegawai->nama_pegawai}}
                    </option>
                  @endforeach
                </select>
                @error('id_pegawai')
                <span class="error invalid-feedback">
                    {{$errors->first('id_pegawai')}}
                </span>
                @enderror
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              <a href="{{ route('admin.AdminProdi')  }}" class="btn btn-secondary">Kembali</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
@section('javascript')
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script>
  $(function () {
    $('.select2').select2()
  })
</script>

@endsection
