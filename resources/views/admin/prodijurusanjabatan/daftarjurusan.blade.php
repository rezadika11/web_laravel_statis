@extends('layout.main')
@section('title','Jurusan')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Data Jurusan</h1>
          @if(Session::has('simpan_sukses'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('simpan_sukses') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-10">
        <div class="card card-primary">
          <div class="card-body">
             <table class="table table-bordered table-striped">
              <thead>
                <tr>
                <th>No.</th>
                <th>Jurusan</th>
                <th>
                  Ketua Jurusan
                </th>
                <th>
                  Mulai Menjabat
                </th>
                <th>
                  Aksi
                </th>
                </tr>
              </thead>

              @foreach ($DataJurusan as $data)
              <tr>
                <td>{{ $no++ }}.</td>
                <td>{{ $data->nama_jurusan }}</td>
                <td>
                  {{$data->nama_dosen}}
                </td>
                <td>
                  {{isset($data->mulai_menjabat) ? date('d-m-Y',strtotime($data->mulai_menjabat)) : '-' }}
                </td>
                <td>
                  <a href="{{ route('admin.UbahJurusan',$data->id_jurusan) }}" class="btn btn-info" title="Ubah Data Jurusan"><i class="fas fa-edit"></i></a>
                </td>
              </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
