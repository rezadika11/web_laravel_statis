@extends('layout.main')
@section('title','Jurusan')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-10">
          <h1 class="m-0">Ubah Jurusan Program Studi {{$Prodi->nama_prodi}}</h1>
          @if(Session::has('simpan_gagal'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('simpan_gagal') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-10">
        <form action="{{ route('admin.SimpanUbahProdi',['id_prodi'=>$Prodi->id_prodi]) }}" method="POST">
            @csrf
            <input type="hidden" name="id_prodi" value="{{$Prodi->id_prodi}}" />
          <div class="card card-primary">
            <div class="card-body">
              <div class="form-group">
                <label for="id_dosen_kajur">Jurusan</label>
                <select class="form-control select2 {{$errors->has('id_jurusan') ? 'is-invalid' : '' }}" name="id_jurusan">
                  <option disabled selected>
                    Pilih Jurusan
                  </option>
                  @foreach($DataJurusan as $Jurusan)
                    <option value="{{$Jurusan->id_jurusan}}" {{old('id_jurusan',$Prodi->id_jurusan)==$Jurusan->id_jurusan ? 'selected' : ''}}>
                      {{$Jurusan->nama_jurusan}}
                    </option>
                  @endforeach
                </select>
                @error('id_jurusan')
                <span class="error invalid-feedback">
                    {{$errors->first('id_jurusan')}}
                </span>
                @enderror
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              <a href="{{ route('admin.Prodi')  }}" class="btn btn-secondary">Kembali</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
