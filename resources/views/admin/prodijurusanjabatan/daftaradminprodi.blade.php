@extends('layout.main')
@section('title','Jurusan')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Data Administrasi Program Studi</h1>
          @if(Session::has('simpan_sukses'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('simpan_sukses') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-10">
        <div class="card card-default">
          <div class="card-header">
            <a href="{{ route('admin.TambahAdminProdi') }}" class="btn btn-success mb-3"><i class="fas fa-plus-circle"></i> Tambah Admin Prodi</a>
          </div>
          <div class="card-body">
             <table class="table table-bordered table-striped">
              <thead>
                <tr>
                <th>No.</th>
                <th>Program Studi</th>
                <th>
                  Pegawai
                </th>
                <th>
                  Aksi
                </th>
                </tr>
              </thead>

              @foreach ($DataAdminProdi as $data)
              <tr>
                <td>{{ $no++ }}.</td>
                <td>{{ $data->nama_prodi }}</td>
                <td>
                  {{$data->nama_pegawai}}
                </td>
                <td>
                  {{--<a href="" class="btn btn-info" title="Ubah Data Program Studi"><i class="fas fa-edit"></i></a>--}}
                </td>
              </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
