@extends('layout.main')
@section('title','Jurusan')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Data Dekan dan Wakil Dekan</h1>
          @if(Session::has('simpan_sukses'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('simpan_sukses') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          @if(Session::has('simpan_gagal'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('simpan_gagal') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-10">
        <div class="card card-primary">
          <div class="card-body">
             <table class="table table-bordered table-striped">
              <thead>
                <tr>
                <th>No.</th>
                <th>Jabatan</th>
                <th>
                  Nama Dosen
                </th>
                <th>
                  Mulai Menjabat
                </th>
                <th>
                  Aksi
                </th>
                </tr>
              </thead>

              @foreach ($DataJabatan as $kd=>$jabatan)
              <tr>
                <td>{{ $no++ }}.</td>
                <td>{{ $jabatan }}</td>
                <td>
                  {{isset($DataPejabat[$kd]) ? $DataPejabat[$kd]['nama_dosen'] : '-'}}
                </td>
                <td>
                  {{isset($DataPejabat[$kd]['mulai_menjabat']) ? date('d-m-Y',strtotime($DataPejabat[$kd]['mulai_menjabat'])) : '-' }}
                </td>
                <td>
                  <a href="{{ route('admin.UbahDekanWadek',$kd) }}" class="btn btn-info" title="Ubah ".{{$jabatan}} ><i class="fas fa-edit"></i></a>
                </td>
              </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
