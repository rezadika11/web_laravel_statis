<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>

    $(document).ready(function(){
        // readData();
        $("#cari").on('keyup',function(){
           var strcari = $("#cari").val();
           if(strcari != ""){
               $("#container").html('<center><p class="text-muted">Menunggu Mencari Data..</p></center>');
               $.ajax({
                   type: "get",
                   url: "{{ url('/admin/admcari') }}",
                   data: "name=" + strcari,
                   success: function(data){
                       $('#container').html(data);
                   }
               });
           }
        });
    });
    // function readData(){
    //     $.get("{{ url('admin/readadm') }}",{},function (data,status){
    //             $("#container").html(data);
    //         });
    // }
   
</script>