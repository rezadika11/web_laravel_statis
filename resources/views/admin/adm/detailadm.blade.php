@extends('layout.main')
@section('title','Halaman Administrasi')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Data Pegawai</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12"> 
        <div class="card card-primary">
          <div class="card-header">
            Data Pegawai
          </div>
          <div class="card-body">
            @if (session('data_disimpan'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              {{ session('data_disimpan') }}
            </div>
            @endif
            @if (session('data_diedit'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              {{ session('data_diedit') }}
            </div>
                
            @endif
            <div class="row">
              <div class="col-lg-9">
                <a href="{{ route('admin.TambahAdm') }}" class="btn btn-success mb-3"><i class="fas fa-plus-circle"></i> Tambah Pegawai</a>
              </div>
              <div class="col-lg-3" >
                @include('admin.adm.ajaxadmcari')
                <form class="form" method="GET">
                  @csrf 
                  <div class="form-group">
                      <input type="text" name="cari" class="form-control w-75 d-inline" id="cari"  placeholder="Masukkan Nama">
                      <button type="submit" class="btn btn-primary mb-1"><i class="fas fa-search"></i></button>
                  </div>
              </form>
              </div>
            </div>
            
              <table class="table table-bordered table-striped" id="container">
              <thead>
                <tr>
                <th>No.</th>
                <th>NIP</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Aksi</th>
                </tr>
              </thead>
              @forelse ($data as $adm)
              <tr>

                <td>{{ $loop->iteration }}.</td>
                <td>{{ $adm->nip }}</td>
                <td>{{ $adm->nama_pegawai }}</td>
                <td>{{ $adm->email }}</td>
                <td>
                  <a href="{{ route('admin.EditAdm', $adm->id_user) }}" class="btn btn-info" title="Edit Data"><i class="fas fa-edit"></i></a>
                  <a href="{{ route('admin.TampilAdm', $adm->id_user) }}" class="btn btn-danger" title="Lihat Detail"><i class="fas fa-eye"></i></a>
                  <a href="{{ route('admin.UbahPasswordAdm',$adm->id_user) }}" class="btn btn-warning" title="Ganti Password"><i class="fas fa-key"></i></a>
                 
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="5" class="text-center" id="read">Data Tidak Ada</td>
              </tr>
              @endforelse


            </table>

            <div class="mt-3">
              {{ $data->links() }}
            </div>
           
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
