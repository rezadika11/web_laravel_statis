@extends('layout.main')
@section('title','Halaman Tambah Adm')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Tambah Adm</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Form Tambah Adm
          </div>
          <div class="card-body">
              <form action="{{ route('admin.SimpanAdm') }}" method="POST">
                {{ csrf_field() }}
                  <div class="mb-3 ">
                    <label for="nip">NIP</label>
                    <input type="text" class="form-control @error('nip')  
                    is-invalid @enderror" name="nip"  placeholder="NIP" value="{{old('nip')}}" autofocus>
                    @error('nip')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                    
                  </div>
                  <div class="mb-3">
                    <label for="nama">Nama Lengkap</label>
                    <input type="text" name="nama_pegawai" placeholder="Nama Lengkap" class="form-control
                    @error('nama_admin')   is-invalid @enderror" value="{{old('nama_pegawai')}}" >
                    @error('nama_admin')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="email">Email</label>
                    <input type="email" name="email" placeholder="Email" class="form-control 
                    @error('email')   is-invalid @enderror" value="{{old('email')}}" >
                    @error('email')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="telp">Nomor Telepon</label>
                    <input type="number" name="no_hp" placeholder="Nomor Telepon" class="form-control 
                    @error('no_hp')   is-invalid @enderror" value="{{old('no_hp')}}" >
                    @error('no_hp')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="password">Password</label>
                    <input type="password" name="password" placeholder="Password" class="form-control
                    @error('password')   is-invalid @enderror">
                    @error('password')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="ulangi_password">Ulangi Password</label>
                    <input type="password" name="ulangi_password" placeholder="Ulangi Password" class="form-control
                    @error('ulangi_password')   is-invalid @enderror">
                    @error('ulangi_password')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                 <a href="{{ route('admin.DetailAdm') }}" class="btn btn-secondary">Kembali</a>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
