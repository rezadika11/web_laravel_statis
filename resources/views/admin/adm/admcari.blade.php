<table class="table table-bordered table-striped" id="container">
    <thead>
      <tr>
      <th>No.</th>
      <th>NIP</th>
      <th>Nama</th>
      <th>Email</th>
      <th>Aksi</th>
      </tr>
    </thead>
    @foreach ($data as $item)
    <tr>
        <td>{{ $loop->iteration }}.</td>
        <td>{{ $item->nip }}</td>
        <td>{{ $item->nama_pegawai }}</td>
        <td>{{ $item->email }}</td>
        <td>
          <a href="{{ route('admin.EditAdm', $item->id_user) }}" class="btn btn-info" title="Edit Data"><i class="fas fa-edit"></i></a>
          <a href="{{ route('admin.TampilAdm', $item->id_user) }}" class="btn btn-danger" title="Lihat Detail"><i class="fas fa-eye"></i></a>
          <a href="{{ route('admin.UbahPasswordAdm',$item->id_user) }}" class="btn btn-warning" title="Ganti Password"><i class="fas fa-key"></i></a>
         
        </td>
    @endforeach
    </table>
   
