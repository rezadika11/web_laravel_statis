@extends('layout.main')
@section('title','Jurusan')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Ubah Ketua Jurusan {{$Jurusan->nama_jurusan}}</h1>
          @if(Session::has('simpan_gagal'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('simpan_gagal') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-10">
        <form action="{{ route('admin.SimpanUbahJurusan',['id_jurusan'=>$Jurusan->id_jurusan]) }}" method="POST">
            @csrf
            <input type="hidden" name="id_jurusan" value="{{$Jurusan->id_jurusan}}" />
          <div class="card card-primary">
            <div class="card-body">
              <div class="form-group">
                <label for="id_dosen_kajur">Ketua Jurusan</label>
                <select class="form-control select2 {{$errors->has('id_dosen_kajur') ? 'is-invalid' : '' }}" name="id_dosen_kajur">
                  <option disabled selected>
                    Pilih Ketua Jurusan
                  </option>
                  @foreach($DataDosen as $Dosen)
                    <option value="{{$Dosen->id_dosen}}" {{old('id_dosen_kajur',$Jurusan->id_dosen_kajur)==$Dosen->id_dosen ? 'selected' : ''}}>
                      {{$Dosen->nama_dosen}}
                    </option>
                  @endforeach
                </select>
                @error('id_dosen_kajur')
                <span class="error invalid-feedback">
                    {{$errors->first('id_dosen_kajur')}}
                </span>
                @enderror
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
              <a href="{{ route('admin.Jurusan')  }}" class="btn btn-secondary">Kembali</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
