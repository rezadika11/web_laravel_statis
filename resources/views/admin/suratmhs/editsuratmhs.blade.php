@extends('layout.main')
@section('title','Halaman Edit Surat Mhs')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Edit Surat Mahasiswa</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Form UbahSurat Mahasiswa
          </div>
          <div class="card-body">
                    
              <form action="{{ route('admin.UpdateSuratMhs', $data->id_surat_mahasiswa) }}" method="POST">
                {{ csrf_field() }}
                  <div class="mb-3 ">
                    <label for="jenis surat">Jenis Surat</label>
                    <input type="text" class="form-control @error('jenis_surat')  
                    is-invalid @enderror" name="jenis_surat"  placeholder="Jenis Surat" value="{{ old('jenis_surat', $data->jenis_surat )  }}" readonly>
                    @error('jenis_surat')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                    
                  </div>
                  <div class="mb-3">
                    <label for="no surat awalan">No Surat Awalan</label>
                    <input type="text" name="no_surat_awalan" placeholder="Nama Lengkap" class="form-control
                    @error('no_surat_awalan')   is-invalid @enderror" value={{ old('no_surat_awalan', $data->no_surat_awalan ) }} >
                    @error('no_surat_awalan')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="telp">No Surat Akhiran</label>
                    <input type="text" name="no_surat_akhiran" placeholder="Nomor Telepon" class="form-control 
                    @error('no_surat_akhiran')   is-invalid @enderror" value="{{ old('no_surat_akhiran', $data->no_surat_akhiran ) }}" >
                    @error('no_surat_akhiran')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="nama tabel">Nama Tabel</label>
                    <input type="text" name="nama_tabel" placeholder="Nomor Telepon" class="form-control 
                    @error('nama_tabel')   is-invalid @enderror" value="{{ old('nama_tabel', $data->nama_tabel ) }}" readonly >
                    @error('nama_tabel')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                  <a href="{{ route('admin.TampilSuratMhs') }}" class="btn btn-secondary">Kembali</a>
              </form>
             
              
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection