@extends('layout.main')
@section('title','Halaman Tampil Surat')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Surat Mahasiswa</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Tampil Surat Mahasiswa
          </div>
          <div class="card-body">
            
            @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              {{ session('status') }}
            </div>
                
            @endif
            @if (session('data_diedit'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              {{ session('data_diedit') }}
            </div>
                
            @endif
            <div class="row">
              <div class="col-lg-9">
                {{-- <a href="{{ route('admin.TambahAdmin') }}" class="btn btn-success mb-3"><i class="fas fa-plus-circle"></i> Tambah Admin</a> --}}
              </div>
              <div class="col-lg-3" >
                <form class="form" method="get" action="{{ route('admin.SuratMhsCari') }}">
                  <div class="form-group">
                      <input type="text" name="cari" class="form-control w-75 d-inline" id="cari" placeholder="Masukkan Nama">
                      <button type="submit" class="btn btn-primary mb-1"><i class="fas fa-search"></i></button>
                  </div>
              </form>
              </div>
            </div>
          
            
             <table class="table table-bordered table-striped">
              <thead>
                <tr>
                <th>No.</th>
                <th>Jenis Surat</th>
                <th>No Surat Awalan</th>
                <th>No Surat Akhiran</th>
                <th>Aksi</th>
                </tr>
              </thead>
              @forelse ($data as $adm)
              <tr>
                <td>{{ $loop->iteration }}.</td>
                <td>{{ $adm->jenis_surat }}</td>
                <td>{{ $adm->no_surat_awalan }}</td>
                <td>{{ $adm->no_surat_akhiran }}</td>
                <td>
                  <a href="{{ route('admin.EditSuratMhs',$adm->id_surat_mahasiswa) }}" class="btn btn-info" title="Edit Data"><i class="fas fa-edit"></i></a>
                  <a href="{{ route('admin.DetailSuratMhs',$adm->id_surat_mahasiswa) }}" class="btn btn-danger" title="Lihat Data"><i class="fas fa-eye"></i></a>
               </a>
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="5" class="text-center">Data Tidak Ada</td>
              </tr>
              @endforelse
            </table>
            <div class="mb-3"></div>
            {{ $data->links() }}
          </div>
          
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
