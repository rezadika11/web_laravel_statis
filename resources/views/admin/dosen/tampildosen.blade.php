@extends('layout.main')
@section('title','Halaman Detail Dosen')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Detail Dosen</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Detail Dosen
          </div>
           <div class="card-body">
               <table class="table table-striped">
                   <tr>
                       <td width="142">NIDN</td>
                       <td width="4">:</td>
                       <td width="300">{{ $data->nidn }}</td>
                   </tr>
                   <tr>
                        <td width="100">NIP</td>
                        <td width="6">:</td>
                        <td width="">{{ $data->nip }}</td>
                    </tr>
                    <tr>
                        <td width="142">Nama</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->nama_dosen }}</td>
                    </tr>
                    <tr>
                        <td width="142">Email</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->email }}</td>
                    </tr>
                    <tr>
                        <td width="142">Prodi</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->nama_prodi }}</td>
                    </tr>
                    <tr>
                        <td width="142">No Hp</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->no_hp }}</td>
                    </tr>
               </table>

               <div class="mt-3">
                <a href="{{ route('admin.EditDosen',$data->id_user) }}" class="btn btn-primary">Edit</a>
                <a href="{{ route('admin.DetailDosen') }}" class="btn btn-secondary">Kembali</a>
               </div>

           </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
