@extends('layout.main')
@section('title','Halaman Tambah Dosen')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Tambah Dosen</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Form Tambah Dosen
          </div>
          <div class="card-body">
              <form action="{{ route('admin.SimpanDosen') }}" method="POST">
                {{ csrf_field() }}
                <div class="mb-3 ">
                    <label for="nip">NIDN</label>
                    <input type="text" class="form-control @error('nidn')
                    is-invalid @enderror" name="nidn"  placeholder="NIDN" value="{{old('nidn')}}" autofocus>
                    @error('nidn')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror

                  </div>
                  <div class="mb-3 ">
                    <label for="nip">NIP</label>
                    <input type="text" class="form-control @error('nip')
                    is-invalid @enderror" name="nip"  placeholder="NIP" value="{{old('nip')}}" autofocus>
                    @error('nip')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror

                  </div>
                  <div class="mb-3">
                    <label for="nama">Nama Lengkap</label>
                    <input type="text" name="nama_dosen" placeholder="Nama Lengkap" class="form-control
                    @error('nama_dosen')   is-invalid @enderror" value="{{old('nama_dosen')}}" >
                    @error('nama_dosen')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="nama">Prodi</label>
                    <select class="form-control {{$errors->has('id_prodi') ? 'is-invalid' : '' }}" name="id_prodi">
                      <option disabled selected>- Jurusan -</option>
                      @foreach ($jurusan as $jrs)
                      <option value="{{$jrs->id_prodi}}" {{old('id_prodi') == $jrs->id_prodi ? 'selected' : ''}}>
                        {{$jrs->nama_prodi}}
                      </option>
                      @endforeach
                    </select>
                    @error('nama_prodi')
                    <span class="error invalid-feedback">
                        {{$errors->first('nama_prodi')}}
                    </span>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="email">Email</label>
                    <input type="email" name="email" placeholder="Email" class="form-control
                    @error('email')   is-invalid @enderror" value="{{old('email')}}" >
                    @error('email')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="telp">Nomor Telepon</label>
                    <input type="number" name="no_hp" placeholder="Nomor Telepon" class="form-control
                    @error('no_hp')   is-invalid @enderror" value="{{old('no_hp')}}" >
                    @error('no_hp')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="password">Password</label>
                    <input type="password" name="password" placeholder="Password" class="form-control
                    @error('password')   is-invalid @enderror">
                    @error('password')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="ulangi_password">Ulangi Password</label>
                    <input type="password" name="ulangi_password" placeholder="Ulangi Password" class="form-control
                    @error('ulangi_password')   is-invalid @enderror">
                    @error('ulangi_password')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                 <a href="{{ route('admin.DetailDosen')  }}" class="btn btn-secondary">Kembali</a>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
