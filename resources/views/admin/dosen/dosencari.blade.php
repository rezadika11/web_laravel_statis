<table class="table table-bordered table-striped" id="container">
    <thead>
      <tr>
      <th>No.</th>
      <th>NIDN</th>
      <th>Nama</th>
      <th>Program Studi</th>
      <th>Aksi</th>
      </tr>
    </thead>
    @foreach ($data as $item)
    <tr>
        <td>{{ $loop->iteration }}.</td>
        <td>{{ $item->nidn }}</td>
        <td>{{ $item->nama_dosen }}</td>
        <td>{{ $item->nama_prodi }}</td>
        <td>
            <a href="{{ route('admin.EditDosen',$item->id_user) }}" class="btn btn-info" title="Edit Data"><i class="fas fa-edit"></i></a>
            <a href="{{ route('admin.TampilDosen',$item->id_user) }}" class="btn btn-danger" title="Lihat Detail"><i class="fas fa-eye"></i></a>
            <a href="{{ route('admin.UbahPasswordDosen',$item->id_user) }}" class="btn btn-warning" title="Ganti Password"><i class="fas fa-key"></i></a>

        </td>
    </tr>
    @endforeach
    </table>
