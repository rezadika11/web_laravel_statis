@extends('layout.main')
@section('title','Halaman Dashboard')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Data Mahasiswa</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Data Mahasiswa
          </div>
          <div class="card-body">
            @if (session('data_diedit'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              {{ session('data_diedit') }}
            </div>
                
            @endif
            <div class="row">
              <div class="col-lg-9">
               
              </div>
              <div class="col-lg-3">
                @include('admin.mhs.ajaxmhscari')
                <form class="form" method="get">
                  @csrf
                  <div class="form-group">
                      <input type="text" name="cari" class="form-control w-75 d-inline" id="cari" placeholder="Masukkan Nama">
                      <button type="submit" class="btn btn-primary mb-1"><i class="fas fa-search"></i></button>
                  </div>
              </form>
              </div>
            </div>
          
            <table class="table table-bordered table-striped" id="container">
              <thead>
                <tr>
                <th>No.</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Aksi</th>
                </tr>
              </thead>
              @forelse ($data as $adm)
              <tr>
                <td>{{ $loop->iteration }}.</td>
                <td>{{ $adm->nim }}</td>
                <td>{{ $adm->nama_mahasiswa }}</td>
                <td>{{ $adm->email }}</td>
                <td>
                  <a href="{{ route('admin.EditMhs', $adm->id_user) }}" title="Edit" class="btn btn-info"><i class="fas fa-edit"></i></a>
                  <a href="{{ route('admin.TampilMhs',$adm->id_user) }}" title="Lihat Detail" class="btn btn-danger"><i class="fas fa-eye"></i></a>
                  <a href="{{ route('admin.UbahPasswordMhs',$adm->id_user) }}" title="Ganti Password" class="btn btn-warning"><i class="fas fa-key"></i></a>
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="5" class="text-center">Data Tidak Ada</td>
              </tr>
              @endforelse


            </table>
            <div class="mt-3">
              {{ $data->links() }}
            </div>
           
           
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
