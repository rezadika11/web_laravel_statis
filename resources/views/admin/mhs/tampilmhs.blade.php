@extends('layout.main')
@section('title','Halaman Detail Mahasiswa')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Detail Mahasiswa</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Detail Mahasiswa
          </div>
           <div class="card-body">
               <table class="table table-striped">
                   <tr>
                        <td width="100">NIM</td>
                        <td width="6">:</td>
                        <td width="">{{ $data->nim }}</td>
                    </tr>
                    <tr>
                        <td width="142">Nama Mahasiswa</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->nama_mahasiswa }}</td>
                    </tr>
                    <tr>
                        <td width="142">Tempat Lahir</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->tempat_lahir }}</td>
                    </tr>
                    <tr>
                        <td width="142">Tanggal Lahir</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->tanggal_lahir }}</td>
                    </tr>
                    <tr>
                        <td width="142">Alamat</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->alamat }}</td>
                    </tr>
                    <tr>
                        <td width="142">Prodi</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->nama_prodi }}</td>
                    </tr>
                    <tr>
                        <td width="142">Email</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->email }}</td>
                    </tr>
                  
                    <tr>
                        <td width="142">No Hp</td>
                        <td width="4">:</td>
                        <td width="274">{{ $data->no_hp }}</td>
                    </tr>
                   

               </table>

               <div class="mt-3">
                <a href="{{ route('admin.EditMhs',$data->id_user) }}" class="btn btn-primary">Edit</a>
                <a href="{{ route('admin.DetailMhs') }}" class="btn btn-secondary">Kembali</a>
               </div>
              
           </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
