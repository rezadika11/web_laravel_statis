<table class="table table-bordered table-striped" id="container">
    <thead>
      <tr>
      <th>No.</th>
      <th>NIM</th>
      <th>Nama</th>
      <th>Email</th>
      <th>Aksi</th>
      </tr>
    </thead>
    @foreach ($data as $item)
    <tr>
        <td>{{ $loop->iteration }}.</td>
        <td>{{ $item->nim }}</td>
        <td>{{ $item->nama_mahasiswa }}</td>
        <td>{{ $item->email }}</td>
        <td>
          <a href="{{ route('admin.EditMhs', $item->id_user) }}" title="Edit" class="btn btn-info"><i class="fas fa-edit"></i></a>
          <a href="{{ route('admin.TampilMhs',$item->id_user) }}" title="Lihat Detail" class="btn btn-danger"><i class="fas fa-eye"></i></a>
          <a href="{{ route('admin.UbahPasswordMhs',$item->id_user) }}" title="Ganti Password" class="btn btn-warning"><i class="fas fa-key"></i></a>
        </td>
      </tr>
    @endforeach
    </table>
   
