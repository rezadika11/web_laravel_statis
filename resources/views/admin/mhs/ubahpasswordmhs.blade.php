@extends('layout.main')
@section('title','Halaman Ubah Password Mahasiswa')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Ubah Password Mahasiswa</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Form Ubah Password Mahasiswa
          </div>
          <div class="card-body">
            @if (session('password_edit'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              {{ session('password_edit') }}
            </div>
                
            @endif
              <form action="{{ route('admin.UpdatePasswordMhs',$data->id_user) }}" method="POST">
                {{ csrf_field() }}
                  <div class="mb-3">
                    <label for="password">Password Baru</label>
                    <input type="password" name="password" placeholder="Password Baru" class="form-control
                    @error('password')   is-invalid @enderror">
                    @error('password')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="ulangi_password">Ulangi Password</label>
                    <input type="password" name="ulangi_password" placeholder="Ulangi Password" class="form-control
                    @error('ulangi_password')   is-invalid @enderror">
                    @error('ulangi_password')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                  <a href="{{ route('admin.DetailMhs') }}" class="btn btn-secondary">Kembali</a>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
