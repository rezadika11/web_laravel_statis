@extends('layout.main')
@section('title','Halaman Edit Adm')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Ubah Adm</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Form Ubah Adm
          </div>
          <div class="card-body">      
              <form action="{{ route('UpdatePenggunaAdm') }}" method="POST">
                {{ csrf_field() }}
                  <div class="mb-3 ">
                    <label for="nip">NIP</label>
                    <input type="text" class="form-control @error('nip')  
                    is-invalid @enderror" placeholder="NIP" value="{{ old('nip', $data->nip )  }}" readonly>
                    @error('nip')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                    
                  </div>
                  <div class="mb-3">
                    <label for="nama">Nama Lengkap</label>
                    <input type="text" name="nama_pegawai" placeholder="Nama Lengkap" class="form-control
                    @error('nama_pegawai')   is-invalid @enderror" value="{{ old('nama_pegawai', $data->nama_pegawai ) }}" >
                    @error('nama_pegawai')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="telp">Nomor Telepon</label>
                    <input type="number" name="no_hp" placeholder="Nomor Telepon" class="form-control 
                    @error('no_hp')   is-invalid @enderror" value="{{ old('no_hp', $data->no_hp ) }}" >
                    @error('no_hp')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                  <a href="{{ route('DetailPenggunaAdm') }}" class="btn btn-secondary">Kembali</a>
              </form>
             
              
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection