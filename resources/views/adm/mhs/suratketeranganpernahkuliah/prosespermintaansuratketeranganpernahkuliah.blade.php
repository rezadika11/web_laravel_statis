@extends('layout.main')
@section('title','Surat Keterangan Pernah Kuliah')
@section('page','Surat Keterangan Pernah Kuliah')

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Keterangan Pernah Kuliah</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Surat Keterangan Pernah Kuliah
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('adm.SimpanProsesPermintaanSuratKeteranganPernahKuliah',['id_surat'=>$Surat->id_surat])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">NIM</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->nim}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->nama_mahasiswa}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="nim">Program Studi</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->nama_prodi}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="alamat">Alamat</label>
                      <textarea class="form-control" readonly>{{$Surat->alamat}}</textarea>
                    </div>
                    <div class="form-group">
                      <label for="angkatan">Tahun Angkatan</label>
                      <select class="form-control select2 {{$errors->has('angkatan') ? 'is-invalid' : '' }}" name="angkatan">
                        <option disabled selected>
                          Tahun Angkatan
                        </option>
                        @foreach($PilihAngkatan as $val)
                          <option value="{{$val}}" {{$Surat->angkatan==$val ? 'selected' : ''}}>
                            {{$val}}
                          </option>
                        @endforeach
                      </select>
                      @error('angkatan')
                      <span class="error invalid-feedback">
                          {{$errors->first('angkatan')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tahun_akademik_masuk">Tahun  Akademik Masuk</label>
                      <select class="form-control select2 {{$errors->has('tahun_akademik_masuk') ? 'is-invalid' : '' }}" name="tahun_akademik_masuk">
                        <option disabled selected>
                          Tahun Akademik Masuk
                        </option>
                        @foreach($PilihTahunAkademik as $val)
                          <option value="{{$val}}" {{$Surat->tahun_akademik_masuk==$val ? 'selected' : ''}}>
                            {{$val}}
                          </option>
                        @endforeach
                      </select>
                      @error('tahun_akademik_masuk')
                      <span class="error invalid-feedback">
                          {{$errors->first('tahun_akademik_masuk')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="semester_keluar">Semester Keluar</label>
                      <select class="form-control select2 {{$errors->has('semester_keluar') ? 'is-invalid' : '' }}" name="semester_keluar">
                        <option disabled selected>
                          Semester Keluar
                        </option>
                        @foreach($PilihSemesterKeluar as $val=>$label)
                          <option value="{{$val}}" {{$Surat->semester_keluar==$val ? 'selected' : ''}}>
                            {{$label}}
                          </option>
                        @endforeach
                      </select>
                      @error('semester_keluar')
                      <span class="error invalid-feedback">
                          {{$errors->first('semester_keluar')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tahun_akademik_keluar">Tahun  Akademik Keluar</label>
                      <select class="form-control select2 {{$errors->has('tahun_akademik_keluar') ? 'is-invalid' : '' }}" name="tahun_akademik_keluar">
                        <option disabled selected>
                          Tahun Akademik Keluar
                        </option>
                        @foreach($PilihTahunAkademik as $val)
                          <option value="{{$val}}" {{$Surat->tahun_akademik_keluar==$val ? 'selected' : ''}}>
                            {{$val}}
                          </option>
                        @endforeach
                      </select>
                      @error('tahun_akademik_keluar')
                      <span class="error invalid-feedback">
                          {{$errors->first('tahun_akademik_keluar')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="selesai_beban_studi">Selesai Beban Studi <sup>* dalam persen (%)</sup></label>
                      <input type="text" name="selesai_beban_studi" class="form-control {{$errors->has('selesai_beban_studi') ? 'is-invalid' : '' }}" id="selesai_beban_studi" value="{{$Surat->selesai_beban_studi}}">
                      @error('selesai_beban_studi')
                      <span class="error invalid-feedback">
                          {{$errors->first('selesai_beban_studi')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="lama_semester_kuliah">Lama Semester Kuliah</label>
                      <select class="form-control select2 {{$errors->has('lama_semester_kuliah') ? 'is-invalid' : '' }}" name="lama_semester_kuliah">
                        <option disabled selected>
                          Lama Semester Kuliah
                        </option>
                        @foreach($PilihSemester as $val=>$label)
                          <option value="{{$val}}" {{$Surat->lama_semester_kuliah==$val ? 'selected' : ''}}>
                            {{$val}} ({{$label}})
                          </option>
                        @endforeach
                      </select>
                      @error('lama_semester_kuliah')
                      <span class="error invalid-feedback">
                          {{$errors->first('lama_semester_kuliah')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="prodi_tujuan">Program Studi Tujuan</label>
                      <input type="text" name="prodi_tujuan" class="form-control {{$errors->has('prodi_tujuan') ? 'is-invalid' : '' }}" id="prodi_tujuan" value="{{$Surat->prodi_tujuan}}">
                      @error('prodi_tujuan')
                      <span class="error invalid-feedback">
                          {{$errors->first('prodi_tujuan')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                        <label for="pt_tujuan">Nama Perguruan Tinggi Tujuan</label>
                        <input type="text" name="pt_tujuan" class="form-control {{$errors->has('pt_tujuan') ? 'is-invalid' : '' }}" id="pt_tujuan" value="{{$Surat->pt_tujuan}}">
                        @error('pt_tujuan')
                        <span class="error invalid-feedback">
                            {{$errors->first('pt_tujuan')}}
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="alamat_pt_tujuan">Alamat Perguruan Tinggi Tujuan</label>
                        <textarea class="form-control {{$errors->has('alamat_pt_tujuan') ? 'is-invalid' : '' }}" name="alamat_pt_tujuan">{{$Surat->alamat_pt_tujuan}}</textarea>
                        @error('alamat_pt_tujuan')
                        <span class="error invalid-feedback">
                            {{$errors->first('alamat_pt_tujuan')}}
                        </span>
                        @enderror
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{route('adm.PermintaanSuratKeteranganPernahKuliah')}}" class="btn btn-secondary">Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('javascript')
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script>
  $(function () {
    $('.select2').select2()
  })
</script>
@endsection
