@extends('layout.main')
@section('title','Proses Surat Ijin Observasi Pendahuluan')
@section('page','Proses Surat Ijin Observasi Pendahuluan')

@section('css')
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Ijin Observasi Pendahuluan</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Surat Ijin Observasi Pendahuluan
            </div>
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('adm.SimpanProsesPermintaanSuratIjinObservasiPendahuluan',['id_surat'=>$Surat->id_surat])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">Nama</label>
                      <input type="text" class="form-control {{$errors->has('nama') ? 'is-invalid' : '' }}" id="nama" value="{{$Surat->nama_mahasiswa}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="nim">NIM</label>
                      <input type="text" class="form-control {{$errors->has('nim') ? 'is-invalid' : '' }}" id="nim" value="{{$Surat->nim}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="nim">Program Studi</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->nama_prodi}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="semester">Semester</label>
                      <input type="text" class="form-control" id="semester" value="{{$semester}}" readonly />
                    </div>
                    <div class="form-group">
                      <label for="semester">Tahun Akademik</label>
                      <input type="text" class="form-control" id="semester" value="{{$Surat->tahun_akademik}}" readonly />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">Kepada</label>
                      <input type="text" name="kepada"  class="form-control {{$errors->has('kepada') ? 'is-invalid' : '' }}" id="kepada" value="{{$Surat->kepada}}">
                      @error('kepada')
                      <span class="error invalid-feedback">
                          {{$errors->first('kepada')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                        <label for="Obyek">Obyek</label>
                        <input type="text" name="obyek" class="form-control {{$errors->has('obyek') ? 'is-invalid' : '' }}" id="obyek" value="{{$Surat->obyek}}">
                        @error('obyek')
                        <span class="error invalid-feedback">
                            {{$errors->first('obyek')}}
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                      <label for="lokasi">Lokasi</label>
                      <input type="text" name="lokasi" class="form-control {{$errors->has('lokasi') ? 'is-invalid' : '' }}" id="tempat" value="{{$Surat->lokasi}}">
                      @error('lokasi')
                      <span class="error invalid-feedback">
                          {{$errors->first('lokasi')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tgl">Tanggal Observasi</label>
                      <input type="date" name="tanggal_observasi" class="form-control {{$errors->has('tanggal_observasi') ? 'is-invalid' : '' }}" id="tgl" value="{{$Surat->tanggal_observasi}}">
                      @error('tanggal_observasi')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_observasi')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
@endsection
