@extends('layout.main')
@section('title','Proses Surat Tugas')
@section('page','Proses Surat Tugas')

@section('css')
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Tugas</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Surat Tugas
            </div>
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('adm.SimpanProsesPermintaanSuratTugas',['id_surat'=>$Surat->id_surat])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">Kegiatan</label>
                      <input type="text" name="kegiatan"  class="form-control {{$errors->has('kegiatan') ? 'is-invalid' : '' }}" id="kegiatan" value="{{$Surat->kegiatan}}">
                      @error('kegiatan')
                      <span class="error invalid-feedback">
                          {{$errors->first('kegiatan')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="nim">Penyelenggara</label>
                      <input type="text" name="penyelenggara"  class="form-control {{$errors->has('penyelenggara') ? 'is-invalid' : '' }}" id="penyelenggara" value="{{$Surat->penyelenggara}}">
                      @error('penyelenggara')
                      <span class="error invalid-feedback">
                          {{$errors->first('penyelenggara')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="tanggal_mulai">Tanggal Mulai</label>
                      <input type="date" name="tanggal_mulai" class="form-control {{$errors->has('tanggal_mulai') ? 'is-invalid' : '' }}" id="tanggal_mulai" value="{{$Surat->tanggal_mulai}}">
                      @error('tanggal_mulai')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_mulai')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tanggal_selesai">Tanggal Selesai</label>
                      <input type="date" name="tanggal_selesai" class="form-control {{$errors->has('tanggal_selesai') ? 'is-invalid' : '' }}" id="tanggal_selesai" value="{{$Surat->tanggal_selesai}}">
                      @error('tanggal_selesai')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_selesai')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <label>Peserta :</label>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 2em;">
                            No.
                          </th>
                          <th>
                            NIM
                          </th>
                          <th>
                            Nama
                          </th>
                          <th>
                            Program Studi
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($Peserta as $pst)
                        <tr>
                          <td>
                            {{$no++}}.
                          </td>
                          <td>
                            {{$pst->nim}}
                          </td>
                          <td>
                            {{$pst->nama}}
                          </td>
                          <td>
                            {{$pst->nama_prodi}}
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
@endsection
