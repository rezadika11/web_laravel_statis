@extends('layout.main')
@section('title','Permintaan Surat Tugas')
@section('page','Permintaan Surat Tugas')

@section('css')

  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Surat Tugas Sudah Diproses</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">

          <!-- jquery validation -->
          <div class="card card-primary">
            <div class="card-header">
              Surat Tugas Sudah Diproses
            </div>
            <div class="card-body">
              @if(Session::has('simpan_gagal'))
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('simpan_gagal') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
              @if(Session::has('simpan_sukses'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('simpan_sukses') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
              @include('adm.mhs.filter')
              <table id="permintaansurat" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>
                      No Surat
                    </th>
                    <th>
                      Tanggal Surat
                    </th>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Kegiatan</th>
                    <th>
                      Penyelenggara
                    </th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($SuratSudahProses as $Surat)
                  <tr>
                    <td>
                      {{$no++}}
                    </td>
                    <td>
                      {{$Surat->no_surat}}
                    </td>
                    <td>
                      {{date('d-m-Y',strtotime($Surat->tanggal_surat))}}
                    </td>
                    <td>
                      {{$Surat->nim}}
                    </td>
                    <td>
                      {{$Surat->nama_mahasiswa}}
                    </td>
                    <td>
                      {{$Surat->kegiatan}}
                    </td>
                    <td>
                      {{$Surat->penyelenggara}}
                    </td>
                    <td>
                      @switch($Surat->persetujuan_wadek3)
                      @case('0')
                        <button title="Menunggu Persetujuan Wakil Dekan 3" class="btn btn-warning btn-sm"><i class="fas fa-sync"></i></button>
                      @break
                      @case('1')
                        <a href="{{route('adm.CetakSuratTugas',['id_surat'=>$Surat->id_surat])}}" title="Cetak Surat" class="btn btn-primary btn-sm"><i class="fas fa-print"></i></a>
                      @break
                      @endswitch
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <div class="card-footer">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
<!-- DataTables  & Plugins -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
@include('adm.mhs.ajaxfilter')
<script>
  $(function () {
    $('.select2').select2()
    $('#permintaansurat').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": true,
      "responsive": true,
      "scrollX": true,
    });
  });
</script>
@endsection
