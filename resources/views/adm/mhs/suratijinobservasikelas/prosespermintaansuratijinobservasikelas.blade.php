@extends('layout.main')
@section('title','Proses Surat Ijin Observasi Kelas')
@section('page','Proses Surat Ijin Observasi Kelas')

@section('css')
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Ijin Observasi Kelas</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Form Proses Permintaan Surat Ijin Observasi Kelas
            </div>
            @if(Session::has('simpan_gagal'))
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('simpan_gagal') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
            <form method="POST" action="{{route('adm.SimpanProsesPermintaanSuratIjinObservasiKelas',['id_surat'=>$Surat->id_surat])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">Program Studi</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->nama_prodi}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="semester">Semester</label>
                      <input type="text" class="form-control" id="semester" value="{{$semester}}" readonly />
                    </div>
                    <div class="form-group">
                      <label for="mata_kuliah">Mata Kuliah</label>
                      <input type="text" name="mata_kuliah" class="form-control {{$errors->has('mata_kuliah') ? 'is-invalid' : '' }}" id="mata_kuliah" value="{{$Surat->mata_kuliah}}"/>
                      @error('mata_kuliah')
                      <span class="error invalid-feedback">
                          {{$errors->first('mata_kuliah')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="dosen_pengampu">Dosen Pengampu</label>
                      <input type="text" name="dosen_pengampu" class="form-control {{$errors->has('dosen_pengampu') ? 'is-invalid' : '' }}" id="dosen_pengampu" value="{{$Surat->dosen_pengampu}}"/>
                      @error('dosen_pengampu')
                      <span class="error invalid-feedback">
                          {{$errors->first('dosen_pengampu')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">Kepada</label>
                      <input type="text" name="kepada"  class="form-control {{$errors->has('kepada') ? 'is-invalid' : '' }}" id="kepada" value="{{$Surat->kepada}}">
                      @error('kepada')
                      <span class="error invalid-feedback">
                          {{$errors->first('kepada')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tanggal_mulai">Tanggal Mulai</label>
                      <input type="date" name="tanggal_mulai" class="form-control {{$errors->has('tanggal_mulai') ? 'is-invalid' : '' }}" id="tanggal_mulai" value="{{$Surat->tanggal_mulai}}">
                      @error('tanggal_mulai')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_mulai')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tanggal_selesai">Tanggal Selesai</label>
                      <input type="date" name="tanggal_selesai" class="form-control {{$errors->has('tanggal_selesai') ? 'is-invalid' : '' }}" id="tanggal_selesai" value="{{$Surat->tanggal_selesai}}">
                      @error('tanggal_selesai')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_selesai')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label>Peserta :</label>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>
                              No.
                            </th>
                            <th>
                              NIM
                            </th>
                            <th>
                              Nama
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($Peserta as $pst)
                          <tr>
                            <td>
                              {{$no++}}.
                            </td>
                            <td>
                              {{$pst->nim}}
                            </td>
                            <td>
                              {{$pst->nama}}
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
@endsection
