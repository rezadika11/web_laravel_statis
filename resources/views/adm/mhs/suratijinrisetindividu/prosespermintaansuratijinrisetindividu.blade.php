@extends('layout.main')
@section('title','Proses Surat Ijin Riset Individu')
@section('page','Proses Surat Ijin Riset Individu')

@section('css')
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Ijin Riset Individu</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        @if(Session::has('simpan_gagal'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          {{ session('simpan_gagal') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          </button>
        </div>
        @endif
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Surat Ijin Riset Individu
            </div>
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('adm.SimpanProsesPermintaanSuratIjinRisetIndividu',['id_surat'=>$Surat->id_surat_ijin_riset_individu])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat_ijin_riset_individu}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">Nama</label>
                      <input type="text" class="form-control {{$errors->has('nama') ? 'is-invalid' : '' }}" id="nama" value="{{$Surat->nama_mahasiswa}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="nim">NIM</label>
                      <input type="text" class="form-control {{$errors->has('nim') ? 'is-invalid' : '' }}" id="nim" value="{{$Surat->nim}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="nim">Program Studi</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->nama_prodi}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="semester">Semester</label>
                      <input type="text" class="form-control" id="semester" value="{{$semester}}" readonly />
                    </div>
                    <div class="form-group" id="tembusan">
                      <label for="tembusan">Tembusan</label>
                      @foreach($Tembusan as $Tmbs)
                        <input type="text" name="tembusan[{{$Tmbs->id_tembusan}}]"  class="form-control {{$errors->has('tembusan') ? 'is-invalid' : '' }}" value="{{$Tmbs->kepada_tembusan}}">
                      @endforeach
                      @error('tembusan')
                      <span class="error invalid-feedback">
                          {{$errors->first('tembusan')}}
                      </span>
                      @enderror
                      <button class="btn btn-xs btn-success" type="button" onclick="TambahTembusan()">Tambah Tembusan</button>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">Kepada</label>
                      <input type="text" name="kepada"  class="form-control {{$errors->has('kepada') ? 'is-invalid' : '' }}" id="kepada" value="{{$Surat->kepada}}">
                      @error('kepada')
                      <span class="error invalid-feedback">
                          {{$errors->first('kepada')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="kec">Kecamatan</label>
                      <input type="text" name="kec"  class="form-control {{$errors->has('kec') ? 'is-invalid' : '' }}" id="kec" value="{{$Surat->kec}}">
                      @error('kec')
                      <span class="error invalid-feedback">
                          {{$errors->first('kec')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="judul">Judul Riset</label>
                      <input type="text" name="judul" class="form-control {{$errors->has('judul') ? 'is-invalid' : '' }}" id="judul" value="{{$Surat->judul}}">
                      @error('judul')
                      <span class="error invalid-feedback">
                          {{$errors->first('judul')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                        <label for="Obyek">Obyek</label>
                        <input type="text" name="obyek" class="form-control {{$errors->has('obyek') ? 'is-invalid' : '' }}" id="obyek" value="{{$Surat->obyek}}">
                        @error('obyek')
                        <span class="error invalid-feedback">
                            {{$errors->first('obyek')}}
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                      <label for="tempat">Tempat</label>
                      <input type="text" name="tempat" class="form-control {{$errors->has('tempat') ? 'is-invalid' : '' }}" id="tempat" value="{{$Surat->tempat}}">
                      @error('tempat')
                      <span class="error invalid-feedback">
                          {{$errors->first('tempat')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tgl">Tanggal Mulai Riset</label>
                      <input type="date" name="tanggal_mulai" class="form-control {{$errors->has('tanggal_mulai') ? 'is-invalid' : '' }}" id="tgl" value="{{$Surat->tanggal_mulai}}">
                      @error('tanggal_mulai')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_mulai')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tgl">Tanggal Selesai Riset</label>
                      <input type="date" name="tanggal_selesai" class="form-control {{$errors->has('tanggal_selesai') ? 'is-invalid' : '' }}" id="tgl" value="{{$Surat->tanggal_selesai}}">
                      @error('tanggal_selesai')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_selesai')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="metode">Metode Penelitian</label>
                      <input type="text" name="metode_penelitian" class="form-control {{$errors->has('metode_penelitian') ? 'is-invalid' : '' }}" id="metode" value="{{$Surat->metode_penelitian}}">
                      @error('metode_penelitian')
                      <span class="error invalid-feedback">
                          {{$errors->first('metode_penelitian')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
<script>
function TambahTembusan(){
  var items=[];
  items.push('<input type="text" name="tembusan[]"  class="form-control">');
  $("#tembusan").append(items);
}
</script>
@endsection
