@extends('layout.main')
@section('title','Proses Surat Keterangan Masih Kuliah')
@section('page','Proses Surat Keterangan Masih Kuliah')

@section('css')
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Ketrangan Masih Kuliah</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Surat Ketrangan Masih Kuliah
            </div>
            <div class="card-body">
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('adm.SimpanProsesPermintaanSuratKeteranganMasihKuliah',['id_surat'=>$Surat->id_surat])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat}}" />
              <input type="hidden" name="id_wali" value="{{$Surat->id_wali}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nama_mahasiswa">Nama</label>
                      <input type="text" class="form-control {{$errors->has('nama_mahasiswa') ? 'is-invalid' : '' }}" id="nama_mahasiswa" value="{{$Surat->nama_mahasiswa}}" readonly/>
                    </div>
                    <div class="form-group">
                      <label for="nim">NIM</label>
                      <input type="text" class="form-control {{$errors->has('nim') ? 'is-invalid' : '' }}" id="nim" value="{{$Surat->nim}}" readonly/>
                    </div>
                    <div class="form-group">
                      <label for="alamat">Alamat</label>
                      <textarea class="form-control {{$errors->has('alamat') ? 'is-invalid' : '' }}" id="alamat" readonly>{{$Surat->alamat}}</textarea>
                    </div>
                    <div class="form-group">
                      <label for="tempat_lahir">Tempat Lahir</label>
                      <input type="text" class="form-control {{$errors->has('tempat_lahir') ? 'is-invalid' : '' }}" id="tempat_lahir" value="{{$Surat->tempat_lahir}}" readonly/>
                    </div>
                    <div class="form-group">
                      <label for="tanggal_lahir">Tanggal Lahir</label>
                      <input type="date" class="form-control {{$errors->has('tanggal_lahir') ? 'is-invalid' : '' }}" id="nama" value="{{$Surat->tanggal_lahir}}" readonly/>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="tahun_akademik">Tahun  Akademik</label>
                      <select class="form-control select2 {{$errors->has('tahun_akademik') ? 'is-invalid' : '' }}" name="tahun_akademik">
                        <option disabled selected>
                          Tahun Akademik
                        </option>
                        @foreach($TahunAkademik as $val)
                          <option value="{{$val}}" {{$Surat->tahun_akademik==$val ? 'selected' : ''}}>
                            {{$val}}
                          </option>
                        @endforeach
                      </select>
                      @error('tahun_akademik')
                      <span class="error invalid-feedback">
                          {{$errors->first('tahun_akademik')}}
                      </span>
                      @enderror
                    @if(isset($Surat->id_wali))
                    <div class="form-group">
                      <label for="nim">Nama Wali</label>
                      <input type="text" name="nama_wali"  class="form-control {{$errors->has('nama_wali') ? 'is-invalid' : '' }}" id="nama_wali" value="{{$Surat->nama_wali}}">
                      @error('nama_wali')
                      <span class="error invalid-feedback">
                          {{$errors->first('nama_wali')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="nim">NIP / NRP</label>
                      <input type="text" name="nip"  class="form-control {{$errors->has('nip') ? 'is-invalid' : '' }}" id="nip" value="{{$Surat->nip}}">
                      @error('nip')
                      <span class="error invalid-feedback">
                          {{$errors->first('nip')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="pangkat_golongan">Pangkat Golongan</label>
                      <input type="text" name="pangkat_golongan" class="form-control {{$errors->has('pangkat_golongan') ? 'is-invalid' : '' }}" id="pangkat_golongan" value="{{$Surat->pangkat_golongan}}">
                      @error('pangkat_golongan')
                      <span class="error invalid-feedback">
                          {{$errors->first('pangkat_golongan')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="instansi">Instansi</label>
                      <input type="text" name="instansi" class="form-control {{$errors->has('instansi') ? 'is-invalid' : '' }}" id="instansi" value="{{$Surat->instansi}}">
                      @error('instansi')
                      <span class="error invalid-feedback">
                          {{$errors->first('instansi')}}
                      </span>
                      @enderror
                    </div>
                    @endif
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
@endsection
