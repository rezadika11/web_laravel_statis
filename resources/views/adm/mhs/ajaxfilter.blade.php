<script>
function Cari(){
  var id_prodi = $("#prodi").val()
  var key = $("#key").val()
  var filter = $("#filter").val()
  var url = "{{route($route,['filter'=>'val','prodi'=>'val_id_prodi'])}}"

  url = url.replace('amp;','')

  if(key != null && key !=""){
    url = url.replace('val',key)
    url = url.replace('filter',filter)
  }else {
    url = url.replace('filter=val&','')
  }

  if(id_prodi != null ){
    url = url.replace('val_id_prodi',id_prodi)
  }else {
    url = url.replace('&prodi=val_id_prodi','')
    url = url.replace('prodi=val_id_prodi','')
  }
  window.location.href=url
}

function Reset(){
  var url = "{{route($route)}}"
  window.location.href=url
}

$(function () {
  $('#filter').change(function(){
    if($("#filter").val() == 'tanggal_surat')
    {
      $("#key").prop('type','date');
    }
    else{
      $("#key").prop('type','text');
    }
  })
  if($("#filter").val() == 'tanggal_surat')
  {
    $("#key").prop('type','date');
  }
  else{
    $("#key").prop('type','text');
  }
});
</script>
