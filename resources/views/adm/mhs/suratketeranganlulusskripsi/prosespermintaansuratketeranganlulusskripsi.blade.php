@extends('layout.main')
@section('title','Proses Surat Keterangan Lulus')
@section('page','Proses Surat Keterangan Lulus')

@section('css')
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Keterangan Lulus</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        @if(Session::has('simpan_gagal'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          {{ session('simpan_gagal') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          </button>
        </div>
        @endif
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Surat Keterangan Lulus
            </div>
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('adm.SimpanProsesPermintaanSuratKeteranganLulusSkripsi',['id_surat'=>$Surat->id_surat])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">NIM</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->nim}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->nama_mahasiswa}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="nim">Program Studi</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->nama_prodi}}" readonly>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="tahun_akademik">Tahun Akademik</label>
                      <input type="text"  class="form-control" id="tahun_akademik" value="{{$Surat->tahun_akademik}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="tanggal_ujian">Tanggal Ujian</label>
                      <input type="date" name="tanggal_ujian" class="form-control {{$errors->has('tanggal_ujian') ? 'is-invalid' : '' }}" id="tanggal_ujian" value="{{$Surat->tanggal_ujian}}">
                      @error('tanggal_ujian')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_ujian')}}
                      </span>
                      @enderror
                    </div>
                     <div class="form-group">
                      <label for="lampiran_munaqosyah">Lampiran Berita Acara Munaqosyah</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input {{$errors->has('lampiran_munaqosyah') ? 'is-invalid' : '' }}" id="lampiran_munaqosyah" name="lampiran_munaqosyah" value="{{$Surat->tanggal_ujian}}"> 
                          <label class="custom-file-label" for="exampleInputFile">Pilih File</label>
                          @error('lampiran_munaqosyah')
                          <span class="error invalid-feedback">
                           {{$errors->first('lampiran_munaqosyah')}}
                          </span>
                           @enderror
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
@endsection
