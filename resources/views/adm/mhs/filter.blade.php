<div class="row">
  <div class="col-md-1">
    Filter :
  </div>
  <div class="col-md-2">
    <select name="filter" class="select2" id="filter" style="width:100%">
      @foreach($JenisFilter as $fkey => $vfilter)
        <option value="{{$fkey}}" {{$filter == $fkey ? 'selected' : ''}}>
          {{$vfilter}}
        </option>
      @endforeach
    </select>
  </div>
  <div class="col-md-2">
    <input type="text" class="form-control" name="key" id="key" value="{{$key}}"/>
  </div>
  <div class="col-md-3">
    <select name="prodi" class="select2" id="prodi" style="width:100%">
      <option disabled selected>
        Filter Menurut Prodi
      </option> 
      @foreach($PilihFilterProdi as $prodi)
        <option value="{{$prodi->id_prodi}}" {{($filter_id_prodi==$prodi->id_prodi) ? 'selected' : ''}}>
          {{$prodi->nama_prodi}}
        </option>
      @endforeach
    </select>
  </div>
  <button class="btn btn-success btn-sm" onclick="Cari()" title="Cari"><i class="fas fa-search"></i></button>
  <button class="btn btn-danger btn-sm" onclick="Reset()" title="Reset"><i class="fas fa-times"></i></button>
</div>
