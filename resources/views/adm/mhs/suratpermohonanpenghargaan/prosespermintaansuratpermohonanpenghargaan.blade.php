@extends('layout.main')
@section('title','Proses Surat Permohonan Penghargaan')
@section('page','Proses Surat Permohonan Penghargaan')

@section('css')
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Permohonan Penghargaan</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Surat Permohonan Penghargaan
            </div>
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('adm.SimpanProsesPermintaanSuratPermohonanPenghargaan',['id_surat'=>$Surat->id_surat])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">Nama</label>
                      <input type="text" class="form-control {{$errors->has('nama') ? 'is-invalid' : '' }}" id="nama" value="{{$Surat->nama_mahasiswa}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="nim">NIM</label>
                      <input type="text" class="form-control {{$errors->has('nim') ? 'is-invalid' : '' }}" id="nim" value="{{$Surat->nim}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="nim">Program Studi</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->nama_prodi}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="alamat">Alamat</label>
                      <textarea class="form-control {{$errors->has('alamat') ? 'is-invalid' : '' }}" id="alamat" readonly>{{$Surat->alamat}}</textarea>
                    </div>
                    <div class="form-group">
                      <label for="nim">No.HP</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->no_hp}}" readonly>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nama_perlombaan">Nama Perlombaan</label>
                      <input type="text" name="nama_perlombaan"  class="form-control {{$errors->has('nama_perlombaan') ? 'is-invalid' : '' }}" id="nama_perlombaan" value="{{$Surat->nama_perlombaan}}">
                      @error('nama_perlombaan')
                      <span class="error invalid-feedback">
                          {{$errors->first('nama_perlombaan')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tanggal_mulai_pelaksanaan">Tanggal Mulai Pelaksanaan</label>
                      <input type="date" name="tanggal_mulai_pelaksanaan" class="form-control {{$errors->has('tanggal_mulai_pelaksanaan') ? 'is-invalid' : '' }}" id="tanggal_mulai_pelaksanaan" value="{{$Surat->tanggal_mulai_pelaksanaan}}">
                      @error('tanggal_mulai_pelaksanaan')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_mulai_pelaksanaan')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tanggal_selesai_pelaksanaan">Tanggal Selesai Pelaksanaan</label>
                      <input type="date" name="tanggal_selesai_pelaksanaan" class="form-control {{$errors->has('tanggal_selesai_pelaksanaan') ? 'is-invalid' : '' }}" id="tanggal_selesai_pelaksanaan" value="{{$Surat->tanggal_selesai_pelaksanaan}}">
                      @error('tanggal_selesai_pelaksanaan')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_selesai_pelaksanaan')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tempat">Tempat</label>
                      <input type="text" name="tempat"  class="form-control {{$errors->has('tempat') ? 'is-invalid' : '' }}" id="tempat" value="{{$Surat->tempat}}">
                      @error('tempat')
                      <span class="error invalid-feedback">
                        {{$errors->first('tempat')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="juara">Juara</label>
                      <select class="form-control select2 {{$errors->has('juara') ? 'is-invalid' : '' }}" name="juara">
                        <option disabled selected>
                          Juara
                        </option>
                        @foreach($PilihJuara as $val=>$label)
                          <option value="{{$val}}" {{$Surat->juara==$val ? 'selected' : ''}}>
                            {{$val}} ({{$label}})
                          </option>
                        @endforeach
                      </select>
                      @error('juara')
                      <span class="error invalid-feedback">
                          {{$errors->first('juara')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="kelompok">Kelompok</label>
                      <input type="text" name="kelompok"  class="form-control {{$errors->has('kelompok') ? 'is-invalid' : '' }}" id="kelompok" value="{{$Surat->kelompok}}">
                      @error('kelompok')
                      <span class="error invalid-feedback">
                          {{$errors->first('kelompok')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('javascript')
@endsection
