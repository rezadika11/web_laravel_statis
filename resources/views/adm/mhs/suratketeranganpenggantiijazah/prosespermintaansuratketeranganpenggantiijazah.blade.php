@extends('layout.main')
@section('title','Surat Keterangan Pengganti Ijazah')
@section('page','Surat Keterangan Pengganti Ijazah')

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Proses Surat Keterangan Pengganti Ijazah</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              Proses Surat Keterangan Pengganti Ijazah
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            @if(Session::has('simpan_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{ session('simpan_gagal') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif
            <form method="POST" action="{{route('adm.SimpanProsesPermintaanSuratKeteranganPenggantiIjazah',['id_surat'=>$Surat->id_surat])}}">
              @csrf
              <input type="hidden" name="id_surat" value="{{$Surat->id_surat}}" />
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nim">NIM</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->nim}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->nama_mahasiswa}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="nim">Program Studi</label>
                      <input type="text" class="form-control" id="prodi" value="{{$Surat->nama_prodi}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="alamat">Alamat</label>
                      <textarea class="form-control" readonly>{{$Surat->alamat}}</textarea>
                    </div>
                    <div class="form-group">
                      <label for="tempat_lahir">Tempat Lahir</label>
                      <input type="text" class="form-control" id="tempat_lahir" value="{{$Surat->tempat_lahir}}" readonly>
                    </div>
                    <div class="form-group">
                      <label for="tanggal_lahir">Tanggal Lahir</label>
                      <input type="text" class="form-control" id="tanggal_lahir" value="{{date('d-m-Y',strtotime($Surat->tanggal_lahir))}}" readonly>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nomor_ijazah">Nomor Ijazah</label>
                      <input type="text" name="nomor_ijazah"  class="form-control {{$errors->has('nomor_ijazah') ? 'is-invalid' : '' }}" id="nomor_ijazah" placeholder="Nomor Ijazah" value="{{old('nomor_ijazah',$Surat->nomor_ijazah)}}">
                      @error('nomor_ijazah')
                      <span class="error invalid-feedback">
                          {{$errors->first('nomor_ijazah')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tanggal_ijazah">Tanggal Ijazah</label>
                      <input type="date" name="tanggal_ijazah"  class="form-control {{$errors->has('tanggal_ijazah') ? 'is-invalid' : '' }}" id="tanggal_ijazah" value="{{old('tanggal_ijazah',$Surat->tanggal_ijazah)}}">
                      @error('tanggal_ijazah')
                      <span class="error invalid-feedback">
                          {{$errors->first('tanggal_ijazah')}}
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="tahun_kehilangan">Tahun Kehilangan</label>
                      <select class="form-control select2 {{$errors->has('tahun_kehilangan') ? 'is-invalid' : '' }}" name="tahun_kehilangan">
                        <option disabled selected>
                          Tahun Kehilangan
                        </option>
                        @foreach($PilihTahunKehilangan as $val)
                          <option value="{{$val}}" {{old('tahun_kehilangan',$Surat->tahun_kehilangan)==$val ? 'selected' : ''}}>
                            {{$val}}
                          </option>
                        @endforeach
                      </select>
                      @error('tahun_kehilangan')
                      <span class="error invalid-feedback">
                          {{$errors->first('tahun_kehilangan')}}
                      </span>
                      @enderror
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{route('adm.PermintaanSuratKeteranganPenggantiIjazah')}}" class="btn btn-secondary">Kembali</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('javascript')
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script>
  $(function () {
    $('.select2').select2()
  })
</script>
@endsection
