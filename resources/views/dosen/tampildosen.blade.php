@extends('layout.main')
@section('title','Halaman Tampil Dosen')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dosen</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            Tampil Dosen
          </div>
          <div class="card-body">
            
            @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              {{ session('status') }}
            </div>
                
            @endif
            @if (session('data_diedit'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              {{ session('data_diedit') }}
            </div>
                
            @endif
             <table class="table table-bordered table-striped">
              <thead>
                <tr>
                <th>No.</th>
                <th>NIP</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Aksi</th>
                </tr>
              </thead>
             @foreach ($data as $item)
             <tr>
                <td>{{ $loop->iteration  }}.</td>
                <td>{{ $item->nip }}</td>
                <td>{{ $item->nama_dosen }}</td>
                <td>{{ $item->email }}</td>
                <td>
                  <a href="{{ route('EditPenggunaDosen') }}" class="btn btn-info" title="Edit Data"><i class="fas fa-edit"></i></a>
                  <a href="{{ route('DetailPenggunaDosen') }}" class="btn btn-danger" title="Lihat Data"><i class="fas fa-eye"></i></a>
                  
               </a>
                </td>
              </tr>
             @endforeach
              
             
            </table>
           
          </div>
          
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
