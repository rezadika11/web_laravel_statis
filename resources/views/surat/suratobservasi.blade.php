@extends('layout.main')
@section('title','Surat Ijin Observasi')
@section('page','Surat Ijin Observasi')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Form Surat Ijin Observasi</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- jquery validation -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Form Surat Ijin Observasi</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="quickForm" novalidate="novalidate">
              <div class="card-body">
                <div class="form-group">
                  <label for="nim">Surat Ditujukan Kepada</label>
                  <input type="text" name="kepada"  class="form-control" id="nim" placeholder="Surat Ditujukan Kepada" autofocus required>
                </div>
                <div class="form-group">
                  <label for="kec">Kecamatan</label>
                  <input type="text" name="kec"  class="form-control" id="kec" placeholder="Kecamatan" required>
                </div>
                <div class="form-group">
                  <label for="judul">Judul Riset</label>
                  <input type="text" name="judul" class="form-control" id="nama" placeholder="Judul Riset" required>
                </div>
                <div class="form-group">
                    <label for="semester">Obyek</label>
                    <input type="text" name="obyek" class="form-control" id="obyek" placeholder="Obyek" required>
                  </div>
                  <div class="form-group">
                    <label for="tempat">Tempat</label>
                    <input type="text" name="tempat" class="form-control" id="tempat" placeholder="Tempat" required>
                  </div>
                  <div class="form-group">
                    <label for="tgl">Tanggal Mulai Riset</label>
                    <input type="date" name="tanggal_mulai" class="form-control" id="tgl" placeholder="Tanggal Riset" required>
                  </div>
                  <div class="form-group">
                    <label for="tgl">Tanggal Selesai Riset</label>
                    <input type="date" name="tanggal_selesai" class="form-control" id="tgl" placeholder="Tanggal Riset" required>
                  </div>
                  <div class="form-group">
                    <label for="metode">Metode Penelitian</label>
                    <input type="text" name="metode_penelitian" class="form-control" id="metode" placeholder="Metode Penelitian" required>
                  </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-secondary">Reset</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
</div>
@endsection
