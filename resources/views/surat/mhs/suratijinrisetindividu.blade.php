<html>
  <head>
    <title>Unduh Surat Ijin Riset Individu</title>
    <style>
    body{
      min-height: 330mm;
      font-family: Sans-serif, Arial;
      font-size: 11pt;
      line-height: 1;
    }
    .page {
        background: white;
      }

    @page {
        size: F4;
        margin: 10mm;
    }

    .page-break {
      page-break-after: always;
    }

    .isisurat{
      margin-left: 80px;
    }

    </style>
  </head>
  <body>
    <img src="{{$logo}}" width="100mm" height="100mm" style="float:left;"/>
    <div style="text-align:center; font-size:14pt; font-weight: bold;">
      KEMENTERIAN AGAMA REPUBLIK INDONESIA
    </div>
    <div style="text-align:center; font-size:12pt; font-weight: bold;">
      UNIVERSITAS ISLAM NEGERI <br />PROFESOR KIAI HAJI SAIFUDDIN ZUHRI PURWOKERTO <br /> FAKULTAS TARBIYAH DAN ILMU KEGURUAN
    </div>
    <div style="text-align:center; font-size:9pt;">
      Jalan Jenderal A. Yani, No. 40A Purwokerto 53126
      <br />Telepon (0281) 635624  Faksimili (0281) 636553
      <br />www.ftik.uinsaizu.ac.id
    </div>
    <hr style="border:0.3mm solid;"/>
    <br />
      <table border="0" width="100%">
        <tr>
          <td style="width:75px;">
            Nomor
          </td>
          <td>
            : {{$Surat->no_surat}}
          </td>
          <td style="text-align:right;" width="25%">
            {{$TanggalSurat}}
          </td>
        </tr>
        <tr>
          <td>
            Lamp.
          </td>
          <td colspan="2">
            : -
          </td>
        </tr>
        <tr>
          <td>
            Hal
          </td>
          <td colspan="2">
            : <strong>Permohonan Ijin Riset Individu</strong>
          </td>
        </tr>
      </table>
      <div class="isisurat">
        <p style="text-align:justify">
          Kepada<br />
          Yth. {{$Surat->kepada}}<br />
          Kec. {{$Surat->kec}}<br />
          di Tempat
          <br />
        </p>
        <p style="text-align:justify">
          <i>Assalamu’alaikum Wr. Wb.</i>
          <br />
    Diberitahukan dengan hormat bahwa dalam rangka pengumpulan data guna penyusunan skripsi, memohon dengan hormat saudara berkenan memberikan ijin riset kepada mahasiswa kami dengan identitas sebagai berikut :
        </p>
          <table border="0">
            <tr>
              <td style="width:200px;">
                1. Nama
              </td>
              <td>
                : {{$Surat->nama_mahasiswa}}
              </td>
            </tr>
            <tr>
              <td>
                2. NIM
              </td>
              <td>
                : {{$Surat->nim}}
              </td>
            </tr>
            <tr>
              <td>
                3. Semester
              </td>
              <td>
                : {{$Surat->semester}}
              </td>
            </tr>
            <tr>
              <td>
                4. Jurusan / Prodi
              </td>
              <td>
                : {{$Surat->nama_prodi}}
              </td>
            </tr>
            <tr>
              <td>
                5. Alamat
              </td>
              <td style="text-indent: -1.2em; padding-left:1em;">
                  : {{$Surat->alamat}}
              </td>
            </tr>
            <tr>
              <td>
                6. Judul
              </td>
              <td>
                : {{$Surat->judul}}
              </td>
            </tr>
          </table>
          <p style="text-align:justify">
          Adapun riset tersebut akan dilaksanakan dengan ketentuan sebagai berikut :
          </p>
          <table border="0">
            <tr>
              <td style="width:200px;">
                1. Obyek
              </td>
              <td>
                : {{$Surat->obyek}}
              </td>
            </tr>
            <tr>
              <td>
                2. Tempat / Lokasi
              </td>
              <td>
                : {{$Surat->tempat}}
              </td>
            </tr>
            <tr>
              <td>
                3. Tanggal Riset
              </td>
              <td>
                : {{date('d-m-Y',strtotime($Surat->tanggal_mulai))}} s/d {{date('d-m-Y',strtotime($Surat->tanggal_selesai))}}
              </td>
            </tr>
            <tr>
              <td>
                4. Metode Penelitian
              </td>
              <td>
                : {{$Surat->metode_penelitian}}
              </td>
            </tr>
          </table>
          <p style="text-align:justify">
          Demikian atas perhatian dan ijin saudara, kami sampaikan terima kasih. <br />
          <i>Wassalamu’alaikum Wr. Wb.</i>
          </p>
          <table width="100%">
            <tr>
              <td style="text-align:left;" width="65%">
              </td>
              <td style="text-align:center">
                An. Dekan <br />
                Ketua Jurusan {{$Kajur->nama_jurusan}}
                <br />
                <br />
                <br />
                <img src="data:image/svg+xml;base64,{{base64_encode($qr)}}" style="display: block; margin: 0 auto;"/>
                <br />
                {{$Kajur->nama_dosen}}
              </td>
            </tr>
          </table>
          @if(count($Tembusan)>0)
          <p style="margin-left:-10px;">
            Tembusan :
            <ol type="1" style="margin-left: -10px;">
              @foreach($Tembusan as $tmbs)
              <li>
                {{$tmbs->kepada_tembusan}}
              </li>
              @endforeach
            </ol>
          </p>
          @endif
      </div>
  </body>
</html>
