<html>
  <head>
    <title>Unduh Surat Keterangan Lulus Skripsi</title>
    <style>
    body{
      min-height: 330mm;
      font-family: Sans-serif, Arial;
      font-size: 11pt;
      line-height: 1;
    }
    .page {
        background: white;
      }

    @page {
        size: F4;
        margin: 10mm;
    }

    .page-break {
      page-break-after: always;
    }

    .isisurat{
      margin-left: 80px;
      margin-right: 10mm;
      text-align: justify;

    }

    #tabel{
      border-collapse: collapse;
      width: 100%;
    }
    #tabel td {
      border: 1px solid;
      padding-left: 5px;
      padding-right: 5px;
      white-space: nowrap;
    }

    </style>
  </head>
  <body>
    <img src="{{$logo}}" width="100mm" height="100mm" style="float:left;"/>
    <div style="text-align:center; font-size:14pt; font-weight: bold;">
      KEMENTERIAN AGAMA REPUBLIK INDONESIA
    </div>
    <div style="text-align:center; font-size:12pt; font-weight: bold;">
      UNIVERSITAS ISLAM NEGERI <br />PROFESOR KIAI HAJI SAIFUDDIN ZUHRI PURWOKERTO <br /> FAKULTAS TARBIYAH DAN ILMU KEGURUAN
    </div>
    <div style="text-align:center; font-size:9pt;">
      Jalan Jenderal A. Yani, No. 40A Purwokerto 53126
      <br />Telepon (0281) 635624  Faksimili (0281) 636553
      <br />www.ftik.uinsaizu.ac.id
    </div>
    <hr style="border:0.3mm solid;"/>
    <br />
    <div class="isisurat">
      <div style="font-family: Sans-serif, Arial; font-size: 14pt; font-weight: bold; text-decoration: underline; text-align:center;">
        SURAT KETERANGAN LULUS MUNAQASYAH SKRIPSI
      </div>
      <div style="font-family: Sans-serif, Arial; font-size: 11pt; text-align:center; padding-top:1px;">
        NOMOR : {{$Surat->no_surat}}
      </div>
      <p style="text-indent: 10mm;">
        Yang  bertanda  tangan  di bawah  ini  Dekan Fakultas Tarbiyah dan Ilmu Keguruan (FTIK) Universitas Islam Negeri (UIN) Profesor K.H. saifuddin Zuhri Purwokerto menerangkan dengan sesungguhnya :
      </p>
      <table border="0" style="margin-left: 10mm;">
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            Nama
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            :
          </td>
          <td>
            {{$Surat->nama_mahasiswa}}
          </td>
        </tr>
        <tr>
          <td>
            NIM
          </td>
          <td>
            :
          </td>
          <td>
            {{$Surat->nim}}
          </td>
        </tr>
        <tr>
          <td>
            Program Studi
          </td>
          <td>
            :
          </td>
          <td>
            {{$Surat->nama_prodi}}
          </td>
        </tr>
        <tr>
          <td>
            Tahun Akademik
          </td>
          <td>
            :
          </td>
          <td>
            {{$Surat->tahun_akademik}}
          </td>
        </tr>
      </table>
      <p style="text-indent: 10mm; margin-bottom: 0px;">
        Saudara tersebut benar–benar sebagai mahasiswa Fakultas Tarbiyah dan Ilmu Keguruan (FTIK) Universitas Islam Negeri Prof. K.H. Saifuddin Zuhri Purwokerto dan telah <strong style="font-style: italic;">Lulus</strong> mengikuti ujian Munaqasyah Skripsi pada tanggal {{$TanggalUjian}} dan bagi saudara tersebut diatas berhak menyandang gelar Sarjana Pendidikan (S.Pd.).
      </p>
      <p style="text-indent: 10mm; margin-top: 0px;">
        Demikian surat keterangan  ini dibuat untuk dan dapat dipergunakan sebagaimana mestinya serta surat keterangan ini berlaku sampai dengan diterbitkannya Ijazah.
      </p>
        <table width="100%">
          <tr>
            <td style="text-align:left;" width="55%">
            </td>
            <td style="text-align:left">
              Purwokerto, {{$TanggalSurat}}
              <br />
              An. Dekan <br />
              Ketua Jurusan {{$Kajur->nama_jurusan}}
              <br />
              <br />
              <br />
              <img src="data:image/svg+xml;base64,{{base64_encode($qr)}}" style="display: block; margin: 0 auto;"/>
              <br />
              {{$Kajur->nama_dosen}}
            </td>
          </tr>
        </table>
    </div>
  </body>
</html>
