<html>
  <head>
    <title>Unduh Surat Permohonan Penghargaan</title>
    <style>
    body{
      min-height: 330mm;
      font-family: Sans-serif, Arial;
      font-size: 11pt;
      line-height: 1;
    }
    .page {
        background: white;
      }

    @page {
        size: F4;
        margin: 10mm;
    }

    .page-break {
      page-break-after: always;
    }

    .isisurat{
      margin-left: 80px;
    }

    #tabel{
      border-collapse: collapse;
      width: 100%;
    }
    #tabel td {
      border: 1px solid;
      padding-left: 5px;
      padding-right: 5px;
      white-space: nowrap;
    }

    </style>
  </head>
  <body>
    <img src="{{$logo}}" width="100mm" height="100mm" style="float:left;"/>
    <div style="text-align:center; font-size:14pt; font-weight: bold;">
      KEMENTERIAN AGAMA REPUBLIK INDONESIA
    </div>
    <div style="text-align:center; font-size:12pt; font-weight: bold;">
      UNIVERSITAS ISLAM NEGERI <br />PROFESOR KIAI HAJI SAIFUDDIN ZUHRI PURWOKERTO <br /> FAKULTAS TARBIYAH DAN ILMU KEGURUAN
    </div>
    <div style="text-align:center; font-size:9pt;">
      Jalan Jenderal A. Yani, No. 40A Purwokerto 53126
      <br />Telepon (0281) 635624  Faksimili (0281) 636553
      <br />www.ftik.uinsaizu.ac.id
    </div>
    <hr style="border:0.3mm solid;"/>
    <br />
      <table border="0" width="100%">
        <tr>
          <td style="width:75px;">
            Nomor
          </td>
          <td>
            : {{$Surat->no_surat}}
          </td>
          <td style="text-align:right;" width="25%">
            {{$TanggalSurat}}
          </td>
        </tr>
        <tr>
          <td>
            Lamp.
          </td>
          <td colspan="2">
            : 1 Bendel
          </td>
        </tr>
        <tr>
          <td>
            Hal
          </td>
          <td colspan="2">
            : <strong>Permohonan Penghargaan</strong>
          </td>
        </tr>
      </table>
    <div class="isisurat">
      <p style="text-align:justify">
        Kepada<br />
        Wakil Rektor III IAIN Purwokerto<br />
        Cq. Kabag. Akademik dan kemahasiswaan<br />
        Di Purwokerto
        <br />
        <br />
      </p>
      <p style="text-align:justify">
        <i>Assalamu’alaikum Wr. Wb.</i>
        <br />
        <br />
        Yang bertanda tangan di bawah ini Wakil Dekan III Fakultas Tarbiyah dan Ilmu Keguruan IAIN Purwokerto bahwa mahasiswa:
        <br />
      </p>
        <table border="0">
          <tr>
            <td style="padding-left:2px; padding-right:2px;">
              Nama
            </td>
            <td style="padding-left:2px; padding-right:2px;">
              : {{$Surat->nama_mahasiswa}}
            </td>
          </tr>
          <tr>
            <td style="padding-left:2px; padding-right:2px;">
              NIM
            </td>
            <td style="padding-left:2px; padding-right:2px;">
              : {{$Surat->nim}}
            </td>
          </tr>
          <tr>
            <td style="padding-left:2px; padding-right:2px;">
              Program Studi/Jurusan
            </td>
            <td style="padding-left:2px; padding-right:2px;">
              : {{$Surat->nama_prodi}}
            </td>
          </tr>
          <tr>
            <td style="padding-left:2px; padding-right:2px;">
              Fakultas
            </td>
            <td style="padding-left:2px; padding-right:2px;">
              : Tarbiyah dan Ilmu Keguruan
            </td>
          </tr>
          <tr>
            <td style="padding-left:2px; padding-right:2px;">
              Alamat Asal
            </td>
            <td style="text-indent: -1em; padding-left:1em;">
              : {{$Surat->alamat}}
            </td>
          </tr>
          <tr>
            <td style="padding-left:2px; padding-right:2px;">
              No HP
            </td>
            <td style="padding-left:2px; padding-right:2px;">
              : {{$Surat->no_hp}}
            </td>
          </tr>
          <tr>
            <td style="padding-left:2px; padding-right:2px;">
              Perlombaan
            </td>
            <td style="padding-left:2px; padding-right:2px;">
              : {{$Surat->nama_perlombaan}}
            </td>
          </tr>
          <tr>
            <td style="padding-left:2px; padding-right:2px;">
              Tanggal Pelaksanaan
            </td>
            <td style="padding-left:2px; padding-right:2px;">
              : {{$TanggalPelaksanaan}}
            </td>
          </tr>
          <tr>
            <td style="padding-left:2px; padding-right:2px;">
              Tempat
            </td>
            <td style="padding-left:2px; padding-right:2px;">
              : {{$Surat->tempat}}
            </td>
          </tr>
          <tr>
            <td style="padding-left:2px; padding-right:2px;">
              Juara
            </td>
            <td style="padding-left:2px; padding-right:2px;">
              : {{$Juara}}
            </td>
          </tr>
          <tr>
            <td style="padding-left:2px; padding-right:2px;">
              Kelompok
            </td>
            <td style="padding-left:2px; padding-right:2px;">
              : {{$Surat->kelompok}}
            </td>
          </tr>
        </table>
        <p style="text-align:justify">
          Dengan ini mengajukan permohonan pernghargaan atas prestasi yang telah dicapai mahasiswa tersebut, sebagai bahan pertimbangan berikut kami lampirlkan:
          <ol>
            <li>
              Surat/undangan/brosur dan atau proposal asli perlombaan/kejuaraan;
            </li>
            <li>
              Sertifikat/piagam juara asli;
            </li>
            <li>
              Dokumentasi saat menerima sertifikat/piagam/tropy kejuaraan atau dokumentasi lain yang relevan;
            </li>
            <li>
              Fotocopy pembayaran UKT terakhir;
            </li>
            <li>
              Fotocopy halaman depan rekening yang masih aktif atas nama penerima atau perwakilan bagi yang beregu;
            </li>
            <li>
              Surat Keputusan atau Surat Tugas mengikuti kegiatan dari Fakultas.
            </li>
          </ol>
        Demikian permohonan ini kami buat, atas perhatian dan kerjasamanya disampaikan terima kasih.
        <br />
        <br />
        <i>Wassalamu’alaikum Wr. Wb.</i>
        </p>
        <table width="100%">
          <tr>
            <td style="text-align:left;" width="65%">
            </td>
            <td style="text-align:center">
              An. Dekan
              <br />
              Wakil Dekan III
              <br />
              <br />
              <br />
              <img src="data:image/svg+xml;base64,{{base64_encode($qr)}}" style="display: block; margin: 0 auto;"/>
              <br />
              {{$Wadek3}}
            </td>
          </tr>
        </table>
    </div>
  </body>
</html>
