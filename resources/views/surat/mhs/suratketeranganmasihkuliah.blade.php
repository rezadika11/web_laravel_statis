<html>
  <head>
    <title>Unduh Surat Keterangan Lulus Skripsi</title>
    <style>
    body{
      min-height: 330mm;
      font-family: Sans-serif, Arial;
      font-size: 11pt;
      line-height: 1;
    }
    .page {
        background: white;
      }

    @page {
        size: F4;
        margin: 10mm;
    }

    .page-break {
      page-break-after: always;
    }

    .isisurat{
      margin-left: 80px;
      margin-right: 10mm;
      text-align: justify;

    }

    #tabel{
      border-collapse: collapse;
      width: 100%;
    }
    #tabel td {
      border: 1px solid;
      padding-left: 5px;
      padding-right: 5px;
      white-space: nowrap;
    }

    </style>
  </head>
  <body>
    <img src="{{$logo}}" width="100mm" height="100mm" style="float:left;"/>
    <div style="text-align:center; font-size:14pt; font-weight: bold;">
      KEMENTERIAN AGAMA REPUBLIK INDONESIA
    </div>
    <div style="text-align:center; font-size:12pt; font-weight: bold;">
      UNIVERSITAS ISLAM NEGERI <br />PROFESOR KIAI HAJI SAIFUDDIN ZUHRI PURWOKERTO <br /> FAKULTAS TARBIYAH DAN ILMU KEGURUAN
    </div>
    <div style="text-align:center; font-size:9pt;">
      Jalan Jenderal A. Yani, No. 40A Purwokerto 53126
      <br />Telepon (0281) 635624  Faksimili (0281) 636553
      <br />www.ftik.uinsaizu.ac.id
    </div>
    <hr style="border:0.3mm solid;"/>
    <br />
    <div class="isisurat">
      <div style="font-family: Sans-serif, Arial; font-size: 14pt; font-weight: bold; text-decoration: underline; text-align:center;">
        SURAT KETERANGAN MASIH KULIAH
      </div>
      <div style="font-family: Sans-serif, Arial; font-size: 11pt; text-align:center; padding-top:1px;">
        NOMOR : {{$Surat->no_surat}}
      </div>
      <p style="text-indent: 5mm;">
        Yang bertanda tangan di bawah ini :
      </p>
      <table border="0" style="margin-left: 10mm;">
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            1. Nama
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            : Suparjo
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            2. Jabatan
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            : Wakil Dekan Bidang Akademik FTIK
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            3. Pada Perguruan Tinggi
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            : UIN Prof. K.H. Saifuddin Zuhri Purwokerto
          </td>
        </tr>
      </table>
      <p style="text-indent:5mm">
        Menerangkan dengan sesungguhnya bahwa:
      </p>
      <table border="0" style="margin-left: 10mm;">
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            1. Nama
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            : {{$Surat->nama_mahasiswa}}
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            2. Tempat/Tanggal Lahir
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            : {{$Surat->tempat_lahir}}/{{date('d-m-Y',strtotime($Surat->tanggal_lahir))}}
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            3. Nomor Induk Mahasiswa
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            : {{$Surat->nim}}
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            4. Fakultas
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            : Fakultas Tarbiyah dan Ilmu Keguruan (FTIK)
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            5. Tahun Akademik
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            : {{$Surat->tahun_akademik}}
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            6. Alamat
          </td>
          <td style="text-indent: -1em; padding-left:1em;">
              : {{$Surat->alamat}}
          </td>
        </tr>
      </table>
      @if(isset($Surat->id_wali))
      <p style="text-indent: 5mm">
        dengan Wali Mahasiswa sebagai berikut:
      </p>
      <table border="0" style="margin-left: 10mm;">
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            1. Nama
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            : {{$Surat->nama_wali}}
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            2. NIP / NRP
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            : {{$Surat->nip}}
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            3. Pangkat/Golongan/Ruang
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            : {{$Surat->pangkat_golongan}}
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            4. Pada Instansi
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            : {{$Surat->instansi}}
          </td>
        </tr>
      </table>
      @endif
      <p style="text-indent: 5mm; margin-bottom: 0px;">
        Adalah benar merupakan mahasiswa aktif pada Fakultas Tarbiyah dan Ilmu Keguruan (FTIK) UIN Prof. K.H. Saifuddin Zuhri Purwokerto Jurusan/Prodi {{$Surat->nama_prodi}} Tahun Akademik {{$Surat->tahun_akademik}}.
      </p>
      <br />
      <p style="text-indent: 5mm; margin-top: 0px;">
        Demikian surat ini dibuat untuk dapat dipergunakan sebagaimana mestinya.
      </p>
        <table width="100%">
          <tr>
            <td style="text-align:left;" width="55%">
            </td>
            <td style="text-align:left">
              Purwokerto, {{$TanggalSurat}}
              <br />
              An. Dekan
              <br />
              {{$penandatangan['instansi']}}
              <br />
              <br />
              <br />
              <img src="data:image/svg+xml;base64,{{base64_encode($qr)}}" style="display: block; margin: 0 auto;"/>
              <br />
              {{$penandatangan['nama']}}
            </td>
          </tr>
        </table>
    </div>
  </body>
</html>
