<html>
  <head>
    <title>Unduh Surat Keterangan Pernah Kuliah</title>
    <style>
    body{
      min-height: 330mm;
      font-family: Sans-serif, Arial;
      font-size: 11pt;
      line-height: 1;
    }
    .page {
        background: white;
      }

    @page {
        size: F4;
        margin: 10mm;
    }

    .page-break {
      page-break-after: always;
    }

    .isisurat{
      margin-left: 80px;
      margin-right: 10mm;
      text-align: justify;

    }

    #tabel{
      border-collapse: collapse;
      width: 100%;
    }
    #tabel td {
      border: 1px solid;
      padding-left: 5px;
      padding-right: 5px;
      white-space: nowrap;
    }

    </style>
  </head>
  <body>
    <img src="{{$logo}}" width="100mm" height="100mm" style="float:left;"/>
    <div style="text-align:center; font-size:14pt; font-weight: bold;">
      KEMENTERIAN AGAMA REPUBLIK INDONESIA
    </div>
    <div style="text-align:center; font-size:12pt; font-weight: bold;">
      UNIVERSITAS ISLAM NEGERI <br />PROFESOR KIAI HAJI SAIFUDDIN ZUHRI PURWOKERTO <br /> FAKULTAS TARBIYAH DAN ILMU KEGURUAN
    </div>
    <div style="text-align:center; font-size:9pt;">
      Jalan Jenderal A. Yani, No. 40A Purwokerto 53126
      <br />Telepon (0281) 635624  Faksimili (0281) 636553
      <br />www.ftik.uinsaizu.ac.id
    </div>
    <hr style="border:0.3mm solid;"/>
    <br />
    <div class="isisurat">
      <div style="font-family: Sans-serif, Arial; font-size: 14pt; font-weight: bold; text-decoration: underline; text-align:center;">
        SURAT KETERANGAN PERNAH KULIAH
      </div>
      <div style="font-family: Sans-serif, Arial; font-size: 11pt; text-align:center; padding-top:1px;">
        NOMOR : {{$Surat->no_surat}}
      </div>
      <p style="text-indent: 5mm;">
        Yang bertanda tangan di bawah ini :
      </p>
      <table border="0" style="margin-left: 10mm;">
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            Nama
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            :
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            Dr. H. Suwito, M.Ag.
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            NIP
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            :
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            19710424 199903 1 002
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            Jabatan
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            :
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            Dekan FTIK UIN Prof. K.H. Saifuddin Zuhri Purwokerto
          </td>
        </tr>
        <tr>
          <td colspan="3" style="padding-right:2px;">
            <p style="margin-left:-5mm;">
              Menerangkan dengan sesungguhnya bahwa:
            </p>
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            Nama
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            :
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            {{$Surat->nama_mahasiswa}}
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            NIM
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            :
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            {{$Surat->nim}}
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            Program Studi
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            :
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            {{$Surat->nama_prodi}}
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            Angkatan Tahun
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            :
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            {{$Surat->angkatan}}
          </td>
        </tr>
        <tr>
          <td style="padding-left:2px; padding-right:2px;">
            Alamat
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            :
          </td>
          <td style="padding-left:2px; padding-right:2px;">
            {{$Surat->alamat}}
          </td>
        </tr>
      </table>
      <p style="text-indent: 5mm; margin-bottom: 0px; line-height: 1.15;">
        Saudara tersebut di atas benar-benar pernah kuliah pada Fakultas Tarbiyah UIN Prof. K.H. Saifuddin Zuhri Purwokerto Jurusan {{$Surat->nama_prodi}} dimulai Tahun Akademik {{$Surat->tahun_akademik_masuk}} dan berdasarkan Peraturan Presiden RI No. 139 Tahun 2014 telah alih status menjadi IAIN Purwokerto dan atau berdasarkan Peraturan Presiden RI No. 41 Tahun 2021 telah alih status menjadi UIN Prof. K.H. Saifuddin Zuhri Purwokerto, serta saudara tersebut berhenti kuliahnya pada semester {{$SemesterKeluar}} Tahun Akademik {{$Surat->tahun_akademik_keluar}}.
      </p>
      <p style="text-indent: 5mm; margin-bottom: 0px; line-height: 1.15;">
        Bahwa berdasarkan catatan yang dimiliki pada Administrasi FTIK UIN Prof. K.H. Saifuddin Zuhri, Sdr. tersebut telah menyelesaikan beban studi sebanyak {{$Surat->selesai_beban_studi}}% dari keseluruhan beban studi yang ditempuh selama {{$LamaSemesterKuliah}} semester, sesuai dengan transkrip nilai yang dimilikinya sebagai hak darinya selama menjadi mahasiswa di IAIN Purwokerto.
      <p style="text-indent: 5mm; margin-bottom: 0px; line-height: 1.15;">
        Surat keterangan ini dibuat atas permohonan yang bersangkutan sebagai salah satu persyaratan melanjutkan/menyelesaikan jenjang Strata 1 (S1) Prodi {{$Surat->prodi_tujuan}} pada Perguruan Tinggi Islam sebagai berikut:
      </p>
      <br />
        <table border="0" style="margin-left: 10mm;">
          <tr>
            <td style="padding-left:2px; padding-right:2px;">
              Nama Perguruan Tinggi
            </td>
            <td style="padding-left:2px; padding-right:2px;">
              :
            </td>
            <td style="padding-left:2px; padding-right:2px;">
              {{$Surat->pt_tujuan}}
            </td>
          </tr>
          <tr>
            <td style="padding-left:2px; padding-right:2px;">
              Alamat
            </td>
            <td style="padding-left:2px; padding-right:2px;">
              :
            </td>
            <td style="text-indent: -1em; padding-left:1em;">
              {{$Surat->alamat_pt_tujuan}}
            </td>
          </tr>
        </table>
      <p style="text-indent: 5mm; margin-bottom: 0px; line-height: 1.15;">
        Demikian surat keterangan ini dibuat dan dapat dipergunakan sebagaimana mestinya.
      </p>
      <br />
        <table width="100%">
          <tr>
            <td style="text-align:left;" width="65%">
            </td>
            <td style="text-align:left">
              Purwokerto, {{$TanggalSurat}}
              <br />
              An. Dekan
              <br />
              Wakil Dekan I
              <br />
              <br />
              <br />
              <img src="data:image/svg+xml;base64,{{base64_encode($qr)}}" style="display: block; margin: 0 auto;"/>
              <br />
              <br />
              {{$Wadek1}}
            </td>
          </tr>
        </table>
    </div>
  </body>
</html>
