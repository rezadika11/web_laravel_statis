<html>
  <head>
    <title>Unduh Surat Tugas</title>
    <style>
    body{
      min-height: 330mm;
      font-family: Sans-serif, Arial;
      font-size: 11pt;
      line-height: 1;
    }
    .page {
        background: white;
      }

    @page {
        size: F4;
        margin: 10mm;
    }

    .page-break {
      page-break-after: always;
    }

    .isisurat{
      margin-left: 80px;
    }

    #tabel{
      border-collapse: collapse;
      width: 100%;
    }
    #tabel td {
      border: 1px solid;
      padding-left: 5px;
      padding-right: 5px;
      white-space: nowrap;
    }

    </style>
  </head>
  <body>
    <img src="{{$logo}}" width="100mm" height="100mm" style="float:left;"/>
    <div style="text-align:center; font-size:14pt; font-weight: bold;">
      KEMENTERIAN AGAMA REPUBLIK INDONESIA
    </div>
    <div style="text-align:center; font-size:12pt; font-weight: bold;">
      UNIVERSITAS ISLAM NEGERI <br />PROFESOR KIAI HAJI SAIFUDDIN ZUHRI PURWOKERTO <br /> FAKULTAS TARBIYAH DAN ILMU KEGURUAN
    </div>
    <div style="text-align:center; font-size:9pt;">
      Jalan Jenderal A. Yani, No. 40A Purwokerto 53126
      <br />Telepon (0281) 635624  Faksimili (0281) 636553
      <br />www.ftik.uinsaizu.ac.id
    </div>
    <hr style="border:0.3mm solid;"/>
    <br />

    <div class="isisurat">
      <div style="font-family: Sans-serif, Arial; font-size: 14pt; font-weight: bold; text-decoration: underline; text-align:center;">
        SURAT TUGAS
      </div>
      <div style="font-family: Sans-serif, Arial; font-size: 11pt; text-align:center; padding-top:1px;">
        NOMOR : {{$Surat->no_surat}}
      </div>
      <br />
      <br />
      <p>
        Yang bertanda tangan di bawah ini, Dekan Fakultas Tarbiyah dan Ilmu Keguruan (FTIK) UIN Prof. K.H. Saifuddin Zuhri Purwokerto memberi tugas kepada:
      </p>
      <br />
      <table id="tabel">
          <thead>
            <tr style="text-align:center;">
              <td style="width:2em;">
                No.
              </td>
              <td>
                Nama
              </td>
              <td>
                NIM
              </td>
              <td>
                Program Studi
              </td>
            </tr>
          </thead>
          <tbody>
            @foreach($Peserta as $pst)
              <tr>
                <td>
                  {{$no++}}
                </td>
                <td>
                  {{$pst->nama}}
                </td>
                <td>
                  {{$pst->nim}}
                </td>
                <td>
                  {{$pst->nama_prodi}}
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        <br />
        <p style="text-align:justify">
        <br/ >
        Untuk mengikuti <strong>{{$Surat->kegiatan}}</strong> yang diselenggarakan oleh {{$Surat->penyelenggara}} pada tanggal {{date('d-m-Y',strtotime($Surat->tanggal_mulai))}} – {{date('d-m-Y',strtotime($Surat->tanggal_selesai))}}.
        <br />
        <br />
        Demikian agar yang bersangkutan menjadi maklum dan dilaksanakan dengan sebaik-baiknya.
        <br />
        </p>
        <table width="100%">
          <tr>
            <td style="text-align:left;" width="65%">
            </td>
            <td style="text-align:left">
              Purwokerto, {{$TanggalSurat}}
              <br />
              An. Dekan
              <br />
              Wakil Dekan III
              <br />
              <br />
              <br />
              <img src="data:image/svg+xml;base64,{{base64_encode($qr)}}" style="display: block; margin: 0 auto;"/>
              <br />
              {{$Wadek3}}
            </td>
          </tr>
        </table>
    </div>
  </body>
</html>
