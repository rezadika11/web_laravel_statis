<html>
  <head>
    <title>Unduh Surat Ijin Observasi Kelas</title>
    <style>
    body{
      min-height: 330mm;
      font-family: Sans-serif, Arial;
      font-size: 11pt;
      line-height: 1;
    }
    .page {
        background: white;
      }

    @page {
        size: F4;
        margin: 10mm;
    }

    .page-break {
      page-break-after: always;
    }

    .isisurat{
      margin-left: 80px;
    }

    #tabel{
      border-collapse: collapse;
      width: 100%;
    }
    #tabel td {
      border: 1px solid;
      padding-left: 5px;
      padding-right: 5px;
      white-space: nowrap;
    }

    </style>
  </head>
  <body>
    <img src="{{$logo}}" width="100mm" height="100mm" style="float:left;"/>
    <div style="text-align:center; font-size:14pt; font-weight: bold;">
      KEMENTERIAN AGAMA REPUBLIK INDONESIA
    </div>
    <div style="text-align:center; font-size:12pt; font-weight: bold;">
      UNIVERSITAS ISLAM NEGERI <br />PROFESOR KIAI HAJI SAIFUDDIN ZUHRI PURWOKERTO <br /> FAKULTAS TARBIYAH DAN ILMU KEGURUAN
    </div>
    <div style="text-align:center; font-size:9pt;">
      Jalan Jenderal A. Yani, No. 40A Purwokerto 53126
      <br />Telepon (0281) 635624  Faksimili (0281) 636553
      <br />www.ftik.uinsaizu.ac.id
    </div>
    <hr style="border:0.3mm solid;"/>
    <br />
      <table border="0" width="100%">
        <tr>
          <td style="width:75px;">
            Nomor
          </td>
          <td>
            : {{$Surat->no_surat}}
          </td>
          <td style="text-align:right;" width="25%">
            {{$TanggalSurat}}
          </td>
        </tr>
        <tr>
          <td>
            Lamp.
          </td>
          <td colspan="2">
            : -
          </td>
        </tr>
        <tr>
          <td>
            Hal
          </td>
          <td colspan="2">
            : <strong>Permohonan Ijin Observasi Kelas</strong>
          </td>
        </tr>
      </table>
    <div class="isisurat">
      <p style="text-align:justify">
        Kepada<br />
        Yth. {{$Surat->kepada}}<br />
        di Tempat
        <br />
        <br />
      </p>
      <p style="text-align:justify">
        <strong><i>Assalamu’alaikum Wr. Wb.</i></strong>
        <br />
        <br />
        Dalam rangka meningkatkan pemahaman dan pendalaman materi perkuliahan bagi mahasiswa pada:
        <br />
      </p>
        <table border="0">
          <tr>
            <td style="width:200px;">
              Mata Kuliah
            </td>
            <td>
              : {{$Surat->mata_kuliah}}
            </td>
          </tr>
          <tr>
            <td>
              Semester
            </td>
            <td>
              : {{$Surat->semester}}
            </td>
          </tr>
          <tr>
            <td>
              Program Studi
            </td>
            <td>
              : {{$Surat->nama_prodi}}
            </td>
          </tr>
          <tr>
            <td>
              Dosen Pengampu
            </td>
            <td>
              : {{$Surat->dosen_pengampu}}
            </td>
          </tr>
        </table>
        <p style="text-align:justify">
        Maka dengan ini kami mohon bantuan bapak/ibu untuk berkenan menerima, mengizinkan dan membantu mahasiswa kami untuk melaksanakan observasi tentang: "{{$Surat->tema}}"
        <br />
        <br />
        Adapun mahasiswa yang akan melaksanakan observasi adalah:
        </p>
        <table id="tabel">
          <thead>
            <tr style="text-align:center;">
              <td style="width:2em;">
                No
              </td>
              <td>
                Nama
              </td>
              <td>
                NIM
              </td>
            </tr>
          </thead>
          <tbody>
            @foreach($Peserta as $pst)
              <tr>
                <td>
                  {{$no++}}
                </td>
                <td>
                  {{$pst->nama}}
                </td>
                <td>
                  {{$pst->nim}}
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        <p style="text-align:justify">
        <br/ >
        <br/ >
        Observasi tersebut akan dilaksanakan pada tanggal {{date('d-m-Y',strtotime($Surat->tanggal_mulai))}} s.d {{date('d-m-Y',strtotime($Surat->tanggal_selesai))}}
        <br />
        <br />
        Demikian permohonan ini disampaikan, atas kesediaan dan kerjasamanya kami sampaikan terima kasih.
        <br />
        <br />
        <strong><i>Wassalamu’alaikum Wr. Wb.</i></strong>
        </p>
        <table width="100%">
          <tr>
            <td style="text-align:left;" width="65%">
            </td>
            <td style="text-align:center">
              An. Dekan <br />
              Ketua Jurusan {{$Kajur->nama_jurusan}}
              <br />
              <br />
              <br />
              <img src="data:image/svg+xml;base64,{{base64_encode($qr)}}" style="display: block; margin: 0 auto;"/>
              <br />
              {{$Kajur->nama_dosen}}
            </td>
          </tr>
        </table>
    </div>
  </body>
</html>
