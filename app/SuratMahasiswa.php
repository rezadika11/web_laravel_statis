<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-12T20:54:11+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-12T20:54:11+07:00
# @Copyright: https://annaya.id


namespace App;

use DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class SuratMahasiswa
{
  public static function Semester($angka=NULL)
  {
    $semester =[
      1=>'Satu',
      2=>'Dua',
      3=>'Tiga',
      4=>'Empat',
      5=>'Lima',
      6=>'Enam',
      7=>'Tujuh',
      8=>'Delapan',
      9=>'Sembilan',
      10=>'Sepuluh',
      11=>'Sebelas',
      12=>'Dua Belas',
      13=>'Tiga Belas',
      14=>'Empat Belas',
    ];

    if($angka == NULL)
    {
      return $semester;
    }
    else {
      return $angka .' ('.$semester[$angka].')';
    }
  }

  public static function TahunAkademik(){
    $TahunSekarang = date('Y');
    $TahunAkademik = [
      ($TahunSekarang-1)."/".$TahunSekarang,
      $TahunSekarang."/".($TahunSekarang+1),
    ];

    return $TahunAkademik;
  }

  public static function TanggalSurat($tanggal_surat){
    $bulan = [
      '1'=>'Januari',
      '2'=>'Februari',
      '3'=>'Maret',
      '4'=>'April',
      '5'=>'Mei',
      '6'=>'Juni',
      '7'=>'Juli',
      '8'=>'Agustus',
      '9'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember',
    ];
    return  date('d',strtotime($tanggal_surat)).' '.$bulan[date('n',strtotime($tanggal_surat))].' '.date('Y',strtotime($tanggal_surat));
  }

  public static function JurusanDanKetua($id_jurusan, $tanggal_surat){
    $KaJur = DB::table('ketua_jurusan')
    ->join('dosen','dosen.id_dosen','ketua_jurusan.id_dosen_kajur')
    ->join('jurusan','jurusan.id_jurusan','ketua_jurusan.id_jurusan')
    ->where('ketua_jurusan.id_jurusan', $id_jurusan)
    ->where(function($q) use($tanggal_surat){
      $q->where([['mulai_menjabat','<=',$tanggal_surat],['selesai_menjabat','>',$tanggal_surat]])
      ->orwhere([['mulai_menjabat','<=',$tanggal_surat],['selesai_menjabat',NULL]]);
    })
    ->select('dosen.nama_dosen','jurusan.nama_jurusan')
    ->first();

    return $KaJur;
  }

  public static function DekanWadek($dekan_wadek,$tanggal_surat){
    $DekanWadek = DB::table('dekan_wadek')
    ->join('dosen','dosen.id_dosen','dekan_wadek.id_dosen')
    ->where('dekan_wadek.dekan_wadek',$dekan_wadek)
    ->where(function($q) use($tanggal_surat){
      $q->where([['mulai_menjabat','<=',$tanggal_surat],['selesai_menjabat','>',$tanggal_surat]])
      ->orwhere([['mulai_menjabat','<=',$tanggal_surat],['selesai_menjabat',NULL]]);
    })
    ->select('dosen.nama_dosen')
    ->first();

    return $DekanWadek;
  }

  public static function CetakSuratIjinRisetIndividu($id_surat){
    $SuratSiapCetak = DB::table('surat_ijin_riset_individu as surat')
    ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
    ->join('prodi','prodi.id_prodi','mahasiswa.id_prodi')
    ->where([['surat.no_surat','!=',NULL],['surat.tanggal_surat','!=',NULL],['surat.unique_key','!=',NULL],['surat.id_surat_ijin_riset_individu',$id_surat]])
    ->select('mahasiswa.nama_mahasiswa','mahasiswa.nim','prodi.nama_prodi','surat.kepada','surat.kec','surat.id_surat_ijin_riset_individu','surat.no_surat','surat.tanggal_surat','surat.semester','mahasiswa.alamat','surat.judul','surat.obyek','surat.tempat','surat.unique_key','surat.tanggal_mulai','surat.tanggal_selesai','surat.metode_penelitian','surat.id_surat_ijin_riset_individu as id_surat','prodi.id_jurusan')
    ->first();

    $Tembusan = DB::table('tembusan_surat_ijin_riset_individu as tembusan')
    ->select('tembusan.kepada_tembusan')
    ->where('tembusan.id_surat_ijin_riset_individu',$id_surat)
    ->get();

    $KaJur = SuratMahasiswa::JurusanDanKetua($SuratSiapCetak->id_jurusan,$SuratSiapCetak->tanggal_surat);
    $SuratSiapCetak->semester = SuratMahasiswa::Semester($SuratSiapCetak->semester);

    $data = [
      'view'=>'surat.mhs.suratijinrisetindividu',
      'data'=>[
        'logo'=>storage_path('img/uin.png'),
        'Surat'=>$SuratSiapCetak,
        'TanggalSurat'=>SuratMahasiswa::TanggalSurat($SuratSiapCetak->tanggal_surat),
        'Tembusan'=>$Tembusan,
        'Kajur'=>$KaJur,
        'qr'=>QrCode::format('svg')->size(125)->generate(route('validasi.SuratIjinRisetIndividu',['id_surat'=>$SuratSiapCetak->id_surat,'unique_key'=>$SuratSiapCetak->unique_key])),
      ]
    ];

    return $data;
  }

  public static function CetakSuratIjinObservasiPendahuluan($id_surat){
    $SuratSiapCetak = DB::table('surat_ijin_observasi_pendahuluan as surat')
    ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
    ->join('prodi','prodi.id_prodi','mahasiswa.id_prodi')
    ->where([['surat.no_surat','!=',NULL],['surat.tanggal_surat','!=',NULL],['surat.unique_key','!=',NULL],['surat.id_surat_ijin_observasi_pendahuluan',$id_surat]])
    ->select('mahasiswa.nama_mahasiswa','mahasiswa.nim','prodi.nama_prodi','surat.kepada','surat.id_surat_ijin_observasi_pendahuluan as id_surat','surat.no_surat','surat.tanggal_surat','surat.semester','surat.obyek','surat.lokasi','surat.unique_key','surat.tanggal_observasi','surat.tahun_akademik','prodi.id_jurusan')
    ->first();

    $KaJur = SuratMahasiswa::JurusanDanKetua($SuratSiapCetak->id_jurusan,$SuratSiapCetak->tanggal_surat);

    $SuratSiapCetak->semester = SuratMahasiswa::Semester($SuratSiapCetak->semester);

    $data = [
      'view'=>'surat.mhs.suratijinobservasipendahuluan',
      'data'=>[
        'logo'=>storage_path('img/uin.png'),
        'Surat'=>$SuratSiapCetak,
        'TanggalSurat'=>SuratMahasiswa::TanggalSurat($SuratSiapCetak->tanggal_surat),
        'Kajur'=>$KaJur,
        'qr'=>QrCode::format('svg')->size(125)->generate(route('validasi.SuratIjinObservasiPendahuluan',['id_surat'=>$SuratSiapCetak->id_surat,'unique_key'=>$SuratSiapCetak->unique_key])),
      ]
    ];

    return $data;
  }

  public static function CetakSuratIjinObservasiKelas($id_surat){
    $SuratSiapCetak = DB::table('surat_ijin_observasi_kelas as surat')
    ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
    ->join('prodi','prodi.id_prodi','mahasiswa.id_prodi')
    ->where([['surat.no_surat','!=',NULL],['surat.tanggal_surat','!=',NULL],['surat.unique_key','!=',NULL],['surat.id_surat_ijin_observasi_kelas',$id_surat]])
    ->select('surat.kepada','surat.tanggal_surat','surat.no_surat','surat.mata_kuliah','surat.semester','prodi.nama_prodi','surat.dosen_pengampu','surat.unique_key','surat.tanggal_mulai','surat.tanggal_selesai','surat.id_surat_ijin_observasi_kelas as id_surat','surat.tema','prodi.id_jurusan')
    ->first();

    $KaJur = SuratMahasiswa::JurusanDanKetua($SuratSiapCetak->id_jurusan,$SuratSiapCetak->tanggal_surat);

    $Peserta = DB::table('peserta_surat_ijin_observasi_kelas as peserta')
    ->select('peserta.nama','peserta.nim')
    ->where('peserta.id_surat_ijin_observasi_kelas',$id_surat)
    ->get();

    $SuratSiapCetak->semester = SuratMahasiswa::Semester($SuratSiapCetak->semester);

    $data = [
      'view'=>'surat.mhs.suratijinobservasikelas',
      'data'=>[
        'logo'=>storage_path('img/uin.png'),
        'Surat'=>$SuratSiapCetak,
        'Peserta'=>$Peserta,
        'no'=>1,
        'TanggalSurat'=>SuratMahasiswa::TanggalSurat($SuratSiapCetak->tanggal_surat),
        'Kajur'=>$KaJur,
        'qr'=>QrCode::format('svg')->size(125)->generate(route('validasi.SuratIjinObservasiKelas',['id_surat'=>$SuratSiapCetak->id_surat,'unique_key'=>$SuratSiapCetak->unique_key])),
      ]
    ];

    return $data;
  }

  public static function CetakSuratKeteranganLulusSkripsi($id_surat){
    $SuratSiapCetak = DB::table('surat_keterangan_lulus_skripsi as surat')
    ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
    ->join('prodi','prodi.id_prodi','mahasiswa.id_prodi')
    ->where([['surat.no_surat','!=',NULL],['surat.tanggal_surat','!=',NULL],['surat.unique_key','!=',NULL],['surat.id_surat_keterangan_lulus_skripsi',$id_surat]])
    ->select('surat.tanggal_surat','surat.no_surat','surat.tanggal_ujian','surat.tahun_akademik','prodi.nama_prodi','surat.unique_key','surat.id_surat_keterangan_lulus_skripsi as id_surat','mahasiswa.nama_mahasiswa','mahasiswa.nim','prodi.id_jurusan')
    ->first();

    $KaJur = SuratMahasiswa::JurusanDanKetua($SuratSiapCetak->id_jurusan,$SuratSiapCetak->tanggal_surat);

    $data = [
      'view'=>'surat.mhs.suratketeranganlulusskripsi',
      'data'=>[
        'logo'=>storage_path('img/uin.png'),
        'Surat'=>$SuratSiapCetak,
        'no'=>1,
        'TanggalUjian'=>SuratMahasiswa::TanggalSurat($SuratSiapCetak->tanggal_ujian),
        'TanggalSurat'=>SuratMahasiswa::TanggalSurat($SuratSiapCetak->tanggal_surat),
        'Kajur'=>$KaJur,
        'qr'=>QrCode::format('svg')->size(125)->generate(route('validasi.SuratKeteranganLulusSkripsi',['id_surat'=>$SuratSiapCetak->id_surat,'unique_key'=>$SuratSiapCetak->unique_key])),
      ]
    ];

    return $data;
  }

  public static function CetakSuratTugas($id_surat){
    $SuratSiapCetak = DB::table('surat_tugas as surat')
    ->where([['surat.no_surat','!=',NULL],['surat.tanggal_surat','!=',NULL],['surat.unique_key','!=',NULL],['surat.id_surat_tugas',$id_surat]])
    ->select('surat.tanggal_surat','surat.no_surat','surat.tanggal_mulai','surat.tanggal_selesai','surat.unique_key','surat.id_surat_tugas as id_surat','surat.kegiatan','surat.penyelenggara')
    ->first();

    $Peserta = DB::table('peserta_surat_tugas as peserta')
    ->join('prodi','prodi.id_prodi','peserta.id_prodi')
    ->select('peserta.nama','peserta.nim','prodi.nama_prodi')
    ->where('peserta.id_surat_tugas',$id_surat)
    ->get();

    $Wadek3 =  SuratMahasiswa::DekanWadek('wadek3',$SuratSiapCetak->tanggal_surat);

    $data = [
      'view'=>'surat.mhs.surattugas',
      'data'=>[
        'logo'=>storage_path('img/uin.png'),
        'Surat'=>$SuratSiapCetak,
        'no'=>1,
        'Peserta'=>$Peserta,
        'Wadek3'=>$Wadek3->nama_dosen,
        'TanggalSurat'=>SuratMahasiswa::TanggalSurat($SuratSiapCetak->tanggal_surat),
        'qr'=>QrCode::format('svg')->size(125)->generate(route('validasi.SuratTugas',['id_surat'=>$SuratSiapCetak->id_surat,'unique_key'=>$SuratSiapCetak->unique_key])),
      ]
    ];

    return $data;
  }

  public static function CetakSuratKeteranganMasihKuliah($id_surat){
    $SuratSiapCetak = DB::table('surat_keterangan_masih_kuliah as surat')
    ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
    ->join('prodi','prodi.id_prodi','mahasiswa.id_prodi')
    ->join('persetujuan_surat_keterangan_masih_kuliah as validasi_wadek1_kajur', function ($q) {
      return $q->on('validasi_wadek1_kajur.id_surat_keterangan_masih_kuliah', 'surat.id_surat_keterangan_masih_kuliah')
      ->where(function($q1){
        return $q1->where('validasi_wadek1_kajur.level', 'wadek1')
        ->orwhere('validasi_wadek1_kajur.level', 'kajur');
      });
    })
    ->leftjoin('wali_surat_keterangan_masih_kuliah as wali','wali.id_surat_keterangan_masih_kuliah','surat.id_surat_keterangan_masih_kuliah')
    ->where([['surat.no_surat','!=',NULL],['surat.tanggal_surat','!=',NULL],['surat.unique_key','!=',NULL],['surat.id_surat_keterangan_masih_kuliah',$id_surat]])
    ->select('surat.tanggal_surat','surat.no_surat','surat.id_surat_keterangan_masih_kuliah as id_surat','surat.unique_key','surat.tahun_akademik','mahasiswa.nama_mahasiswa','mahasiswa.nim','mahasiswa.tempat_lahir','mahasiswa.tanggal_lahir','mahasiswa.alamat','prodi.nama_prodi','wali.nama as nama_wali','wali.nip','wali.pangkat_golongan','wali.instansi','wali.id_wali_surat_keterangan_masih_kuliah as id_wali','validasi_wadek1_kajur.level','prodi.id_jurusan')
    ->first();

    if($SuratSiapCetak->level == 'kajur'){
      $KaJur = SuratMahasiswa::JurusanDanKetua($SuratSiapCetak->id_jurusan,$SuratSiapCetak->tanggal_surat);
      $penandatangan = [
        'instansi'=>'Ketua Jurusan '.$KaJur->nama_jurusan,
        'nama'=>$KaJur->nama_dosen,
      ];
    }elseif($SuratSiapCetak->level == 'wadek1'){
      $Wadek1 = SuratMahasiswa::DekanWadek('wadek1',$SuratSiapCetak->tanggal_surat);
      $penandatangan = [
        'instansi'=>'Wakil Dekan 1',
        'nama'=>$Wadek1->nama_dosen,
      ];
    }

    $data = [
      'view'=>'surat.mhs.suratketeranganmasihkuliah',
      'data'=>[
        'logo'=>storage_path('img/uin.png'),
        'Surat'=>$SuratSiapCetak,
        'TanggalSurat'=>SuratMahasiswa::TanggalSurat($SuratSiapCetak->tanggal_surat),
        'penandatangan'=>$penandatangan,
        'qr'=>QrCode::format('svg')->size(125)->generate(route('validasi.SuratKeteranganMasihKuliah',['id_surat'=>$SuratSiapCetak->id_surat,'unique_key'=>$SuratSiapCetak->unique_key])),
      ]
    ];

    return $data;
  }

  public static function CetakSuratKeteranganPernahKuliah($id_surat){
    $SuratSiapCetak = DB::table('surat_keterangan_pernah_kuliah as surat')
    ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
    ->join('prodi','prodi.id_prodi','mahasiswa.id_prodi')
    ->where([['surat.no_surat','!=',NULL],['surat.tanggal_surat','!=',NULL],['surat.unique_key','!=',NULL],['surat.id_surat_keterangan_pernah_kuliah',$id_surat]])
    ->select('surat.no_surat','surat.tanggal_surat','surat.id_surat_keterangan_pernah_kuliah as id_surat','mahasiswa.nama_mahasiswa','mahasiswa.nim','prodi.nama_prodi','surat.angkatan','mahasiswa.alamat','surat.tahun_akademik_masuk','surat.semester_keluar','surat.tahun_akademik_keluar','surat.selesai_beban_studi','surat.lama_semester_kuliah','surat.prodi_tujuan','surat.pt_tujuan','surat.alamat_pt_tujuan','surat.unique_key')
    ->first();

    switch($SuratSiapCetak->semester_keluar){
      case '1' :
      $SemesterKeluar = 'Ganjil';
      break;
      case '2' :
      $SemesterKeluar = 'Genap';
      break;
    }

    $Wadek1 = SuratMahasiswa::DekanWadek('wadek1',$SuratSiapCetak->tanggal_surat);

    $data = [
      'view'=>'surat.mhs.suratketeranganpernahkuliah',
      'data'=>[
        'logo'=>storage_path('img/uin.png'),
        'Surat'=>$SuratSiapCetak,
        'SemesterKeluar'=>$SemesterKeluar,
        'LamaSemesterKuliah'=>SuratMahasiswa::Semester($SuratSiapCetak->lama_semester_kuliah),
        'TanggalSurat'=>SuratMahasiswa::TanggalSurat($SuratSiapCetak->tanggal_surat),
        'Wadek1'=>$Wadek1->nama_dosen,
        'qr'=>QrCode::format('svg')->size(125)->generate(route('validasi.SuratKeteranganPernahKuliah',['id_surat'=>$SuratSiapCetak->id_surat,'unique_key'=>$SuratSiapCetak->unique_key])),
      ]
    ];

    return $data;
  }

  public static function CetakSuratKeteranganPenggantiIJazah($id_surat){
    $SuratSiapCetak = DB::table('surat_keterangan_pengganti_ijazah as surat')
    ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
    ->join('prodi','prodi.id_prodi','mahasiswa.id_prodi')
    ->where([['surat.no_surat','!=',NULL],['surat.tanggal_surat','!=',NULL],['surat.unique_key','!=',NULL],['surat.id_surat_keterangan_pengganti_ijazah',$id_surat]])
    ->select('surat.no_surat','surat.tanggal_surat','surat.id_surat_keterangan_pengganti_ijazah as id_surat','mahasiswa.nama_mahasiswa','mahasiswa.nim','prodi.nama_prodi','mahasiswa.tanggal_lahir','mahasiswa.tempat_lahir','mahasiswa.alamat','prodi.nama_prodi','surat.nomor_ijazah','surat.tanggal_ijazah','surat.tahun_kehilangan','surat.unique_key')
    ->first();

    $Dekan = SuratMahasiswa::DekanWadek('dekan',$SuratSiapCetak->tanggal_surat);

    $data = [
      'view'=>'surat.mhs.suratketeranganpenggantiijazah',
      'data'=>[
        'logo'=>storage_path('img/uin.png'),
        'Surat'=>$SuratSiapCetak,
        'TanggalIjazah'=>SuratMahasiswa::TanggalSurat($SuratSiapCetak->tanggal_ijazah),
        'TanggalSurat'=>SuratMahasiswa::TanggalSurat($SuratSiapCetak->tanggal_surat),
        'Dekan'=>$Dekan->nama_dosen,
        'qr'=>QrCode::format('svg')->size(125)->generate(route('validasi.SuratKeteranganPenggantiIjazah',['id_surat'=>$SuratSiapCetak->id_surat,'unique_key'=>$SuratSiapCetak->unique_key])),
      ]
    ];

    return $data;
  }

  public static function CetakSuratPermohonanPenghargaan($id_surat){
    $SuratSiapCetak = DB::table('surat_permohonan_penghargaan as surat')
    ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
    ->join('prodi','prodi.id_prodi','mahasiswa.id_prodi')
    ->where([['surat.no_surat','!=',NULL],['surat.tanggal_surat','!=',NULL],['surat.unique_key','!=',NULL],['surat.id_surat_permohonan_penghargaan',$id_surat]])
    ->select('surat.unique_key','surat.no_surat','surat.tanggal_surat','surat.nama_perlombaan', 'surat.tanggal_mulai_pelaksanaan','surat.tanggal_selesai_pelaksanaan','surat.tempat','surat.juara','surat.kelompok','surat.id_surat_permohonan_penghargaan as id_surat','mahasiswa.nama_mahasiswa','mahasiswa.nim','mahasiswa.alamat','mahasiswa.no_hp','prodi.nama_prodi')
    ->first();

    $PilihJuara = [
      1 => 'Satu',
      2 => 'Dua',
      3 => 'Tiga',
    ];

    $Wadek3 = SuratMahasiswa::DekanWadek('wadek3',$SuratSiapCetak->tanggal_surat);

    $data = [
      'view'=>'surat.mhs.suratpermohonanpenghargaan',
      'data'=>[
        'logo'=>storage_path('img/uin.png'),
        'Surat'=>$SuratSiapCetak,
        'TanggalSurat'=>SuratMahasiswa::TanggalSurat($SuratSiapCetak->tanggal_surat),
        'TanggalPelaksanaan'=>SuratMahasiswa::TanggalSurat($SuratSiapCetak->tanggal_mulai_pelaksanaan).' - '.SuratMahasiswa::TanggalSurat($SuratSiapCetak->tanggal_selesai_pelaksanaan),
        'Juara'=>$SuratSiapCetak->juara.' ('.$PilihJuara[$SuratSiapCetak->juara].')',
        'Wadek3'=>$Wadek3->nama_dosen,
        'qr'=>QrCode::format('svg')->size(125)->generate(route('validasi.SuratPermohonanPenghargaan',['id_surat'=>$SuratSiapCetak->id_surat,'unique_key'=>$SuratSiapCetak->unique_key])),
      ]
    ];

    return $data;

  }

  public static function FilterSurat($request , $query, $prodi = []){
    $filter_id_prodi = $request->prodi;
    $filter_nim_nama = $request->nim_nama;
    $filter_tanggal_surat = $request->tanggal_surat;
    $filter_kepada = $request->kepada;
    $filter = null;
    $key = null;

    $JenisFilter  = [
      'nim_nama'=>'Nim dan Nama',
      'tanggal_surat'=>'Tanggal Surat',
      'kepada'=>'Kepada',
    ];

    $PilihFilterProdi = DB::table('prodi')
    ->select('prodi.nama_prodi','prodi.id_prodi')
    ->wherein('prodi.id_prodi',$prodi);

    $PilihFilterProdi = $PilihFilterProdi->get();

    if(isset($filter_id_prodi)){
      $query = $query
      ->where('mahasiswa.id_prodi',$filter_id_prodi);
    }

    if(isset($filter_nim_nama)){
      $query = $query
      ->where(function($q) use ($filter_nim_nama){
        return $q->where('mahasiswa.nim','like','%'.$filter_nim_nama.'%')
        ->orwhere('mahasiswa.nama_mahasiswa','like','%'.$filter_nim_nama.'%');
      });
      $filter = 'nim_nama';
      $key = $filter_nim_nama;
    }

    if(isset($filter_tanggal_surat)){
      $query = $query
      ->where('surat.tanggal_surat',$filter_tanggal_surat);
      $filter = 'tanggal_surat';
      $key = $filter_tanggal_surat;
    }

    if(isset($filter_kepada)){
      $query = $query
      ->where('surat.kepada','like','%'.$filter_kepada.'%');
      $filter = 'kepada';
      $key = $filter_kepada;
    }

    return [
      'query'=>$query,
      'filter'=>$filter,
      'key'=>$key,
      'filter_id_prodi'=>$filter_id_prodi,
      'jenis_filter'=>$JenisFilter,
      'pilih_filter_prodi'=>$PilihFilterProdi,
    ];
  }

}
