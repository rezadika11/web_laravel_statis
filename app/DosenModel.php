<?php
# @Author: Annaya Solusi Infomatika <annaya>
# @Date:   2022-03-11T06:50:26+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   annaya
# @Last modified time: 2022-03-11T06:50:26+07:00
# @Copyright: https://annaya.id




namespace App;

use Illuminate\Database\Eloquent\Model;

class DosenModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //   'id_user', 'kode_prodi', 'nidn','nip','nama_dosen','level_dosen'
    // ];

    protected $guarded = ['id_prodi'];



    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    /*protected $casts = [
          'email_verified_at' => 'datetime',
      ];*/

    protected $table = "dosen";
    protected $primaryKey = "id_dosen";
}
