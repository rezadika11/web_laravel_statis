<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-15T09:47:51+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-23T20:11:25+07:00
# @Copyright: https://annaya.id

namespace App\Http\Controllers\Wadek1\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\SuratMahasiswa;
use Illuminate\Support\Str;
use PDF;
use App\DetilUser;

class SuratKeteranganPernahKuliahController extends Controller
{
  private $tabel_surat = 'surat_keterangan_pernah_kuliah';
  private $tabel_persetujuan = 'persetujuan_surat_keterangan_pernah_kuliah';

  public function __construct()
  {
    $this->middleware(['auth', 'level:wadek1']);
  }

  public function PermintaanSuratKeteranganPernahKuliah()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1', function ($q) {
        return $q->on('validasi_wadek1.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek1.level', 'wadek1');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->where([['validasi_wadek1.status_persetujuan', 0], ['surat.unique_key', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.no_surat', 'surat.tanggal_surat', 'validasi_wadek1.status_persetujuan as status_persetujuan_wadek1', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.angkatan', 'surat.tahun_akademik_masuk', 'surat.semester_keluar', 'surat.tahun_akademik_keluar', 'surat.lama_semester_kuliah')
      ->paginate(4);

    $no = 1;

    return view('wadek1.mhs.suratketeranganpernahkuliah.permintaansuratketeranganpernahkuliah', compact('PermohonanSurat', 'no'));
  }

  public function ProsesPermintaanSuratKeteranganPernahKuliah($id_surat)
  {
    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1', function ($q) {
        return $q->on('validasi_wadek1.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek1.level', 'wadek1');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->where([['validasi_wadek1.status_persetujuan', 0], ['surat.unique_key', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.no_surat', '!=', NULL], ['surat.tanggal_surat', '!=', NULL]])
      ->select('surat.no_surat', 'surat.tanggal_surat', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.angkatan', 'mahasiswa.alamat', 'surat.tahun_akademik_masuk', 'surat.semester_keluar', 'surat.tahun_akademik_keluar', 'surat.selesai_beban_studi', 'surat.lama_semester_kuliah', 'surat.prodi_tujuan', 'surat.pt_tujuan', 'surat.alamat_pt_tujuan')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    switch ($Surat->semester_keluar) {
      case '1':
        $SemesterKeluar = 'Ganjil';
        break;
      case '2':
        $SemesterKeluar = 'Genap';
        break;
    }

    $LamaSemesterKuliah = SuratMahasiswa::Semester($Surat->lama_semester_kuliah);

    return view('wadek1.mhs.suratketeranganpernahkuliah.prosespermintaansuratketeranganpernahkuliah', compact('Surat', 'SemesterKeluar', 'LamaSemesterKuliah'));
  }

  public function SimpanProsesPermintaanSuratKeteranganPernahKuliah(request $request, $id_surat)
  {
    if ($request->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    }

    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1', function ($q) {
        return $q->on('validasi_wadek1.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek1.level', 'wadek1');
      })
      ->where([['validasi_wadek1.status_persetujuan', 0], ['surat.unique_key', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.no_surat', '!=', NULL], ['surat.tanggal_surat', '!=', NULL]]);

    if ($Surat->count() == 0) {
      return redirect(route('wadek1.PermintaanSuratKeteranganPernahKuliah'))
        ->with('simpan_gagal', 'Surat Sudah Divalidasi atau tidak ditemukan');
    }

    DB::beginTransaction();
    try {
      $Surat->update([
        'validasi_wadek1.status_persetujuan' => 1,
        'validasi_wadek1.waktu_persetujuan' => date('Y-m-d H:i:s'),
        'surat.unique_key' => Str::UUID(),
        'validasi_wadek1.id_pegawai_dosen'=>DetilUser::AmbilDetil()->id_dosen,
      ]);
      DB::commit();
      return redirect(route('wadek1.PermintaanSuratKeteranganPernahKuliah'))
        ->with('simpan_sukses', 'Surat Berhasil Ditanda Tangani.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakn Hubungi Pengembang.');
    }
  }

  public function SuratKeteranganPernahKuliahSiap()
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1', function ($q) {
        return $q->on('validasi_wadek1.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek1.level', 'wadek1');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->where([['validasi_wadek1.status_persetujuan', 1], ['surat.unique_key', '!=', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.no_surat', 'surat.tanggal_surat', 'validasi_wadek1.status_persetujuan as status_persetujuan_wadek1', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.angkatan', 'surat.tahun_akademik_masuk', 'surat.semester_keluar', 'surat.tahun_akademik_keluar', 'surat.lama_semester_kuliah')
      ->paginate(4);

    $no = 1;

    return view('wadek1.mhs.suratketeranganpernahkuliah.suratketeranganpernahkuliahsiap', compact('no', 'SuratSiapCetak'));
  }

  public function CetakSuratKeteranganPernahKuliah($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1', function ($q) {
        return $q->on('validasi_wadek1.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek1.level', 'wadek1');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_wadek1.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratKeteranganPernahKuliah($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratPernahKuliahBelumdiProses()
  {
    $count = DB::table('persetujuan_surat_keterangan_pernah_kuliah')
      ->select('status_persetujuan', 'level')
      ->where('level', 'wadek1')
      ->where('status_persetujuan', 0)
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
