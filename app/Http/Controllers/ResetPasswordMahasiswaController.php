<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Validator;

class ResetPasswordMahasiswaController extends Controller
{
    public function ResetPassword(){
      $Jurusan = DB::table('prodi')
      ->select('prodi.id_prodi','prodi.nama_prodi as nama_jurusan')
      ->get();

      return view('pengguna.resetpassword',compact('Jurusan'));
    }

    public function ProsesResetPassword(request $request){
      $request->validate([
        'tanggal_lahir'=>'required|date',
        'tempat_lahir'=>'required',
        'id_prodi'=>'required',
        'email'=>'required|email',
      ],[
        'tanggal_lahir.required'=>'Tanggal Lahir Harus Diisi.',
        'tempat_lahir.required'=>'Tempat Lahir Harus Diisi.',
        'id_prodi.required'=>'Jurusan Harus Dipilih.',
        'email.required'=>'Email Harus Diisi.',
        '*.date'=>'Format Tanggal Salah.',
        '*.email'=>'Format Email Salah.',
      ]);
      DB::BeginTransaction();
      try{
        $Pengguna = DB::table('mahasiswa')
        ->join('users','users.id_user','mahasiswa.id_user')
        ->where([
          'mahasiswa.tempat_lahir'=>$request->tempat_lahir,
          'mahasiswa.tanggal_lahir'=>$request->tanggal_lahir,
          'mahasiswa.id_prodi'=>$request->id_prodi,
          'users.email'=>$request->email,
        ]);

        if(!$Pengguna->first()){
          return redirect(route('LupaPassword'))
          ->with('simpan_gagal','Tempat Lahir, Tanggal Lahir, Jurusan dan Email Tidak Cocok. Silahkan dipastikan itu betul.')
          ->withInput();
        }

        $token = bin2hex(random_bytes(20));

        $Pengguna->update([
          'users.forgot_password'=>$token,
        ]);

        $id_user = $Pengguna->first()
        ->id_user;
        DB::commit();
      }catch(\Exception $e){
        DB::rollback();
        return redirect(url()->previous())
        ->with('simpan_gagal','Terjadi Kesalahan, silahkan coba lagi.');
      }

      return view('pengguna.prosesresetpassword',compact('token','id_user'));
    }

    public function SimpanResetPassword(request $request){
      $validasi = Validator::make($request->all(),[
        'reset_token'=>'required',
        'id_user'=>'required',
        'password'=>'required|min:4',
        'ulangi_password'=>'required|same:password',
      ],[
        'password.min'=>'Password tidak boleh kurang dari 4 karakter',
        'password.required'=>'Password Harus Diisi.',
        'ulangi_password.required'=>'Ulangin Password Harus Diisi.',
        'ulangi_password.same'=>'Ulangi Password Harus Sama Dengan Password.',
      ]);

      if($validasi->fails()){
        return redirect(route('LupaPassword'))
        ->with('simpan_gagal','Terjadi Kesalahan, Silahkan Ulangi Kembali.')
        ->withErrors($validasi);
      }
      DB::BeginTransaction();
      try{
        $ResetPassword = DB::table('users')
        ->where([['id_user',$request->id_user],['forgot_password',$request->reset_token]]);
        if(!$ResetPassword->first()){
          return redirect(route('LupaPassword'))
          ->with('simpan_gagal','Terjadi Kesalahan, Silahkan Ulangi Kembali.');
        }
        $ResetPassword->update([
          'password'=>Hash::make($request->password)
        ]);
        DB::commit();
        return redirect(route('login'))
        ->with('simpan_sukses','Password Berhasil Diubah, Silahkan Login.');
      }catch(\Exception $e){
        return redirect(route('LupaPassword'))
        ->with('simpan_gagal','Terjadi Kesalahan, Silahkan Ulangi Kembali.');
      }
    }
}
