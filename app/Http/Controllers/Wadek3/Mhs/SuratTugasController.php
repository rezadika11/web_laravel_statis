<?php

namespace App\Http\Controllers\Wadek3\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;
use App\SuratMahasiswa;
use PDF;
use App\DetilUser;

class SuratTugasController extends Controller
{
  private $tabel_surat = 'surat_tugas';
  private $tabel_persetujuan = 'persetujuan_surat_tugas';
  private $tabel_peserta = 'peserta_surat_tugas';

  public function __construct()
  {
    $this->middleware(['auth', 'level:wadek3']);
  }

  public function PermintaanSuratTugas()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->where([['validasi_wadek3.status_persetujuan', 0], ['surat.unique_key', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.no_surat', 'surat.tanggal_surat', 'validasi_wadek3.status_persetujuan as status_persetujuan_wadek3', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.kegiatan', 'surat.penyelenggara')
      ->paginate(4);

    $no = 1;

    return view('wadek3.mhs.surattugas.permintaansurattugas', compact('PermohonanSurat', 'no'));
  }

  public function ProsesPermintaanSuratTugas($id_surat)
  {
    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->where([['validasi_wadek3.status_persetujuan', 0], ['surat.unique_key', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.no_surat', '!=', NULL], ['surat.tanggal_surat', '!=', NULL]])
      ->select('validasi_wadek3.status_persetujuan', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.tanggal_mulai', 'surat.tanggal_selesai', 'surat.no_surat', 'surat.tanggal_surat', 'surat.kegiatan', 'surat.penyelenggara')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    $Peserta = DB::table($this->tabel_peserta . ' as peserta')
      ->join('prodi', 'prodi.id_prodi', 'peserta.id_prodi')
      ->select('peserta.nim', 'peserta.nama', 'prodi.nama_prodi')
      ->where('peserta.id_' . $this->tabel_surat, $id_surat)
      ->get();

    $no = 1;


    return view('wadek3.mhs.surattugas.prosespermintaansurattugas', compact('Surat', 'Peserta', 'no'));
  }

  public function SimpanProsesPermintaanSuratTugas(request $request, $id_surat)
  {
    if ($request->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    }

    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->where([['validasi_wadek3.status_persetujuan', 0], ['surat.unique_key', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.no_surat', '!=', NULL], ['surat.tanggal_surat', '!=', NULL]]);

    if ($Surat->count() == 0) {
      return redirect(route('wadek3.PermintaanSuratTugas'))
        ->with('simpan_gagal', 'Surat Sudah Divalidasi atau tidak ditemukan');
    }

    DB::beginTransaction();
    try {
      $Surat->update([
        'validasi_wadek3.status_persetujuan' => 1,
        'validasi_wadek3.waktu_persetujuan' => date('Y-m-d H:i:s'),
        'surat.unique_key' => Str::UUID(),
        'validasi_wadek3.id_pegawai_dosen' => DetilUser::AmbilDetil()->id_dosen,
      ]);
      DB::commit();
      return redirect(route('wadek3.PermintaanSuratTugas'))
        ->with('simpan_sukses', 'Surat Berhasil Ditanda Tangani.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakn Hubungi Pengembang.');
    }
  }

  public function SuratTugasSiap()
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->where([['validasi_wadek3.status_persetujuan', 1], ['surat.unique_key', '!=', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.no_surat', 'surat.tanggal_surat', 'validasi_wadek3.status_persetujuan as status_persetujuan_wadek3', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.kegiatan', 'surat.penyelenggara')
      ->paginate(4);

    $no = 1;

    return view('wadek3.mhs.surattugas.surattugassiap', compact('SuratSiapCetak', 'no'));
  }

  public function CetakSuratTugas($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_wadek3.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratTugas($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratTugasBelumdiProses()
  {
    $count = DB::table('persetujuan_surat_tugas')
      ->select('status_persetujuan', 'level')
      ->where('level', 'wadek3')
      ->where('status_persetujuan', 0)
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
