<?php

namespace App\Http\Controllers\Wadek3\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;
use App\SuratMahasiswa;
use PDF;
use App\DetilUser;

class SuratPermohonanPenghargaanController extends Controller
{
  private $tabel_surat = 'surat_permohonan_penghargaan';
  private $tabel_persetujuan = 'persetujuan_surat_permohonan_penghargaan';

  public function __construct()
  {
    $this->middleware(['auth', 'level:wadek3']);
  }

  public function PermintaanSuratPermohonanPenghargaan()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->where([['validasi_wadek3.status_persetujuan', 0], ['surat.unique_key', NULL]])
      ->select('surat.nama_perlombaan', 'surat.tanggal_mulai_pelaksanaan', 'surat.tanggal_selesai_pelaksanaan', 'surat.tempat', 'surat.juara', 'surat.kelompok', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'validasi_wadek3.status_persetujuan as persetujuan_wadek3', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'mahasiswa.alamat', 'mahasiswa.no_hp', 'prodi.nama_prodi', 'surat.tanggal_surat', 'surat.no_surat')
      ->paginate(4);

    $no = 1;

    $PilihJuara = [
      1 => 'Satu',
      2 => 'Dua',
      3 => 'Tiga',
    ];

    return view('wadek3.mhs.suratpermohonanpenghargaan.permintaansuratpermohonanpenghargaan', compact('PermohonanSurat', 'no', 'PilihJuara'));
  }

  public function ProsesPermintaanSuratPermohonanPenghargaan($id_surat)
  {
    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->where([['validasi_wadek3.status_persetujuan', 0], ['surat.unique_key', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.no_surat', '!=', NULL], ['surat.tanggal_surat', '!=', NULL]])
      ->select('surat.no_surat', 'surat.tanggal_surat', 'surat.nama_perlombaan', 'surat.tanggal_mulai_pelaksanaan', 'surat.tanggal_selesai_pelaksanaan', 'surat.tempat', 'surat.juara', 'surat.kelompok', 'validasi_wadek3.status_persetujuan', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'mahasiswa.alamat', 'mahasiswa.no_hp', 'prodi.nama_prodi')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    $PilihJuara = [
      1 => 'Satu',
      2 => 'Dua',
      3 => 'Tiga',
    ];

    return view('wadek3.mhs.suratpermohonanpenghargaan.prosespermintaansuratpermohonanpenghargaan', compact('Surat', 'PilihJuara'));
  }

  public function SimpanProsesPermintaanSuratPermohonanPenghargaan(request $request, $id_surat)
  {
    if ($request->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    }

    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->where([['validasi_wadek3.status_persetujuan', 0], ['surat.unique_key', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.no_surat', '!=', NULL], ['surat.tanggal_surat', '!=', NULL]]);

    if ($Surat->count() == 0) {
      return redirect(route('wadek3.PermintaanSuratPermohonanPenghargaan'))
        ->with('simpan_gagal', 'Surat Sudah Divalidasi atau tidak ditemukan');
    }

    DB::beginTransaction();
    try {
      $Surat->update([
        'validasi_wadek3.status_persetujuan' => 1,
        'validasi_wadek3.waktu_persetujuan' => date('Y-m-d H:i:s'),
        'surat.unique_key' => Str::UUID(),
        'validasi_wadek3.id_pegawai_dosen' => DetilUser::AmbilDetil()->id_dosen,
      ]);
      DB::commit();
      return redirect(route('wadek3.PermintaanSuratPermohonanPenghargaan'))
        ->with('simpan_sukses', 'Surat Berhasil Ditanda Tangani.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakn Hubungi Pengembang.');
    }
  }

  public function SuratPermohonanPenghargaanSiap()
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->where([['validasi_wadek3.status_persetujuan', 1], ['surat.unique_key', '!=', NULL]])
      ->select('surat.no_surat', 'surat.tanggal_surat', 'surat.nama_perlombaan', 'surat.tanggal_mulai_pelaksanaan', 'surat.tanggal_selesai_pelaksanaan', 'surat.tempat', 'surat.juara', 'surat.kelompok', 'validasi_wadek3.status_persetujuan', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'mahasiswa.alamat', 'mahasiswa.no_hp', 'prodi.nama_prodi')
      ->paginate(4);

    $no = 1;

    $PilihJuara = [
      1 => 'Satu',
      2 => 'Dua',
      3 => 'Tiga',
    ];

    return view('wadek3.mhs.suratpermohonanpenghargaan.suratpermohonanpenghargaansiap', compact('SuratSiapCetak', 'no', 'PilihJuara'));
  }

  public function CetakSuratPermohonanPenghargaan($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_wadek3.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratPermohonanPenghargaan($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratPermohonanPenghargaanBelumdiProses()
  {
    $count = DB::table('persetujuan_surat_permohonan_penghargaan')
      ->select('status_persetujuan', 'level')
      ->where('level', 'wadek3')
      ->where('status_persetujuan', 0)
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
