<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-12T20:55:53+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-12T20:55:53+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB; 
use Illuminate\Http\Request;
use App\SuratMahasiswa;
use Validator;
use App\DetilUser;
use Session;
use PDF;

class SuratIjinRisetIndividuController extends Controller
{
  public function __construct()
  {
    $this->middleware(['auth', 'level:mhs']);
  }

  public function TambahSuratIjinRisetIndividu()
  {
    $semester = SuratMahasiswa::Semester();

    return view('mhs.surat.suratijinrisetindividu', compact('semester'));
  }

  public function SimpanTambahSuratIjinRisetIndividu(request $data)
  {
    $data->validate(
      [
        'kepada' => 'required',
        'kec' => 'required',
        'semester' => 'required',
        'judul' => 'required',
        'obyek' => 'required',
        'tempat' => 'required',
        'tanggal_mulai' => 'required|date',
        'tanggal_selesai' => 'required|date',
        'metode_penelitian' => 'required',
      ],
      [
        'kepada.required' => 'Kepada Harus Diisi.',
        'kec.required' => 'Kecamatan Harus Diisi.',
        'semester.required' => 'Semester Harus Dipilih.',
        'judul.required' => 'Judul Harus Diisi.',
        'obyek.required' => 'Obyek Harus Diisi.',
        'tempat.required' => 'Tempat Harus Diisi.',
        'tanggal_mulai.required' => 'Tanggal Mulai Harus Diisi.',
        'tanggal_selesai.required' => 'Tanggal Selesai Harus Diisi.',
        'metode_penelitian.required' => 'Metode Penelitian Harus Diisi.',
        'tanggal_mulai.date' => 'Format Tanggal Mulai Salah.',
        'tanggal_selesai.date' => 'Format Tanggal Selesai Salah.',
      ]
    );

    $data->merge([
      'id_mahasiswa' => DetilUser::AmbilDetil()->id_mahasiswa,
    ]);

    DB::BeginTransaction();
    try {
      $no_urut = 0;
      $data->merge(['no_urut' => $no_urut]);
      $id_surat = DB::table('surat_ijin_riset_individu')
        ->insertGetId($data->only('id_mahasiswa', 'no_urut', 'kepada', 'kec', 'semester', 'judul', 'obyek', 'tempat', 'tanggal_mulai', 'tanggal_selesai', 'metode_penelitian'));
      $validasi_surat = [
        [
          'id_surat_ijin_riset_individu' => $id_surat,
          'level' => 'adm',
          'status_persetujuan' => 0,
        ],
        [
          'id_surat_ijin_riset_individu' => $id_surat,
          'level' => 'kajur',
          'status_persetujuan' => 0,
        ],
      ];

      if (isset($data->tembusan)) {
        $tembusan = explode(";", $data->tembusan);
        foreach ($tembusan as $tmbs) {
          if (strlen($tmbs) > 1) {
            $InsertTembusan[] = [
              'id_surat_ijin_riset_individu' => $id_surat,
              'kepada_tembusan' => $tmbs,
            ];
          }
        }

        if (isset($InsertTembusan)) {
          DB::table('tembusan_surat_ijin_riset_individu')
            ->insert($InsertTembusan);
        }
      }
      DB::table('persetujuan_surat_ijin_riset_individu')
        ->insert($validasi_surat);
      Session::flash('simpan_sukses', 'Permohonan Surat Berhasil Di Ajukan,Silahkan menunggu sampai di Periksa dan Disetujui');
      DB::commit();
      return redirect(route('mhs.DaftarSuratIjinRisetIndividu'));
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      Session::flash('simpan_gagal', 'Terjadi Kesalahan, Silahkan Hubungi Fakultas.');
      return redirect(url()->previous())
        ->withInput();
    }
  }
  public function DaftarSuratIjinRisetIndividu()
  {
    $DaftarSurat = DB::table('surat_ijin_riset_individu as surat')
      ->join('persetujuan_surat_ijin_riset_individu as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_surat_ijin_riset_individu', 'surat.id_surat_ijin_riset_individu')
          ->where('validasi_adm.level', 'adm');
      })
      ->join('persetujuan_surat_ijin_riset_individu as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_surat_ijin_riset_individu', 'surat.id_surat_ijin_riset_individu')
          ->where('validasi_kajur.level', 'kajur');
      })
      ->select('surat.id_surat_ijin_riset_individu as id_surat', 'surat.kepada', 'surat.kec', 'surat.judul', 'surat.obyek', 'surat.tempat', 'surat.tanggal_mulai', 'surat.tanggal_selesai', 'surat.metode_penelitian', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'validasi_kajur.status_persetujuan as status_persetujuan_kajur')
      ->where('surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa)
      ->paginate(4);

    $i = 1;

    return view('mhs.surat.daftarsuratijinrisetindividu', compact('DaftarSurat', 'i'));
  }

  public function CetakSuratIjinRisetIndividu($id_surat)
  {
    $SuratSiapCetak = DB::table('surat_ijin_riset_individu as surat')
      ->join('persetujuan_surat_ijin_riset_individu as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_surat_ijin_riset_individu', 'surat.id_surat_ijin_riset_individu')
          ->where('validasi_adm.level', 'adm');
      })
      ->join('persetujuan_surat_ijin_riset_individu as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_surat_ijin_riset_individu', 'surat.id_surat_ijin_riset_individu')
          ->where('validasi_kajur.level', 'kajur');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_kajur.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa], ['surat.id_surat_ijin_riset_individu', $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      Session::flash('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
      return redirect(url()->previous());
    }

    $data = SuratMahasiswa::CetakSuratIjinRisetIndividu($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
}
