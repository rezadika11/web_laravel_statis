<?php

namespace App\Http\Controllers\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\DetilUser;
use App\SuratMahasiswa;
use PDF;

class SuratTugasController extends Controller
{
  private $tabel_surat = 'surat_tugas';
  private $tabel_persetujuan = 'persetujuan_surat_tugas';
  private $tabel_peserta = 'peserta_surat_tugas';

  public function __construct()
  {
    $this->middleware(['auth', 'level:mhs']);
  }

  public function DaftarSuratTugas()
  {
    $DataSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . '  as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . '  as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->select('surat.id_' . $this->tabel_surat . ' as id_surat', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'validasi_wadek3.status_persetujuan as status_persetujuan_wadek3', 'surat.kegiatan', 'surat.penyelenggara', 'surat.tanggal_mulai', 'surat.tanggal_selesai')
      ->where('surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa)
      ->paginate(4);

    $no = 1;

    return view('mhs.surat.daftarsurattugas', compact('DataSurat', 'no'));
  }

  public function TambahSuratTugas()
  {
    $Jurusan = DB::table('prodi')
      ->select('prodi.id_prodi', 'prodi.nama_prodi as nama_jurusan')
      ->get();

    return view('mhs.surat.tambahsurattugas', compact('Jurusan'));
  }

  public function SimpanTambahSuratTugas(request $request)
  {
    $request->validate([
      'kegiatan' => 'required|max:255',
      'penyelenggara' => 'required|max:255',
      'tanggal_mulai' => 'required|date',
      'tanggal_selesai' => 'required|date',
      'peserta' => 'required|array',
      'peserta.*.nim' => 'required',
      'peserta.*.nama' => 'required',
      'peserta.*.id_prodi' => 'required',
    ], [
      'kegiatan.required' => 'Kegiatan Harus Diisi.',
      'penyelenggara.required' => 'Penyelenggara Harus Diisi.',
      'tanggal_mulai.required' => 'Tanggal Mulai Harus Diisi.',
      'tanggal_selesai.required' => 'Tanggal Selesai Harus Diisi.',
      'peserta.required' => 'Identitas Peserta Harus Diisi.',
      'peserta.0.nim.required' => 'NIM Peserta Harus Diisi.',
      'peserta.0.nama.required' => 'Nama Peserta Harus Diisi.',
      'peserta.0.id_prodi.required' => 'Program Studi Harus Dipilih.',
      '*.max' => 'Tidak Boleh Lebih dari 255 Karater.',
      '*.date' => 'Format Tanggal Salah.',
    ]);

    $request->merge([
      'id_mahasiswa' => DetilUser::AmbilDetil()->id_mahasiswa,
    ]);

    DB::BeginTransaction();
    try {
      $idSurat = DB::table($this->tabel_surat)
        ->insertGetId($request->only('kegiatan', 'penyelenggara', 'tanggal_mulai', 'tanggal_selesai', 'id_mahasiswa'));
      $validasi_surat = [
        [
          'id_surat_tugas' => $idSurat,
          'level' => 'adm',
          'status_persetujuan' => 0,
        ],
        [
          'id_surat_tugas' => $idSurat,
          'level' => 'wadek3',
          'status_persetujuan' => 0,
        ],
      ];
      DB::table($this->tabel_persetujuan)
        ->insert($validasi_surat);

      $peserta[] = [
        'id_surat_tugas' => $idSurat,
        'nama' => DetilUser::AmbilDetil()->nama,
        'nim' => DetilUser::AmbilDetil()->nim,
        'id_prodi' => DetilUser::AmbilDetil()->id_prodi,
      ];

      foreach ($request->peserta as $psrt) {
        $peserta[] = [
          'id_surat_tugas' => $idSurat,
          'nama' => $psrt['nama'],
          'nim' => $psrt['nim'],
          'id_prodi' => $psrt['id_prodi'],
        ];
      }
      DB::table($this->tabel_peserta)
        ->insert($peserta);
      DB::commit();
      return redirect(route('mhs.DaftarSuratTugas'))
        ->with('simpan_sukses', 'Permohonan Surat Berhasil Di Ajukan,Silahkan menunggu sampai di Periksa dan Disetujui');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Hubungi Fakultas.');
    }
  }

  public function CetakSuratTugas($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_wadek3.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratTugas($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
}
