<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-15T07:37:11+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-15T07:37:11+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\DetilUser;
use App\SuratMahasiswa;
use PDF;

class SuratKeteranganMasihKuliahController extends Controller
{
  private $tabel_surat = 'surat_keterangan_masih_kuliah';
  private $tabel_persetujuan = 'persetujuan_surat_keterangan_masih_kuliah';
  private $tabel_wali = 'wali_surat_keterangan_masih_kuliah';

  public function __construct()
  {
    $this->middleware(['auth', 'level:mhs']);
  }

  public function DaftarSuratKeteranganMasihKuliah()
  {
    $DataSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . '  as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . '  as validasi_wadek1_kajur', function ($q) {
        return $q->on('validasi_wadek1_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where(function($q1){
            return $q1->where('validasi_wadek1_kajur.level', 'wadek1')
            ->orwhere('validasi_wadek1_kajur.level', 'kajur');
          });
      })
      ->select('surat.id_' . $this->tabel_surat . ' as id_surat', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'validasi_wadek1_kajur.status_persetujuan as status_persetujuan_wadek1_kajur', 'surat.tahun_akademik')
      ->where('surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa)
      ->paginate(4);

    $no = 1;

    return view('mhs.surat.daftarsuratketeranganmasihkuliah', compact('DataSurat', 'no'));
  }

  public function TambahSuratKeteranganMasihKuliah()
  {
    $Mahasiswa = DB::table('mahasiswa')
      ->select('tanggal_lahir', 'tempat_lahir')
      ->where('id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa)
      ->first();

    if (($Mahasiswa->tanggal_lahir == NULL) || ($Mahasiswa->tempat_lahir == NULL)) {
      return redirect(route('EditPenggunaMhs'))
        ->with('simpan_gagal', 'Silahkan Isi Tanggal Lahir dan Tempat Lahir Sebelum Meminta Surat Keterangan Masih Kuliah');
    }


    $TahunAkademik = SuratMahasiswa::TahunAkademik();

    return view('mhs.surat.tambahsuratketeranganmasihkuliah', compact('TahunAkademik'));
  }

  public function SimpanTambahSuratKeteranganMasihKuliah(request $request)
  {
    $request->validate([
      'tahun_akademik' => 'required',
      'nama' => 'required_if:wali,on|max:255',
      'nip' => 'required_if:wali,on|max:255',
      'pangkat_golongan' => 'required_if:wali,on|max:255',
      'instansi' => 'required_if:wali,on|max:255',
    ], [
      'tahun_akademik.required' => 'Tahun Akademik Harus Dipilih.',
      'nama.required_if' => 'Nama Orang Tua / Wali Harus Diisi.',
      'nip.required_if' => 'NIP / NRP Harus Diisi.',
      'pangkat_golongan.required_if' => 'Pangkat / Golongan / Ruang Harus Diisi, Gunakan - Jika Tidak Ada.',
      'instansi.required_if' => 'Nama Instansi Harus Diisi.',
      '*.max' => 'Isian Tidak Boleh Lebih Dari 255 Karakter.',
    ]);

    $request->merge([
      'id_mahasiswa' => DetilUser::AmbilDetil()->id_mahasiswa,
    ]);

    DB::BeginTransaction();
    try {
      $idSurat = DB::table($this->tabel_surat)
        ->insertGetId($request->only('tahun_akademik', 'id_mahasiswa'));
      $validasi_surat = [
        [
          'id_' . $this->tabel_surat => $idSurat,
          'level' => 'adm',
          'status_persetujuan' => 0,
        ],
      ];
      if($request->wali == 'on'){
        $validasi_surat[] = [
          'id_' . $this->tabel_surat => $idSurat,
          'level' => 'wadek1',
          'status_persetujuan' => 0,
        ];
      }else {
        $validasi_surat[] = [
          'id_' . $this->tabel_surat => $idSurat,
          'level' => 'kajur',
          'status_persetujuan' => 0,
        ];
      }
      DB::table($this->tabel_persetujuan)
        ->insert($validasi_surat);

      if ($request->wali == "on") {
        $request->merge([
          'id_' . $this->tabel_surat => $idSurat,
        ]);
        DB::table($this->tabel_wali)
          ->insert($request->only('id_' . $this->tabel_surat, 'nama', 'nip', 'pangkat_golongan', 'instansi'));
      }
      DB::commit();
      return redirect(route('mhs.DaftarSuratKeteranganMasihKuliah'))
        ->with('simpan_sukses', 'Permohonan Surat Berhasil Di Ajukan,Silahkan menunggu sampai di Periksa dan Disetujui');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Hubungi Fakultas.');
    }
  }

  public function CetakSuratKeteranganMasihKuliah($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1_kajur', function ($q) {
        return $q->on('validasi_wadek1_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
        ->where(function($q1){
          return $q1->where('validasi_wadek1_kajur.level', 'wadek1')
          ->orwhere('validasi_wadek1_kajur.level', 'kajur');
        });
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_wadek1_kajur.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratKeteranganMasihKuliah($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
}
