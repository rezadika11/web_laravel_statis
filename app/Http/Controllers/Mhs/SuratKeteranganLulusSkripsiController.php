<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-13T08:01:21+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-13T08:01:21+07:00
# @Copyright: https://annaya.id

 


namespace App\Http\Controllers\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\DetilUser;
use App\SuratMahasiswa;
use PDF;

class SuratKeteranganLulusSkripsiController extends Controller
{
  private $tabel_surat = 'surat_keterangan_lulus_skripsi';
  private $tabel_persetujuan = 'persetujuan_surat_keterangan_lulus_skripsi';

  public function __construct()
  {
    $this->middleware(['auth', 'level:mhs']);
  }

  public function DaftarSuratKeteranganLulusSkripsi()
  {
    $DataSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . '  as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . '  as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->select('surat.id_' . $this->tabel_surat . ' as id_surat', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'validasi_kajur.status_persetujuan as status_persetujuan_kajur', 'surat.tahun_akademik', 'surat.tanggal_ujian')
      ->where('surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa)
      ->paginate(4);

    $no = 1;

    return view('mhs.surat.daftarsuratketeranganlulusskripsi', compact('no', 'DataSurat'));
  }

  public function TambahSuratKeteranganLulusSkripsi()
  {
    $TahunAkademik = SuratMahasiswa::TahunAkademik();

    return view('mhs.surat.tambahsuratketeranganlulusskripsi', compact('TahunAkademik'));
  }

  public function SimpanTambahSuratKeteranganLulusSkripsi(request $request)
  {
    $request->validate([
      'tahun_akademik' => 'required',
      'tanggal_ujian' => 'required|date',
      'lampiran_munaqosyah' => 'required'
    ], [
      'tahun_akademik.required' => 'Tahun Akademik Harus Dipilih.',
      'tanggal_ujian.required' => 'Tanggal Ujian Harus Diisi.',
      '*.date' => 'Format Tanggal Salah',
      'lampiran_munaqosyah.required' => 'Lampiran Berita Acara Munaqosyah Harus Diisi.'
    ]);

    $request->merge([
      'id_mahasiswa' => DetilUser::AmbilDetil()->id_mahasiswa,
    ]);

    DB::BeginTransaction();
    try {
      $idSurat = DB::table($this->tabel_surat)
        ->insertGetId($request->only(
          'id_mahasiswa',
          'tahun_akademik',
          'tanggal_ujian',
          'lampiran_munaqosyah'
        ));
      $validasi_surat = [
        [
          'id_surat_keterangan_lulus_skripsi' => $idSurat,
          'level' => 'adm',
          'status_persetujuan' => 0,
        ],
        [
          'id_surat_keterangan_lulus_skripsi' => $idSurat,
          'level' => 'kajur',
          'status_persetujuan' => 0,
        ],
      ];
      DB::table($this->tabel_persetujuan)
        ->insert($validasi_surat);
      DB::commit();
      return redirect(route('mhs.DaftarSuratKeteranganLulusSkripsi'))
        ->with('simpan_sukses', 'Permohonan Surat Berhasil Di Ajukan,Silahkan menunggu sampai di Periksa dan Disetujui');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan!');
    }
  }

  public function CetakSuratKeteranganLulusSkripsi($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_kajur.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratKeteranganLulusSkripsi($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
}
