<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-12T20:55:48+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-12T20:55:48+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SuratMahasiswa;
use DB;
use App\DetilUser;
use PDF;

class SuratIjinObservasiPendahuluanController extends Controller
{

  private $tabel_surat = "surat_ijin_observasi_pendahuluan";
  private $tabel_persetujuan = "persetujuan_surat_ijin_observasi_pendahuluan";

  public function __construct()
  {
    $this->middleware(['auth', 'level:mhs']);
  }

  public function DaftarSuratIjinObservasiPendahuluan()
  {
    $DataSurat = DB::table($this->tabel_surat.' as surat')
      ->join($this->tabel_persetujuan.' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_surat_ijin_observasi_pendahuluan', 'surat.id_surat_ijin_observasi_pendahuluan')
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan.' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_surat_ijin_observasi_pendahuluan', 'surat.id_surat_ijin_observasi_pendahuluan')
          ->where('validasi_kajur.level', 'kajur');
      })
      ->select('surat.id_'.$this->tabel_surat.' as id_surat', 'surat.kepada', 'surat.semester', 'surat.tahun_akademik', 'surat.obyek', 'surat.lokasi', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'validasi_kajur.status_persetujuan as status_persetujuan_kajur', 'surat.tanggal_observasi')
      ->where('surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa)
      ->paginate(4);

    $no = 1;

    return view('mhs.surat.daftarsuratijinobservasipendahuluan', compact('no', 'DataSurat'));
  }

  public function TambahSuratIjinObservasiPendahuluan()
  {
    $semester = SuratMahasiswa::Semester();
    $TahunAkademik = SuratMahasiswa::TahunAkademik();

    return view('mhs.surat.tambahsuratijinobservasipendahuluan', compact('semester', 'TahunAkademik'));
  }

  public function SimpanTambahSuratIjinObservasiPendahuluan(request $data)
  {
    $data->validate([
      'kepada' => 'required|max:255',
      'semester' => 'required',
      'tahun_akademik' => 'required',
      'obyek' => 'required|max:255',
      'lokasi' => 'required|max:255',
      'tanggal_observasi' => 'required|date',
    ], [
      'kepada.required' => 'Kepada Harus Diisi.',
      'semester.required' => 'Semester Harus Dipilih.',
      'tahun_akademik.required' => 'Tahun Akademik Harus Dipilih.',
      'obyek.required' => 'Obyek Harus Diisi.',
      'lokasi.required' => 'Lokasi Harus Diisi.',
      'tanggal_observasi.required' => 'Tanggal Observasi Harus Diisi',
      'kepada.max' => 'Kepada Tidak Boleh Lebih Dari 255 Karakter.',
      'obyek.max' => 'Kepada Tidak Boleh Lebih Dari 255 Karakter.',
      'lokasi.max' => 'Kepada Tidak Boleh Lebih Dari 255 Karakter.',
      'tanggal_observasi.date' => 'Format Tanggal Observasi Salah.',
    ]);

    $data->merge([
      'id_mahasiswa' => DetilUser::AmbilDetil()->id_mahasiswa,
    ]);

    DB::BeginTransaction();
    try {
      $idSurat = DB::table($this->tabel_surat)
        ->insertGetId($data->only(
          'id_mahasiswa',
          'kepada',
          'semester',
          'tahun_akademik',
          'obyek',
          'lokasi',
          'tanggal_observasi'
        ));
      $validasi_surat = [
        [
          'id_'.$this->tabel_surat => $idSurat,
          'level' => 'adm',
          'status_persetujuan' => 0,
        ],
        [
          'id_'.$this->tabel_surat => $idSurat,
          'level' => 'kajur',
          'status_persetujuan' => 0,
        ],
      ];
      DB::table($this->tabel_persetujuan)
        ->insert($validasi_surat);
      DB::commit();
      return redirect(route('mhs.DaftarSuratIjinObservasiPendahuluan'))
      ->with('simpan_sukses', 'Permohonan Surat Berhasil Di Ajukan,Silahkan menunggu sampai di Periksa dan Disetujui');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan!');
    }
  }

  public function CetakSuratIjinObservasiPendahuluan($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat.' as surat')
      ->join($this->tabel_persetujuan.' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_surat_ijin_observasi_pendahuluan', 'surat.id_surat_ijin_observasi_pendahuluan')
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan.' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_surat_ijin_observasi_pendahuluan', 'surat.id_surat_ijin_observasi_pendahuluan')
          ->where('validasi_kajur.level', 'kajur');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_kajur.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_'.$this->tabel_surat, $id_surat], ['surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
      ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratIjinObservasiPendahuluan($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
}
