<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-12T20:55:43+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-12T20:55:43+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\DetilUser;
use App\SuratMahasiswa;
use PDF;

class SuratIjinObservasiKelasController extends Controller
{
  private $tabel_surat = "surat_ijin_observasi_kelas";
  private $tabel_persetujuan = "persetujuan_surat_ijin_observasi_kelas";
  private $tabel_peserta = "peserta_surat_ijin_observasi_kelas";

  public function __construct()
  {
    $this->middleware(['auth', 'level:mhs']);
  }
 
  public function DaftarSuratIjinObservasiKelas()
  {
    $DataSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . '  as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . '  as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->select('surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.kepada', 'surat.semester', 'surat.mata_kuliah', 'surat.dosen_pengampu', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'validasi_kajur.status_persetujuan as status_persetujuan_kajur', 'surat.tanggal_mulai', 'surat.tanggal_selesai', 'surat.tema')
      ->where('surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa)
      ->paginate(4);

    $no = 1;

    return view('mhs.surat.daftarsuratijinobservasikelas', compact('DataSurat', 'no'));
  }

  public function TambahSuratIjinObservasiKelas()
  {
    $semester = SuratMahasiswa::Semester();
    $TahunAkademik = SuratMahasiswa::TahunAkademik();
    return view('mhs.surat.tambahsuratijinobservasikelas', compact('semester', 'TahunAkademik'));
  }

  public function SimpanTambahSuratIjinObservasiKelas(request $data)
  {
    $data->validate([
      'kepada' => 'required|max:255',
      'mata_kuliah' => 'required|max:255',
      'dosen_pengampu' => 'required|max:255',
      'semester' => 'required',
      'tema' => 'required|max:255',
      'tanggal_mulai' => 'required|date',
      'tanggal_selesai' => 'required|date',
      'peserta' => 'required|array',
      'peserta.nim.*' => 'required',
      'peserta.nama.*' => 'required',
    ], [
      'kepada.required' => 'Kepada Harus Diisi.',
      'mata_kuliah.required' => 'Mata Kuliah Harus Diisi.',
      'dosen_pengampu.required' => 'Dosen Pengampu Harus Diisi.',
      'semester.required' => 'Semester Harus Dipilih.',
      'tema.required' => 'Tema Harus Diisi',
      'tanggal_mulai.required' => 'Tanggal Mulai Harus Diisi.',
      'tanggal_selesai.required' => 'Tanggal Selesai Harus Diisi.',
      'peserta.required' => 'Identitas Peserta Harus Diisi.',
      'peserta.nim.0.required' => 'NIM Peserta Harus Diisi.',
      'peserta.nama.0.required' => 'Nama Peserta Harus Diisi.',
      '.max' => 'Tidak Boleh Lebih dari 255 Karater.',
      '.date' => 'Format Tanggal Salah.',
    ]);

    $data->merge([
      'id_mahasiswa' => DetilUser::AmbilDetil()->id_mahasiswa,
      'id_prodi' => DetilUser::AmbilDetil()->id_prodi,
    ]);

    DB::BeginTransaction();
    try {
      $idSurat = DB::table($this->tabel_surat)
        ->insertGetId($data->only('kepada', 'mata_kuliah', 'semester', 'dosen_pengampu', 'tema', 'tanggal_mulai', 'tanggal_selesai', 'id_mahasiswa', 'id_prodi'));
      $validasi_surat = [
        [
          'id_surat_ijin_observasi_kelas' => $idSurat,
          'level' => 'adm',
          'status_persetujuan' => 0,
        ],
        [
          'id_surat_ijin_observasi_kelas' => $idSurat,
          'level' => 'kajur',
          'status_persetujuan' => 0,
        ],
      ];
      DB::table($this->tabel_persetujuan)
        ->insert($validasi_surat);

      $peserta[] = [
        'id_surat_ijin_observasi_kelas' => $idSurat,
        'nama' => DetilUser::AmbilDetil()->nama,
        'nim' => DetilUser::AmbilDetil()->nim,
      ];

      foreach ($data->peserta as $psrt) {
        if($psrt['nama'] != NULL || $psrt['nim'] != NULL){
          $peserta[] = [
            'id_surat_ijin_observasi_kelas' => $idSurat,
            'nama' => $psrt['nama'],
            'nim' => $psrt['nim'],
          ];
        }
      }
      DB::table($this->tabel_peserta)
        ->insert($peserta);
      DB::commit();
      return redirect(route('mhs.DaftarSuratIjinObservasiKelas'))
        ->with('simpan_sukses', 'Permohonan Surat Berhasil Di Ajukan,Silahkan menunggu sampai di Periksa dan Disetujui');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Hubungi Fakultas.');
    }
  }

  public function CetakSuratIjinObservasiKelas($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_kajur.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratIjinObservasiKelas($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
}
