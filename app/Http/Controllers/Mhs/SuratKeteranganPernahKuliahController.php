<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-15T09:48:06+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-15T09:48:06+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\DetilUser;
use App\SuratMahasiswa;
use PDF;

class SuratKeteranganPernahKuliahController extends Controller
{
  private $tabel_surat = 'surat_keterangan_pernah_kuliah';
  private $tabel_persetujuan = 'persetujuan_surat_keterangan_pernah_kuliah';

  public function __construct()
  {
    $this->middleware(['auth', 'level:mhs']);
  }

  public function DaftarSuratKeteranganPernahKuliah()
  {
    $DataSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . '  as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . '  as validasi_wadek1', function ($q) {
        return $q->on('validasi_wadek1.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek1.level', 'wadek1');
      })
      ->select('surat.id_' . $this->tabel_surat . ' as id_surat', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'validasi_wadek1.status_persetujuan as status_persetujuan_wadek1', 'surat.angkatan', 'surat.tahun_akademik_masuk', 'surat.semester_keluar', 'surat.tahun_akademik_keluar', 'surat.pt_tujuan')
      ->where('surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa)
      ->paginate(4);

    $no = 1;

    return view('mhs.surat.daftarsuratketeranganpernahkuliah', compact('DataSurat', 'no'));
  }

  public function TambahSuratKeteranganPernahKuliah()
  {
    for ($x = 2000; $x < date('Y'); $x++) {
      $PilihAngkatan[] = $x;
      $PilihTahunAkademik[] = $x . '/' . ($x + 1);
    }

    $PilihSemesterKeluar = [
      1 => 'Ganjil',
      2 => 'Genap',
    ];
    $PilihSemester = SuratMahasiswa::Semester();

    return view('mhs.surat.tambahsuratketeranganpernahkuliah', compact('PilihAngkatan', 'PilihTahunAkademik', 'PilihSemester', 'PilihSemesterKeluar'));
  }

  public function SimpanTambahSuratKeteranganPernahKuliah(request $request)
  {
    $request->validate([
      'angkatan' => 'required|numeric',
      'tahun_akademik_masuk' => 'required',
      'semester_keluar' => 'required',
      'tahun_akademik_keluar' => 'required',
      'selesai_beban_studi' => 'required|numeric',
      'lama_semester_kuliah' => 'required',
      'prodi_tujuan' => 'required|max:255',
      'pt_tujuan' => 'required|max:255',
      'alamat_pt_tujuan' => 'required|max:255',
    ], [
      'angkatan.required' => 'Tahun Angkatan Harus Diisi.',
      'angkatan.required' => 'Tahun Angkatan Harus Angka.',
      'tahun_akademik_masuk.required' => 'Tahun Akademik Masuk Harus Dipilih.',
      'semester_keluar.required' => 'Semester Keluar Harus Dipilih.',
      'tahun_akademik_keluar.required' => 'Tahun Akademik Keluar Harus Dipilih.',
      'selesai_beban_studi.required' => 'Selesai Beban Studi Harus Diisi.',
      'selesai_beban_studi.numeric' => 'Selesai Beban Studi Harus Angka.',
      'lama_semester_kuliah.required' => 'Lama Semester Kuliah Harus Dipilih.',
      'prodi_tujuan.required' => 'Program Studi Tujuan Harus Diisi.',
      'pt_tujuan.required' => 'Perguruan Tinggi Tujuan Harus Diisi.',
      'alamat_pt_tujuan.required' => 'Alamat Perguruan Tinggi Tujuan Harus Diisi.',
      '*.max' => 'Tidak Boleh Lebih Dari 255 Karakter.',
    ]);

    $request->merge([
      'id_mahasiswa' => DetilUser::AmbilDetil()->id_mahasiswa,
    ]);

    DB::BeginTransaction();
    try {
      $idSurat = DB::table($this->tabel_surat)
        ->insertGetId($request->only('angkatan', 'tahun_akademik_masuk', 'semester_keluar', 'tahun_akademik_keluar', 'selesai_beban_studi', 'lama_semester_kuliah', 'prodi_tujuan', 'pt_tujuan', 'alamat_pt_tujuan', 'id_mahasiswa'));
      $validasi_surat = [
        [
          'id_' . $this->tabel_surat => $idSurat,
          'level' => 'adm',
          'status_persetujuan' => 0,
        ],
        [
          'id_' . $this->tabel_surat => $idSurat,
          'level' => 'wadek1',
          'status_persetujuan' => 0,
        ],
      ];
      DB::table($this->tabel_persetujuan)
        ->insert($validasi_surat);
      DB::commit();
      return redirect(route('mhs.DaftarSuratKeteranganPernahKuliah'))
        ->with('simpan_sukses', 'Permohonan Surat Berhasil Di Ajukan,Silahkan menunggu sampai di Periksa dan Disetujui');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Hubungi Fakultas.');
    }
  }

  public function CetakSuratKeteranganPernahKuliah($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . '  as validasi_wadek1', function ($q) {
        return $q->on('validasi_wadek1.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek1.level', 'wadek1');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_wadek1.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratKeteranganPernahKuliah($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
}
