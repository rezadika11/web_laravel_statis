<?php

namespace App\Http\Controllers\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\DetilUser;
use App\SuratMahasiswa;
use PDF;


class SuratKeteranganPenggantiIjazahController extends Controller
{
  private $tabel_surat = 'surat_keterangan_pengganti_ijazah';
  private $tabel_persetujuan = 'persetujuan_surat_keterangan_pengganti_ijazah';

  public function __construct()
  {
    $this->middleware(['auth', 'level:mhs']);
  }

  public function DaftarSuratKeteranganPenggantiIjazah()
  {
    $DataSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . '  as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . '  as validasi_dekan', function ($q) {
        return $q->on('validasi_dekan.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_dekan.level', 'dekan');
      })
      ->select('surat.id_' . $this->tabel_surat . ' as id_surat', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'validasi_dekan.status_persetujuan as status_persetujuan_dekan', 'surat.nomor_ijazah', 'surat.tanggal_ijazah', 'surat.tahun_kehilangan')
      ->where('surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa)
      ->paginate(4);

    $no = 1;

    return view('mhs.surat.daftarsuratketeranganpenggantiijazah', compact('DataSurat', 'no'));
  }

  public function TambahSuratKeteranganPenggantiIjazah()
  {
    for ($x = 2000; $x <= date('Y'); $x++) {
      $PilihTahunKehilangan[] = $x;
    }

    return view('mhs.surat.tambahsuratketeranganpenggantiijazah', compact('PilihTahunKehilangan'));
  }

  public function SimpanTambahSuratKeteranganPenggantiIjazah(request $request)
  {
    $request->validate([
      'nomor_ijazah' => 'required',
      'tanggal_ijazah' => 'required|date',
      'tahun_kehilangan' => 'required',
    ], [
      'nomor_ijazah.required' => 'Nomor Ijazah Harus Diisi.',
      'tanggal_ijazah.required' => 'Tanggal Ijazah Harus Diisi.',
      'tanggal_ijazah.date' => 'Format Tanggal Salah.',
      'tahun_kehilangan.required' => 'Tahun Kehilangan Harus Dipilih.',
    ]);

    $request->merge([
      'id_mahasiswa' => DetilUser::AmbilDetil()->id_mahasiswa,
    ]);

    DB::BeginTransaction();
    try {
      $idSurat = DB::table($this->tabel_surat)
        ->insertGetId($request->only('nomor_ijazah', 'tanggal_ijazah', 'tahun_kehilangan', 'id_mahasiswa'));
      $validasi_surat = [
        [
          'id_' . $this->tabel_surat => $idSurat,
          'level' => 'adm',
          'status_persetujuan' => 0,
        ],
        [
          'id_' . $this->tabel_surat => $idSurat,
          'level' => 'dekan',
          'status_persetujuan' => 0,
        ],
      ];
      DB::table($this->tabel_persetujuan)
        ->insert($validasi_surat);
      DB::commit();
      return redirect(route('mhs.DaftarSuratKeteranganPenggantiIjazah'))
        ->with('simpan_sukses', 'Permohonan Surat Berhasil Di Ajukan,Silahkan menunggu sampai di Periksa dan Disetujui');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Hubungi Fakultas.');
    }
  }

  public function CetakSuratKeteranganPenggantiIJazah($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_dekan', function ($q) {
        return $q->on('validasi_dekan.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_dekan.level', 'dekan');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_dekan.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratKeteranganPenggantiIJazah($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
}
