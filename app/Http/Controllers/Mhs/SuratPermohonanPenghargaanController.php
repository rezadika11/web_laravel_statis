<?php

namespace App\Http\Controllers\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\DetilUser;
use App\SuratMahasiswa;
use PDF;

class SuratPermohonanPenghargaanController extends Controller
{
  private $tabel_surat = 'surat_permohonan_penghargaan';
  private $tabel_persetujuan = 'persetujuan_surat_permohonan_penghargaan';

  public function __construct()
  {
    $this->middleware(['auth', 'level:mhs']);
  }

  public function DaftarSuratPermohonanPenghargaan()
  {
    $DataSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . '  as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . '  as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->select('surat.id_' . $this->tabel_surat . ' as id_surat', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'validasi_wadek3.status_persetujuan as status_persetujuan_wadek3', 'surat.nama_perlombaan', 'surat.juara', 'surat.kelompok', 'surat.tempat')
      ->where('surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa)
      ->paginate(4);

    $PilihJuara = [
      1 => 'Satu',
      2 => 'Dua',
      3 => 'Tiga',
    ];

    $no = 1;

    return view('mhs.surat.daftarsuratpermohonanpenghargaan', compact('DataSurat', 'no', 'PilihJuara'));
  }

  public function TambahSuratPermohonanPenghargaan()
  {
    $PilihJuara = [
      1 => 'Satu',
      2 => 'Dua',
      3 => 'Tiga',
    ];

    return view('mhs.surat.tambahsuratpermohonanpenghargaan', compact('PilihJuara'));
  }

  public function SimpanTambahSuratPermohonanPenghargaan(request $request)
  {
    $request->validate([
      'nama_perlombaan' => 'required|max:255',
      'tanggal_mulai_pelaksanaan' => 'required|date',
      'tanggal_selesai_pelaksanaan' => 'required|date',
      'tempat' => 'required|max:255',
      'juara' => 'required',
      'kelompok' => 'required|max:255',
    ], [
      'nama_perlombaan.required' => 'Nama Perlombaan Harus Diisi.',
      'tanggal_mulai_pelaksanaan.required' => 'Tanggal Mulai Pelaksanaan Harus Diisi.',
      'tanggal_selesai_pelaksanaan.required' => 'Tanggal Selesai Pelaksanaan Harus Diisi.',
      'tempat.required' => 'Tempat Harus Diisi.',
      'juara.required' => 'Juara Harus Dipilih.',
      'kelompok.required' => 'Kelompok Harus Diisi.',
      '*.max' => 'Tidak Boleh Lebih Dari 255 Karakter.',
      '*.date' => 'Format Tanggal Salah.',
    ]);

    $request->merge([
      'id_mahasiswa' => DetilUser::AmbilDetil()->id_mahasiswa,
    ]);

    DB::BeginTransaction();
    try {
      $idSurat = DB::table($this->tabel_surat)
        ->insertGetId($request->only('nama_perlombaan', 'tanggal_mulai_pelaksanaan', 'tanggal_selesai_pelaksanaan', 'tempat', 'juara', 'kelompok', 'id_mahasiswa'));
      $validasi_surat = [
        [
          'id_surat_permohonan_penghargaan' => $idSurat,
          'level' => 'adm',
          'status_persetujuan' => 0,
        ],
        [
          'id_surat_permohonan_penghargaan' => $idSurat,
          'level' => 'wadek3',
          'status_persetujuan' => 0,
        ],
      ];
      DB::table($this->tabel_persetujuan)
        ->insert($validasi_surat);
      DB::commit();
      return redirect(route('mhs.DaftarSuratPermohonanPenghargaan'))
        ->with('simpan_sukses', 'Permohonan Surat Berhasil Di Ajukan,Silahkan menunggu sampai di Periksa dan Disetujui');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Hubungi Fakultas.');
    }
  }

  public function CetakSuratPermohonanPenghargaan($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_wadek3.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.id_mahasiswa', DetilUser::AmbilDetil()->id_mahasiswa]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratPermohonanPenghargaan($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
}
