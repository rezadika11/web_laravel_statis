<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-13T10:24:37+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-13T10:24:37+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Kajur\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;
use App\DetilUser;
use PDF;
use App\SuratMahasiswa;

class SuratKeteranganMasihKuliahController extends Controller
{
  private $tabel_surat = 'surat_keterangan_masih_kuliah';
  private $tabel_persetujuan = 'persetujuan_surat_keterangan_masih_kuliah';

  public function __construct()
  {
    $this->middleware(['auth', 'level:kajur']);
  }

  public function PermintaanSuratKeteranganMasihKuliah()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->wherein('mahasiswa.id_prodi',DetilUser::DosenJurusan())
      ->where([['validasi_kajur.status_persetujuan', 0], ['surat.unique_key', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.tahun_akademik','surat.tanggal_surat', 'surat.no_surat', 'validasi_kajur.status_persetujuan as status_persetujuan_kajur', 'validasi_adm.status_persetujuan as status_persetujuan_adm')
      ->paginate(4);

    $no = 1;

    return view('kajur.mhs.suratketeranganmasihkuliah.permintaansuratketeranganmasihkuliah', compact('no', 'PermohonanSurat'));
  }

  public function ProsesPermintaanSuratKeteranganMasihKuliah($id_surat)
  {
    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->wherein('mahasiswa.id_prodi',DetilUser::DosenJurusan())
      ->where([['validasi_kajur.status_persetujuan', 0], ['surat.unique_key', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.no_surat', '!=', NULL], ['surat.tanggal_surat', '!=', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.tahun_akademik', 'mahasiswa.tempat_lahir', 'mahasiswa.tanggal_lahir', 'mahasiswa.alamat','surat.no_surat', 'surat.tanggal_surat')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    return view('kajur.mhs.suratketeranganmasihkuliah.prosespermintaansuratketeranganmasihkuliah', compact('Surat'));
  }

  public function SimpanProsesPermintaanSuratKeteranganMasihKuliah(request $request, $id_surat)
  {
    if ($request->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    }

    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->where([['validasi_kajur.status_persetujuan', 0], ['surat.unique_key', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.no_surat', '!=', NULL], ['surat.tanggal_surat', '!=', NULL]]);

    if ($Surat->count() == 0) {
      return redirect(route('kajur.PermintaanSuratKeteranganMasihKuliah'))
        ->with('simpan_gagal', 'Surat Sudah Divalidasi atau tidak ditemukan');
    }

    DB::beginTransaction();
    try {
      $Surat->update([
        'validasi_kajur.status_persetujuan' => 1,
        'validasi_kajur.waktu_persetujuan' => date('Y-m-d H:i:s'),
        'surat.unique_key' => Str::UUID(),
        'validasi_kajur.id_pegawai_dosen'=>DetilUser::AmbilDetil()->id_dosen,
      ]);
      DB::commit();
      return redirect(route('kajur.PermintaanSuratKeteranganMasihKuliah'))
        ->with('simpan_sukses', 'Surat Berhasil Ditanda Tangani.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakn Hubungi Pengembang.');
    }
  }

  public function SuratKeteranganMasihKuliahSiap()
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->wherein('mahasiswa.id_prodi',DetilUser::DosenJurusan())
      ->where([['validasi_kajur.status_persetujuan', 1], ['surat.unique_key', '!=', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.tahun_akademik', 'surat.tanggal_surat', 'surat.no_surat', 'validasi_kajur.status_persetujuan as status_persetujuan_kajur', 'validasi_adm.status_persetujuan as status_persetujuan_adm')
      ->paginate(4);

    $no = 1;

    return view('kajur.mhs.suratketeranganmasihkuliah.suratketeranganmasihkuliahsiap', compact('no', 'SuratSiapCetak'));
  }

  public function CetakSuratKeteranganMasihKuliah($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_kajur.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratKeteranganMasihKuliah($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratKeteranganMasihKuliahBelumdiProses()
  {
    $count = DB::table($this->tabel_persetujuan.' as persetujuan')
      ->join($this->tabel_surat.' as surat','surat.id_'.$this->tabel_surat,'persetujuan.id_'.$this->tabel_surat)
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->select('status_persetujuan', 'level')
      ->where('level', 'kajur')
      ->where('status_persetujuan', 0)
      ->wherein('mahasiswa.id_prodi',DetilUser::DosenJurusan())
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
