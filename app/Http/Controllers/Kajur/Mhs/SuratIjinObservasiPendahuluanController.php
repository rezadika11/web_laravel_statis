<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-12T20:55:31+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-12T20:55:31+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Kajur\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\SuratMahasiswa;
use Illuminate\Support\Str;
use App\DetilUser;
use PDF;

class SuratIjinObservasiPendahuluanController extends Controller
{
  private $tabel_surat = "surat_ijin_observasi_pendahuluan";
  private $tabel_persetujuan = "persetujuan_surat_ijin_observasi_pendahuluan";

  public function __construct()
  {
    $this->middleware(['auth', 'level:kajur']);
  }

  public function PermintaanSuratIjinObservasiPendahuluan()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::DosenJurusan())
      ->where([['validasi_kajur.status_persetujuan', 0], ['surat.unique_key', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.kepada', 'surat.no_surat', 'surat.tanggal_surat', 'validasi_kajur.status_persetujuan as status_persetujuan_kajur', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'surat.id_surat_ijin_observasi_pendahuluan as id_surat')
      ->paginate(4);

    $no = 1;

    return view('kajur.mhs.suratobservasipendahuluan.permintaansuratijinobservasipendahuluan', compact('PermohonanSurat', 'no'));
  }

  public function ProsesPermintaanSuratIjinObservasiPendahuluan($id_surat)
  {
    $Surat = $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::DosenJurusan())
      ->where([['validasi_kajur.status_persetujuan', 0], ['surat.unique_key', NULL], ['surat.id_surat_ijin_observasi_pendahuluan', $id_surat], ['surat.no_surat', '!=', NULL], ['surat.tanggal_surat', '!=', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.kepada', 'surat.tahun_akademik', 'validasi_kajur.status_persetujuan', 'surat.id_'.$this->tabel_surat.' as id_surat', 'surat.semester', 'surat.obyek', 'surat.lokasi', 'surat.tanggal_observasi', 'surat.no_surat', 'surat.tanggal_surat')
      ->first();

    if (!$Surat) {
      Session::flash('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
      return redirect(url()->previous());
    }
    $semester = SuratMahasiswa::Semester($Surat->semester);

    return view('kajur.mhs.suratobservasipendahuluan.prosespermintaansuratijinobservasipendahuluan', compact('Surat', 'semester'));
  }

  public function SimpanProsesPermintaanSuratIjinObservasiPendahuluan(request $data, $id_surat)
  {
    if ($data->id_surat != $id_surat) {
      Session::flash('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
      return redirect(url()->previous())
        ->withInput();
    }

    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->where([['validasi_kajur.status_persetujuan', 0], ['surat.unique_key', NULL], ['surat.id_'.$this->tabel_surat, $id_surat], ['surat.no_surat', '!=', NULL], ['surat.tanggal_surat', '!=', NULL]]);

    if ($Surat->count() == 0) {
      Sesssion::flash('simpan_gagal', 'Surat Sudah Divalidasi atau tidak ditemukan');
      return redirect(route('kajur.PermintaanSuratIjinObservasiPendahuluan'));
    }

    DB::beginTransaction();
    try {
      $Surat->update([
        'validasi_kajur.status_persetujuan' => 1,
        'validasi_kajur.waktu_persetujuan' => date('Y-m-d H:i:s'),
        'surat.unique_key' => Str::UUID(),
        'validasi_kajur.id_pegawai_dosen'=>DetilUser::AmbilDetil()->id_dosen,
      ]);
      DB::commit();
      return redirect(route('kajur.PermintaanSuratIjinObservasiPendahuluan'))
      ->with('simpan_sukses', 'Surat Berhasil Ditanda Tangani.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
      ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakn Hubungi Pengembang.');
    }
  }

  public function SuratIjinObservasiPendahuluanSiap()
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::DosenJurusan())
      ->where([['validasi_kajur.status_persetujuan', 1], ['surat.unique_key', '!=', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.kepada', 'surat.no_surat', 'surat.tanggal_surat', 'validasi_kajur.status_persetujuan as status_persetujuan_kajur', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'surat.id_surat_ijin_observasi_pendahuluan as id_surat')
      ->paginate(4);

    $no = 1;

    return view('kajur.mhs.suratobservasipendahuluan.suratijinobservasipendahuluansiap', compact('SuratSiapCetak', 'no'));
  }

  public function CetakSuratIjinObservasiPendahuluan($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_kajur.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_surat_ijin_observasi_pendahuluan', $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
      ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratIjinObservasiPendahuluan($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratIjinObservasiPendahuluanBelumdiProses()
  {
    $count = DB::table($this->tabel_persetujuan.' as persetujuan')
      ->join($this->tabel_surat.' as surat','surat.id_'.$this->tabel_surat,'persetujuan.id_'.$this->tabel_surat)
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->select('status_persetujuan', 'level')
      ->where('level', 'kajur')
      ->where('status_persetujuan', 0)
      ->wherein('mahasiswa.id_prodi',DetilUser::DosenJurusan())
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
