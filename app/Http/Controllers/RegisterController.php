<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Hash;
use Session;

class RegisterController extends Controller
{
    public function register()
    {
        $Jurusan = DB::table('prodi')
        ->select('prodi.id_prodi','prodi.nama_prodi as nama_jurusan')
        ->get();

        $JenisKelamin = [
          'L'=> 'Laki - Laki',
          'P'=> 'Perempuan',
        ];
        return view('pengguna.register',compact('Jurusan','JenisKelamin'));
    }

    public function SimpanRegister(request $data){
      $validasi = Validator::make($data->all(),
      [
        'nim'=>'required|unique:mahasiswa,nim',
        'nama_mahasiswa'=>'required|max:255',
        'jenis_kelamin'=>'required',
        'id_prodi'=>'required',
        'alamat'=>'required',
        'no_hp'=>'required|numeric|unique:mahasiswa,no_hp',
        'email'=>'required|email|unique:users,email',
        'password'=>'required',
        'tempat_lahir'=>'required',
        'tanggal_lahir'=>'required|date',
        'ulangi_password'=>'required|same:password',
      ],
      [
        'nim.required'=>'NIM Harus Diisi.',
        'nim.unique'=>'NIM Sudah Digunakan, Silahkan Hubungi Administrasi untuk Reset Password.',
        'nama_mahasiswa.required'=>'Nama Mahasiswa Harus Diisi.',
        'jenis_kelamin.required'=>'Jenis Kelamin Harus Dipilih.',
        'id_prodi.required'=>'Jurusan Harus Dipilih.',
        'alamat.required'=>'Alamat Harus Diisi.',
        'no_hp.required'=>'Nomor HP Harus Diisi.',
        'email.required'=>'Email Harus Diisi.',
        'password.required'=>'Password Harus Diisi.',
        'ulangi_password.required'=>'Ulangi Password Harus Disi.',
        'nama_mahasiswa.max'=>'Nama Mahasiswa Tidak Boleh Lebih dari 255 Karakter.',
        'no_hp.numeric'=>'Nomor HP Harus Angka.',
        'email.email'=>'Format Email Salah.',
        'ulangi_password.same'=>'Ulangi Password Harus Sama Dengan Password',
        'tempat_lahir.required'=>'Tempat Lahir Harus Diisi.',
        'tanggal_lahir.required'=>'Tanggal Lahir Harus Diisi.',
        'tanggal_lahir.date'=>'Format Tanggal Salah.',
        'email.unique'=>'Email Sudah Digunakan, Silahkan Gunakan Email Lain.',
        'no_hp.unique'=>'No HP Sudah Digunakan, Silahkan Gunakan No HP yang lain.',
      ]);

      if($validasi->fails()){
        return redirect(url()->previous())
        ->withInput()
        ->withErrors($validasi);
      }

      $data->merge([
        'password'=>Hash::make($data->password),
        'level'=>'mhs',
      ]);
      DB::BeginTransaction();
      try{
        $id_user = DB::table('users')
        ->insertGetId($data->only('email','password','level'));
        $data->merge([
          'id_user'=>$id_user,
        ]);
        DB::table('mahasiswa')
        ->insert($data->only('id_user','id_prodi','nim','nama_mahasiswa','alamat','no_hp','tempat_lahir','tanggal_lahir'));
        Session::flash('simpan_sukses','Registrasi Berhasil, Silahkan Login.');
        DB::commit();
        return redirect(route('login'));
      }catch(\Exception $e){
        DB::rollback();
        report($e);
        Session::flash('simpan_gagal','Registrasi Gagal, Silahkan Hubungi Fakultas');
        return redirect(url()->previous())
        ->withInput();
      }
    }
}
