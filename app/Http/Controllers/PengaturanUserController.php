<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class PengaturanUserController extends Controller
{
  public function __construct(){
    $this->middleware(['auth']);
  }

    //Controller Dekan
    public function TampilPenggunaDekan()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->select('dosen.nip', 'dosen.nama_dosen', 'users.email', 'users.id_user')
            ->where('dosen.level_dosen', '=', 'dekan')
            ->where('dosen.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('dekan.tampildekan', compact('data'));
    }
    public function UbahPasswordPenggunaDekan()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->select(
                'users.password',
                'users.id_user'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('dekan.ubahpassworddekan', compact('data'));
    }
    public function UpdatePasswordPenggunaDekan(Request $request)
    {
        $validasi = $request->validate(
            [
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password'
            ],
            [
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Disi.',
                'password.min' => 'Password Minimal 4 Karakter.',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password Baru.',
            ]
        );

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'password' => Hash::make($request->password),
            ]);
        return redirect(route('UbahPasswordPenggunaDekan'))->with('password_edit', 'Password Berhasil Diubah.');
    }
    public function DetailPenggunaDekan()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'users.id_user',
                'users.email',
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('dekan.detaildekan', compact('data'));
    }
    public function EditPenggunaDekan()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'users.id_user',
                'users.email'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('dekan.editdekan', compact('data'));
    }
    public function UpdatePenggunaDekan(Request $request)
    {
        $validasi = $request->validate(
            [
                'nama_dosen' => 'required|max:255',
                'no_hp' => 'required|min:10|max:20',
                'nama_prodi' => 'required'
            ],
            [
                'nama_dosen.required' => 'Nama Dosen Harus Diisi.',
                'nama_dosen.max' => 'Nama Dosen Tidak Boleh Lebih dari 255 Karakter.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.max' => 'No Hp Maximal 20 Karakter',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
                'nama_prodi.required' => 'Prodi Tidak Boleh Kosong',

            ]
        );
        $nidn = $request->input('nidn');
        $nip = $request->input('nip');
        $nama_dosen = $request->input('nama_dosen');
        $nama_prodi = $request->input('nama_prodi');
        $no_hp = $request->input('no_hp');

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'nidn' => $nidn,
                'nip' => $nip,
                'nama_dosen' => $nama_dosen,
                'nama_prodi' => $nama_prodi,
                'no_hp' => $no_hp,
            ]);
        return redirect(route('DetailPenggunaDekan'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }

    //Controller Wadek1
    public function TampilPenggunaWadek1()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->select('dosen.nip', 'dosen.nama_dosen', 'users.email', 'users.id_user')
            ->where('dosen.level_dosen', '=', 'wadek1')
            ->where('dosen.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('wadek1.tampilwadek1', compact('data'));
    }
    public function UbahPasswordPenggunaWadek1()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->select(
                'users.password',
                'users.id_user'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('wadek1.ubahpasswordwadek1', compact('data'));
    }
    public function UpdatePasswordPenggunaWadek1(Request $request)
    {
        $validasi = $request->validate(
            [
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password'
            ],
            [
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Disi.',
                'password.min' => 'Password Minimal 4 Karakter.',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password Baru.',
            ]
        );

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'password' => Hash::make($request->password),
            ]);
        return redirect(route('UbahPasswordPenggunaWadek1'))->with('password_edit', 'Password Berhasil Diubah.');
    }
    public function DetailPenggunaWadek1()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'users.id_user',
                'users.email',
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('wadek1.detailwadek1', compact('data'));
    }
    public function EditPenggunaWadek1()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'users.id_user',
                'users.email'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('wadek1.editwadek1', compact('data'));
    }
    public function UpdatePenggunaWadek1(Request $request)
    {
        $validasi = $request->validate(
            [
                'nama_dosen' => 'required|max:255',
                'no_hp' => 'required|min:10|max:20',
                'nama_prodi' => 'required'
            ],
            [
                'nama_dosen.required' => 'Nama Dosen Harus Diisi.',
                'nama_dosen.max' => 'Nama Dosen Tidak Boleh Lebih dari 255 Karakter.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.max' => 'No Hp Maximal 20 Karakter',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
                'nama_prodi.required' => 'Prodi Tidak Boleh Kosong',

            ]
        );
        $nidn = $request->input('nidn');
        $nip = $request->input('nip');
        $nama_dosen = $request->input('nama_dosen');
        $nama_prodi = $request->input('nama_prodi');
        $no_hp = $request->input('no_hp');

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'nidn' => $nidn,
                'nip' => $nip,
                'nama_dosen' => $nama_dosen,
                'nama_prodi' => $nama_prodi,
                'no_hp' => $no_hp,
            ]);
        return redirect(route('DetailPenggunaWadek1'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }

    //Controller Wadek2
    public function UbahPasswordPenggunaWadek2()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->select(
                'users.password',
                'users.id_user'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('wadek2.ubahpasswordwadek2', compact('data'));
    }
    public function UpdatePasswordPenggunaWadek2(Request $request)
    {
        $validasi = $request->validate(
            [
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password'
            ],
            [
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Disi.',
                'password.min' => 'Password Minimal 4 Karakter.',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password Baru.',
            ]
        );

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'password' => Hash::make($request->password),
            ]);
        return redirect(route('UbahPasswordPenggunaWadek2'))->with('password_edit', 'Password Berhasil Diubah.');
    }
    public function DetailPenggunaWadek2()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'users.id_user',
                'users.email',
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('wadek2.detailwadek2', compact('data'));
    }
    public function EditPenggunaWadek2()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'users.id_user',
                'users.email'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('wadek2.editwadek2', compact('data'));
    }
    public function UpdatePenggunaWadek2(Request $request)
    {
        $validasi = $request->validate(
            [
                'nama_dosen' => 'required|max:255',
                'no_hp' => 'required|min:10|max:20',
                'nama_prodi' => 'required'
            ],
            [
                'nama_dosen.required' => 'Nama Dosen Harus Diisi.',
                'nama_dosen.max' => 'Nama Dosen Tidak Boleh Lebih dari 255 Karakter.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.max' => 'No Hp Maximal 20 Karakter',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
                'nama_prodi.required' => 'Prodi Tidak Boleh Kosong',

            ]
        );
        $nidn = $request->input('nidn');
        $nip = $request->input('nip');
        $nama_dosen = $request->input('nama_dosen');
        $nama_prodi = $request->input('nama_prodi');
        $no_hp = $request->input('no_hp');

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'nidn' => $nidn,
                'nip' => $nip,
                'nama_dosen' => $nama_dosen,
                'nama_prodi' => $nama_prodi,
                'no_hp' => $no_hp,
            ]);
        return redirect(route('DetailPenggunaWadek2'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }

    //Controller Wadek3
    public function UbahPasswordPenggunaWadek3()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->select(
                'users.password',
                'users.id_user'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('wadek3.ubahpasswordwadek3', compact('data'));
    }
    public function UpdatePasswordPenggunaWadek3(Request $request)
    {
        $validasi = $request->validate(
            [
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password'
            ],
            [
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Disi.',
                'password.min' => 'Password Minimal 4 Karakter.',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password Baru.',
            ]
        );

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'password' => Hash::make($request->password),
            ]);
        return redirect(route('UbahPasswordPenggunaWadek3'))->with('password_edit', 'Password Berhasil Diubah.');
    }
    public function DetailPenggunaWadek3()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'users.id_user',
                'users.email',
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('wadek3.detailwadek3', compact('data'));
    }
    public function EditPenggunaWadek3()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'users.id_user',
                'users.email'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('wadek3.editwadek3', compact('data'));
    }
    public function UpdatePenggunaWadek3(Request $request)
    {
        $validasi = $request->validate(
            [
                'nama_dosen' => 'required|max:255',
                'no_hp' => 'required|min:10|max:20',
                'nama_prodi' => 'required'
            ],
            [
                'nama_dosen.required' => 'Nama Dosen Harus Diisi.',
                'nama_dosen.max' => 'Nama Dosen Tidak Boleh Lebih dari 255 Karakter.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.max' => 'No Hp Maximal 20 Karakter',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
                'nama_prodi.required' => 'Prodi Tidak Boleh Kosong',

            ]
        );
        $nidn = $request->input('nidn');
        $nip = $request->input('nip');
        $nama_dosen = $request->input('nama_dosen');
        $nama_prodi = $request->input('nama_prodi');
        $no_hp = $request->input('no_hp');

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'nidn' => $nidn,
                'nip' => $nip,
                'nama_dosen' => $nama_dosen,
                'nama_prodi' => $nama_prodi,
                'no_hp' => $no_hp,
            ]);
        return redirect(route('DetailPenggunaWadek3'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }

    // Controler Dosen
    public function TampilPenggunaDosen()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->select('dosen.nip', 'dosen.nama_dosen', 'users.email', 'users.id_user')
            ->where('dosen.level_dosen', '=', 'dosen')
            ->where('dosen.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('dosen.tampildosen', compact('data'));
    }
    public function UbahPasswordPenggunaDosen()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->select(
                'users.password',
                'users.id_user'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('dosen.ubahpassworddosen', compact('data'));
    }
    public function UpdatePasswordPenggunaDosen(Request $request)
    {
        $validasi = $request->validate(
            [
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password'
            ],
            [
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Disi.',
                'password.min' => 'Password Minimal 4 Karakter.',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password Baru.',
            ]
        );

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'password' => Hash::make($request->password),
            ]);
        return redirect(route('UbahPasswordPenggunaDosen'))->with('password_edit', 'Password Berhasil Diubah.');
    }
    public function DetailPenggunaDosen()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'users.id_user',
                'users.email',
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('dosen.detaildosen', compact('data'));
    }
    public function EditPenggunaDosen()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'users.id_user',
                'users.email'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('dosen.editdosen', compact('data'));
    }
    public function UpdatePenggunaDosen(Request $request)
    {
        $validasi = $request->validate(
            [
                'nama_dosen' => 'required|max:255',
                'no_hp' => 'required|min:10|max:20',
                'nama_prodi' => 'required'
            ],
            [
                'nama_dosen.required' => 'Nama Dosen Harus Diisi.',
                'nama_dosen.max' => 'Nama Dosen Tidak Boleh Lebih dari 255 Karakter.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.max' => 'No Hp Maximal 20 Karakter',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
                'nama_prodi.required' => 'Prodi Tidak Boleh Kosong',

            ]
        );
        $nidn = $request->input('nidn');
        $nip = $request->input('nip');
        $nama_dosen = $request->input('nama_dosen');
        $nama_prodi = $request->input('nama_prodi');
        $no_hp = $request->input('no_hp');

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'nidn' => $nidn,
                'nip' => $nip,
                'nama_dosen' => $nama_dosen,
                'nama_prodi' => $nama_prodi,
                'no_hp' => $no_hp,
            ]);
        return redirect(route('DetailPenggunaDosen'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }

    //COntroller Kaprodi
    public function TampilPenggunaKaprodi()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->select('dosen.nip', 'dosen.nama_dosen', 'users.email', 'users.id_user')
            ->where('dosen.level_dosen', '=', 'kaprodi')
            ->where('dosen.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('kaprodi.tampilkaprodi', compact('data'));
    }
    public function UbahPasswordPenggunaKaprodi()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->select(
                'users.password',
                'users.id_user'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('kaprodi.ubahpasswordkaprodi', compact('data'));
    }
    public function UpdatePasswordPenggunaKaprodi(Request $request)
    {
        $validasi = $request->validate(
            [
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password'
            ],
            [
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Disi.',
                'password.min' => 'Password Minimal 4 Karakter.',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password Baru.',
            ]
        );

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'password' => Hash::make($request->password),
            ]);
        return redirect(route('UbahPasswordPenggunaKaprodi'))->with('password_edit', 'Password Berhasil Diubah.');
    }
    public function DetailPenggunakaprodi()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'users.id_user',
                'users.email',
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('kaprodi.detailkaprodi', compact('data'));
    }
    public function EditPenggunaKaprodi()
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'users.id_user',
                'users.email'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('kaprodi.editkaprodi', compact('data'));
    }
    public function UpdatePenggunaKaprodi(Request $request)
    {
        $validasi = $request->validate(
            [
                'nama_dosen' => 'required|max:255',
                'no_hp' => 'required|min:10|max:20',
                'nama_prodi' => 'required'
            ],
            [
                'nama_dosen.required' => 'Nama Dosen Harus Diisi.',
                'nama_dosen.max' => 'Nama Dosen Tidak Boleh Lebih dari 255 Karakter.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.max' => 'No Hp Maximal 20 Karakter',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
                'nama_prodi.required' => 'Prodi Tidak Boleh Kosong',

            ]
        );
        $nidn = $request->input('nidn');
        $nip = $request->input('nip');
        $nama_dosen = $request->input('nama_dosen');
        $nama_prodi = $request->input('nama_prodi');
        $no_hp = $request->input('no_hp');

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'dosen.id_prodi')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'nidn' => $nidn,
                'nip' => $nip,
                'nama_dosen' => $nama_dosen,
                'nama_prodi' => $nama_prodi,
                'no_hp' => $no_hp,
            ]);
        return redirect(route('DetailPenggunaKaprodi'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }

    //Controller Adm

    public function TampilPenggunaAdm()
    {
        $data = DB::table('users')
            ->join('pegawai', 'users.id_user', '=', 'pegawai.id_user')
            ->select('pegawai.nip', 'pegawai.nama_pegawai', 'users.email', 'users.id_user', 'users.password')
            ->where('users.level', '=', 'adm')
            ->where('pegawai.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('adm.tampiladm', compact('data'));
    }
    public function UbahPassworPenggunadAdm()
    {
        $data = DB::table('users')
            ->join('pegawai', 'users.id_user', '=', 'pegawai.id_user')
            ->select(
                'users.password',
                'users.id_user'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('adm.ubahpasswordadm', compact('data'));
    }
    public function UpdatePasswordPenggunaAdm(Request $request)
    {
        $validasi = $request->validate(
            [
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password'
            ],
            [
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Disi.',
                'password.min' => 'Password Minimal 4 Karakter.',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password Baru.',
            ]
        );

        $data = DB::table('users')
            ->join('pegawai', 'pegawai.id_user', '=', 'users.id_user')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'password' => Hash::make($request->password),
            ]);
        return redirect(route('UbahPassworPenggunadAdm'))->with('data_password', 'Password Berhasil Dirubah.');
    }
    public function DetailPenggunaAdm()
    {
        $data = DB::table('users')
            ->join('pegawai', 'users.id_user', '=', 'pegawai.id_user')
            ->select(
                'pegawai.nip',
                'pegawai.nama_pegawai',
                'pegawai.no_hp',
                'users.id_user',
                'users.email',
                'users.level'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();



        return view('adm.detailadm', compact('data'));
    }
    public function EditPenggunaAdm()
    {
        $data = DB::table('users')
            ->join('pegawai', 'users.id_user', '=', 'pegawai.id_user')
            ->select(
                'pegawai.nip',
                'pegawai.nama_pegawai',
                'users.id_user',
                'pegawai.no_hp',
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('adm.editadm', compact('data'));
    }
    public function UpdatePenggunaAdm(Request $request)
    {
        $validasi = $request->validate(
            [
                'nama_pegawai' => 'required|max:255',
                'no_hp' => 'required|min:11|max:20',
            ],
            [
                'nama_pegawai.required' => 'Nama Admin Harus Diisi.',
                'nama_pegawai.max' => 'Nama Admin Tidak Boleh Lebih dari 255 Karakter.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
                'no_hp.max' => 'No Hp Maximal 20 Karakter'
            ]
        );
        $nama_pegawai = $request->input('nama_pegawai');
        $no_hp = $request->input('no_hp');

        $data = DB::table('users')
            ->join('pegawai', 'pegawai.id_user', '=', 'users.id_user')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'nama_pegawai' => $nama_pegawai,
                'no_hp' => $no_hp,
            ]);

        // dd($data);

        // $data =  DB::update(
        //     'update users, pegawai set users.password = ?,  pegawai.nip = ?, pegawai. nama_pegawai = ?,
        // pegawai.no_hp = ? where users.id_user = pegawai.id_user and  users.id_user = ?',
        //     [$password, $nip, $nama_pegawai, $no_hp, $id_user]
        // );


        return redirect(route('DetailPenggunaAdm'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }

    //Controller Mhs
    public function TampilPenggunaMhs()
    {
        $data = DB::table('users')
            ->join('mahasiswa', 'mahasiswa.id_user', 'users.id_user')
            ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'users.email',  'users.id_user')
            ->where('users.level', '=', 'mhs')
            ->where('mahasiswa.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('mhs.tampilmhs', compact('data'));
    }
    public function UbahPasswordPenggunaMhs()
    {
        $data = DB::table('users')
            ->join('mahasiswa', 'users.id_user', '=', 'mahasiswa.id_user')
            ->select(
                'users.password',
                'users.id_user'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->get();

        return view('mhs.ubahpasswordmhs', compact('data'));
    }
    public function UpdatePasswordPenggunaMhs(Request $request)
    {

        $validasi = $request->validate(
            [
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password'
            ],
            [
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Disi.',
                'password.min' => 'Password Minimal 4 Karakter.',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password Baru.',
            ]
        );

        $data = DB::table('users')
            ->join('mahasiswa', 'mahasiswa.id_user', '=', 'users.id_user')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'password' => Hash::make($request->password),
            ]);
        return redirect(route('UbahPasswordPenggunaMhs'))->with('password_edit', 'Password Berhasil Diubah.');
    }
    public function DetailPenggunaMhs()
    {
        $data = DB::table('users')
            ->join('mahasiswa', 'users.id_user', '=', 'mahasiswa.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'mahasiswa.id_prodi')
            ->select(
                'mahasiswa.nim',
                'mahasiswa.nama_mahasiswa',
                'mahasiswa.tempat_lahir',
                'mahasiswa.tanggal_lahir',
                'mahasiswa.no_hp',
                'mahasiswa.alamat',
                'users.id_user',
                'users.email',
                'prodi.nama_prodi'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('mhs.detailmhs', compact('data'));
    }
    public function EditPenggunaMhs()
    {
        $data = DB::table('users')
            ->join('mahasiswa', 'users.id_user', '=', 'mahasiswa.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'mahasiswa.id_prodi')
            ->select(
                'mahasiswa.nim',
                'mahasiswa.nama_mahasiswa',
                'mahasiswa.alamat',
                'mahasiswa.no_hp',
                'mahasiswa.id_prodi',
                'mahasiswa.tempat_lahir',
                'mahasiswa.tanggal_lahir',
                'users.id_user',
                'users.email',
                'prodi.nama_prodi'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('mhs.editmhs', compact('data'));
    }
    public function UpdatePenggunaMhs(Request $request)
    {
        $validasi = $request->validate(
            [
                'nama_mahasiswa' => 'required|max:255',
                'no_hp' => 'required|min:10|max:20',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required',
                'alamat' => 'required',

            ],
            [
                'nama_mahasiswa.required' => 'Nama Mahasiswa Harus Diisi.',
                'nama_mahasiswa.max' => 'Nama Mahasiswa Tidak Boleh Lebih dari 255 Karakter.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
                'no_hp.max' => 'No Hp Maximal 20 Angka',
                'alamat.required' => 'Alamat Tidak Boleh Kosong'

            ]
        );

        $nama_mahasiswa = $request->input('nama_mahasiswa');
        $nama_prodi = $request->input('nama_prodi');
        $alamat = $request->input('alamat');
        $tempat_lahir = $request->input('tempat_lahir');
        $tanggal_lahir = $request->input('tanggal_lahir');
        $no_hp = $request->input('no_hp');

        $data = DB::table('users')
            ->join('mahasiswa', 'mahasiswa.id_user', '=', 'users.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'mahasiswa.id_prodi')
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->update([
                'nama_mahasiswa' => $nama_mahasiswa,
                'nama_prodi' => $nama_prodi,
                'no_hp' => $no_hp,
                'tempat_lahir' => $tempat_lahir,
                'tanggal_lahir' => $tanggal_lahir,
                'alamat' => $alamat,
            ]);

        return redirect(route('DetailPenggunaMhs'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }
}
