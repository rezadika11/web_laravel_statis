<?php

namespace App\Http\Controllers\Admin;

use App\DosenModel;
use App\Http\Controllers\Controller;
use App\ProdiModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use Dotenv\Regex\Result;
use Illuminate\Support\Facades\Hash;

class DosenController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'level:admin']);
    }

    public function DetailDosen()
    {
        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', 'users.id_user')
            ->join('prodi','prodi.id_prodi','dosen.id_prodi')
            ->select('dosen.nama_dosen', 'dosen.nidn', 'users.id_user','prodi.nama_prodi')
            ->where('users.level', '=', 'dosen')
            ->paginate(4);

        return view('admin.dosen.detaildosen', compact('data'));
    }

    public function TambahDosen()
    {
        $jurusan = DB::table('prodi')
            ->select('prodi.id_prodi', 'prodi.nama_prodi')
            ->get();

        return view('admin.dosen.tambahdosen', compact('jurusan'));
    }
    public function SimpanDosen(Request $request)
    {
        $validasi = $request->validate(
            [
                'nidn' => 'required|max:255',
                'nip' => 'required|max:255',
                'nama_dosen' => 'required|max:255',
                'email' => 'required|email|unique:users',
                'no_hp' => 'required|min:10|max:20',
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password',
                'id_prodi' => 'required'
            ],
            [
                'nidn.required' => 'MIDN Harus Diisi',
                'nidn.max' => 'NIDN Tidak Boleh Lebih dari 255 Karakter',
                'nip.required' => 'NIP Harus Diisi.',
                'nama_dosen.required' => 'Nama Dosen Harus Diisi.',
                'email.required' => 'Email Harus Diisi.',
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Diisi.',
                'nama_dosen.max' => 'Nama Dosen Tidak Boleh Lebih dari 255 Karakter.',
                'email.email' => 'Format Email Salah.',
                'email.unique' => 'Email Tersebut Sudah Ada.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
                'no_hp.max' => 'No Hp Maximal 20 Karakter',
                'password.min' => 'Password Minimal 4 Karakter',
                'id_prodi.required' => 'Prodi Tidak Boleh Kosong',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password',

            ]
        );

        $data = $request->all();
        $d = array(
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'level' => 'dosen',
        );

        $id_user = DB::table('users')->insertGetId($d);

        DB::table('dosen')->insertGetId([
            'nidn' => $request->nidn,
            'nip' => $request->nip,
            'nama_dosen' => $request->nama_dosen,
            'no_hp' => $request->no_hp,
            'level_dosen' => 'dosen',
            'id_prodi' => $request->id_prodi,
            'id_user' => $id_user
        ]);

        return redirect(route('admin.DetailDosen'))->with('data_disimpan', 'Data Baru Ditambahkan.');
    }
    public function EditDosen($id_user)
    {
        $jurusan = DB::table('prodi')
            ->select('prodi.id_prodi', 'prodi.nama_prodi')
            ->get();
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'dosen.id_prodi', '=', 'prodi.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'dosen.level_dosen',
                'users.email',
                'users.id_user',
                'prodi.id_prodi'
            )
            ->where('users.id_user', '=', $id_user)
            ->first();
        return view('admin.dosen.editdosen', compact('data', 'jurusan'));
    }
    public function UpdateDosen(Request $request, $id_user)
    {
        $validasi = $request->validate(
            [
                'nidn' => 'required|max:255',
                'nip' => 'required|max:255',
                'nama_dosen' => 'required|max:255',
                'no_hp' => 'required|min:10|max:20',
                'nama_prodi' => 'required',
                'email' => 'required'
            ],
            [
                'nidn.required' => 'MIDN Harus Diisi',
                'nidn.max' => 'NIDN Tidak Boleh Lebih dari 255 Karakter',
                'nip.required' => 'NIP Harus Diisi.',
                'nama_dosen.required' => 'Nama Dosen Harus Diisi.',
                'nama_dosen.max' => 'Nama Dosen Tidak Boleh Lebih dari 255 Karakter.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.max' => 'No Hp Maximal 20 Karakter',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
                'nama_prodi.required' => 'Prodi Tidak Boleh Kosong',
                'email.required' => 'Email Tidak Boleh Kosong',

            ]
        );
        $nidn = $request->input('nidn');
        $nip = $request->input('nip');
        $nama_dosen = $request->input('nama_dosen');
        $nama_prodi = $request->input('nama_prodi');
        $level_dosen = 'dosen';
        $no_hp = $request->input('no_hp');
        $email = $request->input('email');

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->join('prodi', 'dosen.id_prodi', '=', 'prodi.id_prodi')
            ->where('users.id_user', $id_user)
            ->update([
                'dosen.nidn' => $nidn,
                'dosen.nip' => $nip,
                'dosen.nama_dosen' => $nama_dosen,
                'prodi.nama_prodi' => $nama_prodi,
                'dosen.level_dosen' => $level_dosen,
                'dosen.level_dosen' => $level_dosen,
                'users.email' => $email,
            ]);
        return redirect(route('admin.DetailDosen'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }
    public function TampilDosen($id_user)
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi', 'dosen.id_prodi', '=', 'prodi.id_prodi')
            ->select(
                'dosen.nidn',
                'dosen.nip',
                'prodi.nama_prodi',
                'dosen.nama_dosen',
                'dosen.no_hp',
                'users.id_user',
                'users.email',
            )
            ->where('users.id_user', '=', $id_user)
            ->first();

        return view('admin.dosen.tampildosen', compact('data'));
    }
    public function UbahPassword($id_user)
    {
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->select(
                'users.password',
                'users.id_user'
            )
            ->where('users.id_user', '=', $id_user)
            ->first();

        return view('admin.dosen.ubahpassworddosen', compact('data'));
    }
    public function UpdatePassword(Request $request, $id_user)
    {
        $validasi = $request->validate(
            [
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password'
            ],
            [
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Disi.',
                'password.min' => 'Password Minimal 4 Karakter.',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password Baru.',
            ]
        );

        $data = DB::table('users')
            ->join('dosen', 'dosen.id_user', '=', 'users.id_user')
            ->where('users.id_user', $id_user)
            ->update([
                'password' => Hash::make($request->password),
            ]);
        return redirect(route('admin.DetailDosen'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }
    public function DosenCari(Request $request)
    {
        $cari = $request->name;
        $data = DB::table('users')
            ->join('dosen', 'users.id_user', '=', 'dosen.id_user')
            ->join('prodi','prodi.id_prodi','dosen.id_prodi')
            ->select('dosen.nama_dosen', 'dosen.nidn', 'users.id_user','prodi.nama_prodi')
            ->where('dosen.nama_dosen', 'like', "%" . $cari . "%")
            ->paginate(4);

        $c = count($data);

        if ($c == 0) {
            return '<table class="table table-bordered table-striped">
                <thead>
                  <tr>
                  <th>No.</th>
                  <th>NIDN</th>
                  <th>Nama</th>
                  <th>Program Studi</th>
                  <th>Aksi</th>
                  </tr>
                </thead>

                <td colspan="5" class="text-center bg-light">Maaf data tidak ditemukan</td>
                </table>';
        } else {
            return view('admin.dosen.dosencari')->with([
                'data' => $data
            ]);
        }
    }
}
