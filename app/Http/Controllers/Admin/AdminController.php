<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PegawaiModel;
use App\User;
use FontLib\Table\Type\post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'level:admin']);
    }
    public function DetailAdmin()
    {
        $data = DB::table('users')
            ->join('pegawai', 'users.id_user', '=', 'pegawai.id_user')
            ->select('pegawai.nip', 'pegawai.nama_pegawai', 'users.email', 'users.id_user')
            ->where('users.level', '=', 'admin')
            ->paginate(4);

        return view('admin.detailadmin', compact('data'));
    }
    // public function TambahAdmin()
    // {
    //     return view('admin.tambahadmin');
    // }
    // public function SimpanAdmin(Request $request)
    // {

    //     $validasi = $request->validate(
    //         [
    //             'nip' => 'required|max:255',
    //             'nama_admin' => 'required|max:255',
    //             'email' => 'required|email|unique:users',
    //             'no_hp' => 'required|min:11',
    //             'password' => 'required|min:4',
    //             'ulangi_password' => 'required|same:password'
    //         ],
    //         [
    //             'nip.required' => 'NIP Harus Diisi.',
    //             'nama_admin.required' => 'Nama Admin Harus Diisi.',
    //             'email.required' => 'Email Harus Diisi.',
    //             'password.required' => 'Password Harus Diisi.',
    //             'ulangi_password.required' => 'Ulangi Password Harus Disi.',
    //             'nama_admin.max' => 'Nama Admin Tidak Boleh Lebih dari 255 Karakter.',
    //             'email.email' => 'Format Email Salah.',
    //             'email.unique' => 'Email Tersebut Sudah Ada.',
    //             'no_hp.required' => 'No Hp Wajib Diisi',
    //             'no_hp.min' => 'No Hp Minimal 10 Angka',
    //             'password.min' => 'Password Minimal 4 Karakter',
    //             'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password',
    //         ]
    //     );

    //     $request->merge([
    //         'password' => Hash::make($request->password),
    //         'level' => 'admin',
    //     ]);

    //     $data = $request->all();

    //     $user = new User();
    //     $user->email = $data['email'];
    //     $user->password = $data['password'];
    //     $user->level = 'admin';
    //     $user->save();

    //     $pegawai = new PegawaiModel();
    //     $pegawai->id_user = $user->id_user;
    //     $pegawai->nip = $data['nip'];
    //     $pegawai->nama_pegawai = $data['nama_admin'];
    //     $pegawai->no_hp = $data['no_hp'];
    //     $pegawai->save();

    //     return redirect(route('admin.DetailAdmin'))->with('status', 'Data Berhasil Disimpan.');
    // }

    // public function HapusAdmin($id_user)
    // {
    //     DB::table('users')
    //         ->where('id_user', $id_user)
    //         ->delete();
    //     DB::table('pegawai')
    //         ->where('id_user', $id_user)
    //         ->delete();

    //     return redirect()->back()->with('data_dihapus', 'Data Berhasil Dihapus');
    // }
    public function EditAdmin($id_user)
    {

        $data = DB::table('users')
            ->join('pegawai', 'users.id_user', '=', 'pegawai.id_user')
            ->select(
                'pegawai.nip',
                'pegawai.nama_pegawai',
                'users.id_user',
                'pegawai.no_hp'
            )
            ->where('users.id_user', '=', $id_user)
            ->first();

        return view('admin.editadmin', compact('data'));
    }
    public function UpdateAdmin(Request $request, $id_user)
    {
        $validasi = $request->validate(
            [
                'nip' => 'required|max:255',
                'nama_admin' => 'required|max:255',
                'no_hp' => 'required|min:11',

            ],
            [
                'nip.required' => 'NIP Harus Diisi.',
                'nama_admin.required' => 'Nama Admin Harus Diisi.',
                'nama_admin.max' => 'Nama Admin Tidak Boleh Lebih dari 255 Karakter.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
            ]
        );

        $nip = $request->input('nip');
        $nama_admin = $request->input('nama_admin');
        $no_hp = $request->input('no_hp');
        DB::table('pegawai')
            ->where('id_user', $id_user)
            ->update(['nip' => $nip, 'nama_pegawai' => $nama_admin, 'no_hp' => $no_hp]);

        // $data = DB::update('update pegawai set nip = ?, nama_pegawai = ?,
        // no_hp = ? where id_user = ?', [$nip, $nama_admin, $no_hp, $id_user]);

        return redirect(route('admin.TampilAdmin'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }
    public function TampilAdmin()
    {
        $data = DB::table('users')
            ->join('pegawai', 'users.id_user', '=', 'pegawai.id_user')
            ->select(
                'pegawai.nip',
                'pegawai.nama_pegawai',
                'pegawai.no_hp',
                'users.id_user',
                'users.email',
                'users.level'
            )
            ->where('users.id_user', '=', Auth::user()->id_user)
            ->first();

        return view('admin.tampiladmin', compact('data'));
    }
    public function UbahPassword()
    {

        return view('admin.ubahpassword');
    }
    public function UpdatePassword(Request $request)
    {
        $validasi = $request->validate(
            [
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password'
            ],
            [
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Disi.',
                'password.min' => 'Password Minimal 4 Karakter.',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password Baru.',
            ]
        );
        $request->user()->update([
            'password' => Hash::make($request->get('password'))
        ]);


        return redirect(route('admin.UbahPassword'))->with('data_password', 'Password Berhasil Diubah.');
    }
    public function AdminCari(Request $request)
    {
        $cari = $request->cari;
        // $keyword = $request->search;
        // $data = PegawaiModel::where('nama_pegawai', 'like', "%" . $keyword . "%")->paginate(4);
        // return view('admin.DetailAdmin', compact('data'))->with('i', (request()->input('page', 1) - 1) * 4);
        $data = DB::table('users')
            ->join('pegawai', 'users.id_user', '=', 'pegawai.id_user')
            ->where('users.level', '=', 'admin')
            ->where('pegawai.nama_pegawai', 'like', "%" . $cari . "%")
            ->paginate(4);

        return view('admin.DetailAdmin', compact('data'));
    }
    public function TampilSuratMhs()
    {
        $data = DB::table('surat_mahasiswa')
            ->select('id_surat_mahasiswa', 'jenis_surat', 'no_surat_awalan', 'no_surat_akhiran', 'nama_tabel')
            ->paginate(4);

        return view('admin.suratmhs.tampilsuratmhs', compact('data'));
    }
    public function DetailSuratMhs($id_surat_mahasiswa)
    {
        $data = DB::table('surat_mahasiswa')
            ->select('id_surat_mahasiswa', 'jenis_surat', 'no_surat_awalan', 'no_surat_akhiran', 'nama_tabel')
            ->where('id_surat_mahasiswa', '=', $id_surat_mahasiswa)
            ->first();

        return view('admin.suratmhs.detailsuratmhs', compact('data'));
    }
    public function EditSuratMhs($id_surat_mahasiswa)
    {

        $data = DB::table('surat_mahasiswa')
            ->select('id_surat_mahasiswa', 'jenis_surat', 'no_surat_awalan', 'no_surat_akhiran', 'nama_tabel')
            ->where('id_surat_mahasiswa', '=', $id_surat_mahasiswa)
            ->first();


        return view('admin.suratmhs.EditSuratMhs', compact('data'));
    }
    public function UpdateSuratMhs(Request $request, $id_surat_mahasiswa)
    {
        $validasi = $request->validate(
            [
                'no_surat_awalan' => 'required|max:255',
                'no_surat_akhiran' => 'required|max:255',

            ],
            [
                'no_surat_awalan.required' => 'No Surat Awalan Harus Diisi.',
                'no_surat_awalan.required' => 'No Surat Akhiran Harus Diisi.',
            ]
        );

        $no_surat_awalan = $request->input('no_surat_awalan');
        $no_surat_akhiran = $request->input('no_surat_akhiran');
        DB::table('surat_mahasiswa')
            ->where('id_surat_mahasiswa', $id_surat_mahasiswa)
            ->update(['no_surat_awalan' => $no_surat_awalan, 'no_surat_akhiran' => $no_surat_akhiran]);

        return redirect(route('admin.TampilSuratMhs'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }
    public function SuratMhsCari(Request $request)
    {
        $cari = $request->cari;
        // $keyword = $request->search;
        // $data = PegawaiModel::where('nama_pegawai', 'like', "%" . $keyword . "%")->paginate(4);
        // return view('admin.DetailAdmin', compact('data'))->with('i', (request()->input('page', 1) - 1) * 4);
        $data = DB::table('surat_mahasiswa')
            ->where('jenis_surat', 'like', "%" . $cari . "%")
            ->paginate(4);

        return view('admin.suratmhs.TampilSuratMhs', compact('data'));
    }
}
