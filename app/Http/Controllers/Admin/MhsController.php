<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MhsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'level:admin']);
    }

    public function DetailMhs()
    {
        $data = DB::table('users')
            ->join('mahasiswa', 'mahasiswa.id_user', 'users.id_user')
            ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'users.email',  'users.id_user')
            ->where('users.level', '=', 'mhs')
            ->paginate(4);

        return view('admin.mhs.detailmhs', compact('data'));
    }
    public function EditMhs($id_user)
    {
        $data = DB::table('users')
            ->join('mahasiswa', 'users.id_user', '=', 'mahasiswa.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'mahasiswa.id_prodi')
            ->select(
                'mahasiswa.nim',
                'mahasiswa.nama_mahasiswa',
                'mahasiswa.alamat',
                'mahasiswa.no_hp',
                'prodi.nama_prodi',
                'mahasiswa.tempat_lahir',
                'mahasiswa.tanggal_lahir',
                'users.id_user',
                'users.email'
            )
            ->where('users.id_user', '=', $id_user)
            ->first();

        return view('admin.mhs.editmhs', compact('data'));
    }
    public function UpdateMhs(Request $request, $id_user)
    {
        $validasi = $request->validate(
            [
                'nama_mahasiswa' => 'required|max:255',
                'no_hp' => 'required|min:10|max:20',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required',
                'alamat' => 'required',

            ],
            [
                'nama_mahasiswa.required' => 'Nama Mahasiswa Harus Diisi.',
                'nama_mahasiswa.max' => 'Nama Mahasiswa Tidak Boleh Lebih dari 255 Karakter.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
                'no_hp.max' => 'No Hp Maximal 20 Angka',
                'alamat.required' => 'Alamat Tidak Boleh Kosong'

            ]
        );

        $nama_mahasiswa = $request->input('nama_mahasiswa');
        $nama_prodi = $request->input('nama_prodi');
        $alamat = $request->input('alamat');
        $tempat_lahir = $request->input('tempat_lahir');
        $tanggal_lahir = $request->input('tanggal_lahir');
        $no_hp = $request->input('no_hp');

        $data = DB::table('users')
            ->join('mahasiswa', 'mahasiswa.id_user', '=', 'users.id_user')
            ->join('prodi', 'mahasiswa.id_prodi', '=', 'prodi.id_prodi')
            ->where('users.id_user', $id_user)
            ->update([
                'nama_mahasiswa' => $nama_mahasiswa,
                'nama_prodi' => $nama_prodi,
                'no_hp' => $no_hp,
                'tempat_lahir' => $tempat_lahir,
                'tanggal_lahir' => $tanggal_lahir,
                'alamat' => $alamat,
            ]);

        return redirect(route('admin.DetailMhs'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }
    public function TampilMhs($id_user)
    {
        $data = DB::table('users')
            ->join('mahasiswa', 'users.id_user', '=', 'mahasiswa.id_user')
            ->join('prodi', 'prodi.id_prodi', '=', 'mahasiswa.id_prodi')
            ->select(
                'mahasiswa.nim',
                'mahasiswa.nama_mahasiswa',
                'prodi.nama_prodi',
                'mahasiswa.tempat_lahir',
                'mahasiswa.tanggal_lahir',
                'mahasiswa.no_hp',
                'mahasiswa.alamat',
                'users.id_user',
                'users.email',
            )
            ->where('users.id_user', '=', $id_user)
            ->first();

        return view('admin.mhs.tampilmhs', compact('data'));
    }
    public function UbahPassword($id_user)
    {
        $data = DB::table('users')
            ->join('mahasiswa', 'users.id_user', '=', 'mahasiswa.id_user')
            ->select(
                'users.password',
                'users.id_user'
            )
            ->where('users.id_user', '=', $id_user)
            ->first();

        return view('admin.mhs.ubahpasswordmhs', compact('data'));
    }
    public function updatepassword(Request $request, $id_user)
    {
        $validasi = $request->validate(
            [
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password'
            ],
            [
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Disi.',
                'password.min' => 'Password Minimal 4 Karakter.',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password Baru.',
            ]
        );

        $data = DB::table('users')
            ->join('mahasiswa', 'mahasiswa.id_user', '=', 'users.id_user')
            ->where('users.id_user', $id_user)
            ->update([
                'password' => Hash::make($request->password),
            ]);
        return redirect(route('admin.DetailMhs'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }
    public function MhsCari(Request $request)
    {
        $cari = $request->name;
        $data = DB::table('users')
            ->join('mahasiswa', 'users.id_user', '=', 'mahasiswa.id_user')
            ->where('mahasiswa.nama_mahasiswa', 'like', "%" . $cari . "%")
            ->paginate(4);

        $c = count($data);

        if ($c == 0) {
            return '<table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      <th>No.</th>
                      <th>NIM</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>Aksi</th>
                      </tr>
                    </thead>
                    
                    <td colspan="5" class="text-center bg-light">Maaf data tidak ditemukan</td>
                    </table>';
        } else {
            return view('admin.mhs.mhscari')->with([
                'data' => $data
            ]);
        }
    }
}
