<?php

namespace App\Http\Controllers\Admin;

use App\DataUser;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\PegawaiModel;
use Dotenv\Regex\Result;
use Illuminate\Support\Facades\Hash;

class AdmController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'level:admin']);
    }

    public function DetailAdm()
    {
        $data = DB::table('users')
            ->join('pegawai', 'users.id_user', '=', 'pegawai.id_user')
            ->select('pegawai.nip', 'pegawai.nama_pegawai', 'users.email', 'users.id_user', 'users.password')
            ->where('users.level', '=', 'adm')
            ->paginate(4);

        return view('admin.adm.detailadm', compact('data'));
    }
    public function TambahAdm()
    {
        return view('admin.adm.tambahadm');
    }
    public function SimpanAdm(Request $request)
    {
        $validasi = $request->validate(
            [
                'nip' => 'required|max:255',
                'nama_pegawai' => 'required|max:255',
                'email' => 'required|email|unique:users',
                'no_hp' => 'required|min:10|max:20',
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password'
            ],
            [
                'nip.required' => 'NIP Harus Diisi.',
                'nama_pegawai.required' => 'Nama Adm Harus Diisi.',
                'email.required' => 'Email Harus Diisi.',
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Diisi.',
                'nama_pegawai.max' => 'Nama Adm Tidak Boleh Lebih dari 255 Karakter.',
                'email.email' => 'Format Email Salah.',
                'email.unique' => 'Email Tersebut Sudah Ada.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
                'no_hp.max' => 'No Hp Maximal 20 Karakter',
                'password.min' => 'Password Minimal 4 Karakter',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password',
            ]
        );

        $data = $request->all();
        $d = array(
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'level' => 'adm'
        );

        $id_user = DB::table('users')->insertGetId($d);

        DB::table('pegawai')->insertGetId([
            'nip' => $request->nip,
            'nama_pegawai' => $request->nama_pegawai,
            'no_hp' => $request->no_hp,
            'id_user' => $id_user
        ]);

        return redirect(route('admin.DetailAdm'))->with('data_disimpan', 'Data Berhasil Disimpan.');
    }

    public function EditAdm($id_user)
    {
        $data = DB::table('users')
            ->join('pegawai', 'users.id_user', '=', 'pegawai.id_user')
            ->select(
                'pegawai.nip',
                'pegawai.nama_pegawai',
                'users.id_user',
                'pegawai.no_hp',
            ) 
            ->where('users.id_user', '=', $id_user)
            ->first();

        return view('admin.adm.editadm', compact('data'));
    }
    public function UpdateAdm(Request $request, $id_user)
    {
        $validasi = $request->validate(
            [
                'nama_pegawai' => 'required|max:255',
                'no_hp' => 'required|min:11|max:20',
            ],
            [
                'nama_pegawai.required' => 'Nama Admin Harus Diisi.',
                'nama_pegawai.max' => 'Nama Admin Tidak Boleh Lebih dari 255 Karakter.',
                'no_hp.required' => 'No Hp Wajib Diisi',
                'no_hp.min' => 'No Hp Minimal 10 Angka',
                'no_hp.max' => 'No Hp Maximal 20 Karakter'
            ]
        );
        $nama_pegawai = $request->input('nama_pegawai');
        $no_hp = $request->input('no_hp');

        $data = DB::table('users')
            ->join('pegawai', 'pegawai.id_user', '=', 'users.id_user')
            ->where('users.id_user', $id_user)
            ->update([
                'nama_pegawai' => $nama_pegawai,
                'no_hp' => $no_hp,
            ]);

        // dd($data);

        // $data =  DB::update(
        //     'update users, pegawai set users.password = ?,  pegawai.nip = ?, pegawai. nama_pegawai = ?,
        // pegawai.no_hp = ? where users.id_user = pegawai.id_user and  users.id_user = ?',
        //     [$password, $nip, $nama_pegawai, $no_hp, $id_user]
        // );


        return redirect(route('admin.DetailAdm'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }
    public function TampilAdm($id_user)
    {
        $data = DB::table('users')
            ->join('pegawai', 'users.id_user', '=', 'pegawai.id_user')
            ->select(
                'pegawai.nip',
                'pegawai.nama_pegawai',
                'pegawai.no_hp',
                'users.id_user',
                'users.email',
                'users.level'
            )
            ->where('users.id_user', '=', $id_user)
            ->first();

        return view('admin.adm.tampiladm', compact('data'));
    }
    public function UbahPassword($id_user)
    {
        $data = DB::table('users')
            ->join('pegawai', 'users.id_user', '=', 'pegawai.id_user')
            ->select(
                'users.password',
                'users.id_user'
            )
            ->where('users.id_user', '=', $id_user)
            ->first();

        return view('admin.adm.ubahpasswordadm', compact('data'));
    }
    public function UpdatePassword(Request $request, $id_user)
    {
        $validasi = $request->validate(
            [
                'password' => 'required|min:4',
                'ulangi_password' => 'required|same:password'
            ],
            [
                'password.required' => 'Password Harus Diisi.',
                'ulangi_password.required' => 'Ulangi Password Harus Disi.',
                'password.min' => 'Password Minimal 4 Karakter.',
                'ulangi_password.same' => 'Ulangi Password Harus Sama Dengan Password Baru.',
            ]
        );

        $data = DB::table('users')
            ->join('pegawai', 'pegawai.id_user', '=', 'users.id_user')
            ->where('users.id_user', $id_user)
            ->update([
                'password' => Hash::make($request->password),
            ]);
        return redirect(route('admin.DetailAdm'))->with('data_diedit', 'Data Berhasil DiUpdate.');
    }
    public function AdmCari(Request $request)
    {
        $cari = $request->name;
        $data = DB::table('users')
            ->join('pegawai', 'users.id_user', '=', 'pegawai.id_user')
            ->where('users.level', '=', 'adm')
            ->where('pegawai.nama_pegawai', 'like', "%" . $cari . "%")
            ->paginate(4);

        $c = count($data);

        if ($c == 0) {
            return '<table class="table table-bordered table-striped">
            <thead>
              <tr>
              <th>No.</th>
              <th>NIP</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Aksi</th>
              </tr>
            </thead>
            
            <td colspan="5" class="text-center bg-light">Maaf data tidak ditemukan</td>
            </table>';
        } else {
            return view('admin.adm.admcari')->with([
                'data' => $data
            ]);
        }
    }
}
