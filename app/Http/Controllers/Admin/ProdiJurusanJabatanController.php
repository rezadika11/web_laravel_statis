<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-12T20:55:18+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-12T20:55:18+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ProdiJurusanJabatanController extends Controller
{
  public function __construct()
  {
      $this->middleware(['auth', 'level:admin']);
  }

  public function DaftarJurusan(){
    $DataJurusan = DB::table('jurusan')
    ->leftjoin('ketua_jurusan', function($q){
      return $q->on('ketua_jurusan.id_jurusan','jurusan.id_jurusan')->where('ketua_jurusan.aktif',1);
    })
    ->leftjoin('dosen','dosen.id_dosen','ketua_jurusan.id_dosen_kajur')
    ->select('jurusan.nama_jurusan','jurusan.id_jurusan','dosen.nama_dosen','ketua_jurusan.mulai_menjabat','ketua_jurusan.selesai_menjabat')
    ->get();

    $no=1;
    return view('admin.prodijurusanjabatan.daftarjurusan',compact('no','DataJurusan'));
  }

  public function UbahJurusan($id_jurusan){
    $Jurusan = DB::table('jurusan')
    ->leftjoin('ketua_jurusan', function($q){
      return $q->on('ketua_jurusan.id_jurusan','jurusan.id_jurusan')->where('ketua_jurusan.aktif',1);
    })
    ->select('jurusan.id_jurusan','jurusan.nama_jurusan','ketua_jurusan.id_dosen_kajur','ketua_jurusan.mulai_menjabat','ketua_jurusan.selesai_menjabat')
    ->where('jurusan.id_jurusan',$id_jurusan)
    ->first();

    $DataDosen = DB::table('dosen')
    ->select('dosen.nama_dosen','dosen.id_dosen')
    ->get();

    return view('admin.prodijurusanjabatan.ubahjurusan',compact('Jurusan','DataDosen'));
  }

  public function SimpanUbahJurusan(request $request,$id_jurusan){
    $request->validate([
      'id_dosen_kajur'=>'required',
      'mulai_menjabat'=>'required|date',
    ],[
      'id_dosen_kajur.required'=>'Ketua Jurusan Harus Dipilih.',
      'mulai_menjabat.required'=>'Waktu Mulai Menjabat Harus Diisi.',
      '*.date'=>'Format Tanggal Salah.',
    ]);

    if($request->id_jurusan != $id_jurusan){
      return redirect(url()->previous())
      ->with('simpan_gagal','Silahkan Ulangi.');
    }


    $CekLevel = DB::table('dosen')
    ->leftjoin('ketua_jurusan','ketua_jurusan.id_dosen_kajur','dosen.id_dosen')
    ->where([['id_dosen',$request->id_dosen_kajur],['level_dosen','!=','dosen'],['ketua_jurusan.aktif',1]]);

    if($CekLevel->count() != 0 && ($CekLevel->first()->id_jurusan != $request->id_jurusan || $CekLevel->first()->id_dosen_kajur != $request->id_dosen_kajur)){
      return redirect(url()->previous())
      ->witherrors(['id_dosen_kajur'=>'Dosen Memiliki Jabatan Lain, Silahkan Pilih Dosen yang Tidak Memiliki Jabatan Lain.']);
    }

    DB::beginTransaction();
    try{
      DB::table('dosen')
      ->join('ketua_jurusan','ketua_jurusan.id_dosen_kajur','dosen.id_dosen')
      ->where('ketua_jurusan.id_jurusan',$request->id_jurusan)
      ->update([
        'dosen.level_dosen'=>'dosen',
      ]);
      DB::table('dosen')
      ->where('dosen.id_dosen',$request->id_dosen_kajur)
      ->update([
        'dosen.level_dosen'=>'kajur',
      ]);

      DB::table('ketua_jurusan')
      ->where([['ketua_jurusan.id_jurusan',$request->id_jurusan],['ketua_jurusan.aktif',1]])
      ->update([
        'ketua_jurusan.aktif'=>0,
        'ketua_jurusan.selesai_menjabat'=>$request->mulai_menjabat,
      ]);
      DB::table('ketua_jurusan')
      ->insert([
        'ketua_jurusan.aktif'=>1,
        'ketua_jurusan.id_dosen_kajur'=>$request->id_dosen_kajur,
        'ketua_jurusan.id_jurusan'=>$request->id_jurusan,
        'ketua_jurusan.mulai_menjabat'=>$request->mulai_menjabat,
      ]);
      DB::commit();
      return redirect(route('admin.Jurusan'))
      ->with('simpan_sukses','Ketua Jurusan Berhasil Diperbaharui.');
    }catch(\Exception $e){
      DB::rollback();
      report($e);
      return redirect(url()->previous())
      ->with('simpan_gagal','Terjadi Kesalahan. Silahkan Hubungi Pengembang.');
    }
  }

  public function DaftarProdi(){
    $DataProdi = DB::table('prodi')
    ->leftjoin('jurusan','jurusan.id_jurusan','prodi.id_jurusan')
    ->select('prodi.id_prodi','prodi.nama_prodi','jurusan.nama_jurusan')
    ->get();

    $no = 1;

    return view('admin.prodijurusanjabatan.daftarprodi',compact('DataProdi','no'));
  }

  public function UbahProdi($id_prodi){
    $Prodi = DB::table('prodi')
    ->leftjoin('jurusan','jurusan.id_jurusan','prodi.id_jurusan')
    ->select('prodi.id_prodi','prodi.nama_prodi','jurusan.nama_jurusan','prodi.id_jurusan')
    ->where('prodi.id_prodi',$id_prodi)
    ->first();

    $DataJurusan = DB::table('jurusan')
    ->select('jurusan.id_jurusan','jurusan.nama_jurusan')
    ->get();

    return view('admin.prodijurusanjabatan.ubahprodi',compact('Prodi','DataJurusan'));
  }

  public function SimpanUbahProdi(request $request, $id_prodi){
    $request->validate([
      'id_jurusan'=>'required',
    ],[
      'id_jurusan.required'=>'Jurusan Harus Dipilih.',
    ]);

    if($request->id_prodi != $id_prodi){
      return redirect(url()->previous())
      ->with('simpan_gagal','Silahkan Ulangi.');
    }

    DB::beginTransaction();
    try{
      DB::table('prodi') 
      ->where('prodi.id_prodi',$request->id_prodi)
      ->update([
        'id_jurusan'=>$request->id_jurusan,
      ]);

      DB::commit();
      return redirect(route('admin.Prodi'))
      ->with('simpan_sukses','Data Program Studi Berhasil Diperbaharui');
    }catch(\Exception $e){
      DB::rollback();
      report($e);
      return redirect(url()->previous())
      ->with('simpan_gagal','Terjadi Kesalahan. Silahkan Hubungi Pengembang.');
    }
  }

  public function DaftarDekanWadek(){
    $DataJabatan = [
      'dekan'=>'Dekan',
      'wadek1'=>'Wakil Dekan I',
      'wadek2'=>'Wakil Dekan II',
      'wadek3'=>'Wakil Dekan III',
    ];

    foreach($DataJabatan as $kd=>$jabatan){
      $whereJabatan[] = $kd;
    }

    $DataDekanWadek = DB::table('dosen')
    ->leftjoin('dekan_wadek',function($q){
      return $q->on('dekan_wadek.id_dosen','dosen.id_dosen')->where('dekan_wadek.aktif',1);
    })
    ->select('dosen.level_dosen','dosen.nama_dosen','dosen.id_dosen','dekan_wadek.dekan_wadek','dekan_wadek.mulai_menjabat')
    ->wherein('dekan_wadek.dekan_wadek',$whereJabatan)
    ->get();

    $DataPejabat=[];
    foreach ($DataDekanWadek as $DekanWadek) {
      $DataPejabat[$DekanWadek->level_dosen] = [
        'nama_dosen' => $DekanWadek->nama_dosen,
        'id_dosen' => $DekanWadek->id_dosen,
        'mulai_menjabat'=>$DekanWadek->mulai_menjabat,
      ];
    }

    $no = 1;
    return view('admin.prodijurusanjabatan.daftardekanwadek',compact('DataJabatan','DataPejabat','no'));
  }

  public function UbahDekanWadek($jabatan){

    $DataJabatan = [
      'dekan'=>'Dekan',
      'wadek1'=>'Wakil Dekan I',
      'wadek2'=>'Wakil Dekan II',
      'wadek3'=>'Wakil Dekan III',
      'kajur'=>'Ketua Jurusan',
    ];

    if(!isset($DataJabatan[$jabatan])){
      return redirect(route('admin.DekanWadek'))
      ->with('simpan_gagal','Jabatan Tidak Ditemukan.');
    }

    $DataDekanWadek = DB::table('dosen')
    ->leftjoin('dekan_wadek',function($q){
      return $q->on('dekan_wadek.id_dosen','dosen.id_dosen')->where('dekan_wadek.aktif',1);
    })
    ->select('dosen.level_dosen','dosen.nama_dosen','dekan_wadek.id_dosen','dekan_wadek.mulai_menjabat')
    ->where('dekan_wadek.dekan_wadek',$jabatan)
    ->first();

    $DataDosen = DB::table('dosen')
    ->select('dosen.nama_dosen','dosen.level_dosen','dosen.id_dosen')
    ->get();

    return view('admin.prodijurusanjabatan.ubahjabatan',compact('DataJabatan','jabatan','DataDekanWadek','DataDosen'));
  }

  public function SimpanUbahDekanWadek(request $request, $jabatan){
    if($request->jabatan!=$jabatan){
      return redirect(url()->previous())
      ->with('simpan_gagal','Silhkan Ulangi Kembali.');
    }

    $request->validate([
      'id_dosen'=>'required',
      'mulai_menjabat'=>'required|date',
    ],[
      'id_dosen.required'=>'Dosen Harus Dipilih.',
      'mulai_menjabat.required'=>'Waktu Mulai menjabat Harus Diisi.',
      'mulai_menjabat.date'=>'Format Tanggal Salah.',
    ]);


    DB::beginTransaction();
    try{
      DB::table('dosen')
      ->where('dosen.level_dosen',$request->jabatan)
      ->update([
        'dosen.level_dosen'=>'dosen',
      ]);

      DB::table('dekan_wadek')
      ->where([['dekan_wadek.dekan_wadek',$request->jabatan],['dekan_wadek.aktif',1]])
      ->update([
        'dekan_wadek.aktif'=>0,
        'dekan_wadek.selesai_menjabat'=>$request->mulai_menjabat,
      ]);
      DB::table('dekan_wadek')
      ->insert([
        'dekan_wadek.aktif'=>1,
        'dekan_wadek.id_dosen'=>$request->id_dosen,
        'dekan_wadek.dekan_wadek'=>$request->jabatan,
        'dekan_wadek.mulai_menjabat'=>$request->mulai_menjabat,
      ]);

      DB::table('dosen')
      ->where('dosen.id_dosen',$request->id_dosen)
      ->update([
        'level_dosen'=>$request->jabatan,
      ]);

      DB::commit();
      return redirect(route('admin.DekanWadek'))
      ->with('simpan_sukses','Data Jabatan Berhasil Diperbaharui.');
    }catch(\Exception $e){
      DB::rollback();
      report($e);
      return redirect(url()->previous())
      ->with('simpan_gagal','Terjadi Kesalahan, Silahkan Hubungi Pengembang');
    }
  }

  public function AdminProdi(){
    $DataAdminProdi = DB::table('admin_prodi')
    ->join('prodi','admin_prodi.id_prodi','prodi.id_prodi')
    ->join('pegawai','pegawai.id_pegawai','admin_prodi.id_pegawai')
    ->select('prodi.nama_prodi','pegawai.nama_pegawai','admin_prodi.id_admin_prodi')
    ->get();

    $no=1;

    return view('admin.prodijurusanjabatan.daftaradminprodi',compact('DataAdminProdi','no'));
  }

  public function TambahAdminProdi(){
    $DataProdi = DB::table('prodi')
    ->select('prodi.id_prodi','prodi.nama_prodi')
    ->get();

    $DataPegawai = DB::table('pegawai')
    ->join('users','users.id_user','pegawai.id_user')
    ->select('pegawai.id_pegawai','pegawai.nama_pegawai')
    ->where('users.level','adm')
    ->get();

    return view('admin.prodijurusanjabatan.tambahadminprodi',compact('DataProdi','DataPegawai'));
  }

  public function SimpanTambahAdminProdi(request $request){
    $request->validate([
      'id_prodi'=>'required',
      'id_pegawai'=>'required',
    ],[
      'id_prodi.required'=>'Program Studi Harus Dipilih.',
      'id_pegawai.required'=>'Pegawai Harus Dipilih.',
    ]);

    $CEK = DB::table('admin_prodi')
    ->where($request->only('id_prodi','id_pegawai'))
    ->count();

    if($CEK!=0){
      return redirect(url()->previous())
      ->with('simpan_gagal','Pegawai dan Program Studi sudah ada.')
      ->witherrors(['id_prodi'=>'Silahkan Pilih Program Studi Lain.','id_pegawai'=>'Silahkan Pilih Pegawai Lain.']);
    }

    DB::beginTransaction();
    try{
      DB::table('admin_prodi')
      ->insert($request->only('id_prodi','id_pegawai'));
      DB::commit();
      return redirect(route('admin.AdminProdi'))
      ->with('simpan_sukses','Administrasi Program Studi Berhasil Ditambahkan.');
    }catch(\Exception $e){
      DB::rollback();
      report($e);
      return redirect(url()->previous())
      ->with('simpan_gagal','Terjadi Kesalahan. Silahkan Hubungi Pengembang.');
    }
  }
}
