<?php

namespace App\Http\Controllers\Dekan\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\SuratMahasiswa;
use Illuminate\Support\Str;
use PDF;
use App\DetilUser;

class SuratKeteranganPenggantiIjazahController extends Controller
{
  private $tabel_surat = 'surat_keterangan_pengganti_ijazah';
  private $tabel_persetujuan = 'persetujuan_surat_keterangan_pengganti_ijazah';

  public function __construct()
  {
    $this->middleware(['auth', 'level:dekan']);
  }

  public function PermintaanSuratKeteranganPenggantiIjazah()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_dekan', function ($q) {
        return $q->on('validasi_dekan.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_dekan.level', 'dekan');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->where([['validasi_dekan.status_persetujuan', 0], ['surat.unique_key', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.no_surat', 'surat.tanggal_surat', 'validasi_dekan.status_persetujuan as status_persetujuan_dekan', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.nomor_ijazah', 'surat.tanggal_ijazah', 'surat.tahun_kehilangan')
      ->paginate(4);

    $no = 1;

    return view('dekan.mhs.suratketeranganpenggantiijazah.permintaansuratketeranganpenggantiijazah', compact('PermohonanSurat', 'no'));
  }

  public function ProsesPermintaanSuratKeteranganPenggantiIjazah($id_surat)
  {
    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_dekan', function ($q) {
        return $q->on('validasi_dekan.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_dekan.level', 'dekan');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->where([['validasi_dekan.status_persetujuan', 0], ['surat.unique_key', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.no_surat', '!=', NULL], ['surat.tanggal_surat', '!=', NULL]])
      ->select('surat.no_surat', 'surat.tanggal_surat', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'mahasiswa.alamat', 'surat.nomor_ijazah', 'surat.tanggal_ijazah', 'surat.tahun_kehilangan', 'mahasiswa.tempat_lahir', 'mahasiswa.tanggal_lahir', 'mahasiswa.alamat')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    return view('dekan.mhs.suratketeranganpenggantiijazah.prosespermintaansuratketeranganpenggantiijazah', compact('Surat'));
  }

  public function SimpanProsesPermintaanSuratKeteranganPenggantiIjazah(request $request, $id_surat)
  {
    if ($request->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    }

    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_dekan', function ($q) {
        return $q->on('validasi_dekan.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_dekan.level', 'dekan');
      })
      ->where([['validasi_dekan.status_persetujuan', 0], ['surat.unique_key', NULL], ['surat.id_' . $this->tabel_surat, $id_surat], ['surat.no_surat', '!=', NULL], ['surat.tanggal_surat', '!=', NULL]]);

    if ($Surat->count() == 0) {
      return redirect(route('dekan.PermintaanSuratKeteranganPenggantiIjazah'))
        ->with('simpan_gagal', 'Surat Sudah Divalidasi atau tidak ditemukan');
    }

    DB::beginTransaction();
    try {
      $Surat->update([
        'validasi_dekan.status_persetujuan' => 1,
        'validasi_dekan.waktu_persetujuan' => date('Y-m-d H:i:s'),
        'surat.unique_key' => Str::UUID(),
        'validasi_dekan.id_pegawai_dosen' => DetilUser::AmbilDetil()->id_dosen,
      ]);
      DB::commit();
      return redirect(route('dekan.PermintaanSuratKeteranganPenggantiIjazah'))
        ->with('simpan_sukses', 'Surat Berhasil Ditanda Tangani.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakn Hubungi Pengembang.');
    }
  }

  public function SuratKeteranganPenggantiIjazahSiap()
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_dekan', function ($q) {
        return $q->on('validasi_dekan.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_dekan.level', 'dekan');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->where([['validasi_dekan.status_persetujuan', 1], ['surat.unique_key', '!=', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.no_surat', 'surat.tanggal_surat', 'validasi_dekan.status_persetujuan as status_persetujuan_dekan', 'validasi_adm.status_persetujuan as status_persetujuan_adm', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.nomor_ijazah', 'surat.tanggal_ijazah', 'surat.tahun_kehilangan')
      ->paginate(4);

    $no = 1;

    return view('dekan.mhs.suratketeranganpenggantiijazah.suratketeranganpenggantiijazah', compact('no', 'SuratSiapCetak'));
  }

  public function CetakSuratKeteranganPenggantiIJazah($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_dekan', function ($q) {
        return $q->on('validasi_dekan.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_dekan.level', 'dekan');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_dekan.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratKeteranganPenggantiIJazah($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratPenggantiIjazahBelumdiProses()
  {
    $count = DB::table('persetujuan_surat_keterangan_pengganti_ijazah')
      ->select('status_persetujuan', 'level')
      ->where('level', 'dekan')
      ->where('status_persetujuan', 0)
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
