<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetilUser;
use Log;

class DashboardController extends Controller
{
    public function __construct(){
      $this->middleware('auth');
    }

    public function index()
    {
        return redirect(route('Dashboard'));
    }

    public function Dashboard(){
      //dd(DetilUser::AmbilDetil());
      $detil = json_encode(DetilUser::AmbilDetil());
      Log::debug($detil);
      return view('dashboard.dashboard');
    }
}
