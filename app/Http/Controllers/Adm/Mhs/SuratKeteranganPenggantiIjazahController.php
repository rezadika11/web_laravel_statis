<?php

namespace App\Http\Controllers\Adm\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\DetilUser;
use App\SuratMahasiswa;
use PDF;
use App\NomorSurat;

class SuratKeteranganPenggantiIjazahController extends Controller
{
  private $tabel_surat = 'surat_keterangan_pengganti_ijazah';
  private $tabel_persetujuan = 'persetujuan_surat_keterangan_pengganti_ijazah';

  public function __construct()
  {
    $this->middleware(['auth', 'level:adm']);
  }

  public function PermintaanSuratKeteranganPenggantiIjazah()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_dekan', function ($q) {
        return $q->on('validasi_dekan.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_dekan.level', 'dekan');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.nomor_ijazah', 'surat.tanggal_ijazah', 'surat.tahun_kehilangan')
      ->paginate(4);

    $no = 1;

    return view('adm.mhs.suratketeranganpenggantiijazah.permintaansuratketeranganpenggantiijazah', compact('no', 'PermohonanSurat'));
  }

  public function ProsesPermintaanSuratKeteranganPenggantiIjazah($id_surat)
  {
    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_dekan', function ($q) {
        return $q->on('validasi_dekan.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_dekan.level', 'dekan');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->select('validasi_dekan.status_persetujuan as status_persetujuan_dekan', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.nomor_ijazah', 'surat.tanggal_ijazah', 'surat.tahun_kehilangan', 'mahasiswa.tanggal_lahir', 'mahasiswa.tempat_lahir', 'mahasiswa.alamat')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    for ($x = 2000; $x <= date('Y'); $x++) {
      $PilihTahunKehilangan[] = $x;
    }

    return view('adm.mhs.suratketeranganpenggantiijazah.prosespermintaansuratketeranganpenggantiijazah', compact('Surat', 'PilihTahunKehilangan'));
  }

  public function SimpanProsesPermintaanSuratKeteranganPenggantiIjazah(request $request, $id_surat)
  {
    if ($request->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    };

    $request->validate([
      'nomor_ijazah' => 'required',
      'tanggal_ijazah' => 'required|date',
      'tahun_kehilangan' => 'required',
    ], [
      'nomor_ijazah.required' => 'Nomor Ijazah Harus Diisi.',
      'tanggal_ijazah.required' => 'Tanggal Ijazah Harus Diisi.',
      'tanggal_ijazah.date' => 'Format Tanggal Salah.',
      'tahun_kehilangan.required' => 'Tahun Kehilangan Harus Dipilih.',
    ]);

    DB::beginTransaction();
    try {
      $BuatNomorSurat = NomorSurat::BuatNomorSurat('1');
      $request->merge([
        'no_urut' => $BuatNomorSurat['no_urut'],
        'no_surat' => $BuatNomorSurat['no_surat'],
        'id_no_surat'=>$BuatNomorSurat['id_no_surat'],
        'tanggal_surat'=>$BuatNomorSurat['tanggal_surat'],
      ]);

      DB::table($this->tabel_surat)
        ->where('id_' . $this->tabel_surat, $id_surat)
        ->update($request->only('no_surat', 'tanggal_surat', 'nomor_ijazah', 'tanggal_ijazah', 'tahun_kehilangan','id_no_surat','no_urut'));

      DB::table($this->tabel_persetujuan)
        ->where([['id_' . $this->tabel_surat, $id_surat], ['level', DetilUser::AmbilDetil()->level]])
        ->update([
          'status_persetujuan' => 1,
          'waktu_persetujuan' => date('Y-m-d H:i:s'),
          'id_pegawai_dosen' => DetilUser::AmbilDetil()->id_pegawai,
        ]);
      DB::commit();
      return redirect(route('adm.PermintaanSuratKeteranganPenggantiIjazah'))
        ->with('simpan_sukses', 'Surat Berhasil disimpan dengan nomor surat ' . $request->no_surat . ' tanggal ' . date('d-m-Y', strtotime($request->tanggal_surat)) . '.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakan Hubungi TIM Pengembang Perangkat Lunak.');
    }
  }

  public function SuratKeteranganPenggantiIjazahSudahProses(request $request)
  {
    $SuratSudahProses = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_dekan', function ($q) {
        return $q->on('validasi_dekan.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_dekan.level', 'dekan');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL]]);

    $FilterSurat = SuratMahasiswa::FilterSurat($request, $SuratSudahProses, DetilUser::AmbilProdiAdmin());

    $SuratSudahProses = $FilterSurat['query']->select('surat.no_surat', 'surat.tanggal_surat', 'mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.nomor_ijazah', 'surat.tanggal_ijazah', 'surat.tahun_kehilangan', 'validasi_dekan.status_persetujuan as persetujuan_dekan')
      ->paginate(4);

    $filter_id_prodi = $FilterSurat['filter_id_prodi'];
    $filter = $FilterSurat['filter'];
    $key = $FilterSurat['key'];
    $JenisFilter =  [
      'nim_nama' => 'Nim dan Nama',
      'tanggal_surat' => 'Tanggal Surat',
    ];
    $PilihFilterProdi = $FilterSurat['pilih_filter_prodi'];
    $route = 'adm.SuratKeteranganPenggangtiIjazahSudahProses';

    $no = 1;

    return view('adm.mhs.suratketeranganpenggantiijazah.suratketeranganpenggantiijazahsudahproses', compact('SuratSudahProses', 'no', 'PilihFilterProdi', 'filter_id_prodi', 'JenisFilter', 'filter', 'key', 'route'));
  }

  public function CetakSuratKeteranganPenggantiIJazah($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_dekan', function ($q) {
        return $q->on('validasi_dekan.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_dekan.level', 'dekan');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_dekan.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratKeteranganPenggantiIJazah($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratKeteranganPenggantiIjazahBelumdiProses()
  {
    $count = DB::table($this->tabel_persetujuan.' as persetujuan')
      ->join($this->tabel_surat.' as surat','surat.id_'.$this->tabel_surat,'persetujuan.id_'.$this->tabel_surat)
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->select('status_persetujuan', 'level')
      ->where('level', 'adm')
      ->where('status_persetujuan', 0)
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
