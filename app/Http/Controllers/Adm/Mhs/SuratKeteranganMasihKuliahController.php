<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-15T07:37:06+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-15T07:37:06+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Adm\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\SuratMahasiswa;
use App\DetilUser;
use App\NomorSurat;
use PDF;

class SuratKeteranganMasihKuliahController extends Controller
{
  private $tabel_surat = 'surat_keterangan_masih_kuliah';
  private $tabel_persetujuan = 'persetujuan_surat_keterangan_masih_kuliah';
  private $tabel_wali = 'wali_surat_keterangan_masih_kuliah';

  public function __construct()
  {
    $this->middleware(['auth', 'level:adm']);
  }

  public function PermintaanSuratKeteranganMasihKuliah()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1_kajur', function ($q) {
        return $q->on('validasi_wadek1_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
        ->where(function($q1){
          return $q1->where('validasi_wadek1_kajur.level', 'wadek1')
          ->orwhere('validasi_wadek1_kajur.level', 'kajur');
        });
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->leftjoin($this->tabel_wali . ' as wali', 'wali.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.tahun_akademik', 'wali.instansi')
      ->paginate(4);

    $no = 1;

    return view('adm.mhs.suratketeranganmasihkuliah.permintaansuratketeranganmasihkuliah', compact('PermohonanSurat', 'no'));
  }

  public function ProsesPermintaanSuratKeteranganMasihKuliah($id_surat)
  {
    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1_kajur', function ($q) {
        return $q->on('validasi_wadek1_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
        ->where(function($q1){
          return $q1->where('validasi_wadek1_kajur.level', 'wadek1')
          ->orwhere('validasi_wadek1_kajur.level', 'kajur');
        });
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->leftjoin($this->tabel_wali . ' as wali', 'wali.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.tahun_akademik', 'wali.instansi', 'mahasiswa.tempat_lahir', 'mahasiswa.tanggal_lahir', 'mahasiswa.alamat', 'wali.nip', 'wali.pangkat_golongan', 'wali.nama as nama_wali', 'wali.id_' . $this->tabel_wali . ' as id_wali')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    $TahunAkademik = SuratMahasiswa::TahunAkademik();

    return view('adm.mhs.suratketeranganmasihkuliah.prosespermintaansuratketeranganmasihkuliah', compact('Surat', 'TahunAkademik'));
  }

  public function SimpanProsesPermintaanSuratKeteranganMasihKuliah(request $request, $id_surat)
  {
    if ($request->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    }

    $request->validate([
      'tahun_akademik' => 'required',
      'nama_wali' => 'required_with:id_wali|max:255',
      'nip' => 'required_with:id_wali|max:255',
      'pangkat_golongan' => 'required_with:id_wali|max:255',
      'instansi' => 'required_with:id_wali|max:255',
    ], [
      'tahun_akademik.required' => 'Tahun Akademik Harus Diisi.',
      'nama_wali.required_with' => 'Nama Orang Tua / Wali Harus Diisi.',
      'nip.required_with' => 'NIP / NRP Harus Diisi.',
      'pangkat_golongan.required_with' => 'Pangkat / Golongan / Ruang Harus Diisi, Gunakan - Jika Tidak Ada.',
      'instansi.required_with' => 'Nama Instansi Harus Diisi.',
      '*.max' => 'Isian Tidak Boleh Lebih Dari 255 Karakter.',
    ]);

    DB::beginTransaction();
    try {
      $BuatNomorSurat = NomorSurat::BuatNomorSurat('2');
      $request->merge([
        'no_urut' => $BuatNomorSurat['no_urut'],
        'no_surat' => $BuatNomorSurat['no_surat'],
        'id_no_surat'=>$BuatNomorSurat['id_no_surat'],
        'tanggal_surat'=>$BuatNomorSurat['tanggal_surat'],
      ]);

      DB::table($this->tabel_surat)
        ->where('id_' . $this->tabel_surat, $id_surat)
        ->update($request->only('no_surat', 'tanggal_surat', 'tahun_akademik', 'no_urut','id_no_surat'));

      DB::table($this->tabel_persetujuan)
        ->where([['id_' . $this->tabel_surat, $id_surat], ['level', DetilUser::AmbilDetil()->level]])
        ->update([
          'status_persetujuan' => 1,
          'waktu_persetujuan' => date('Y-m-d H:i:s'),
          'id_pegawai_dosen' =>DetilUser::AmbilDetil()->id_pegawai,
        ]);

      if (isset($request->id_wali)) {
        $request->merge([
          'nama' => $request->nama_wali,
        ]);
        DB::table($this->tabel_wali . ' as wali')
          ->where('wali.id_' . $this->tabel_wali, $request->id_wali)
          ->update($request->only('nama', 'nip', 'pangkat_golongan', 'instansi'));
      }
      DB::commit();
      return redirect(route('adm.PermintaanSuratKeteranganMasihKuliah'))
        ->with('simpan_sukses', 'Surat Berhasil disimpan dengan nomor surat ' . $request->no_surat . ' tanggal ' . date('d-m-Y', strtotime($request->tanggal_surat)) . '.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakan Hubungi TIM Pengembang Perangkat Lunak.');
    }
  }

  public function SuratKeteranganMasihKuliahSudahProses(request $request)
  {
    $SuratSudahProses = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1_kajur', function ($q) {
        return $q->on('validasi_wadek1_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
        ->where(function($q1){
          return $q1->where('validasi_wadek1_kajur.level', 'wadek1')
          ->orwhere('validasi_wadek1_kajur.level', 'kajur');
        });
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->leftjoin($this->tabel_wali . ' as wali', 'wali.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL]]);

    $FilterSurat = SuratMahasiswa::FilterSurat($request, $SuratSudahProses,DetilUser::AmbilProdiAdmin());

    $SuratSudahProses = $FilterSurat['query']->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'validasi_wadek1_kajur.status_persetujuan as status_persetujuan_wadek1_kajur', 'surat.no_surat', 'surat.tahun_akademik', 'wali.instansi', 'surat.tanggal_surat')
      ->paginate(4);

    $filter_id_prodi = $FilterSurat['filter_id_prodi'];
    $filter = $FilterSurat['filter'];
    $key = $FilterSurat['key'];
    $JenisFilter =  [
      'nim_nama' => 'Nim dan Nama',
      'tanggal_surat' => 'Tanggal Surat',
    ];
    $PilihFilterProdi = $FilterSurat['pilih_filter_prodi'];
    $route = 'adm.SuratKeteranganMasihKuliahSudahProses';


    $no = 1;

    return view('adm.mhs.suratketeranganmasihkuliah.suratketeranganmasihkuliahsudahproses', compact('no', 'SuratSudahProses', 'PilihFilterProdi', 'filter_id_prodi', 'JenisFilter', 'filter', 'key', 'route'));
  }

  public function CetakSuratKeteranganMasihKuliah($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1', function ($q) {
        return $q->on('validasi_wadek1.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek1.level', 'wadek1');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_wadek1.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratKeteranganMasihKuliah($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratKeteranganMasihKuliahBelumdiProses()
  {
    $count = DB::table($this->tabel_persetujuan.' as persetujuan')
      ->join($this->tabel_surat.' as surat','surat.id_'.$this->tabel_surat,'persetujuan.id_'.$this->tabel_surat)
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->select('status_persetujuan', 'level')
      ->where('level', 'adm')
      ->where('status_persetujuan', 0)
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
