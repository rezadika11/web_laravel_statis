<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-13T08:01:40+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-13T08:01:40+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Adm\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\DetilUser;
use App\SuratMahasiswa; 
use App\NomorSurat;
use PDF;

class SuratKeteranganLulusSkripsiController extends Controller
{
  private $tabel_surat = 'surat_keterangan_lulus_skripsi';
  private $tabel_persetujuan = 'persetujuan_surat_keterangan_lulus_skripsi';

  public function __construct()
  {
    $this->middleware(['auth', 'level:adm']);
  }

  public function PermintaanSuratKeteranganLulusSkripsi()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.tanggal_ujian', 'surat.tahun_akademik', 'surat.id_' . $this->tabel_surat . ' as id_surat')
      ->paginate(4);

    $no = 1;

    return view('adm.mhs.suratketeranganlulusskripsi.permintaansuratketeranganlulusskripsi', compact('PermohonanSurat', 'no'));
  }

  public function ProsesPermintaanSuratKeteranganLulusSkripsi($id_surat)
  {
    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->select('prodi.nama_prodi', 'validasi_kajur.status_persetujuan', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.tanggal_ujian', 'surat.tahun_akademik', 'mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.lampiran_munaqosyah')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    return view('adm.mhs.suratketeranganlulusskripsi.prosespermintaansuratketeranganlulusskripsi', compact('Surat'));
  }

  public function SimpanProsesPermintaanSuratKeteranganLulusSkripsi(request $request, $id_surat)
  {
    if ($request->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    }

    $request->validate([
      'tanggal_ujian' => 'required|date',
    ], [
      'tanggal_ujian.required' => 'Tanggal Ujian Harus Diisi.',
      '*.date' => 'Format Tanggal Salah.',
    ]);

    DB::beginTransaction();
    try {
      $BuatNomorSurat = NomorSurat::BuatNomorSurat('1');
      $request->merge([
        'no_urut' => $BuatNomorSurat['no_urut'],
        'no_surat' => $BuatNomorSurat['no_surat'],
        'id_no_surat'=>$BuatNomorSurat['id_no_surat'],
        'tanggal_surat'=>$BuatNomorSurat['tanggal_surat'],
      ]);

      DB::table($this->tabel_surat)
        ->where('id_' . $this->tabel_surat, $id_surat)
        ->update($request->only('no_surat', 'tanggal_surat', 'tanggal_ujian', 'no_urut','id_no_surat'));

      DB::table($this->tabel_persetujuan)
        ->where([['id_' . $this->tabel_surat, $id_surat], ['level', DetilUser::AmbilDetil()->level]])
        ->update([
          'status_persetujuan' => 1,
          'waktu_persetujuan' => date('Y-m-d H:i:s'),
          'id_pegawai_dosen' =>DetilUser::AmbilDetil()->id_pegawai,
        ]);
      DB::commit();
      return redirect(route('adm.PermintaanSuratKeteranganLulusSkripsi'))
        ->with('simpan_sukses', 'Surat Berhasil disimpan dengan nomor surat ' . $request->no_surat . ' tanggal ' . date('d-m-Y', strtotime($request->tanggal_surat)) . '.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakan Hubungi TIM Pengembang Perangkat Lunak.');
    }
  }

  public function SuratKeteranganLulusSkripsiSudahProses(request $request)
  {
    $SuratSudahProses = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL]]);

    $FilterSurat = SuratMahasiswa::FilterSurat($request, $SuratSudahProses);

    $SuratSudahProses = $FilterSurat['query']->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'validasi_kajur.status_persetujuan as persetujuan_kajur', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.no_surat', 'surat.tanggal_surat', 'surat.tanggal_ujian', 'surat.tahun_akademik')
      ->paginate(4);

    $filter_id_prodi = $FilterSurat['filter_id_prodi'];
    $filter = $FilterSurat['filter'];
    $key = $FilterSurat['key'];
    $JenisFilter =  [
      'nim_nama' => 'Nim dan Nama',
      'tanggal_surat' => 'Tanggal Surat',
    ];
    $PilihFilterProdi = $FilterSurat['pilih_filter_prodi'];
    $route = 'adm.SuratKeteranganLulusSkripsiSudahProses';

    $no = 1;

    return view('adm.mhs.suratketeranganlulusskripsi.suratketeranganlulusskripsisudahproses', compact('no', 'SuratSudahProses', 'PilihFilterProdi', 'filter_id_prodi', 'JenisFilter', 'filter', 'key', 'route'));
  }

  public function CetakSuratKeteranganLulusSkripsi($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_kajur.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratKeteranganLulusSkripsi($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratLulusSkripsiBelumdiProses()
  {
    $count = DB::table($this->tabel_persetujuan.' as persetujuan')
      ->join($this->tabel_surat.' as surat','surat.id_'.$this->tabel_surat,'persetujuan.id_'.$this->tabel_surat)
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->select('status_persetujuan', 'level')
      ->where('level', 'adm')
      ->where('status_persetujuan', 0)
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
