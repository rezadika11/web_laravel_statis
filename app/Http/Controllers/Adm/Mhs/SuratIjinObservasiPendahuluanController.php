<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-12T20:55:03+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-12T20:55:03+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Adm\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\SuratMahasiswa;
use App\DetilUser;
use App\NomorSurat;
use PDF;

class SuratIjinObservasiPendahuluanController extends Controller
{

  private $tabel_surat = "surat_ijin_observasi_pendahuluan";
  private $tabel_persetujuan = "persetujuan_surat_ijin_observasi_pendahuluan";

  public function __construct()
  {
    $this->middleware(['auth', 'level:adm']);
  }

  public function PermintaanSuratIjinObservasiPendahuluan()
  {
    $PermohonanSurat = DB::table($this->tabel_surat.' as surat')
      ->join($this->tabel_persetujuan.' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_surat_ijin_observasi_pendahuluan', 'surat.id_surat_ijin_observasi_pendahuluan')
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan.' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_surat_ijin_observasi_pendahuluan', 'surat.id_surat_ijin_observasi_pendahuluan')
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.kepada', 'surat.id_surat_ijin_observasi_pendahuluan as id_surat')
      ->paginate(4);
    $no = 1;

    return view('adm.mhs.suratijinobservasipendahuluan.permintaansuratijinobservasipendahuluan', compact('PermohonanSurat', 'no'));
  }

  public function ProsesPermintaanSuratIjinObservasiPendahuluan($id_surat)
  {
    $Surat = DB::table('surat_ijin_observasi_pendahuluan as surat')
      ->join('persetujuan_surat_ijin_observasi_pendahuluan as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_surat_ijin_observasi_pendahuluan', 'surat.id_surat_ijin_observasi_pendahuluan')
          ->where('validasi_adm.level', 'adm');
      })
      ->join('persetujuan_surat_ijin_observasi_pendahuluan as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_surat_ijin_observasi_pendahuluan', 'surat.id_surat_ijin_observasi_pendahuluan')
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL], ['surat.id_surat_ijin_observasi_pendahuluan', $id_surat]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.kepada', 'validasi_kajur.status_persetujuan', 'surat.id_surat_ijin_observasi_pendahuluan as id_surat', 'surat.semester', 'surat.tahun_akademik', 'surat.obyek', 'surat.lokasi', 'surat.tanggal_observasi')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
      ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    $semester = SuratMahasiswa::Semester($Surat->semester);

    return view('adm.mhs.suratijinobservasipendahuluan.prosespermintaansuratijinobservasipendahuluan', compact('semester', 'Surat'));
  }

  public function SimpanProsesPermintaanSuratIjinObservasiPendahuluan(request $data, $id_surat)
  {
    if ($data->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    }

    $data->validate([
      'kepada' => 'required',
      'obyek' => 'required',
      'lokasi' => 'required',
      'tanggal_observasi' => 'required|date',
    ], [
      'kepada.required' => 'Kepada Harus Diisi',
      'obyek.required' => 'Obyek Penelitian Harus Diisi.',
      'lokasi.required' => 'Lokasi Harus Diisi.',
      'tanggal_observasi.required' => 'Tanggal Observasi Harus Diisi.',
      '*.date' => 'Format Taggal Salah.',
    ]);

    DB::beginTransaction();
    try {
      $BuatNomorSurat = NomorSurat::BuatNomorSurat('3');
      $data->merge([
        'no_urut' => $BuatNomorSurat['no_urut'],
        'no_surat' => $BuatNomorSurat['no_surat'],
        'id_no_surat'=>$BuatNomorSurat['id_no_surat'],
        'tanggal_surat'=>$BuatNomorSurat['tanggal_surat'],
      ]);

      DB::table('surat_ijin_observasi_pendahuluan')
        ->where('id_surat_ijin_observasi_pendahuluan', $id_surat)
        ->update($data->only('no_surat', 'tanggal_surat', 'kepada', 'obyek', 'lokasi', 'tanggal_observasi', 'no_urut','id_no_surat'));
      DB::table('persetujuan_surat_ijin_observasi_pendahuluan')
        ->where([['id_surat_ijin_observasi_pendahuluan', $id_surat], ['level', DetilUser::AmbilDetil()->level]])
        ->update([
          'status_persetujuan' => 1,
          'waktu_persetujuan' => date('Y-m-d H:i:s'),
          'id_pegawai_dosen' => DetilUser::AmbilDetil()->id_pegawai,
        ]);
      DB::commit();
      return redirect(route('adm.PermintaanSuratIjinObservasiPendahuluan'))
      ->with('simpan_sukses', 'Surat Berhasil disimpan dengan nomor surat ' . $data->no_surat . ' tanggal ' . date('d-m-Y', strtotime($data->tanggal_surat)) . '.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakan Hubungi TIM Pengembang Perangkat Lunak.');
    }
  }

  public function SuratIjinObservasiPendahuluanSudahProses(request $request)
  {
    $SuratSudahProses = DB::table('surat_ijin_observasi_pendahuluan as surat')
      ->join('persetujuan_surat_ijin_observasi_pendahuluan as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_surat_ijin_observasi_pendahuluan', 'surat.id_surat_ijin_observasi_pendahuluan')
          ->where('validasi_adm.level', 'adm');
      })
      ->join('persetujuan_surat_ijin_observasi_pendahuluan as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_surat_ijin_observasi_pendahuluan', 'surat.id_surat_ijin_observasi_pendahuluan')
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL]]);

    $FilterSurat = SuratMahasiswa::FilterSurat($request, $SuratSudahProses, DetilUser::AmbilProdiAdmin());

    $SuratSudahProses = $FilterSurat['query']->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.kepada', 'validasi_kajur.status_persetujuan as persetujuan_kajur', 'surat.id_surat_ijin_observasi_pendahuluan as id_surat', 'surat.no_surat', 'surat.tanggal_surat')
      ->paginate(4);

    $no = 1;

    $filter_id_prodi = $FilterSurat['filter_id_prodi'];
    $filter = $FilterSurat['filter'];
    $key = $FilterSurat['key'];
    $JenisFilter = $FilterSurat['jenis_filter'];
    $PilihFilterProdi = $FilterSurat['pilih_filter_prodi'];
    $route = 'adm.SuratIjinObservasiPendahuluanSudahProses';

    return view('adm.mhs.suratijinobservasipendahuluan.suratijinobservasipendahuluansudahproses', compact('no', 'SuratSudahProses', 'PilihFilterProdi', 'filter_id_prodi', 'JenisFilter', 'filter', 'key', 'route'));
  }

  public function CetakSuratIjinObservasiPendahuluan($id_surat)
  {
    $SuratSiapCetak = DB::table('surat_ijin_observasi_pendahuluan as surat')
      ->join('persetujuan_surat_ijin_observasi_pendahuluan as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_surat_ijin_observasi_pendahuluan', 'surat.id_surat_ijin_observasi_pendahuluan')
          ->where('validasi_adm.level', 'adm');
      })
      ->join('persetujuan_surat_ijin_observasi_pendahuluan as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_surat_ijin_observasi_pendahuluan', 'surat.id_surat_ijin_observasi_pendahuluan')
          ->where('validasi_kajur.level', 'kajur');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_kajur.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_surat_ijin_observasi_pendahuluan', $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
      ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratIjinObservasiPendahuluan($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }

  public function JumlahSuratObservasiPendahuluanBelumdiProses()
  {
    $count = DB::table($this->tabel_persetujuan.' as persetujuan')
      ->join($this->tabel_surat.' as surat','surat.id_'.$this->tabel_surat,'persetujuan.id_'.$this->tabel_surat)
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->select('status_persetujuan', 'level')
      ->where('level', 'adm')
      ->where('status_persetujuan', 0)
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
