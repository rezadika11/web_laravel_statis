<?php

namespace App\Http\Controllers\Adm\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\DetilUser;
use App\SuratMahasiswa;
use PDF;
use App\NomorSurat;

class SuratTugasController extends Controller
{
  private $tabel_surat = 'surat_tugas';
  private $tabel_persetujuan = 'persetujuan_surat_tugas';
  private $tabel_peserta = 'peserta_surat_tugas';

  public function __construct()
  {
    $this->middleware(['auth', 'level:adm']);
  }

  public function PermintaanSuratTugas()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.kegiatan', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.penyelenggara')
      ->paginate(4);

    $no = 1;

    return view('adm.mhs.surattugas.permintaansurattugas', compact('PermohonanSurat', 'no'));
  }

  public function ProsesPermintaanSuratTugas($id_surat)
  {
    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->select('surat.kegiatan', 'surat.penyelenggara', 'validasi_wadek3.status_persetujuan', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.tanggal_mulai', 'surat.tanggal_selesai')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    $Peserta = DB::table($this->tabel_peserta . ' as peserta')
      ->join('prodi', 'prodi.id_prodi', 'peserta.id_prodi')
      ->select('peserta.nim', 'peserta.nama', 'prodi.nama_prodi')
      ->where('peserta.id_' . $this->tabel_surat, $id_surat)
      ->get();

    $no = 1;

    return view('adm.mhs.surattugas.prosespermintaansurattugas', compact('Surat', 'no', 'Peserta'));
  }

  public function SimpanProsesPermintaanSuratTugas(request $request, $id_surat)
  {
    if ($request->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    }

    $request->validate([
      'kegiatan' => 'required',
      'penyelenggara' => 'required',
      'tanggal_mulai' => 'required|date',
      'tanggal_selesai' => 'required|date',
    ], [
      'kegiatan.required' => 'Kepada Harus Diisi',
      'penyelenggara.required' => 'Mata Kuliah Harus Diisi.',
      'tanggal_mulai.required' => 'Tanggal Mulai Harus Diisi.',
      'tanggal_selesai.required' => 'Tanggal Selesai Harus Diisi.',
      '*.date' => 'Format Taggal Salah.',
    ]);

    DB::beginTransaction();
    try {
      $BuatNomorSurat = NomorSurat::BuatNomorSurat('4');
      $request->merge([
        'no_urut' => $BuatNomorSurat['no_urut'],
        'no_surat' => $BuatNomorSurat['no_surat'],
        'id_no_surat'=>$BuatNomorSurat['id_no_surat'],
        'tanggal_surat'=>$BuatNomorSurat['tanggal_surat'],
      ]);

      DB::table($this->tabel_surat)
        ->where('id_' . $this->tabel_surat, $id_surat)
        ->update($request->only('no_surat', 'tanggal_surat', 'kegiatan', 'penyelenggara', 'tanggal_mulai', 'tanggal_selesai', 'no_urut','id_no_surat'));

      DB::table($this->tabel_persetujuan)
        ->where([['id_' . $this->tabel_surat, $id_surat], ['level', DetilUser::AmbilDetil()->level]])
        ->update([
          'status_persetujuan' => 1,
          'waktu_persetujuan' => date('Y-m-d H:i:s'),
          'id_pegawai_dosen' => DetilUser::AmbilDetil()->id_pegawai,
        ]);
      DB::commit();
      return redirect(route('adm.PermintaanSuratTugas'))
        ->with('simpan_sukses', 'Surat Berhasil disimpan dengan nomor surat ' . $request->no_surat . ' tanggal ' . date('d-m-Y', strtotime($request->tanggal_surat)) . '.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakan Hubungi TIM Pengembang Perangkat Lunak.');
    }
  }

  public function SuratTugasSudahProses(request $request)
  {
    $SuratSudahProses = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL]]);

    $FilterSurat = SuratMahasiswa::FilterSurat($request, $SuratSudahProses, DetilUser::AmbilProdiAdmin());

    $SuratSudahProses = $FilterSurat['query']->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.kegiatan', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.penyelenggara', 'validasi_wadek3.status_persetujuan as persetujuan_wadek3', 'surat.no_surat', 'surat.tanggal_surat', 'surat.tanggal_mulai', 'surat.tanggal_selesai')
      ->paginate(4);

    $filter_id_prodi = $FilterSurat['filter_id_prodi'];
    $filter = $FilterSurat['filter'];
    $key = $FilterSurat['key'];
    $JenisFilter =  [
      'nim_nama' => 'Nim dan Nama',
      'tanggal_surat' => 'Tanggal Surat',
    ];
    $PilihFilterProdi = $FilterSurat['pilih_filter_prodi'];
    $route = 'adm.SuratTugasSudahProses';

    $no = 1;

    return view('adm.mhs.surattugas.surattugassudahproses', compact('no', 'SuratSudahProses', 'PilihFilterProdi', 'filter_id_prodi', 'JenisFilter', 'filter', 'key', 'route'));
  }

  public function CetakSuratTugas($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_wadek3.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratTugas($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratTugasBelumdiProses()
  {
    $count = DB::table($this->tabel_persetujuan.' as persetujuan')
      ->join($this->tabel_surat.' as surat','surat.id_'.$this->tabel_surat,'persetujuan.id_'.$this->tabel_surat)
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->select('status_persetujuan', 'level')
      ->where('level', 'adm')
      ->where('status_persetujuan', 0)
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
