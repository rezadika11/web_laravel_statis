<?php

namespace App\Http\Controllers\Adm\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\DetilUser;
use App\SuratMahasiswa;
use PDF;
use App\NomorSurat;

class SuratPermohonanPenghargaanController extends Controller
{
  private $tabel_surat = 'surat_permohonan_penghargaan';
  private $tabel_persetujuan = 'persetujuan_surat_permohonan_penghargaan';

  public function __construct()
  {
    $this->middleware(['auth', 'level:adm']);
  }

  public function PermintaanSuratPermohonanPenghargaan()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.nama_perlombaan', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.juara', 'surat.kelompok')
      ->paginate(4);

    $PilihJuara = [
      1 => 'Satu',
      2 => 'Dua',
      3 => 'Tiga',
    ];

    $no = 1;

    return view('adm.mhs.suratpermohonanpenghargaan.permintaansuratpermohonanpenghargaan', compact('PilihJuara', 'no', 'PermohonanSurat'));
  }

  public function ProsesPermintaanSuratPermohonanPenghargaan($id_surat)
  {
    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->select('surat.nama_perlombaan', 'surat.tanggal_mulai_pelaksanaan', 'surat.tanggal_selesai_pelaksanaan', 'surat.tempat', 'surat.juara', 'surat.kelompok', 'validasi_wadek3.status_persetujuan', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'mahasiswa.alamat', 'mahasiswa.no_hp', 'prodi.nama_prodi')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    $PilihJuara = [
      1 => 'Satu',
      2 => 'Dua',
      3 => 'Tiga',
    ];

    return view('adm.mhs.suratpermohonanpenghargaan.prosespermintaansuratpermohonanpenghargaan', compact('Surat', 'PilihJuara'));
  }

  public function SimpanProsesPermintaanSuratPermohonanPenghargaan(request $request, $id_surat)
  {
    if ($request->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    };

    $request->validate([
      'nama_perlombaan' => 'required|max:255',
      'tanggal_mulai_pelaksanaan' => 'required|date',
      'tanggal_selesai_pelaksanaan' => 'required|date',
      'tempat' => 'required|max:255',
      'juara' => 'required',
      'kelompok' => 'required|max:255',
    ], [
      'nama_perlombaan.required' => 'Nama Perlombaan Harus Diisi.',
      'tanggal_mulai_pelaksanaan.required' => 'Tanggal Mulai Pelaksanaan Harus Diisi.',
      'tanggal_selesai_pelaksanaan.required' => 'Tanggal Selesai Pelaksanaan Harus Diisi.',
      'tempat.required' => 'Tempat Harus Diisi.',
      'juara.required' => 'Juara Harus Dipilih.',
      'kelompok.required' => 'Kelompok Harus Diisi.',
      '*.max' => 'Tidak Boleh Lebih Dari 255 Karakter.',
      '*.date' => 'Format Tanggal Salah.',
    ]);

    DB::beginTransaction();
    try {
      $BuatNomorSurat = NomorSurat::BuatNomorSurat('5');
      $request->merge([
        'no_urut' => $BuatNomorSurat['no_urut'],
        'no_surat' => $BuatNomorSurat['no_surat'],
        'id_no_surat'=>$BuatNomorSurat['id_no_surat'],
        'tanggal_surat'=>$BuatNomorSurat['tanggal_surat'],
      ]);

      DB::table($this->tabel_surat)
        ->where('id_' . $this->tabel_surat, $id_surat)
        ->update($request->only('no_surat', 'tanggal_surat', 'nama_perlombaan', 'tanggal_mulai_pelaksanaan', 'tanggal_selesai_pelaksanaan', 'tempat', 'juara', 'kelompok', 'no_urut','id_no_surat'));

      DB::table($this->tabel_persetujuan)
        ->where([['id_' . $this->tabel_surat, $id_surat], ['level', DetilUser::AmbilDetil()->level]])
        ->update([
          'status_persetujuan' => 1,
          'waktu_persetujuan' => date('Y-m-d H:i:s'),
          'id_pegawai_dosen' => DetilUser::AmbilDetil()->id_pegawai,
        ]);
      DB::commit();
      return redirect(route('adm.PermintaanSuratKeteranganPernahKuliah'))
        ->with('simpan_sukses', 'Surat Berhasil disimpan dengan nomor surat ' . $request->no_surat . ' tanggal ' . date('d-m-Y', strtotime($request->tanggal_surat)) . '.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakan Hubungi TIM Pengembang Perangkat Lunak.');
    }
  }

  public function SuratPermohonanPenghargaanSudahProses(request $request)
  {
    $SuratSudahProses = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL]]);

    $FilterSurat = SuratMahasiswa::FilterSurat($request, $SuratSudahProses, DetilUser::AmbilProdiAdmin());

    $SuratSudahProses = $FilterSurat['query']->select('surat.nama_perlombaan', 'surat.tanggal_mulai_pelaksanaan', 'surat.tanggal_selesai_pelaksanaan', 'surat.tempat', 'surat.juara', 'surat.kelompok', 'validasi_wadek3.status_persetujuan as persetujuan_wadek3', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'mahasiswa.alamat', 'mahasiswa.no_hp', 'prodi.nama_prodi', 'surat.tanggal_surat', 'surat.no_surat')
      ->paginate(4);

    $filter_id_prodi = $FilterSurat['filter_id_prodi'];
    $filter = $FilterSurat['filter'];
    $key = $FilterSurat['key'];
    $JenisFilter =  [
      'nim_nama' => 'Nim dan Nama',
      'tanggal_surat' => 'Tanggal Surat',
    ];
    $PilihFilterProdi = $FilterSurat['pilih_filter_prodi'];
    $route = 'adm.SuratPermohonanPenghargaanSudahProses';

    $no = 1;

    $PilihJuara = [
      1 => 'Satu',
      2 => 'Dua',
      3 => 'Tiga',
    ];

    return view('adm.mhs.suratpermohonanpenghargaan.suratpermohonanpenghargaansudahproses', compact('no', 'SuratSudahProses', 'PilihJuara', 'PilihFilterProdi', 'filter_id_prodi', 'JenisFilter', 'filter', 'key', 'route'));
  }

  public function CetakSuratPermohonanPenghargaan($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek3', function ($q) {
        return $q->on('validasi_wadek3.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek3.level', 'wadek3');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_wadek3.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratPermohonanPenghargaan($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratPermohonanPenghargaanBelumdiProses()
  {
    $count = DB::table($this->tabel_persetujuan.' as persetujuan')
      ->join($this->tabel_surat.' as surat','surat.id_'.$this->tabel_surat,'persetujuan.id_'.$this->tabel_surat)
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->select('status_persetujuan', 'level')
      ->where('level', 'adm')
      ->where('status_persetujuan', 0)
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
