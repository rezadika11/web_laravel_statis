<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-15T09:47:57+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-15T09:47:57+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Adm\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\SuratMahasiswa;
use App\DetilUser;
use PDF;
use App\NomorSurat;

class SuratKeteranganPernahKuliahController extends Controller
{
  private $tabel_surat = 'surat_keterangan_pernah_kuliah';
  private $tabel_persetujuan = 'persetujuan_surat_keterangan_pernah_kuliah';

  public function __construct()
  {
    $this->middleware(['auth', 'level:adm']);
  }

  public function PermintaanSuratKeteranganPernahKuliah()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1', function ($q) {
        return $q->on('validasi_wadek1.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek1.level', 'wadek1');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.angkatan', 'surat.tahun_akademik_masuk', 'surat.semester_keluar', 'surat.tahun_akademik_keluar', 'surat.lama_semester_kuliah')
      ->paginate(4);

    $no = 1;

    return view('adm.mhs.suratketeranganpernahkuliah.permintaansuratketeranganpernahkuliah', compact('no', 'PermohonanSurat'));
  }

  public function ProsesPermintaanSuratKeteranganPernahKuliah($id_surat)
  {
    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1', function ($q) {
        return $q->on('validasi_wadek1.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek1.level', 'wadek1');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->select('validasi_wadek1.status_persetujuan as status_persetujuan_wadek1', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.angkatan', 'mahasiswa.alamat', 'surat.tahun_akademik_masuk', 'surat.semester_keluar', 'surat.tahun_akademik_keluar', 'surat.selesai_beban_studi', 'surat.lama_semester_kuliah', 'surat.prodi_tujuan', 'surat.pt_tujuan', 'surat.alamat_pt_tujuan')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    for ($x = 2000; $x < date('Y'); $x++) {
      $PilihAngkatan[] = $x;
      $PilihTahunAkademik[] = $x . '/' . ($x + 1);
    }

    $PilihSemesterKeluar = [
      1 => 'Ganjil',
      2 => 'Genap',
    ];
    $PilihSemester = SuratMahasiswa::Semester();

    return view('adm.mhs.suratketeranganpernahkuliah.prosespermintaansuratketeranganpernahkuliah', compact('Surat', 'PilihAngkatan', 'PilihTahunAkademik', 'PilihSemesterKeluar', 'PilihSemester'));
  }

  public function SimpanProsesPermintaanSuratKeteranganPernahKuliah(request $request, $id_surat)
  {
    if ($request->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    };

    $request->validate([
      'angkatan' => 'required|numeric',
      'tahun_akademik_masuk' => 'required',
      'semester_keluar' => 'required',
      'tahun_akademik_keluar' => 'required',
      'selesai_beban_studi' => 'required|numeric',
      'lama_semester_kuliah' => 'required',
      'prodi_tujuan' => 'required|max:255',
      'pt_tujuan' => 'required|max:255',
      'alamat_pt_tujuan' => 'required|max:255',
    ], [
      '*.date' => 'Format Taggal Salah.',
      'angkatan.required' => 'Tahun Angkatan Harus Diisi.',
      'angkatan.required' => 'Tahun Angkatan Harus Angka.',
      'tahun_akademik_masuk.required' => 'Tahun Akademik Masuk Harus Dipilih.',
      'semester_keluar.required' => 'Semester Keluar Harus Dipilih.',
      'tahun_akademik_keluar.required' => 'Tahun Akademik Keluar Harus Dipilih.',
      'selesai_beban_studi.required' => 'Selesai Beban Studi Harus Diisi.',
      'selesai_beban_studi.numeric' => 'Selesai Beban Studi Harus Angka.',
      'lama_semester_kuliah.required' => 'Lama Semester Kuliah Harus Dipilih.',
      'prodi_tujuan.required' => 'Program Studi Tujuan Harus Diisi.',
      'pt_tujuan.required' => 'Perguruan Tinggi Tujuan Harus Diisi.',
      'alamat_pt_tujuan.required' => 'Alamat Perguruan Tinggi Tujuan Harus Diisi.',
      '*.max' => 'Tidak Boleh Lebih Dari 255 Karakter.',
    ]);

    DB::beginTransaction();
    try {
      $BuatNomorSurat = NomorSurat::BuatNomorSurat('2');
      $request->merge([
        'no_urut' => $BuatNomorSurat['no_urut'],
        'no_surat' => $BuatNomorSurat['no_surat'],
        'id_no_surat'=>$BuatNomorSurat['id_no_surat'],
        'tanggal_surat'=>$BuatNomorSurat['tanggal_surat'],
      ]);

      DB::table($this->tabel_surat)
        ->where('id_' . $this->tabel_surat, $id_surat)
        ->update($request->only('no_surat', 'tanggal_surat', 'angkatan', 'tahun_akademik_masuk', 'semester_keluar', 'tahun_akademik_keluar', 'selesai_beban_studi', 'lama_semester_kuliah', 'prodi_tujuan', 'pt_tujuan', 'alamat_pt_tujuan', 'no_urut','id_no_surat'));

      DB::table($this->tabel_persetujuan)
        ->where([['id_' . $this->tabel_surat, $id_surat], ['level', DetilUser::AmbilDetil()->level]])
        ->update([
          'status_persetujuan' => 1,
          'waktu_persetujuan' => date('Y-m-d H:i:s'),
          'id_pegawai_dosen'=>DetilUser::AmbilDetil()->id_pegawai,
        ]);
      DB::commit();
      return redirect(route('adm.PermintaanSuratKeteranganPernahKuliah'))
        ->with('simpan_sukses', 'Surat Berhasil disimpan dengan nomor surat ' . $request->no_surat . ' tanggal ' . date('d-m-Y', strtotime($request->tanggal_surat)) . '.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakan Hubungi TIM Pengembang Perangkat Lunak.');
    }
  }

  public function SuratKeteranganPernahKuliahSudahProses(request $request)
  {
    $SuratSudahProses = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1', function ($q) {
        return $q->on('validasi_wadek1.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek1.level', 'wadek1');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL]]);

    $FilterSurat = SuratMahasiswa::FilterSurat($request, $SuratSudahProses, DetilUser::AmbilProdiAdmin());

    $SuratSudahProses = $FilterSurat['query']->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.angkatan', 'surat.tahun_akademik_masuk', 'surat.semester_keluar', 'surat.tahun_akademik_keluar', 'surat.lama_semester_kuliah', 'surat.no_surat', 'surat.tanggal_surat', 'validasi_wadek1.status_persetujuan as persetujuan_wadek1')
      ->paginate(4);

    $filter_id_prodi = $FilterSurat['filter_id_prodi'];
    $filter = $FilterSurat['filter'];
    $key = $FilterSurat['key'];
    $JenisFilter =  [
      'nim_nama' => 'Nim dan Nama',
      'tanggal_surat' => 'Tanggal Surat',
    ];
    $PilihFilterProdi = $FilterSurat['pilih_filter_prodi'];
    $route = 'adm.SuratKeteranganPernahKuliahSudahProses';

    $no = 1;

    return view('adm.mhs.suratketeranganpernahkuliah.suratketeranganpernahkuliahsudahproses', compact('no', 'SuratSudahProses', 'PilihFilterProdi', 'filter_id_prodi', 'JenisFilter', 'filter', 'key', 'route'));
  }

  public function CetakSuratKeteranganPernahKuliah($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_wadek1', function ($q) {
        return $q->on('validasi_wadek1.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_wadek1.level', 'wadek1');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_wadek1.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratKeteranganPernahKuliah($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratKeteranganPernahKuliahBelumdiProses()
  {
    $count = DB::table($this->tabel_persetujuan.' as persetujuan')
      ->join($this->tabel_surat.' as surat','surat.id_'.$this->tabel_surat,'persetujuan.id_'.$this->tabel_surat)
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->select('status_persetujuan', 'level')
      ->where('level', 'adm')
      ->where('status_persetujuan', 0)
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
