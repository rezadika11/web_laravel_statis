<?php
# @Author: Annaya Solusi Infomatika <annaya>
# @Date:   2022-03-11T06:43:16+07:00
# @Email:  annayainformatika@gmail.com
# @Project: Sistem Surat FTIK UIN Saizu
# @Last modified by:   annaya
# @Last modified time: 2022-03-11T06:43:17+07:00


namespace App\Http\Controllers\Adm\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\SuratMahasiswa;
use App\DetilUser;
use PDF;
use App\NomorSurat;

class SuratIjinObservasiKelasController extends Controller
{
  private $tabel_surat = "surat_ijin_observasi_kelas";
  private $tabel_persetujuan = "persetujuan_surat_ijin_observasi_kelas";
  private $tabel_peserta = "peserta_surat_ijin_observasi_kelas";

  public function __construct()
  {
    $this->middleware(['auth', 'level:adm']);
  }

  public function PermintaanSuratIjinObservasiKelas()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.kepada', 'surat.id_surat_ijin_observasi_kelas as id_surat', 'surat.mata_kuliah', 'surat.dosen_pengampu')
      ->paginate(4);

    $no = 1;

    return view('adm.mhs.suratijinobservasikelas.permintaansuratijinobservasikelas', compact('PermohonanSurat', 'no'));
  }

  public function ProsesPermintaanSuratIjinObservasiKelas($id_surat)
  {
    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->select('surat.mata_kuliah', 'surat.semester', 'prodi.nama_prodi', 'surat.kepada', 'validasi_kajur.status_persetujuan', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.tanggal_mulai', 'surat.tanggal_selesai', 'surat.dosen_pengampu')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    $Peserta = DB::table($this->tabel_peserta . ' as peserta')
      ->select('peserta.nim', 'peserta.nama')
      ->where('peserta.id_' . $this->tabel_surat, $id_surat)
      ->get();

    $semester = SuratMahasiswa::Semester($Surat->semester);
    $no = 1;

    return view('adm.mhs.suratijinobservasikelas.prosespermintaansuratijinobservasikelas', compact('semester', 'Surat', 'Peserta', 'no'));
  }

  public function SimpanProsesPermintaanSuratIjinObservasiKelas(request $data, $id_surat)
  {
    if ($data->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    }

    $data->validate([
      'kepada' => 'required',
      'mata_kuliah' => 'required',
      'dosen_pengampu' => 'required',
      'tanggal_mulai' => 'required|date',
      'tanggal_selesai' => 'required|date',
    ], [
      'kepada.required' => 'Kepada Harus Diisi',
      'mata_kuliah.required' => 'Mata Kuliah Harus Diisi.',
      'dosen_pengampu.required' => 'Dosen Pengampu Harus Diisi',
      'tanggal_mulai.required' => 'Tanggal Mulai Harus Diisi.',
      'tanggal_selesai.required' => 'Tanggal Selesai Harus Diisi.',
      '*.date' => 'Format Taggal Salah.',
    ]);

    DB::beginTransaction();
    try {

      $BuatNomorSurat = NomorSurat::BuatNomorSurat('3');
      $data->merge([
        'no_urut' => $BuatNomorSurat['no_urut'],
        'no_surat' => $BuatNomorSurat['no_surat'],
        'id_no_surat'=>$BuatNomorSurat['id_no_surat'],
        'tanggal_surat'=>$BuatNomorSurat['tanggal_surat'],
      ]);

      DB::table($this->tabel_surat)
        ->where('id_' . $this->tabel_surat, $id_surat)
        ->update($data->only('no_surat', 'tanggal_surat', 'kepada', 'mata_kuliah', 'dosen_pengampu', 'tanggal_mulai', 'tanggal_selesai', 'no_urut','id_no_surat'));

      DB::table($this->tabel_persetujuan)
        ->where([['id_' . $this->tabel_surat, $id_surat], ['level', DetilUser::AmbilDetil()->level]])
        ->update([
          'status_persetujuan' => 1,
          'waktu_persetujuan' => date('Y-m-d H:i:s'),
          'id_pegawai_dosen' => DetilUser::AmbilDetil()->id_pegawai,
        ]);
      DB::commit();
      return redirect(route('adm.PermintaanSuratIjinObservasiKelas'))
        ->with('simpan_sukses', 'Surat Berhasil disimpan dengan nomor surat ' . $data->no_surat . ' tanggal ' . date('d-m-Y', strtotime($data->tanggal_surat)) . '.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakan Hubungi TIM Pengembang Perangkat Lunak.');
    }
  }

  public function SuratIjinObservasiKelasSudahProses(request $request)
  {

    $SuratSudahProses = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL]]);

    $FilterSurat = SuratMahasiswa::FilterSurat($request, $SuratSudahProses, DetilUser::AmbilProdiAdmin());

    $SuratSudahProses = $FilterSurat['query']->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.kepada', 'validasi_kajur.status_persetujuan as persetujuan_kajur', 'surat.id_' . $this->tabel_surat . ' as id_surat', 'surat.no_surat', 'surat.tanggal_surat', 'surat.dosen_pengampu', 'surat.mata_kuliah')
      ->paginate(4);

    $no = 1;

    $filter_id_prodi = $FilterSurat['filter_id_prodi'];
    $filter = $FilterSurat['filter'];
    $key = $FilterSurat['key'];
    $JenisFilter = $FilterSurat['jenis_filter'];
    $PilihFilterProdi = $FilterSurat['pilih_filter_prodi'];
    $route = 'adm.SuratIjinObservasiKelasSudahProses';

    return view('adm.mhs.suratijinobservasikelas.suratijinobservasikelassudahproses', compact('SuratSudahProses', 'no', 'PilihFilterProdi', 'filter_id_prodi', 'JenisFilter', 'filter', 'key', 'route'));
  }

  public function CetakSuratIjinObservasiKelas($id_surat)
  {
    $SuratSiapCetak = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_kajur.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_' . $this->tabel_surat, $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
        ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratIjinObservasiKelas($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratObservasiKelasBelumdiProses()
  {
    $count = DB::table($this->tabel_persetujuan.' as persetujuan')
      ->join($this->tabel_surat.' as surat','surat.id_'.$this->tabel_surat,'persetujuan.id_'.$this->tabel_surat)
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->select('status_persetujuan', 'level')
      ->where('level', 'adm')
      ->where('status_persetujuan', 0)
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
