<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-12T20:55:09+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-12T20:55:09+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Adm\Mhs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\SuratMahasiswa;
use Validator;
use App\DetilUser;
use PDF;
use App\NomorSurat;

class SuratIjinRisetIndividuController extends Controller
{
  private $tabel_surat = "surat_ijin_riset_individu";
  private $tabel_persetujuan = "persetujuan_surat_ijin_riset_individu";

  public function __construct()
  {
    $this->middleware(['auth', 'level:adm']);
  }

  public function PermintaanSuratIjinRisetIndividu()
  {
    $PermohonanSurat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.kepada', 'surat.kec', 'validasi_kajur.status_persetujuan', 'surat.id_surat_ijin_riset_individu')
      ->paginate(4);
    $no = 1;

    return view('adm.mhs.suratijinrisetindividu.permintaansuratijinrisetindividu', compact('PermohonanSurat', 'no'));
  }

  public function ProsesPermintaanSuratIjinRisetIndividu($id_surat)
  {
    $Surat = DB::table($this->tabel_surat . ' as surat')
      ->join($this->tabel_persetujuan . ' as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_adm.level', 'adm');
      })
      ->join($this->tabel_persetujuan . ' as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_' . $this->tabel_surat, 'surat.id_' . $this->tabel_surat)
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->where([['surat.no_surat', NULL], ['validasi_adm.status_persetujuan', 0], ['surat.tanggal_surat', NULL], ['surat.id_surat_ijin_riset_individu', $id_surat]])
      ->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.kepada', 'surat.kec', 'validasi_kajur.status_persetujuan', 'surat.id_surat_ijin_riset_individu', 'surat.semester', 'surat.judul', 'surat.obyek', 'surat.tempat', 'surat.tanggal_mulai', 'surat.tanggal_selesai', 'surat.metode_penelitian')
      ->first();

    if (!$Surat) {
      return redirect(url()->previous())
      ->with('simpan_gagal', 'Surat Tidak Ditemukan / Sudah Divalidasi.');
    }

    $Tembusan = DB::table('tembusan_surat_ijin_riset_individu as tembusan')
      ->select('kepada_tembusan', 'id_tembusan_surat_ijin_riset_individu as id_tembusan')
      ->where('tembusan.id_surat_ijin_riset_individu', $id_surat)
      ->get();

    $semester = SuratMahasiswa::Semester($Surat->semester);

    return view('adm.mhs.suratijinrisetindividu.prosespermintaansuratijinrisetindividu', compact('semester', 'Surat', 'Tembusan'));
  }

  public function SimpanProsesPermintaanSuratIjinRisetIndividu(request $data, $id_surat)
  {
    if ($data->id_surat != $id_surat) {
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahkan Ulangi Lagi.');
    }

    $data->validate([
      'kepada' => 'required',
      'kec' => 'required',
      'judul' => 'required',
      'obyek' => 'required',
      'tempat' => 'required',
      'tanggal_mulai' => 'required|date',
      'tanggal_selesai' => 'required|date',
      'metode_penelitian' => 'required',
    ], [
      'kepada.required' => 'Kepada Harus Diisi',
      'kec.required' => 'Kecamatan Harus Diisi.',
      'judul.required' => 'Judul Penelitian Harus Diisi',
      'obyek.required' => 'Obyek Penelitian Harus Diisi.',
      'tempat.required' => 'Tempat Harus Diisi.',
      'tanggal_mulai.required' => 'Tanggal Mulai Harus Diisi.',
      'tanggal_selesai.required' => 'Tanggal Selesai Harus Diisi.',
      'metode_penelitian.required' => 'Metode Penelitian Harus Diisi.',
      '*.date' => 'Format Taggal Salah.',
    ]);

    $DataTembusan = $data->tembusan;

    $CekTembusanSekarang = DB::table('tembusan_surat_ijin_riset_individu as tembusan')
      ->select('tembusan.id_tembusan_surat_ijin_riset_individu')
      ->where('tembusan.id_surat_ijin_riset_individu', $id_surat)
      ->get();

    foreach ($CekTembusanSekarang as $Cktm) {
      if (isset($DataTembusan[$Cktm->id_tembusan_surat_ijin_riset_individu])) {
        $UpdateTembusan[$Cktm->id_tembusan_surat_ijin_riset_individu] = [
          'kepada_tembusan' => $data->tembusan[$Cktm->id_tembusan_surat_ijin_riset_individu],
        ];
      }
      unset($DataTembusan[$Cktm->id_tembusan_surat_ijin_riset_individu]);
    }

    if($DataTembusan!=NULL){
      foreach ($DataTembusan as $tmbs) {
        if (strlen($tmbs) > 1) {
          $InsertTembusan[] = [
            'id_surat_ijin_riset_individu' => $id_surat,
            'kepada_tembusan' => $tmbs,
          ];
        }
      }
    }

    DB::beginTransaction();
    try {
      if (isset($InsertTembusan)) {
        DB::table('tembusan_surat_ijin_riset_individu')
          ->insert($InsertTembusan);
      }

      if(isset($UpdateTembusan)){
        foreach ($UpdateTembusan as $id_tmbs => $kepada_tembusan) {
          DB::table('tembusan_surat_ijin_riset_individu')
            ->where('tembusan_surat_ijin_riset_individu.id_tembusan_surat_ijin_riset_individu', $id_tmbs)
            ->update($kepada_tembusan);
        }
      }

      $BuatNomorSurat = NomorSurat::BuatNomorSurat('3');
      $data->merge([
        'no_urut' => $BuatNomorSurat['no_urut'],
        'no_surat' => $BuatNomorSurat['no_surat'],
        'id_no_surat'=>$BuatNomorSurat['id_no_surat'],
        'tanggal_surat'=>$BuatNomorSurat['tanggal_surat'],
      ]);

      DB::table('surat_ijin_riset_individu')
        ->where('id_surat_ijin_riset_individu', $id_surat)
        ->update($data->only('no_surat', 'tanggal_surat', 'kepada', 'kec', 'judul', 'obyek', 'tempat', 'tanggal_mulai', 'tanggal_selesai', 'metode_penelitian', 'no_urut','id_no_surat'));
      DB::table('persetujuan_surat_ijin_riset_individu')
        ->where([['id_surat_ijin_riset_individu', $id_surat], ['level', DetilUser::AmbilDetil()->level]])
        ->update([
          'status_persetujuan' => 1,
          'waktu_persetujuan' => date('Y-m-d H:i:s'),
          'id_pegawai_dosen'=> DetilUser::AmbilDetil()->id_pegawai,
        ]);
      DB::commit();
      return redirect(route('adm.PermintaanSuratIjinRisetIndividu'))
      ->with('simpan_sukses', 'Surat Berhasil disimpan dengan nomor surat ' . $data->no_surat . ' tanggal ' . date('d-m-Y', strtotime($data->tanggal_surat)) . '.');
    } catch (\Exception $e) {
      DB::rollback();
      report($e);
      return redirect(url()->previous())
        ->withInput()
        ->with('simpan_gagal', 'Terjadi Kesalahan, Silahakan Hubungi TIM Pengembang Perangkat Lunak.');
    }
  }

  public function SuratIjinRisetIndividuSudahProses(request $request)
  {
    $SuratSudahProses = DB::table('surat_ijin_riset_individu as surat')
      ->join('persetujuan_surat_ijin_riset_individu as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_surat_ijin_riset_individu', 'surat.id_surat_ijin_riset_individu')
          ->where('validasi_adm.level', 'adm');
      })
      ->join('persetujuan_surat_ijin_riset_individu as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_surat_ijin_riset_individu', 'surat.id_surat_ijin_riset_individu')
          ->where('validasi_kajur.level', 'kajur');
      })
      ->join('mahasiswa', 'mahasiswa.id_mahasiswa', 'surat.id_mahasiswa')
      ->join('prodi', 'prodi.id_prodi', 'mahasiswa.id_prodi')
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL]]);

    $FilterSurat = SuratMahasiswa::FilterSurat($request, $SuratSudahProses,DetilUser::AmbilProdiAdmin());

    $SuratSudahProses = $FilterSurat['query']->select('mahasiswa.nama_mahasiswa', 'mahasiswa.nim', 'prodi.nama_prodi', 'surat.kepada', 'surat.kec', 'validasi_kajur.status_persetujuan as persetujuan_kajur', 'surat.id_surat_ijin_riset_individu', 'surat.no_surat', 'surat.tanggal_surat', 'surat.id_surat_ijin_riset_individu as id_surat')
      ->paginate(4);

    $filter_id_prodi = $FilterSurat['filter_id_prodi'];
    $filter = $FilterSurat['filter'];
    $key = $FilterSurat['key'];
    $JenisFilter = $FilterSurat['jenis_filter'];
    $PilihFilterProdi = $FilterSurat['pilih_filter_prodi'];
    $route = 'adm.SuratIjinRisetIndividuSudahProses';

    $no = 1;

    return view('adm.mhs.suratijinrisetindividu.suratijinrisetindividusudahproses', compact('no', 'SuratSudahProses', 'PilihFilterProdi', 'filter_id_prodi', 'JenisFilter', 'filter', 'key', 'route'));
  }

  public function CetakSuratIjinRisetIndividu($id_surat)
  {
    $SuratSiapCetak = DB::table('surat_ijin_riset_individu as surat')
      ->join('persetujuan_surat_ijin_riset_individu as validasi_adm', function ($q) {
        return $q->on('validasi_adm.id_surat_ijin_riset_individu', 'surat.id_surat_ijin_riset_individu')
          ->where('validasi_adm.level', 'adm');
      })
      ->join('persetujuan_surat_ijin_riset_individu as validasi_kajur', function ($q) {
        return $q->on('validasi_kajur.id_surat_ijin_riset_individu', 'surat.id_surat_ijin_riset_individu')
          ->where('validasi_kajur.level', 'kajur');
      })
      ->where([['surat.no_surat', '!=', NULL], ['validasi_adm.status_persetujuan', 1], ['surat.tanggal_surat', '!=', NULL], ['validasi_kajur.status_persetujuan', 1], ['surat.unique_key', '!=', NULL], ['surat.id_surat_ijin_riset_individu', $id_surat]])
      ->count();

    if ($SuratSiapCetak != 1) {
      return redirect(url()->previous())
      ->with('simpan_gagal', 'Surat Tidak Dapat Dicetak.');
    }

    $data = SuratMahasiswa::CetakSuratIjinRisetIndividu($id_surat);
    $pdf = PDF::loadView($data['view'], $data['data']);
    return $pdf->stream();
  }
  public function JumlahSuratRisetIndividuBelumdiProses()
  {
    $count = DB::table($this->tabel_persetujuan.' as persetujuan')
      ->join($this->tabel_surat.' as surat','surat.id_'.$this->tabel_surat,'persetujuan.id_'.$this->tabel_surat)
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->select('status_persetujuan', 'level')
      ->where('level', 'adm')
      ->where('status_persetujuan', 0)
      ->wherein('mahasiswa.id_prodi',DetilUser::AmbilProdiAdmin())
      ->count('status_persetujuan');

    return response()->json($count);
  }
}
