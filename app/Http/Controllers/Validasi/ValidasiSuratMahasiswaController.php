<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-29T06:26:11+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-29T06:26:11+07:00
# @Copyright: https://annaya.id




namespace App\Http\Controllers\Validasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ValidasiSuratMahasiswaController extends Controller
{
    //

    public function ValidasiSuratIjinRisetIndividu($id_surat,$unique_key){
      $Validasi = DB::table('surat_ijin_riset_individu as surat')
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->leftjoin('prodi','prodi.id_prodi','mahasiswa.id_prodi')
      ->leftjoin('jurusan','jurusan.id_jurusan','prodi.id_jurusan')
      ->where([['surat.id_surat_ijin_riset_individu',$id_surat],['surat.unique_key',$unique_key]])
      ->select('mahasiswa.nama_mahasiswa','mahasiswa.nim','surat.kepada','surat.judul','surat.no_surat','jurusan.nama_jurusan')
      ->first();

      return view('validasi.mhs.suratijinrisetindividu',compact('Validasi'));
    }

    public function ValidasiSuratIjinObservasiPendahuluan($id_surat,$unique_key){
      $Validasi = DB::table('surat_ijin_observasi_pendahuluan as surat')
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->leftjoin('prodi','prodi.id_prodi','mahasiswa.id_prodi')
      ->leftjoin('jurusan','jurusan.id_jurusan','prodi.id_jurusan')
      ->where([['surat.id_surat_ijin_observasi_pendahuluan',$id_surat],['surat.unique_key',$unique_key]])
      ->select('mahasiswa.nama_mahasiswa','mahasiswa.nim','surat.kepada','surat.obyek','surat.no_surat','jurusan.nama_jurusan')
      ->first();

      return view('validasi.mhs.suratijinobservasipendahuluan',compact('Validasi'));
    }

    public function ValidasiSuratIjinObservasiKelas($id_surat,$unique_key){
      $Validasi = DB::table('surat_ijin_observasi_kelas as surat')
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->join('prodi','prodi.id_prodi','mahasiswa.id_prodi')
      ->leftjoin('jurusan','jurusan.id_jurusan','prodi.id_jurusan')
      ->where([['surat.unique_key',$unique_key],['surat.id_surat_ijin_observasi_kelas',$id_surat]])
      ->select('surat.kepada','surat.tanggal_surat','surat.no_surat','surat.mata_kuliah','surat.tema','jurusan.nama_jurusan')
      ->first();

      $Peserta = DB::table('peserta_surat_ijin_observasi_kelas as peserta')
      ->select('peserta.nama','peserta.nim')
      ->where('peserta.id_surat_ijin_observasi_kelas',$id_surat)
      ->get();

      $no=1;

      return view('validasi.mhs.suratijinobservasikelas',compact('Validasi','Peserta','no'));
    }

    public function ValidasiSuratKeteranganLulusSkripsi($id_surat,$unique_key){
      $Validasi = DB::table('surat_keterangan_lulus_skripsi as surat')
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->join('prodi','prodi.id_prodi','mahasiswa.id_prodi')
      ->leftjoin('jurusan','jurusan.id_jurusan','prodi.id_jurusan')
      ->where([['surat.unique_key',$unique_key],['surat.id_surat_keterangan_lulus_skripsi',$id_surat]])
      ->select('surat.tanggal_ujian','surat.no_surat','surat.tahun_akademik','mahasiswa.nama_mahasiswa','mahasiswa.nim','jurusan.nama_jurusan')
      ->first();

      return view('validasi.mhs.suratketeranganlulusskripsi',compact('Validasi'));
    }

    public function ValidasiSuratTugas($id_surat,$unique_key){
      $Validasi = DB::table('surat_tugas as surat')
      ->where([['surat.unique_key',$unique_key],['surat.id_surat_tugas',$id_surat]])
      ->select('surat.no_surat','surat.kegiatan','surat.penyelenggara','surat.tanggal_mulai','surat.tanggal_selesai')
      ->first();

      $Peserta = DB::table('peserta_surat_tugas as peserta')
      ->select('peserta.nama','peserta.nim')
      ->where('peserta.id_surat_tugas',$id_surat)
      ->get();

      $no=1;

      return view('validasi.mhs.surattugas',compact('Validasi','Peserta','no'));
    }

    public function ValidasiSuratKeteranganMasihKuliah($id_surat,$unique_key){
        $Validasi = DB::table('surat_keterangan_masih_kuliah as surat')
        ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
        ->join('prodi','prodi.id_prodi','mahasiswa.id_prodi')
        ->leftjoin('jurusan','jurusan.id_jurusan','prodi.id_jurusan')
        ->join( 'persetujuan_surat_keterangan_masih_kuliah  as validasi_wadek1_kajur', function ($q) {
          return $q->on('validasi_wadek1_kajur.id_surat_keterangan_masih_kuliah', 'surat.id_surat_keterangan_masih_kuliah')
            ->where(function($q1){
              return $q1->where('validasi_wadek1_kajur.level', 'wadek1')
              ->orwhere('validasi_wadek1_kajur.level', 'kajur');
            });
        })
        ->where([['surat.unique_key',$unique_key],['surat.id_surat_keterangan_masih_kuliah',$id_surat]])
        ->select('surat.no_surat','surat.tanggal_surat','mahasiswa.nim','prodi.nama_prodi','mahasiswa.nama_mahasiswa','surat.tahun_akademik','jurusan.nama_jurusan','validasi_wadek1_kajur.level')
        ->first();

        return view('validasi.mhs.suratketeranganmasihkuliah',compact('Validasi'));
    }

    public function ValidasiSuratKeteranganPernahKuliah($id_surat,$unique_key){
       $Validasi = DB::table('surat_keterangan_pernah_kuliah as surat')
       ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
       ->join('prodi','prodi.id_prodi','mahasiswa.id_prodi')
       ->where([['surat.unique_key',$unique_key],['surat.id_surat_keterangan_pernah_kuliah',$id_surat]])
       ->select('surat.no_surat','surat.tanggal_surat','mahasiswa.nim','prodi.nama_prodi','mahasiswa.nama_mahasiswa','surat.prodi_tujuan','surat.pt_tujuan','surat.alamat_pt_tujuan')
       ->first();

       return view('validasi.mhs.suratketeranganpernahkuliah',compact('Validasi'));
    }

    public function ValidasiSuratKeteranganPenggantiIjazah($id_surat,$unique_key){
      $Validasi = DB::table('surat_keterangan_pengganti_ijazah as surat')
      ->join('mahasiswa','mahasiswa.id_mahasiswa','surat.id_mahasiswa')
      ->join('prodi','prodi.id_prodi','mahasiswa.id_prodi')
      ->where([['surat.unique_key',$unique_key],['surat.id_surat_keterangan_pengganti_ijazah',$id_surat]])
      ->select('surat.no_surat','surat.tanggal_surat','mahasiswa.nim','prodi.nama_prodi','mahasiswa.nama_mahasiswa','surat.tanggal_ijazah','surat.tahun_kehilangan','surat.nomor_ijazah')
      ->first();

      return view('validasi.mhs.suratketeranganpenggantiijazah',compact('Validasi'));
    }

    public function ValidasiSuratPermohonanPenghargaan($id_surat,$unique_key){
      $Validasi = DB::table('surat_permohonan_penghargaan as surat')
      ->where([['surat.unique_key',$unique_key],['surat.id_surat_permohonan_penghargaan',$id_surat]])
      ->select('surat.no_surat','surat.tanggal_surat','surat.tempat','surat.tanggal_mulai_pelaksanaan','surat.tanggal_selesai_pelaksanaan','surat.nama_perlombaan','surat.juara','surat.tempat')
      ->first();

      return view('validasi.mhs.suratpermohonanpenghargaan',compact('Validasi'));
    }
}
