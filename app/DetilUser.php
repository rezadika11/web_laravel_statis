<?php
# @Author: Annaya Solusi Infomatika <annaya>
# @Date:   2022-03-11T06:42:50+07:00
# @Email:  annayainformatika@gmail.com
# @Project: Sistem Surat FTIK UIN Saizu
# @Last modified by:   annaya
# @Last modified time: 2022-03-11T06:42:52+07:00




namespace App;

use DB;
use Auth;

class DetilUser
{
  public static function AmbilDetil()
  {
    $data = NULL;
    switch (Auth::user()->level) {
      case 'dosen':
        $data = DB::table('users')
          ->join('dosen', 'dosen.id_user', 'users.id_user')
          ->select('dosen.nama_dosen as nama', 'dosen.nidn', 'dosen.level_dosen as level_dosen', 'users.level as level','dosen.id_dosen')
          ->where('users.id_user', Auth::user()->id_user)
          ->first();
        break;

      case 'mhs':
        $data = DB::table('users')
          ->join('mahasiswa', 'mahasiswa.id_user', 'users.id_user')
          ->select('mahasiswa.nama_mahasiswa as nama', 'mahasiswa.nim', 'mahasiswa.id_mahasiswa', 'users.level', 'mahasiswa.id_prodi')
          ->where('users.id_user', Auth::user()->id_user)
          ->first();
        break;

      case 'admin':
        $data = DB::table('users')
          ->join('pegawai', 'pegawai.id_user', 'users.id_user')
          ->select('pegawai.nama_pegawai as nama', 'pegawai.nip', 'users.level as level','pegawai.id_pegawai')
          ->where('users.id_user', Auth::user()->id_user)
          ->first();
        break;

      case 'adm':
        $data = DB::table('users')
          ->join('pegawai', 'pegawai.id_user', 'users.id_user')
          ->select('pegawai.nama_pegawai as nama', 'pegawai.nip', 'users.level as level','pegawai.id_pegawai')
          ->where('users.id_user', Auth::user()->id_user)
          ->first();
        break;
    }

    return $data;
  }

  public static function AmbilNama()
  {
    switch (Auth::user()->level) {
      case 'dosen':
        $identitas = DB::table('dosen')
          ->select('dosen.nama_dosen as nama', 'dosen.level_dosen as level_dosen')
          ->where('dosen.id_user', Auth::user()->id_user)
          ->first();
        switch ($identitas->level_dosen) {
          case 'dekan':
            $data['level'] = 'Dekan';
            break;
          case 'wadek1':
            $data['level'] = 'Wakil Dekan 1';
            break;
          case 'wadek2':
            $data['level'] = 'Wakil Dekan 2';
            break;
          case 'wadek3':
            $data['level'] = 'Wakil Dekan 3';
            break;
          case 'dosen':
            $data['level'] = 'Dosen';
            break;
          case 'kaprodi':
            $data['level'] = 'Ketua Program Studi';
            break;
          case 'kajur':
              $data['level'] = 'Ketua Jurusan';
              break;
          default:
            $data['level'] = 'Level Tidak Ditemukan';
            break;
        }
        break;
      case 'mhs':
        $identitas = DB::table('mahasiswa')
          ->select('mahasiswa.nama_mahasiswa as nama')
          ->where('mahasiswa.id_user', Auth::user()->id_user)
          ->first();
        $data['level'] = 'Mahasiswa';
        break;

      case 'admin':
        $identitas = DB::table('pegawai')
          ->select('pegawai.nama_pegawai as nama')
          ->where('pegawai.id_user', Auth::user()->id_user)
          ->first();
        $data['level'] = 'Administrator';
        break;
      case 'adm':
        $identitas = DB::table('pegawai')
          ->select('pegawai.nama_pegawai as nama')
          ->where('pegawai.id_user', Auth::user()->id_user)
          ->first();
        $data['level'] = 'Administrasi';
        break;
    }
    $data['nama'] = $identitas->nama;
    return $data;
  }

  public static function LevelDosen()
  {
    $identitas = DB::table('dosen')
      ->select('dosen.level_dosen as level_dosen')
      ->where('dosen.id_user', Auth::user()->id_user)
      ->first();

    return $identitas->level_dosen;
  }

  public static function DosenJurusan(){
    $identitas = DB::table('dosen')
      ->join('ketua_jurusan','ketua_jurusan.id_dosen_kajur','dosen.id_dosen')
      ->join('prodi','prodi.id_jurusan','ketua_jurusan.id_jurusan')
      ->select('prodi.id_prodi')
      ->where([['dosen.id_user', Auth::user()->id_user],['dosen.level_dosen','kajur'],['ketua_jurusan.aktif',1]])
      ->get();
    $prodi = [];
    foreach($identitas as $id){
      $prodi[] = $id->id_prodi;
    }

    return $prodi;
  }

  public static function AmbilProdiAdmin(){
    $AmbilProdiAdmin = DB::table('admin_prodi')
    ->select('id_prodi')
    ->where('id_pegawai',DetilUser::AmbilDetil()->id_pegawai)
    ->get();

    $IdProdi = [];
    foreach($AmbilProdiAdmin as $Prd){
      $IdProdi[] = $Prd->id_prodi ;
    }

    return $IdProdi;
  }
}
