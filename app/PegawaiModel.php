<?php
# @Author: Annaya Solusi Infomatika <mgpnata>
# @Date:   2022-03-12T20:53:59+07:00
# @Email:  annayainformatika@gmail.com
# @Last modified by:   mgpnata
# @Last modified time: 2022-03-12T20:53:59+07:00
# @Copyright: https://annaya.id


namespace App;

use Illuminate\Database\Eloquent\Model;

class PegawaiModel extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //   'level', 'email', 'password',
    // ];
    protected $guarded = ['id_pegawai'];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    /*protected $casts = [
          'email_verified_at' => 'datetime',
      ];*/

    protected $table = "pegawai";
    protected $primaryKey = "id_pegawai";
}
