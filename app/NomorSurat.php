<?php
# @Author: Annaya Solusi Infomatika <annaya>
# @Date:   2022-03-11T06:42:20+07:00
# @Email:  annayainformatika@gmail.com
# @Project: Sistem Surat FTIK UIN Saizu
# @Last modified by:   annaya
# @Last modified time: 2022-03-11T06:50:09+07:00




namespace App;

use DB;

Class NomorSurat{
    public static function BuatNomorSurat($jenis,$tanggal_surat = NULL){
      $TabelNoSurat = [
        '1'=>'no_surat_PP_06_3',
        '2'=>'no_surat_PP_01_4',
        '3'=>'no_surat_PP_05_3',
        '4'=>'no_surat_KM_05_1',
        '5'=>'no_surat_KM_05_2',
      ];

      $MasterNoSurat = [
        /*
        Keterangan Pengganti Ijazah
        Surat Keterangan Lulus Munaqasyah Skripsi
        */
        '1'=>[
          'awalan'=>'B.m.',
          'akhiran'=>'/Un.19/D.FTIK/PP.06.3/',
        ],

        /*
        Keterangan Pernah Kuliah
        Surat Keterangan Masih Kuliah
        Surat Keterangan Masih Kuliah (Untuk Tunjangan Pns)
        */
        '2'=>[
          'awalan'=>'B.m.',
          'akhiran'=>'/Un.19/D.FTIK/PP.01.4/',
        ],

        /*
        Permohonan Ijin Observasi Kelas
        Permohonan Ijin Observasi Pendahuluan
        Permohanan Ijin Riset Individu
        */
        '3'=>[
          'awalan'=>'B.m.',
          'akhiran'=>'/Un.19/D.FTIK/PP.05.3/',
        ],
        /*
        Surat Tugas
        */
        '4'=>[
          'awalan'=>'B.m.',
          'akhiran'=>'/Un.19/D.FTIK/KM.05.1/',
        ],

        /*
        Permohonan Penghargaan
        */
        '5'=>[
          'awalan'=>'B.m.',
          'akhiran'=>'/Un.19/D.FTIK/KM.05.2/',
        ],
      ];

      if($tanggal_surat == NULL){
        $tanggal_surat = date('Y-m-d');
      }

      $NoUrut = DB::table($TabelNoSurat[$jenis].' as no_surat')
      ->WhereYear('no_surat.tanggal_surat',date('Y',strtotime($tanggal_surat)))
      ->max('no_urut')+1;

      $NoAngka = sprintf("%03d", $NoUrut);
      $NoSurat = $MasterNoSurat[$jenis]['awalan'].$NoAngka.$MasterNoSurat[$jenis]['akhiran'].date('m/Y',strtotime($tanggal_surat));

        $id_no_surat = DB::table($TabelNoSurat[$jenis])
        ->insertGetId([
          'no_urut'=>$NoUrut,
          'no_surat'=>$NoSurat,
          'tanggal_surat'=>$tanggal_surat,
        ]);
        return [
          'no_urut'=>$NoUrut,
          'no_surat'=>$NoSurat,
          'tanggal_surat'=>$tanggal_surat,
          'id_no_surat'=>$id_no_surat,
        ];
    }



}
